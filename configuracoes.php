<?php 
    include 'cabecalho.php'; 
    $user = $_SESSION['usuarioID'];
    
    echo '<div class="ui container">';
    if ($user == 1) 
        {
            echo '<a href="backup_database/realiza_bkp.php" class="ui secondary button">Backup</a>';
        }
    elseif ($user != 1)
        {
            echo 'Você não tem permissão!';
            echo '<a href="index.php" class="ui inverted button">Voltar</a>';
        }
    echo '</div>';
    
?>
<script>
    var menu = window.location.pathname;
    if (menu === '/pde/configuracoes.php') {
        $('#config').addClass('active');
        
    }    
</script>