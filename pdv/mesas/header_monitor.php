<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=nos">
    <meta charset="utf-8">
    <title>PDV | Pedido Mesa</title>
    <link rel="shortcut icon" href="../uploads/favicon.png"/>
    <link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="estilo.css">
    <link rel="stylesheet" type="text/css" href="bower_components/semantic/dist/semantic.min.css">
    <script type="text/javascript" src="bower_components/bootstrap/dist/js/bootstrap.js"></script>
    <script type="text/javascript" src="bower_components/bootstrap/js/modal.js"></script>
    <script type="text/javascript" src="bower_components/jquery/dist/jquery.js"></script>
    <script src="bower_components/semantic/dist/semantic.min.js"></script>
</head>
<body>
<!--<nav class="navbar navbar-inverse">
  <div class="container-fluid">
  	<a class="navbar-brand" href="../pos/">PDV</a>
  	<ul class="nav navbar-nav">
            <a href="mesas_monitor.php" class="btn btn-default">Mesas</a>
        <a href="delivery.php" class="ui small orange button">Delivery</a>
  	</ul>
  </div>
</nav>-->

<div class="ui labeled icon inverted menu">
    <div class="header item">
        <br>
        PontoDaEsfiha®
    </div>
  <a href="../pos" class="item">
    <i class="calculator icon"></i>
    PDV
  </a>
  <a href="mesas_monitor.php" class="item">
    <i class="home icon"></i>
    Mesas
  </a>
  <a href="delivery.php" class="item">
    <i class="text telephone icon"></i>
    Delivery
  </a>
</div>

<div class="ui container">