<?php
echo '
<div class="ui form">
  <div class="inline fields">
    <label for="fruit">Select your favorite fruit:</label>
    <div class="field">
      <div class="ui radio checkbox">
        <input type="radio" name="fruit" checked="" tabindex="0" class="hidden">
        <label>1/2 - Meia</label>
      </div>
    </div>
    <div class="field">
      <div class="ui radio checkbox">
        <input type="radio" name="fruit" tabindex="0" class="hidden">
        <label>Inteira</label>
      </div>
    </div>    
  </div>

';