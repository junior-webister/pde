<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=no">
	<meta charset="utf-8">
	<title>PDV | Pedido Mesa</title>
        <link rel="shortcut icon" href="../uploads/favicon.png"/>
	<link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<link rel="stylesheet" type="text/css" href="bower_components/semantic/dist/semantic.min.css">
	<script type="text/javascript" src="bower_components/bootstrap/dist/js/bootstrap.js"></script>
	<script type="text/javascript" src="bower_components/bootstrap/js/modal.js"></script>
    <script type="text/javascript" src="bower_components/jquery/dist/jquery.js"></script>
    <script src="bower_components/semantic/dist/semantic.min.js"></script>
</head>
<body>
<!--<nav class="navbar navbar-inverse">
  <div class="container-fluid">
  	<a class="navbar-brand" href="">PDV</a>
  	<ul class="nav navbar-nav">
  		<a href="mesas.php" class="ui medium orange button">Mesas</a>
  	</ul>
  </div>
</nav>-->
<div class="ui labeled icon inverted menu">
  <a href="mesas.php" class="item">
    <i class="home icon"></i>
    Mesas
  </a>
</div>
<div class="ui fluid container">