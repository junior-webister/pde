-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 28-Nov-2016 às 18:35
-- Versão do servidor: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pdv`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `atualiza`
--

CREATE TABLE `atualiza` (
  `id` int(11) NOT NULL,
  `atualizado` varchar(45) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `atualiza`
--

INSERT INTO `atualiza` (`id`, `atualizado`) VALUES
(1, 'sim');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_categories`
--

CREATE TABLE `tec_categories` (
  `id` int(11) NOT NULL,
  `code` varchar(20) NOT NULL,
  `name` varchar(55) NOT NULL,
  `image` varchar(100) DEFAULT 'no_image.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tec_categories`
--

INSERT INTO `tec_categories` (`id`, `code`, `name`, `image`) VALUES
(1, 'G01', 'Bebidas', 'fe71884b38b50fd00b18d3fb2305dc9f.jpg'),
(2, 'G02', 'Alimentos', '29170e7ecbb92b9a35bdfab60757617b.jpg'),
(3, '003', 'Pizzas', 'no_image.png');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_combo_items`
--

CREATE TABLE `tec_combo_items` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `item_code` varchar(20) NOT NULL,
  `quantity` decimal(12,4) NOT NULL,
  `price` decimal(25,2) DEFAULT NULL,
  `cost` decimal(25,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tec_combo_items`
--

INSERT INTO `tec_combo_items` (`id`, `product_id`, `item_code`, `quantity`, `price`, `cost`) VALUES
(5, 18, '03', '1.0000', NULL, NULL),
(6, 18, '02', '1.0000', NULL, NULL),
(7, 18, '0015', '1.0000', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_customers`
--

CREATE TABLE `tec_customers` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `cf1` varchar(255) NOT NULL,
  `cf2` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `endereco` varchar(125) DEFAULT NULL,
  `delivery` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tec_customers`
--

INSERT INTO `tec_customers` (`id`, `name`, `cf1`, `cf2`, `phone`, `email`, `endereco`, `delivery`) VALUES
(1, 'Cliente Padrão', '9999999999', '99999999', '012345678', 'cliente@pdvparatodos.com.br', 'Rua Exemplo, 12', 'entrega'),
(2, 'Mesa 01', '999999999', '999999999', '999999999', 'mesa@mesa.com', 'Avenida Teste, 230', 'entrega'),
(3, 'Amy Lee', '1234123123', '12441232', '54432039', 'amylee@teste.com', 'Rua dos Rockeiros, 2121', 'entrega'),
(4, 'Gerson', '1212121212121', '2122121212', '11998776544', 'gerson@teste.com', 'Rua do Vale, 77', 'entrega'),
(5, 'Edson', '01101010231', '1312341241', '10101010', 'reipele@santos.com', 'Rua Arantis do Nascimento, 10', 'entrega'),
(7, 'Ronaldo', '74492342309', '1234994005', '99090909', 'r9@nine.com', 'Avenida Fenomeno, 999', NULL),
(8, 'Corey Taylor', '12854822894', '456935874', '92134678', 'duality@newmetal.com', 'Rua Slipknot, 192', 'entrega');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_expenses`
--

CREATE TABLE `tec_expenses` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference` varchar(50) NOT NULL,
  `amount` decimal(25,2) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `created_by` varchar(55) NOT NULL,
  `attachment` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tec_expenses`
--

INSERT INTO `tec_expenses` (`id`, `date`, `reference`, `amount`, `note`, `created_by`, `attachment`) VALUES
(1, '2015-11-02 13:52:00', 'Frete da Empada', '5.00', '', '1', NULL),
(2, '2016-01-25 23:22:00', 'batata', '2.00', '', '1', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_gift_cards`
--

CREATE TABLE `tec_gift_cards` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `card_no` varchar(20) NOT NULL,
  `value` decimal(25,2) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `balance` decimal(25,2) NOT NULL,
  `expiry` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_groups`
--

CREATE TABLE `tec_groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tec_groups`
--

INSERT INTO `tec_groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'staff', 'Staff');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_login_attempts`
--

CREATE TABLE `tec_login_attempts` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tec_login_attempts`
--

INSERT INTO `tec_login_attempts` (`id`, `ip_address`, `login`, `time`) VALUES
(3, 0x3139322e3136382e302e33, 'jjunior', 1480351045);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_mesas`
--

CREATE TABLE `tec_mesas` (
  `id` int(11) NOT NULL,
  `mesa` varchar(45) NOT NULL,
  `estado` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tec_mesas`
--

INSERT INTO `tec_mesas` (`id`, `mesa`, `estado`) VALUES
(1, 'Mesa 01', 'free'),
(2, 'Mesa 02', 'free'),
(3, 'Mesa 03', 'free'),
(4, 'Mesa 04', 'free'),
(5, 'Mesa 05', 'free'),
(6, 'Mesa 06', 'free'),
(7, 'Mesa 07', 'free'),
(8, 'Mesa 08', 'free'),
(9, 'Mesa 09', 'free'),
(10, 'Mesa 10', 'free'),
(11, 'Mesa 11', 'free'),
(12, 'Mesa 12', 'free'),
(13, 'Mesa 13', 'free'),
(14, 'Mesa 14', 'free'),
(15, 'Mesa 15', 'free'),
(16, 'Mesa 16', 'free'),
(17, 'Mesa 17', 'free'),
(18, 'Mesa 18', 'free'),
(19, 'Mesa 19', 'free'),
(20, 'Mesa 20', 'free'),
(21, 'Mesa 21', 'free'),
(22, 'Mesa 22', 'free'),
(23, 'Mesa 23', 'free'),
(24, 'Mesa 24', 'free'),
(25, 'Mesa 25', 'free'),
(26, 'Mesa 26', 'free'),
(27, 'Mesa 27', 'free');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_payments`
--

CREATE TABLE `tec_payments` (
  `id` int(11) NOT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `sale_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `transaction_id` varchar(50) DEFAULT NULL,
  `paid_by` varchar(20) NOT NULL,
  `cheque_no` varchar(20) DEFAULT NULL,
  `cc_no` varchar(20) DEFAULT NULL,
  `cc_holder` varchar(25) DEFAULT NULL,
  `cc_month` varchar(2) DEFAULT NULL,
  `cc_year` varchar(4) DEFAULT NULL,
  `cc_type` varchar(20) DEFAULT NULL,
  `amount` decimal(25,2) NOT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `attachment` varchar(55) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `pos_paid` decimal(25,2) DEFAULT '0.00',
  `pos_balance` decimal(25,2) DEFAULT '0.00',
  `gc_no` varchar(20) DEFAULT NULL,
  `reference` varchar(50) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tec_payments`
--

INSERT INTO `tec_payments` (`id`, `date`, `sale_id`, `customer_id`, `transaction_id`, `paid_by`, `cheque_no`, `cc_no`, `cc_holder`, `cc_month`, `cc_year`, `cc_type`, `amount`, `currency`, `created_by`, `attachment`, `note`, `pos_paid`, `pos_balance`, `gc_no`, `reference`, `updated_by`, `updated_at`) VALUES
(1, '2015-11-02 13:47:00', 1, 1, NULL, 'CC', '', '', '', '', '', 'Visa', '4.00', NULL, 1, NULL, '', '0.00', '0.00', '', '', NULL, NULL),
(2, '2015-11-03 12:43:13', 2, 1, NULL, 'cash', '', '', '', '', '', '', '16.00', NULL, 1, NULL, '', '20.00', '4.00', '', NULL, NULL, NULL),
(3, '2015-12-12 19:10:09', 4, 1, NULL, 'cash', '', '', '', '', '', '', '10.99', NULL, 2, NULL, '', '50.00', '39.01', '', NULL, NULL, NULL),
(4, '2015-12-12 19:13:40', 5, 1, NULL, 'cash', '', '', '', '', '', '', '32.97', NULL, 2, NULL, '', '70.00', '37.03', '', NULL, NULL, NULL),
(5, '2016-01-22 17:29:59', 6, 1, NULL, 'cash', '', '', '', '', '', '', '20.00', NULL, 1, NULL, '', '20.00', '0.00', '', NULL, NULL, NULL),
(6, '2016-01-25 23:15:47', 7, 1, NULL, 'cash', '', '', '', '', '', '', '4.00', NULL, 1, NULL, '', '10.00', '6.00', '', NULL, NULL, NULL),
(7, '2016-01-25 23:50:55', 8, 1, NULL, 'cash', '', '', '', '', '', '', '4.00', NULL, 1, NULL, '', '10.00', '6.00', '', NULL, NULL, NULL),
(8, '2016-01-25 23:54:16', 10, 1, NULL, 'cash', '', '', '', '', '', '', '6.00', NULL, 1, NULL, '', '50.00', '44.00', '', NULL, NULL, NULL),
(9, '2016-01-26 00:00:29', 11, 1, NULL, 'cash', '', '', '', '', '', '', '4.00', NULL, 1, NULL, '', '4.00', '6.00', '', NULL, NULL, NULL),
(10, '2016-01-26 00:02:46', 12, 1, NULL, 'cash', '', '', '', '', '', '', '7.00', NULL, 1, NULL, '', '10.00', '3.00', '', NULL, NULL, NULL),
(11, '2016-01-26 00:04:10', 13, 1, NULL, 'cash', '', '', '', '', '', '', '2.00', NULL, 1, NULL, '', '5.00', '3.00', '', NULL, NULL, NULL),
(12, '2016-01-26 00:04:47', 14, 1, NULL, 'cash', '', '', '', '', '', '', '8.00', NULL, 1, NULL, '', '10.00', '2.00', '', NULL, NULL, NULL),
(13, '2016-01-26 00:11:13', 15, 1, NULL, 'cash', '', '', '', '', '', '', '24.00', NULL, 1, NULL, '', '50.00', '26.00', '', NULL, NULL, NULL),
(14, '2016-02-01 05:03:58', 16, 1, NULL, 'cash', '', '', '', '', '', '', '6.00', NULL, 1, NULL, '', '50.00', '44.00', '', NULL, NULL, NULL),
(15, '2016-07-28 23:29:36', 18, 1, NULL, 'cash', '', '', '', '', '', '', '11.00', NULL, 4, NULL, '', '20.00', '9.00', '', NULL, NULL, NULL),
(16, '2016-07-29 00:57:49', 20, 2, NULL, 'cash', '', '', '', '', '', '', '4.00', NULL, 4, NULL, '', '10.00', '6.00', '', NULL, NULL, NULL),
(17, '2016-07-29 22:02:02', 21, 2, NULL, 'cash', '', '', '', '', '', '', '6.00', NULL, 4, NULL, '', '10.00', '4.00', '', NULL, NULL, NULL),
(18, '2016-07-29 22:02:25', 22, 1, NULL, 'cash', '', '', '', '', '', '', '3.00', NULL, 4, NULL, '', '5.00', '2.00', '', NULL, NULL, NULL),
(19, '2016-09-29 20:08:17', 23, 1, NULL, 'cash', '', '', '', '', '', '', '10.00', NULL, 4, NULL, '', '10.00', '0.00', '', NULL, NULL, NULL),
(20, '2016-09-29 20:11:52', 24, 1, NULL, 'cash', '', '', '', '', '', '', '7.00', NULL, 4, NULL, '', '7.00', '0.00', '', NULL, NULL, NULL),
(21, '2016-09-29 20:12:14', 25, 1, NULL, 'cash', '', '', '', '', '', '', '7.00', NULL, 4, NULL, '', '7.00', '0.00', '', NULL, NULL, NULL),
(22, '2016-09-29 20:12:33', 26, 1, NULL, 'cash', '', '', '', '', '', '', '1.00', NULL, 4, NULL, '', '1.00', '0.00', '', NULL, NULL, NULL),
(23, '2016-10-04 20:20:17', 28, 1, NULL, 'cash', '', '', '', '', '', '', '10.00', NULL, 4, NULL, '', '10.00', '0.00', '', NULL, NULL, NULL),
(24, '2016-10-14 17:43:41', 29, 1, NULL, 'cash', '', '', '', '', '', '', '3.00', NULL, 4, NULL, '', '3.00', '0.00', '', NULL, NULL, NULL),
(25, '2016-10-14 17:55:39', 30, 1, NULL, 'cash', '', '', '', '', '', '', '4.00', NULL, 4, NULL, '', '4.00', '0.00', '', NULL, NULL, NULL),
(26, '2016-10-16 21:24:14', 31, 4, NULL, 'cash', '', '', '', '', '', '', '2.00', NULL, 4, NULL, '', '2.00', '0.00', '', NULL, NULL, NULL),
(27, '2016-10-18 11:16:26', 32, 1, NULL, 'cash', '', '', '', '', '', '', '3.00', NULL, 4, NULL, '', '3.00', '0.00', '', NULL, NULL, NULL),
(28, '2016-11-24 17:41:42', 33, 1, NULL, 'cash', '', '', '', '', '', '', '5.00', NULL, 4, NULL, '', '5.00', '0.00', '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_pedido_mesa`
--

CREATE TABLE `tec_pedido_mesa` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `total` decimal(10,2) NOT NULL,
  `id_mesa` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `impresso` int(11) NOT NULL,
  `cozinha` int(11) DEFAULT NULL,
  `foi_pedido` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_products`
--

CREATE TABLE `tec_products` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `name` char(255) NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '1',
  `price` decimal(25,2) NOT NULL,
  `image` varchar(255) DEFAULT 'no_image.png',
  `tax` varchar(20) DEFAULT NULL,
  `cost` decimal(25,2) DEFAULT NULL,
  `tax_method` tinyint(1) DEFAULT '1',
  `quantity` decimal(15,2) DEFAULT '0.00',
  `barcode_symbology` varchar(20) NOT NULL DEFAULT 'code39',
  `type` varchar(20) NOT NULL DEFAULT 'standard',
  `details` text,
  `alert_quantity` decimal(10,2) DEFAULT '0.00',
  `cozinha` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tec_products`
--

INSERT INTO `tec_products` (`id`, `code`, `name`, `category_id`, `price`, `image`, `tax`, `cost`, `tax_method`, `quantity`, `barcode_symbology`, `type`, `details`, `alert_quantity`, `cozinha`) VALUES
(1, '0001', 'Hamburquer', 2, '2.00', '99ba81363ddbfe5a92c93023e1fd550a.jpg', '0', '4.00', 0, '5.00', 'code39', 'standard', 'Hamburguer com P?o de Hamburguer, queijo, carne, presunto e salada', '5.00', 1),
(2, '0002', 'Mixto Quente', 2, '1.00', '3ba18844e23b27e8224f8fa6b1752208.jpg', '0', '3.00', 0, '9.00', 'code39', 'standard', '', '5.00', 1),
(3, '0003', 'Cahorro Quente', 2, '2.00', '573bc5101fabefd864960416b1752899.jpg', '0', '3.00', 0, '4.00', 'code39', 'standard', '', '5.00', 1),
(4, '0004', 'Bolo de Chocolate', 2, '2.00', '8ad58758122f3a886e859def53da6a6a.jpg', '0', '3.00', 0, '7.00', 'code39', 'standard', '', '5.00', 0),
(5, '0005', 'Coxinha de Frango', 2, '2.00', 'd3115abf501ce492bdf449f72f185fb1.jpg', '0', '3.00', 0, '9.00', 'code39', 'standard', '', '5.00', 0),
(6, '0006', 'Empada', 2, '2.00', '76fed631b7861010869172aa83d78e0a.jpg', '0', '3.00', 0, '19.00', 'code39', 'standard', '', '5.00', 0),
(7, '0007', 'Monteiro Lopes', 2, '2.00', '3274477f5b7d3ef257c4562c56ef387e.jpg', '0', '3.00', 0, '10.00', 'code39', 'standard', '', '5.00', 0),
(8, '0008', 'Risole de Carne', 2, '2.00', '32a3ac97716a9dc68812aecbaf11840a.jpg', '0', '4.00', 0, '6.00', 'code39', 'standard', '', '5.00', 0),
(9, '0009', 'Coxinha de Caranguejo', 2, '4.00', '8bd5b89b645b1bc2d4d08816b5ad3d0b.jpg', '0', '6.00', 0, '6.00', 'code39', 'standard', '', '5.00', 0),
(10, '0010', 'Coxinha de Camar?o', 2, '4.00', '272825062f261b126f1996ed099b4b87.jpg', '0', '6.00', 0, '8.00', 'code39', 'standard', '', '5.00', 0),
(11, '0011', 'Sonho', 2, '2.00', '1f56837339171226e7e33eb0c5e8eae0.jpg', '0', '3.00', 0, '8.00', 'code39', 'standard', '', '5.00', 0),
(12, '0012', 'Lasanha', 2, '6.00', 'fd1c25461a5fbb0597c68bb78100c6ec.jpg', '0', '9.00', 0, '10.00', 'code39', 'standard', '', '5.00', 0),
(13, '0013', 'Torta de Chocolate', 2, '3.00', '11fcdf61a2d8c2d6b7c3e9c0a6996a54.jpg', '0', '6.00', 0, '10.00', 'code39', 'standard', '', '5.00', 0),
(14, '0014', 'Fanta Laranja Lata', 1, '2.00', 'f0ed23add960528f5da95d8fb2a8a106.jpg', '0', '4.00', 0, '11.00', 'code39', 'standard', '', '5.00', 0),
(15, '0015', 'Coca-Cola Lata', 1, '2.00', 'd1ae8344e2fdfc3fcd80a96bb1f00240.jpg', '0', '4.00', 0, '8.00', 'code39', 'standard', '', '5.00', 0),
(16, '0016', '?gua Mineral', 1, '2.00', '91b3bcff369f45e167c3544bad752912.jpg', '0', '3.00', 0, '9.00', 'code39', 'standard', '', '5.00', 0),
(17, '0017', 'Suco de Laranja', 1, '4.00', 'f4cab501731cb47389a6c1a9a54cf736.jpg', '0', '6.00', 0, '6.00', 'code39', 'standard', '', '5.00', 0),
(18, '01', 'Combo M', 2, '10.99', 'no_image.png', '5', '8.71', 0, '0.00', 'code39', 'combo', '', '0.00', 0),
(19, '02', 'Batata M', 2, '7.99', 'no_image.png', '0', '4.72', 0, '0.00', 'code39', 'standard', '', '0.00', 0),
(20, '03', 'Cobertura Cheddar', 2, '1.00', 'no_image.png', '0', '0.27', 0, '0.00', 'code39', 'standard', '', '0.00', 0),
(21, '012', 'Pizza Calabresa', 3, '24.00', 'no_image.png', '0', '12.00', 1, '10.00', 'code39', 'standard', '', '1.00', NULL),
(22, '20', 'Pizza Mussarela', 3, '26.00', 'no_image.png', '0', '13.00', 1, '10.00', 'code39', 'standard', '', '0.00', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_purchases`
--

CREATE TABLE `tec_purchases` (
  `id` int(11) NOT NULL,
  `reference` varchar(55) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `note` varchar(1000) NOT NULL,
  `total` decimal(25,2) NOT NULL,
  `attachment` varchar(255) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `received` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tec_purchases`
--

INSERT INTO `tec_purchases` (`id`, `reference`, `date`, `note`, `total`, `attachment`, `supplier_id`, `received`) VALUES
(1, '', '2015-11-02 13:51:00', '', '30.00', NULL, NULL, NULL),
(2, '', '2016-01-25 23:19:00', '', '40.00', NULL, NULL, NULL),
(3, '', '2016-01-26 00:09:00', '', '40.00', NULL, NULL, NULL),
(4, '', '2016-07-28 23:40:00', '', '40.00', NULL, NULL, NULL),
(5, '', '2016-10-14 18:01:00', '', '11.00', NULL, NULL, NULL),
(6, '', '2016-10-14 18:02:00', '', '67.00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_purchase_items`
--

CREATE TABLE `tec_purchase_items` (
  `id` int(11) NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` decimal(15,2) NOT NULL,
  `cost` decimal(25,2) NOT NULL,
  `subtotal` decimal(25,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tec_purchase_items`
--

INSERT INTO `tec_purchase_items` (`id`, `purchase_id`, `product_id`, `quantity`, `cost`, `subtotal`) VALUES
(1, 1, 6, '10.00', '3.00', '30.00'),
(2, 2, 14, '10.00', '4.00', '40.00'),
(3, 3, 1, '10.00', '4.00', '40.00'),
(6, 4, 1, '5.00', '4.00', '20.00'),
(7, 4, 15, '5.00', '4.00', '20.00'),
(8, 5, 1, '1.00', '4.00', '4.00'),
(9, 5, 2, '1.00', '3.00', '3.00'),
(10, 5, 15, '1.00', '4.00', '4.00'),
(11, 6, 1, '10.00', '4.00', '40.00'),
(12, 6, 2, '9.00', '3.00', '27.00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_registers`
--

CREATE TABLE `tec_registers` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `cash_in_hand` decimal(25,2) NOT NULL,
  `status` varchar(10) NOT NULL,
  `total_cash` decimal(25,2) DEFAULT NULL,
  `total_cheques` int(11) DEFAULT NULL,
  `total_cc_slips` int(11) DEFAULT NULL,
  `total_cash_submitted` decimal(25,2) DEFAULT NULL,
  `total_cheques_submitted` int(11) DEFAULT NULL,
  `total_cc_slips_submitted` int(11) DEFAULT NULL,
  `note` text,
  `closed_at` timestamp NULL DEFAULT NULL,
  `transfer_opened_bills` varchar(50) DEFAULT NULL,
  `closed_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tec_registers`
--

INSERT INTO `tec_registers` (`id`, `date`, `user_id`, `cash_in_hand`, `status`, `total_cash`, `total_cheques`, `total_cc_slips`, `total_cash_submitted`, `total_cheques_submitted`, `total_cc_slips_submitted`, `note`, `closed_at`, `transfer_opened_bills`, `closed_by`) VALUES
(1, '2015-11-02 12:39:22', 1, '0.00', 'close', '0.00', 0, 1, '0.00', 0, 1, '', '2015-11-02 13:49:29', NULL, 1),
(2, '2015-11-02 14:00:24', 1, '0.00', 'close', '36.00', 0, 0, '36.00', 0, 0, '', '2016-01-25 23:11:28', NULL, 1),
(3, '2015-12-12 18:59:48', 2, '50.00', 'open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, '2016-01-25 23:12:25', 1, '0.00', 'close', '2.00', 0, 0, '2.00', 0, 0, '', '2016-01-25 23:28:28', '0', 1),
(5, '2016-01-25 23:46:53', 1, '0.00', 'close', '0.00', 0, 0, '0.00', 0, 0, '', '2016-01-25 23:48:44', NULL, 1),
(6, '2016-01-25 23:50:22', 1, '100.00', 'close', '110.00', 0, 0, '110.00', 0, 0, '', '2016-01-25 23:56:02', NULL, 1),
(7, '2016-01-25 23:59:31', 1, '100.00', 'close', '104.00', 0, 0, '104.00', 0, 0, '', '2016-01-26 00:01:16', NULL, 1),
(8, '2016-01-26 00:01:58', 1, '100.00', 'open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, '2016-07-27 05:43:25', 4, '2000.00', 'close', '2000.00', 0, 0, '2000.00', 0, 0, '', '2016-07-27 15:02:30', NULL, 4),
(10, '2016-07-28 01:02:46', 4, '2000.00', 'close', '2011.00', 0, 0, '2011.00', 0, 5, '', '2016-07-28 23:46:15', NULL, 4),
(11, '2016-07-28 23:53:09', 4, '873.00', 'close', '877.00', 0, 0, '877.00', 0, 0, '', '2016-07-29 00:58:03', NULL, 4),
(12, '2016-07-29 21:56:50', 4, '800.00', 'close', '809.00', 0, 0, '809.00', 0, 0, '', '2016-07-29 22:02:39', NULL, 4),
(13, '2016-08-02 02:33:49', 4, '200.00', 'close', '244.00', 0, 0, '244.00', 0, 0, '', '2016-10-16 21:26:12', '0', 4),
(14, '2016-10-16 21:26:19', 4, '800.00', 'open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_sales`
--

CREATE TABLE `tec_sales` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer_name` varchar(55) NOT NULL,
  `total` decimal(25,2) NOT NULL,
  `product_discount` decimal(25,2) DEFAULT NULL,
  `order_discount_id` varchar(20) DEFAULT NULL,
  `order_discount` decimal(25,2) DEFAULT NULL,
  `total_discount` decimal(25,2) DEFAULT NULL,
  `product_tax` decimal(25,2) DEFAULT NULL,
  `order_tax_id` varchar(20) DEFAULT NULL,
  `order_tax` decimal(25,2) DEFAULT NULL,
  `total_tax` decimal(25,2) DEFAULT NULL,
  `grand_total` decimal(25,2) NOT NULL,
  `total_items` int(11) DEFAULT NULL,
  `total_quantity` decimal(15,2) DEFAULT NULL,
  `paid` decimal(25,2) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `rounding` decimal(8,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tec_sales`
--

INSERT INTO `tec_sales` (`id`, `date`, `customer_id`, `customer_name`, `total`, `product_discount`, `order_discount_id`, `order_discount`, `total_discount`, `product_tax`, `order_tax_id`, `order_tax`, `total_tax`, `grand_total`, `total_items`, `total_quantity`, `paid`, `created_by`, `updated_by`, `updated_at`, `note`, `status`, `rounding`) VALUES
(1, '2015-11-02 11:42:47', 1, 'Cliente Padr?o', '4.00', '0.00', NULL, '0.00', '0.00', '0.00', NULL, '0.00', '0.00', '4.00', 2, '2.00', '4.00', 1, 1, '2015-11-02 11:47:25', '', 'paid', '0.00'),
(2, '2015-11-03 10:43:13', 1, 'Cliente Padr?o', '16.00', '0.00', NULL, '0.00', '0.00', '0.00', '0%', '0.00', '0.00', '16.00', 5, '6.00', '16.00', 1, NULL, NULL, '', 'paid', '0.00'),
(3, '2015-12-12 17:08:16', 1, 'Cliente Padr?o', '10.47', '0.00', NULL, '0.00', '0.00', '0.52', '0%', '0.00', '0.52', '10.99', 1, '1.00', '0.00', 2, NULL, NULL, '', 'due', '0.00'),
(4, '2015-12-12 17:10:09', 1, 'Cliente Padr?o', '10.99', '0.00', NULL, '0.00', '0.00', '0.00', '0%', '0.00', '0.00', '10.99', 3, '3.00', '10.99', 2, NULL, NULL, '', 'paid', '0.00'),
(5, '2015-12-12 17:13:40', 1, 'Cliente Padr?o', '31.41', '0.00', NULL, '0.00', '0.00', '1.56', '0%', '0.00', '1.56', '32.97', 1, '3.00', '32.97', 2, NULL, NULL, '', 'paid', '0.00'),
(6, '2016-01-22 15:29:59', 1, 'Cliente Padr?o', '20.00', '0.00', NULL, '0.00', '0.00', '0.00', '0%', '0.00', '0.00', '20.00', 6, '8.00', '20.00', 1, NULL, NULL, '', 'paid', '0.00'),
(7, '2016-01-25 21:15:47', 1, 'Cliente Padr?o', '4.00', '0.00', NULL, '0.00', '0.00', '0.00', '0%', '0.00', '0.00', '4.00', 2, '2.00', '4.00', 1, NULL, NULL, '', 'paid', '0.00'),
(8, '2016-01-25 21:50:55', 1, 'Cliente Padr?o', '4.00', '0.00', NULL, '0.00', '0.00', '0.00', '0%', '0.00', '0.00', '4.00', 2, '2.00', '4.00', 1, NULL, NULL, '', 'paid', '0.00'),
(9, '2016-01-25 21:52:35', 1, 'Cliente Padr?o', '6.00', '0.00', NULL, '0.00', '0.00', '0.00', '0%', '0.00', '0.00', '6.00', 3, '3.00', '6.00', 1, NULL, NULL, '', 'paid', '0.00'),
(10, '2016-01-25 21:54:16', 1, 'Cliente Padr?o', '6.00', '0.00', NULL, '0.00', '0.00', '0.00', '0%', '0.00', '0.00', '6.00', 3, '3.00', '6.00', 1, NULL, NULL, '', 'paid', '0.00'),
(11, '2016-01-25 22:00:29', 1, 'Cliente Padr?o', '4.00', '0.00', NULL, '0.00', '0.00', '0.00', '0%', '0.00', '0.00', '4.00', 2, '2.00', '4.00', 1, NULL, NULL, '', 'paid', '0.00'),
(12, '2016-01-25 22:02:46', 1, 'Cliente Padr?o', '7.00', '0.00', NULL, '0.00', '0.00', '0.00', '0%', '0.00', '0.00', '7.00', 3, '3.00', '7.00', 1, NULL, NULL, '', 'paid', '0.00'),
(13, '2016-01-25 22:04:10', 1, 'Cliente Padr?o', '2.00', '0.00', NULL, '0.00', '0.00', '0.00', '0%', '0.00', '0.00', '2.00', 1, '1.00', '2.00', 1, NULL, NULL, '', 'paid', '0.00'),
(14, '2016-01-25 22:04:47', 1, 'Cliente Padr?o', '8.00', '0.00', NULL, '0.00', '0.00', '0.00', '0%', '0.00', '0.00', '8.00', 3, '3.00', '8.00', 1, NULL, NULL, '', 'paid', '0.00'),
(15, '2016-01-25 22:11:13', 1, 'Cliente Padr?o', '24.00', '0.00', NULL, '0.00', '0.00', '0.00', '0%', '0.00', '0.00', '24.00', 1, '12.00', '24.00', 1, NULL, NULL, '', 'paid', '0.00'),
(16, '2016-02-01 02:03:58', 1, 'Cliente Padr?o', '6.00', '0.00', NULL, '0.00', '0.00', '0.00', '0%', '0.00', '0.00', '6.00', 2, '2.00', '6.00', 1, NULL, NULL, '', 'paid', '0.00'),
(17, '2016-07-27 02:48:14', 1, 'Cliente Padr?o', '3.00', '0.00', NULL, '0.00', '0.00', '0.00', '0%', '0.00', '0.00', '3.00', 2, '2.00', '0.00', 4, NULL, NULL, '', 'due', '0.00'),
(18, '2016-07-28 20:29:36', 1, 'Cliente Padr?o', '11.00', '0.00', NULL, '0.00', '0.00', '0.00', '0%', '0.00', '0.00', '11.00', 3, '5.00', '11.00', 4, NULL, NULL, '', 'paid', '0.00'),
(19, '2016-07-28 20:37:34', 2, 'Mesa 01', '5.00', '0.00', NULL, '0.00', '0.00', '0.00', '0%', '0.00', '0.00', '5.00', 3, '3.00', '0.00', 4, NULL, NULL, '', 'due', '0.00'),
(20, '2016-07-28 21:57:49', 2, 'Mesa 01', '4.00', '0.00', NULL, '0.00', '0.00', '0.00', '0%', '0.00', '0.00', '4.00', 2, '2.00', '4.00', 4, NULL, NULL, '', 'paid', '0.00'),
(21, '2016-07-29 19:02:02', 2, 'Mesa 01', '6.00', '0.00', NULL, '0.00', '0.00', '0.00', '0%', '0.00', '0.00', '6.00', 2, '2.00', '6.00', 4, NULL, NULL, '', 'paid', '0.00'),
(22, '2016-07-29 19:02:25', 1, 'Cliente Padr?o', '3.00', '0.00', NULL, '0.00', '0.00', '0.00', '0%', '0.00', '0.00', '3.00', 2, '2.00', '3.00', 4, NULL, NULL, '', 'paid', '0.00'),
(23, '2016-09-29 17:08:17', 1, 'Cliente Padr?o', '10.00', '0.00', NULL, '0.00', '0.00', '0.00', NULL, '0.00', '0.00', '10.00', 3, '3.00', '10.00', 4, NULL, NULL, '', 'paid', '0.00'),
(24, '2016-09-29 17:11:52', 1, 'Cliente Padr?o', '7.00', '0.00', NULL, '0.00', '0.00', '0.00', NULL, '0.00', '0.00', '7.00', 2, '2.00', '7.00', 4, NULL, NULL, '', 'paid', '0.00'),
(25, '2016-09-29 17:12:14', 1, 'Cliente Padr?o', '7.00', '0.00', NULL, '0.00', '0.00', '0.00', NULL, '0.00', '0.00', '7.00', 2, '2.00', '7.00', 4, NULL, NULL, '', 'paid', '0.00'),
(26, '2016-09-29 17:12:33', 1, 'Cliente Padr?o', '1.00', '0.00', NULL, '0.00', '0.00', '0.00', NULL, '0.00', '0.00', '1.00', 1, '1.00', '1.00', 4, NULL, NULL, '', 'paid', '0.00'),
(27, '2016-09-30 15:23:27', 1, 'Cliente Padr?o', '7.00', '0.00', NULL, '0.00', '0.00', '0.00', NULL, '0.00', '0.00', '7.00', 2, '2.00', '0.00', 4, NULL, NULL, '', 'due', '0.00'),
(28, '2016-10-04 17:20:17', 1, 'Cliente Padrão', '10.00', '0.00', NULL, '0.00', '0.00', '0.00', '0%', '0.00', '0.00', '10.00', 2, '5.00', '10.00', 4, NULL, NULL, '', 'paid', '0.00'),
(29, '2016-10-14 14:43:41', 1, 'Cliente Padrão', '3.00', '0.00', NULL, '0.00', '0.00', '0.00', '0%', '0.00', '0.00', '3.00', 2, '2.00', '3.00', 4, NULL, NULL, '', 'paid', '0.00'),
(30, '2016-10-14 14:55:39', 1, 'Cliente Padrão', '4.00', '0.00', NULL, '0.00', '0.00', '0.00', '0%', '0.00', '0.00', '4.00', 1, '1.00', '4.00', 4, NULL, NULL, '', 'paid', '0.00'),
(31, '2016-10-16 19:24:14', 4, 'Gerson', '2.00', '0.00', NULL, '0.00', '0.00', '0.00', NULL, '0.00', '0.00', '2.00', 1, '1.00', '2.00', 4, NULL, NULL, '', 'paid', '0.00'),
(32, '2016-10-18 09:16:26', 1, 'Cliente Padrão', '3.00', '0.00', NULL, '0.00', '0.00', '0.00', NULL, '0.00', '0.00', '3.00', 1, '1.00', '3.00', 4, NULL, NULL, '', 'paid', '0.00'),
(33, '2016-11-24 15:41:42', 1, 'Cliente Padrão', '5.00', '0.00', NULL, '0.00', '0.00', '0.00', '0%', '0.00', '0.00', '5.00', 3, '3.00', '5.00', 4, NULL, NULL, '', 'paid', '0.00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_sale_items`
--

CREATE TABLE `tec_sale_items` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` decimal(15,2) NOT NULL,
  `unit_price` decimal(25,2) NOT NULL,
  `net_unit_price` decimal(25,2) NOT NULL,
  `discount` varchar(20) DEFAULT NULL,
  `item_discount` decimal(25,2) DEFAULT NULL,
  `tax` int(20) DEFAULT NULL,
  `item_tax` decimal(25,2) DEFAULT NULL,
  `subtotal` decimal(25,2) NOT NULL,
  `real_unit_price` decimal(25,2) DEFAULT NULL,
  `cost` decimal(25,2) DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tec_sale_items`
--

INSERT INTO `tec_sale_items` (`id`, `sale_id`, `product_id`, `quantity`, `unit_price`, `net_unit_price`, `discount`, `item_discount`, `tax`, `item_tax`, `subtotal`, `real_unit_price`, `cost`) VALUES
(3, 1, 1, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '4.00'),
(4, 1, 15, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '4.00'),
(5, 2, 1, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '4.00'),
(6, 2, 3, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '3.00'),
(7, 2, 9, '2.00', '4.00', '4.00', '0', '0.00', 0, '0.00', '8.00', '4.00', '6.00'),
(8, 2, 14, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '4.00'),
(9, 2, 15, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '4.00'),
(10, 3, 18, '1.00', '10.99', '10.47', '0', '0.00', 5, '0.52', '10.99', '10.99', '8.71'),
(11, 4, 15, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '4.00'),
(12, 4, 19, '1.00', '7.99', '7.99', '0', '0.00', 0, '0.00', '7.99', '7.99', '4.72'),
(13, 4, 20, '1.00', '1.00', '1.00', '0', '0.00', 0, '0.00', '1.00', '1.00', '0.27'),
(14, 5, 18, '3.00', '10.99', '10.47', '0', '0.00', 5, '1.56', '32.97', '10.99', '8.71'),
(15, 6, 6, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '3.00'),
(16, 6, 10, '1.00', '4.00', '4.00', '0', '0.00', 0, '0.00', '4.00', '4.00', '6.00'),
(17, 6, 11, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '3.00'),
(18, 6, 14, '2.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '4.00', '2.00', '4.00'),
(19, 6, 15, '2.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '4.00', '2.00', '4.00'),
(20, 6, 17, '1.00', '4.00', '4.00', '0', '0.00', 0, '0.00', '4.00', '4.00', '6.00'),
(21, 7, 1, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '4.00'),
(22, 7, 14, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '4.00'),
(23, 8, 1, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '4.00'),
(24, 8, 14, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '4.00'),
(25, 9, 4, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '3.00'),
(26, 9, 8, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '4.00'),
(27, 9, 14, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '4.00'),
(28, 10, 8, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '4.00'),
(29, 10, 11, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '3.00'),
(30, 10, 14, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '4.00'),
(31, 11, 3, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '3.00'),
(32, 11, 15, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '4.00'),
(33, 12, 1, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '4.00'),
(34, 12, 2, '1.00', '1.00', '1.00', '0', '0.00', 0, '0.00', '1.00', '1.00', '3.00'),
(35, 12, 17, '1.00', '4.00', '4.00', '0', '0.00', 0, '0.00', '4.00', '4.00', '6.00'),
(36, 13, 14, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '4.00'),
(37, 14, 10, '1.00', '4.00', '4.00', '0', '0.00', 0, '0.00', '4.00', '4.00', '6.00'),
(38, 14, 14, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '4.00'),
(39, 14, 15, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '4.00'),
(40, 15, 1, '12.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '24.00', '2.00', '4.00'),
(41, 16, 16, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '3.00'),
(42, 16, 17, '1.00', '4.00', '4.00', '0', '0.00', 0, '0.00', '4.00', '4.00', '6.00'),
(43, 17, 2, '1.00', '1.00', '1.00', '0', '0.00', 0, '0.00', '1.00', '1.00', '3.00'),
(44, 17, 15, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '4.00'),
(45, 18, 1, '3.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '6.00', '2.00', '4.00'),
(46, 18, 2, '1.00', '1.00', '1.00', '0', '0.00', 0, '0.00', '1.00', '1.00', '3.00'),
(47, 18, 17, '1.00', '4.00', '4.00', '0', '0.00', 0, '0.00', '4.00', '4.00', '6.00'),
(48, 19, 2, '1.00', '1.00', '1.00', '0', '0.00', 0, '0.00', '1.00', '1.00', '3.00'),
(49, 19, 3, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '3.00'),
(50, 19, 15, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '4.00'),
(51, 20, 3, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '3.00'),
(52, 20, 15, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '4.00'),
(53, 21, 8, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '4.00'),
(54, 21, 9, '1.00', '4.00', '4.00', '0', '0.00', 0, '0.00', '4.00', '4.00', '6.00'),
(55, 22, 1, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '4.00'),
(56, 22, 2, '1.00', '1.00', '1.00', '0', '0.00', 0, '0.00', '1.00', '1.00', '3.00'),
(57, 23, 1, '1.00', '4.00', '4.00', '0', '0.00', 0, '0.00', '4.00', '4.00', '4.00'),
(58, 23, 2, '1.00', '3.00', '3.00', '0', '0.00', 0, '0.00', '3.00', '3.00', '3.00'),
(59, 23, 3, '1.00', '3.00', '3.00', '0', '0.00', 0, '0.00', '3.00', '3.00', '3.00'),
(60, 24, 1, '1.00', '4.00', '4.00', '0', '0.00', 0, '0.00', '4.00', '4.00', '4.00'),
(61, 24, 2, '1.00', '3.00', '3.00', '0', '0.00', 0, '0.00', '3.00', '3.00', '3.00'),
(62, 25, 1, '1.00', '4.00', '4.00', '0', '0.00', 0, '0.00', '4.00', '4.00', '4.00'),
(63, 25, 4, '1.00', '3.00', '3.00', '0', '0.00', 0, '0.00', '3.00', '3.00', '3.00'),
(64, 26, 2, '1.00', '1.00', '1.00', '0', '0.00', 0, '0.00', '1.00', '1.00', '3.00'),
(65, 27, 5, '1.00', '3.00', '3.00', '0', '0.00', 0, '0.00', '3.00', '3.00', '3.00'),
(66, 27, 8, '1.00', '4.00', '4.00', '0', '0.00', 0, '0.00', '4.00', '4.00', '4.00'),
(67, 28, 1, '4.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '8.00', '2.00', '4.00'),
(68, 28, 4, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '3.00'),
(69, 29, 1, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '4.00'),
(70, 29, 2, '1.00', '1.00', '1.00', '0', '0.00', 0, '0.00', '1.00', '1.00', '3.00'),
(71, 30, 9, '1.00', '4.00', '4.00', '0', '0.00', 0, '0.00', '4.00', '4.00', '6.00'),
(72, 31, 1, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '4.00'),
(73, 32, 2, '1.00', '3.00', '3.00', '0', '0.00', 0, '0.00', '3.00', '3.00', '3.00'),
(74, 33, 1, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '4.00'),
(75, 33, 2, '1.00', '1.00', '1.00', '0', '0.00', 0, '0.00', '1.00', '1.00', '3.00'),
(76, 33, 3, '1.00', '2.00', '2.00', '0', '0.00', 0, '0.00', '2.00', '2.00', '3.00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_sessions`
--

CREATE TABLE `tec_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tec_sessions`
--

INSERT INTO `tec_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('1f925a53c4af37708f9db118cb885c3312120240', '::1', 1480188215, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303138383231353b),
('2e93c2ce8c9b83758cd6f78e707e30b306ab41aa', '::1', 1480012645, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303031323634353b),
('50ac02abdd6606ec1b378b26e3ca30680567e993', '192.168.0.3', 1480351202, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303335303939393b6964656e746974797c733a363a226a756e696f72223b757365726e616d657c733a363a226a756e696f72223b656d61696c7c733a32323a226a756e696f724077656269737465722e636f6d2e6272223b757365725f69647c733a313a2234223b66697273745f6e616d657c733a363a226a756e696f72223b6c6173745f6e616d657c733a31303a226e617363696d656e746f223b637265617465645f6f6e7c733a32323a2232372f30372f323031362030323a34323a343520414d223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343830303132373037223b6c6173745f69707c733a31313a223139322e3136382e302e36223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b72656769737465725f69647c733a323a223134223b636173685f696e5f68616e647c733a363a223830302e3030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031362d31302d31362031393a32363a3139223b),
('55431e49164895e0b1ac9abff624f9ea7485d932', '192.168.0.3', 1480352360, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303335323233313b6964656e746974797c733a363a226a756e696f72223b757365726e616d657c733a363a226a756e696f72223b656d61696c7c733a32323a226a756e696f724077656269737465722e636f6d2e6272223b757365725f69647c733a313a2234223b66697273745f6e616d657c733a363a226a756e696f72223b6c6173745f6e616d657c733a31303a226e617363696d656e746f223b637265617465645f6f6e7c733a32323a2232372f30372f323031362030323a34323a343520414d223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343830303132373037223b6c6173745f69707c733a31313a223139322e3136382e302e36223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b72656769737465725f69647c733a323a223134223b636173685f696e5f68616e647c733a363a223830302e3030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031362d31302d31362031393a32363a3139223b),
('57d6f9fa7a98b9f474149f38e1b1d576be115b64', '::1', 1479156679, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437393135363637393b),
('754b0d8462c16914a1bbeb134b4317c5097409d7', '192.168.0.6', 1479156679, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437393135363637393b),
('8c900d7267b1bbdda51b6933bf84c2fa18395904', '192.168.1.104', 1478026832, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383032363538313b6964656e746974797c733a363a226a756e696f72223b757365726e616d657c733a363a226a756e696f72223b656d61696c7c733a32323a226a756e696f724077656269737465722e636f6d2e6272223b757365725f69647c733a313a2234223b66697273745f6e616d657c733a363a226a756e696f72223b6c6173745f6e616d657c733a31303a226e617363696d656e746f223b637265617465645f6f6e7c733a32323a2232372f30372f323031362030323a34323a343520414d223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343737393735353437223b6c6173745f69707c733a31333a223139322e3136382e312e313034223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b72656769737465725f69647c733a323a223134223b636173685f696e5f68616e647c733a363a223830302e3030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031362d31302d31362031393a32363a3139223b),
('a35952d7f96cda0978eb1ebf4a790e21863fc3a2', '192.168.0.6', 1480012948, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303031323634363b6964656e746974797c733a363a226a756e696f72223b757365726e616d657c733a363a226a756e696f72223b656d61696c7c733a32323a226a756e696f724077656269737465722e636f6d2e6272223b757365725f69647c733a313a2234223b66697273745f6e616d657c733a363a226a756e696f72223b6c6173745f6e616d657c733a31303a226e617363696d656e746f223b637265617465645f6f6e7c733a32323a2232372f30372f323031362030323a34323a343520414d223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343738303236353935223b6c6173745f69707c733a31333a223139322e3136382e312e313034223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b72656769737465725f69647c733a323a223134223b636173685f696e5f68616e647c733a363a223830302e3030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031362d31302d31362031393a32363a3139223b),
('f162bb7ed8484e2237533098ffe736b6337c6795', '::1', 1478026581, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383032363538313b),
('f6654111bb0b041aefda76997dbf37e2fe6c5f71', '192.168.0.3', 1480354071, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303335333937333b6964656e746974797c733a363a226a756e696f72223b757365726e616d657c733a363a226a756e696f72223b656d61696c7c733a32323a226a756e696f724077656269737465722e636f6d2e6272223b757365725f69647c733a313a2234223b66697273745f6e616d657c733a363a226a756e696f72223b6c6173745f6e616d657c733a31303a226e617363696d656e746f223b637265617465645f6f6e7c733a32323a2232372f30372f323031362030323a34323a343520414d223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343830303132373037223b6c6173745f69707c733a31313a223139322e3136382e302e36223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b72656769737465725f69647c733a323a223134223b636173685f696e5f68616e647c733a363a223830302e3030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031362d31302d31362031393a32363a3139223b),
('f8f9ce174dcb109098f1c5125fa8c6ef9d3f7301', '192.168.0.3', 1480352059, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303335313835303b6964656e746974797c733a363a226a756e696f72223b757365726e616d657c733a363a226a756e696f72223b656d61696c7c733a32323a226a756e696f724077656269737465722e636f6d2e6272223b757365725f69647c733a313a2234223b66697273745f6e616d657c733a363a226a756e696f72223b6c6173745f6e616d657c733a31303a226e617363696d656e746f223b637265617465645f6f6e7c733a32323a2232372f30372f323031362030323a34323a343520414d223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343830303132373037223b6c6173745f69707c733a31313a223139322e3136382e302e36223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b72656769737465725f69647c733a323a223134223b636173685f696e5f68616e647c733a363a223830302e3030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031362d31302d31362031393a32363a3139223b),
('fd16d40c2b103ef8513cf0cf5e81ec90639dd9f9', '::1', 1480350998, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303335303939373b);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_settings`
--

CREATE TABLE `tec_settings` (
  `setting_id` int(1) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `site_name` varchar(55) NOT NULL,
  `tel` varchar(20) NOT NULL,
  `dateformat` varchar(20) DEFAULT NULL,
  `timeformat` varchar(20) DEFAULT NULL,
  `default_email` varchar(100) NOT NULL,
  `language` varchar(20) NOT NULL,
  `version` varchar(5) NOT NULL DEFAULT '1.0',
  `theme` varchar(20) NOT NULL,
  `timezone` varchar(255) NOT NULL DEFAULT '0',
  `protocol` varchar(20) NOT NULL DEFAULT 'mail',
  `smtp_host` varchar(255) DEFAULT NULL,
  `smtp_user` varchar(100) DEFAULT NULL,
  `smtp_pass` varchar(255) DEFAULT NULL,
  `smtp_port` varchar(10) DEFAULT '25',
  `smtp_crypto` varchar(5) DEFAULT NULL,
  `mmode` tinyint(1) NOT NULL,
  `captcha` tinyint(1) NOT NULL DEFAULT '1',
  `mailpath` varchar(55) DEFAULT NULL,
  `currency_prefix` varchar(3) NOT NULL,
  `default_customer` int(11) NOT NULL,
  `default_tax_rate` varchar(20) NOT NULL,
  `rows_per_page` int(2) NOT NULL,
  `total_rows` int(2) NOT NULL,
  `header` varchar(1000) NOT NULL,
  `footer` varchar(1000) NOT NULL,
  `bsty` tinyint(4) NOT NULL,
  `display_kb` tinyint(4) NOT NULL,
  `default_category` int(11) NOT NULL,
  `default_discount` varchar(20) NOT NULL,
  `item_addition` tinyint(1) NOT NULL,
  `barcode_symbology` varchar(55) NOT NULL,
  `pro_limit` tinyint(4) NOT NULL,
  `decimals` tinyint(1) NOT NULL DEFAULT '2',
  `thousands_sep` varchar(2) NOT NULL DEFAULT ',',
  `decimals_sep` varchar(2) NOT NULL DEFAULT '.',
  `focus_add_item` varchar(55) DEFAULT NULL,
  `add_customer` varchar(55) DEFAULT NULL,
  `toggle_category_slider` varchar(55) DEFAULT NULL,
  `cancel_sale` varchar(55) DEFAULT NULL,
  `suspend_sale` varchar(55) DEFAULT NULL,
  `print_order` varchar(55) DEFAULT NULL,
  `print_bill` varchar(55) DEFAULT NULL,
  `finalize_sale` varchar(55) DEFAULT NULL,
  `today_sale` varchar(55) DEFAULT NULL,
  `open_hold_bills` varchar(55) DEFAULT NULL,
  `close_register` varchar(55) DEFAULT NULL,
  `java_applet` tinyint(1) NOT NULL,
  `receipt_printer` varchar(55) DEFAULT NULL,
  `pos_printers` varchar(255) DEFAULT NULL,
  `cash_drawer_codes` varchar(55) DEFAULT NULL,
  `char_per_line` tinyint(4) DEFAULT '42',
  `rounding` tinyint(1) DEFAULT '0',
  `pin_code` varchar(20) DEFAULT NULL,
  `stripe` tinyint(1) DEFAULT NULL,
  `stripe_secret_key` varchar(100) DEFAULT NULL,
  `stripe_publishable_key` varchar(100) DEFAULT NULL,
  `purchase_code` varchar(100) DEFAULT NULL,
  `envato_username` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tec_settings`
--

INSERT INTO `tec_settings` (`setting_id`, `logo`, `site_name`, `tel`, `dateformat`, `timeformat`, `default_email`, `language`, `version`, `theme`, `timezone`, `protocol`, `smtp_host`, `smtp_user`, `smtp_pass`, `smtp_port`, `smtp_crypto`, `mmode`, `captcha`, `mailpath`, `currency_prefix`, `default_customer`, `default_tax_rate`, `rows_per_page`, `total_rows`, `header`, `footer`, `bsty`, `display_kb`, `default_category`, `default_discount`, `item_addition`, `barcode_symbology`, `pro_limit`, `decimals`, `thousands_sep`, `decimals_sep`, `focus_add_item`, `add_customer`, `toggle_category_slider`, `cancel_sale`, `suspend_sale`, `print_order`, `print_bill`, `finalize_sale`, `today_sale`, `open_hold_bills`, `close_register`, `java_applet`, `receipt_printer`, `pos_printers`, `cash_drawer_codes`, `char_per_line`, `rounding`, `pin_code`, `stripe`, `stripe_secret_key`, `stripe_publishable_key`, `purchase_code`, `envato_username`) VALUES
(1, 'logo1.png', 'Webister Food', '55 11 986352205', 'd/m/Y', 'h:i:s A', 'junior@webister.com.br', 'portugues', '4.0', 'default', 'Amercia/Belem', 'mail', 'pop.gmail.com', 'geraldopatricio.melo@gmail.com', '', '25', '', 0, 0, NULL, 'REA', 1, '0%', 10, 30, '<h2><strong>Ponto da Esfiha</strong></h2>\r\n       Av. Teste, 123 - Bairro Teste,<br>\r\n                                                                                              CEP 99.999-999, Cidade-UF<br>', 'Volte Sempre!\r\n<br>', 3, 0, 2, '0', 1, '', 100, 2, ',', '.', 'ALT+F1', 'ALT+F2', 'ALT+F10', 'ALT+F5', 'ALT+F6', 'ALT+F11', 'ALT+F12', 'ALT+F8', 'Ctrl+F1', 'Ctrl+F2', 'ALT+F7', 0, '', '', '', 42, 0, '0709', 1, 'sk_test_jHf4cEzAYtgcXvgWPCsQAn50', 'pk_test_beat8SWPORb0OVdF2H77A7uG', 'ff2400d9-f3aa-4db5-9dc5-4eee236c6254', 'patriciomelo');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_suppliers`
--

CREATE TABLE `tec_suppliers` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `cf1` varchar(255) NOT NULL,
  `cf2` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tec_suppliers`
--

INSERT INTO `tec_suppliers` (`id`, `name`, `cf1`, `cf2`, `phone`, `email`) VALUES
(1, 'Fornecedor Padr?o', '1', '2', '0123456789', 'fornecedor@pdvparatodos.com.br');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_suspended_items`
--

CREATE TABLE `tec_suspended_items` (
  `id` int(11) NOT NULL,
  `suspend_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` decimal(15,2) NOT NULL,
  `unit_price` decimal(25,2) NOT NULL,
  `net_unit_price` decimal(25,2) NOT NULL,
  `discount` varchar(20) DEFAULT NULL,
  `item_discount` decimal(25,2) DEFAULT NULL,
  `tax` int(20) DEFAULT NULL,
  `item_tax` decimal(25,2) DEFAULT NULL,
  `subtotal` decimal(25,2) NOT NULL,
  `real_unit_price` decimal(25,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_suspended_sales`
--

CREATE TABLE `tec_suspended_sales` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer_name` varchar(55) NOT NULL,
  `total` decimal(25,2) NOT NULL,
  `product_discount` decimal(25,2) DEFAULT NULL,
  `order_discount_id` varchar(20) DEFAULT NULL,
  `order_discount` decimal(25,2) DEFAULT NULL,
  `total_discount` decimal(25,2) DEFAULT NULL,
  `product_tax` decimal(25,2) DEFAULT NULL,
  `order_tax_id` varchar(20) DEFAULT NULL,
  `order_tax` decimal(25,2) DEFAULT NULL,
  `total_tax` decimal(25,2) DEFAULT NULL,
  `grand_total` decimal(25,2) NOT NULL,
  `total_items` int(11) DEFAULT NULL,
  `total_quantity` decimal(15,2) DEFAULT NULL,
  `paid` decimal(25,2) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `hold_ref` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_users`
--

CREATE TABLE `tec_users` (
  `id` int(11) UNSIGNED NOT NULL,
  `last_ip_address` varbinary(45) DEFAULT NULL,
  `ip_address` varbinary(45) DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(40) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `avatar` varchar(55) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `group_id` int(11) UNSIGNED NOT NULL DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tec_users`
--

INSERT INTO `tec_users` (`id`, `last_ip_address`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `avatar`, `gender`, `group_id`) VALUES
(1, 0x3a3a31, 0x3132372e302e302e31, 'admin', '0944ac2da688d44e978d579d44111909ea560c4d', NULL, 'temdetudoprogramas@hotmail.com', NULL, NULL, NULL, NULL, 1435204774, 1454302937, 0, 'Tem de Tudo', 'Programas', 'Tecdiary', '55  75 99189-4547', 'geraldo.png', 'male', 1),
(2, 0x3a3a31, 0x3138372e3131362e3233312e313231, 'vendedor', 'c9c7537a1fe1275298d2e2df08426ee51513bb9f', NULL, 'pontodigitalprogramas@hotmail.com', NULL, NULL, NULL, NULL, 1449891567, 1469631537, 0, 'vendedor', 'pdv', NULL, '999', NULL, 'male', 1),
(4, 0x3139322e3136382e302e33, 0x3a3a31, 'junior', 'beab1fb0adade374792847a85f31d2361091ec6a', NULL, 'junior@webister.com.br', NULL, NULL, NULL, NULL, 1469598165, 1480351055, 1, 'junior', 'nascimento', NULL, '9000000000', NULL, 'male', 1),
(5, 0x3139322e3136382e312e313031, 0x3139322e3136382e312e313034, 'garcom', 'e8d3adee5b1ea100404b182cb184ef151072b0c3', NULL, 'garcom@garcom.com', NULL, NULL, NULL, NULL, 1469749965, 1469750008, 1, 'Garçom', 'Pedro', NULL, '99999999', NULL, 'male', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_user_logins`
--

CREATE TABLE `tec_user_logins` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tec_user_logins`
--

INSERT INTO `tec_user_logins` (`id`, `user_id`, `company_id`, `ip_address`, `login`, `time`) VALUES
(1, 1, NULL, 0x3a3a31, 'geraldopatricio.melo@gmail.com', '2015-11-02 12:33:39'),
(2, 1, NULL, 0x3a3a31, 'geraldopatricio.melo@gmail.com', '2015-11-02 12:53:18'),
(3, 1, NULL, 0x3a3a31, 'geraldopatricio.melo@gmail.com', '2015-11-02 13:15:21'),
(4, 1, NULL, 0x3a3a31, 'geraldopatricio.melo@gmail.com', '2015-11-02 13:15:30'),
(5, 1, NULL, 0x3139312e3230372e3135392e313737, 'geraldopatricio.melo@gmail.com', '2015-11-02 22:20:35'),
(6, 1, NULL, 0x3139312e3230372e3135392e313737, 'geraldopatricio.melo@gmail.com', '2015-11-02 22:30:36'),
(7, 1, NULL, 0x3139312e3230372e3135392e313737, 'geraldopatricio.melo@gmail.com', '2015-11-02 22:31:22'),
(8, 1, NULL, 0x3138392e38392e3235302e313934, 'geraldopatricio.melo@gmail.com', '2015-11-03 12:34:44'),
(9, 1, NULL, 0x3137372e3134352e352e313135, 'geraldopatricio.melo@gmail.com', '2015-11-11 20:38:46'),
(10, 1, NULL, 0x3230302e3232322e32312e313338, 'geraldopatricio.melo@gmail.com', '2015-11-25 10:45:05'),
(11, 1, NULL, 0x3138372e3131362e3233312e313231, 'geraldopatricio.melo@gmail.com', '2015-12-12 03:37:53'),
(12, 2, NULL, 0x3138372e3131342e39302e3834, 'pdv@pdvparatodos.com.br', '2015-12-12 19:59:25'),
(13, 1, NULL, 0x3138392e38392e3235302e313934, 'geraldopatricio.melo@gmail.com', '2016-01-16 13:56:15'),
(14, 1, NULL, 0x3138392e38392e3235302e313934, 'geraldopatricio.melo@gmail.com', '2016-01-21 16:22:16'),
(15, 1, NULL, 0x3138392e38392e3235302e313934, 'geraldopatricio.melo@gmail.com', '2016-01-21 19:06:56'),
(16, 1, NULL, 0x3138392e38392e3235302e313934, 'geraldopatricio.melo@gmail.com', '2016-01-22 14:44:26'),
(17, 1, NULL, 0x3137372e3137322e31362e323232, 'geraldopatricio.melo@gmail.com', '2016-01-22 15:06:16'),
(18, 1, NULL, 0x3138392e38392e3235302e313934, 'geraldopatricio.melo@gmail.com', '2016-01-22 18:29:43'),
(19, 1, NULL, 0x3137372e3137322e31362e323232, 'geraldopatricio.melo@gmail.com', '2016-01-22 22:51:12'),
(20, 1, NULL, 0x3139312e3139352e3235302e3333, 'geraldopatricio.melo@gmail.com', '2016-01-25 23:15:36'),
(21, 1, NULL, 0x3137392e3130302e3133362e3737, 'geraldopatricio.melo@gmail.com', '2016-01-26 00:11:10'),
(22, 1, NULL, 0x3137392e3130302e3133362e3737, 'geraldopatricio.melo@gmail.com', '2016-01-26 00:11:52'),
(23, 1, NULL, 0x3137392e3130302e3133362e3737, 'geraldopatricio.melo@gmail.com', '2016-01-26 00:49:37'),
(24, 1, NULL, 0x3137392e3130302e3133362e3737, 'geraldopatricio.melo@gmail.com', '2016-01-26 00:49:56'),
(25, 1, NULL, 0x3137392e3130302e3133362e3737, 'geraldopatricio.melo@gmail.com', '2016-01-26 00:59:18'),
(26, 1, NULL, 0x3137392e3130302e3133362e3737, 'geraldopatricio.melo@gmail.com', '2016-01-26 01:01:39'),
(27, 1, NULL, 0x3a3a31, 'geraldopatricio.melo@gmail.com', '2016-02-01 04:39:25'),
(28, 2, NULL, 0x3a3a31, 'pontodigitalprogramas@hotmail.com', '2016-02-01 04:58:54'),
(29, 1, NULL, 0x3a3a31, 'temdetudoprogramas@hotmail.com', '2016-02-01 05:02:17'),
(30, 2, NULL, 0x3a3a31, 'pontodigitalprogramas@hotmail.com', '2016-07-27 05:15:25'),
(31, 2, NULL, 0x3a3a31, 'pontodigitalprogramas@hotmail.com', '2016-07-27 05:23:53'),
(32, 2, NULL, 0x3a3a31, 'pontodigitalprogramas@hotmail.com', '2016-07-27 05:40:33'),
(33, 4, NULL, 0x3a3a31, 'junior@webister.com.br', '2016-07-27 05:43:08'),
(34, 2, NULL, 0x3a3a31, 'pontodigitalprogramas@hotmail.com', '2016-07-27 14:58:57'),
(35, 4, NULL, 0x3a3a31, 'junior@webister.com.br', '2016-07-27 15:00:04'),
(36, 4, NULL, 0x3a3a31, 'junior@webister.com.br', '2016-07-28 00:58:07'),
(37, 4, NULL, 0x3139322e3136382e312e313031, 'junior@webister.com.br', '2016-07-28 01:02:30'),
(38, 4, NULL, 0x3139322e3136382e312e313031, 'junior@webister.com.br', '2016-07-28 01:42:26'),
(39, 4, NULL, 0x3139322e3136382e312e313034, 'junior@webister.com.br', '2016-07-28 21:43:20'),
(40, 5, NULL, 0x3139322e3136382e312e313031, 'garcom@garcom.com', '2016-07-28 23:53:28'),
(41, 4, NULL, 0x3139322e3136382e312e313036, 'junior@webister.com.br', '2016-07-29 21:56:16'),
(42, 4, NULL, 0x3139322e3136382e312e313034, 'junior', '2016-08-01 21:33:02'),
(43, 4, NULL, 0x3139322e3136382e312e313034, 'junior', '2016-08-02 02:32:28'),
(44, 4, NULL, 0x3139322e3136382e312e313031, 'junior', '2016-08-02 04:56:08'),
(45, 4, NULL, 0x3139322e3136382e312e313034, 'junior', '2016-08-02 18:13:04'),
(46, 4, NULL, 0x3a3a31, 'junior', '2016-09-23 02:05:08'),
(47, 4, NULL, 0x3a3a31, 'junior', '2016-09-28 23:15:37'),
(48, 4, NULL, 0x3139322e3136382e302e33, 'junior', '2016-09-29 19:38:54'),
(49, 4, NULL, 0x3139322e3136382e34332e323031, 'junior', '2016-09-30 18:11:36'),
(50, 4, NULL, 0x3139322e3136382e34332e323031, 'junior', '2016-09-30 18:15:03'),
(51, 4, NULL, 0x3139322e3136382e34332e323031, 'junior', '2016-09-30 18:24:05'),
(52, 4, NULL, 0x3139322e3136382e312e313037, 'junior', '2016-10-03 18:47:42'),
(53, 4, NULL, 0x3139322e3136382e312e313037, 'junior', '2016-10-04 20:06:10'),
(54, 4, NULL, 0x3139322e3136382e312e313037, 'junior', '2016-10-04 20:23:28'),
(55, 4, NULL, 0x3139322e3136382e312e313037, 'junior', '2016-10-04 20:23:39'),
(56, 4, NULL, 0x3139322e3136382e312e313037, 'junior', '2016-10-04 20:24:23'),
(57, 4, NULL, 0x3139322e3136382e312e313037, 'junior', '2016-10-04 20:25:29'),
(58, 4, NULL, 0x3139322e3136382e312e313037, 'junior', '2016-10-06 02:46:00'),
(59, 4, NULL, 0x3139322e3136382e302e36, 'junior', '2016-10-12 19:34:21'),
(60, 4, NULL, 0x3139322e3136382e302e36, 'junior', '2016-10-12 19:43:06'),
(61, 4, NULL, 0x3139322e3136382e302e36, 'junior', '2016-10-14 17:41:36'),
(62, 4, NULL, 0x3139322e3136382e302e36, 'junior', '2016-10-14 21:46:53'),
(63, 4, NULL, 0x3139322e3136382e302e36, 'junior', '2016-10-15 20:01:04'),
(64, 4, NULL, 0x3139322e3136382e302e37, 'junior', '2016-10-16 22:04:32'),
(65, 4, NULL, 0x3139322e3136382e312e313037, 'junior', '2016-10-17 16:42:21'),
(66, 4, NULL, 0x3139322e3136382e312e313037, 'junior', '2016-10-17 23:42:18'),
(67, 4, NULL, 0x3139322e3136382e312e313037, 'junior', '2016-10-18 10:46:42'),
(68, 4, NULL, 0x3139322e3136382e312e313037, 'junior', '2016-10-19 01:35:34'),
(69, 4, NULL, 0x3139322e3136382e302e34, 'junior', '2016-10-20 18:42:39'),
(70, 4, NULL, 0x3139322e3136382e302e33, 'junior', '2016-10-25 20:08:55'),
(71, 4, NULL, 0x3139322e3136382e312e313034, 'junior', '2016-10-26 18:38:02'),
(72, 4, NULL, 0x3139322e3136382e312e313034, 'junior', '2016-11-01 04:45:48'),
(73, 4, NULL, 0x3139322e3136382e312e313034, 'junior', '2016-11-01 18:56:35'),
(74, 4, NULL, 0x3139322e3136382e302e36, 'junior', '2016-11-24 18:38:27'),
(75, 4, NULL, 0x3139322e3136382e302e33, 'junior', '2016-11-28 16:37:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `atualiza`
--
ALTER TABLE `atualiza`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tec_categories`
--
ALTER TABLE `tec_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tec_combo_items`
--
ALTER TABLE `tec_combo_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tec_customers`
--
ALTER TABLE `tec_customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tec_expenses`
--
ALTER TABLE `tec_expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tec_gift_cards`
--
ALTER TABLE `tec_gift_cards`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `card_no` (`card_no`);

--
-- Indexes for table `tec_groups`
--
ALTER TABLE `tec_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tec_login_attempts`
--
ALTER TABLE `tec_login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tec_mesas`
--
ALTER TABLE `tec_mesas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tec_payments`
--
ALTER TABLE `tec_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tec_pedido_mesa`
--
ALTER TABLE `tec_pedido_mesa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tec_products`
--
ALTER TABLE `tec_products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `tec_purchases`
--
ALTER TABLE `tec_purchases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tec_purchase_items`
--
ALTER TABLE `tec_purchase_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tec_registers`
--
ALTER TABLE `tec_registers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tec_sales`
--
ALTER TABLE `tec_sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tec_sale_items`
--
ALTER TABLE `tec_sale_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tec_sessions`
--
ALTER TABLE `tec_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `tec_settings`
--
ALTER TABLE `tec_settings`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `tec_suppliers`
--
ALTER TABLE `tec_suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tec_suspended_items`
--
ALTER TABLE `tec_suspended_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tec_suspended_sales`
--
ALTER TABLE `tec_suspended_sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tec_users`
--
ALTER TABLE `tec_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `tec_user_logins`
--
ALTER TABLE `tec_user_logins`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `atualiza`
--
ALTER TABLE `atualiza`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tec_categories`
--
ALTER TABLE `tec_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tec_combo_items`
--
ALTER TABLE `tec_combo_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tec_customers`
--
ALTER TABLE `tec_customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tec_expenses`
--
ALTER TABLE `tec_expenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tec_gift_cards`
--
ALTER TABLE `tec_gift_cards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tec_groups`
--
ALTER TABLE `tec_groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tec_login_attempts`
--
ALTER TABLE `tec_login_attempts`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tec_mesas`
--
ALTER TABLE `tec_mesas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `tec_payments`
--
ALTER TABLE `tec_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `tec_pedido_mesa`
--
ALTER TABLE `tec_pedido_mesa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tec_products`
--
ALTER TABLE `tec_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `tec_purchases`
--
ALTER TABLE `tec_purchases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tec_purchase_items`
--
ALTER TABLE `tec_purchase_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tec_registers`
--
ALTER TABLE `tec_registers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tec_sales`
--
ALTER TABLE `tec_sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `tec_sale_items`
--
ALTER TABLE `tec_sale_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `tec_suppliers`
--
ALTER TABLE `tec_suppliers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tec_suspended_items`
--
ALTER TABLE `tec_suspended_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tec_suspended_sales`
--
ALTER TABLE `tec_suspended_sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tec_users`
--
ALTER TABLE `tec_users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tec_user_logins`
--
ALTER TABLE `tec_user_logins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
