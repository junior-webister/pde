<?php 
    include 'cabecalho.php';
    //include 'submenu_delivery_caixa.php';
?>
<style type="text/css">
    body {
      background-color: #e3e3e3;
    }
    .sumir {
        display: none;
    }
</style>
<?php
    $consulta_pdv_aberto = mysql_query("SELECT status FROM caixa01 where id = (select max(id) from caixa01)");
    if (mysql_result($consulta_pdv_aberto,0) == 'Fechado')
    {
        echo '<br><br>
            <div class="ui center aligned grid">
            <div class="ui negative message">
                <i class="close icon"></i>
                <div class="header">
                  Caixa está fechado!
                </div>
                <p>Por favor, faça abertura do caixa para utilizar o Delivery!
              </p>
              <p><a href="pdv.php" class="ui green button">Abrir caixa</a></p></div></div>';
    }
 else {
        
?>
<!--
################ Estilo da DIV Subtotal ##################
-->
<style>
  .subtotal {
    background-color: #eee;
    padding: 40px 15px 40px 0px;
    text-align: right;
    font-size: 28pt;
    border-radius: 3px;
    box-shadow: 2px 2px 6px #ccc;
  }

  .subtotal span {
    text-align: center;
    font-size: 12pt;
    vertical-align: middle;
    margin-right: 20px;

  }

.sumir {
    display: none;
  }
</style>
<!--
################ ATALHOS DO TECLADO ##################
-->
<script>

  document.onkeyup=function(e){

   if(e.which == 107){
          window.location.hash = "#venda";
     return false;
   }

   if(e.which == 37){
          window.location.hash = "#lanches";
          window.location.reload();
     return false;
   }

   if(e.which == 39){
          window.location.hash = "#bebidas";
          window.location.reload();
     return false;
   }

  }

</script>
<div class="ui container">

<div class="ui secondary pointing red menu">
    <a class="item" href="delivery.php" onclick="location.reload()">
        Delivery
      </a>
        <a class="item" href="delivery.php#cadastrar" onclick="location.reload()">
        Cadastrar
      </a>
        <a class="item active" href="pdv_delivery.php">
        Caixa
      </a>
</div>
<div class="ui segment">
<!--
################ LISTAGEM DE PRODUTOS E PEDIDOS ##################
-->
<?php
    $consulta_cliente = mysql_query("SELECT
                                            a.id_cliente
                                            ,b.name
                                    FROM
                                            pedido_delivery a
                                    INNER JOIN
                                            clientes b
                                    ON
                                            a.id_cliente = b.id");
    
    $consulta_motoboy = mysql_query("SELECT
                                        distinct c.nome
                                    FROM
                                        pedido_delivery a
                                    INNER JOIN
                                        clientes b
                                    ON
                                        a.id_cliente = b.id
                                    INNER JOIN
                                        usuarios c
                                    ON
                                        a.id_motoboy = c.id");
?>
<div class="ui two column doubling stackable grid container">
  <div class="column">
    <p>        
        <?php
            if (mysql_num_rows($consulta_cliente) == 0)
            {
                echo "<h3 class='ui center aligned header'>Selecionar Cliente</h3>";
                echo '<br>';
                exit();
            }
            elseif (mysql_num_rows($consulta_motoboy) == 0)
            {   
                $ver_consulta = mysql_fetch_array($consulta_cliente);
                echo "<h3 class='ui center aligned header'>PDV</h3>";                
                echo '<h5 class="ui center aligned header">Cliente: '.$ver_consulta['name'].''
                        . '<br><br>'
                        . 'Motoboy: -'
                        . '</h5>';
            }
            else
            {
                $ver_consulta = mysql_fetch_array($consulta_cliente);
                $ver_motoboy = mysql_result($consulta_motoboy,0);
                echo "<h3 class='ui center aligned header'>PDV</h3>";                
                echo '<h5 class="ui center aligned header">Cliente: '.$ver_consulta['name'].''
                        . '<br><br>'
                        . 'Motoboy: ' . $ver_motoboy
                        . '</h5>';
            }
        ?>        
    </p>
    <?php
      echo "<p>"
            ."<table class='ui bordered table'>"
              ."<thead>"
                ."<th>Qtd</th>"
                ."<th>Produto</th>"
                ."<th>Preço</th>"
                ."<th>Total</th>"
                ."<th>Ação</th>"
              ."</thead>"
      ;
      $query_pdv = mysql_query('
        SELECT
	       a.id,
         b.name as Produto,
         a.quantidade,
         b.cost as Preço,
         a.quantidade*b.cost as Total,
         a.obs
       FROM
	        pedido_delivery a
        INNER JOIN
	         tec_products b
         ON
            a.id_produto = b.id
          ORDER BY a.id');
      while ($pedido=mysql_fetch_array($query_pdv)) {
        echo "<tr>"
              ."<td>".$pedido['quantidade']."x</td>"
              ."<td>".$pedido['Produto']."</td>"
              ."<td>R$ ".$pedido['Preço']."</td>"
              ."<td>R$ ".$pedido['Total']."</td>"
              ."<td>"."<a href='balcaoDAO_delivery.php?id_del=".$pedido['id']."'><i class='trash icon'></i></a>"."</td>"
            ."</tr>"
        ;
        $subtotal+=$pedido['Total'];
      }
      echo "<tr>"
            ."<td colspan='1'><a href='#motoboys' class='ui basic fluid button'>Motoboy</a></td>"
            ."<td colspan='1'><a href='suspender_venda.php?tipo=".$ver_consulta['id_cliente']."&total=".$subtotal."' class='ui basic fluid button'>Aguardar</a></td>"
            ."<td colspan='3' rowspan='2' width='80%'><div class='subtotal'><span>subtotal </span>R$ ".number_format($subtotal, 2,',','.')."</div></td>"
          ."</tr>"
          ."<tr>"
            ."<td colspan='1'><a href='balcaoDAO_delivery.php?truncar=yes' class='ui red fluid button'>Cancelar</a></td>"
            ."<td colspan='1'><a href='#venda' id='botao' class='ui green fluid button'>Finalizar</a></td>"
          ."</tr>"
          ."</table>"
          ."</p>";
      
          
     ?>
  </div>
  <div class="column">
    <p><h3 class='ui center aligned header'>Produtos</h3><br></p>
    <p><div class="ui tabular menu">
      <a href="#lanches" class="item lanches active" onclick="location.reload()">
        Lanches
      </a>
      <a href="#bebidas" class="item bebidas" onclick="location.reload()">
        Bebidas
      </a>
    </div>
    <div class="ui bottom attached segment">
      <p>
        <?php
              echo "<p>"
                    ."<div class='prod2 sumir'>"
                    ."<div class='ui four column doubling stackable grid container'>"
                    ;

                $query = mysql_query('select * from tec_products where category_id = 2');
                while ($produtos=mysql_fetch_array($query)) {
                  echo "<div class='column'>"
                        ."<p>"
                          //."<a href='#popup".$produtos['id']."'><img class='ui tiny bordered rounded image' src='pdv/uploads/".$produtos['image']."'></a>"
                          ."<a href='insere_prod_delivery.php?cliente=".$ver_consulta['id_cliente']."&id=".$produtos['id']."'><img class='ui tiny bordered rounded image' src='images/".$produtos['image']."'></a>"
                        ."</p>"
                        ."<p>"
                          .$produtos['name']
                        ."</p>"
                      ."</div>"
                  ;
                  #include 'popup_caixa.php';
                }
                echo "</div>"
                    ."</div>"
                    ."</p>";
              ?>
              <?php
                    echo "<p>"
                          ."<div class='prod1 sumir'>"
                          ."<div class='ui four column doubling stackable grid container'>"
                          ;

                      $query = mysql_query('select * from tec_products where category_id = 1');
                      while ($produtos=mysql_fetch_array($query)) {
                        echo "<div class='column'>"
                              ."<p>"
                                //."<a href='#popup".$produtos['id']."'><img class='ui tiny bordered rounded image' src='pdv/uploads/".$produtos['image']."'></a>"
                                ."<a href='insere_prod_delivery.php?cliente=".$ver_consulta['id_cliente']."&id=".$produtos['id']."'><img class='ui tiny bordered rounded image' src='images/".$produtos['image']."'></a>"
                              ."</p>"
                              ."<p>"
                                .$produtos['name']
                              ."</p>"
                            ."</div>"
                        ;
                        #include 'popup_caixa.php';
                      }
                      echo "</div>"
                          ."</div>"
                          ."</p>";
                    ?>
          </p>
        </div>
      </p>
    </div>
  </div>
  <script>
    var target = window.location.hash;
    if (target === "#lanches" || target === "") {
      $('.lanches').addClass('active');
      $('.bebidas').removeClass('active');
      $('.geral').removeClass('sumir');
      $('#pdv').addClass('active');
      $('.prod2').removeClass('sumir');
    } else if (target === "#bebidas") {
      $('.lanches').removeClass('active');
      $('.bebidas').addClass('active');
      $('.geral').removeClass('sumir');
      $('#pdv').addClass('active');
      $('.prod1').removeClass('sumir');
    }
    
    var menu = window.location.pathname;
    if (menu === '/pde/pdv_delivery.php') {
        $('#delivery').addClass('active');
        $('#pdv').removeClass('active');
    }
  </script>
<?php
  include 'popup_caixa.php';
  include 'popup_venda_delivery.php';
  include 'popup_motoboy.php';
  include 'rodape.php';
 
?>

</div>
</div>
<?php
}
?>