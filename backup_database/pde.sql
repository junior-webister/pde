-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 08-Fev-2017 às 14:54
-- Versão do servidor: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pde`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `bkp_produtos`
--

CREATE TABLE `bkp_produtos` (
  `id` int(11) NOT NULL DEFAULT '0',
  `code` varchar(50) CHARACTER SET utf8 NOT NULL,
  `name` char(255) CHARACTER SET utf8 NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '1',
  `price` decimal(25,2) NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT 'no_image.png',
  `tax` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `cost` decimal(25,2) DEFAULT NULL,
  `tax_method` tinyint(1) DEFAULT '1',
  `quantity` decimal(15,2) DEFAULT '0.00',
  `barcode_symbology` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT 'code39',
  `type` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT 'standard',
  `details` text CHARACTER SET utf8,
  `alert_quantity` decimal(10,2) DEFAULT '0.00',
  `cozinha` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `bkp_produtos`
--

INSERT INTO `bkp_produtos` (`id`, `code`, `name`, `category_id`, `price`, `image`, `tax`, `cost`, `tax_method`, `quantity`, `barcode_symbology`, `type`, `details`, `alert_quantity`, `cozinha`) VALUES
(1, '0001', 'Hamburquer', 2, '2.00', '99ba81363ddbfe5a92c93023e1fd550a.jpg', '0', '4.00', 0, '53.00', 'code39', 'standard', 'Hamburguer com P?o de Hamburguer, queijo, carne, presunto e salada', '5.00', 1),
(2, '0002', 'Mixto Quente', 2, '1.00', '3ba18844e23b27e8224f8fa6b1752208.jpg', '0', '3.00', 0, '8.00', 'code39', 'standard', '', '5.00', 1),
(3, '0003', 'Cahorro Quente', 2, '2.00', '573bc5101fabefd864960416b1752899.jpg', '0', '3.00', 0, '4.00', 'code39', 'standard', '', '5.00', 1),
(4, '0004', 'Bolo de Chocolate', 2, '2.00', '8ad58758122f3a886e859def53da6a6a.jpg', '0', '3.00', 0, '7.00', 'code39', 'standard', '', '5.00', 0),
(5, '0005', 'Coxinha de Frango', 2, '2.00', 'd3115abf501ce492bdf449f72f185fb1.jpg', '0', '3.00', 0, '9.00', 'code39', 'standard', '', '5.00', 0),
(6, '0006', 'Empada', 2, '2.00', '76fed631b7861010869172aa83d78e0a.jpg', '0', '3.00', 0, '19.00', 'code39', 'standard', '', '5.00', 0),
(7, '0007', 'Monteiro Lopes', 2, '2.00', '3274477f5b7d3ef257c4562c56ef387e.jpg', '0', '3.00', 0, '10.00', 'code39', 'standard', '', '5.00', 0),
(8, '0008', 'Risole de Carne', 2, '2.00', '32a3ac97716a9dc68812aecbaf11840a.jpg', '0', '4.00', 0, '4.00', 'code39', 'standard', '', '5.00', 0),
(9, '0009', 'Coxinha de Caranguejo', 2, '4.00', '8bd5b89b645b1bc2d4d08816b5ad3d0b.jpg', '0', '6.00', 0, '6.00', 'code39', 'standard', '', '5.00', 0),
(10, '0010', 'Coxinha de Camar?o', 2, '4.00', '272825062f261b126f1996ed099b4b87.jpg', '0', '6.00', 0, '7.00', 'code39', 'standard', '', '5.00', 0),
(11, '0011', 'Sonho', 2, '2.00', '1f56837339171226e7e33eb0c5e8eae0.jpg', '0', '3.00', 0, '7.00', 'code39', 'standard', '', '5.00', 0),
(12, '0012', 'Lasanha', 2, '6.00', 'fd1c25461a5fbb0597c68bb78100c6ec.jpg', '0', '9.00', 0, '10.00', 'code39', 'standard', '', '5.00', 0),
(13, '0013', 'Torta de Chocolate', 2, '3.00', '11fcdf61a2d8c2d6b7c3e9c0a6996a54.jpg', '0', '6.00', 0, '10.00', 'code39', 'standard', '', '5.00', 0),
(14, '0014', 'Fanta Laranja Lata', 1, '2.00', 'f0ed23add960528f5da95d8fb2a8a106.jpg', '0', '4.00', 0, '10.00', 'code39', 'standard', '', '5.00', 0),
(15, '0015', 'Coca-Cola Lata', 1, '2.00', 'd1ae8344e2fdfc3fcd80a96bb1f00240.jpg', '0', '4.00', 0, '7.00', 'code39', 'standard', '', '5.00', 0),
(16, '0016', '?gua Mineral', 1, '2.00', '91b3bcff369f45e167c3544bad752912.jpg', '0', '3.00', 0, '9.00', 'code39', 'standard', '', '5.00', 0),
(17, '0017', 'Suco de Laranja', 1, '4.00', 'f4cab501731cb47389a6c1a9a54cf736.jpg', '0', '6.00', 0, '5.00', 'code39', 'standard', '', '5.00', 0),
(18, '01', 'Combo M', 2, '10.99', 'no_image.png', '5', '8.71', 0, '0.00', 'code39', 'combo', '', '0.00', 0),
(19, '02', 'Batata M', 2, '7.99', 'no_image.png', '0', '4.72', 0, '0.00', 'code39', 'standard', '', '0.00', 0),
(20, '03', 'Cobertura Cheddar', 2, '1.00', 'no_image.png', '0', '0.27', 0, '0.00', 'code39', 'standard', '', '0.00', 0),
(21, '012', 'Pizza Calabresa', 3, '24.00', 'no_image.png', '0', '12.00', 1, '10.00', 'code39', 'standard', '', '1.00', 1),
(22, '20', 'Pizza Mussarela', 3, '26.00', 'no_image.png', '0', '13.00', 1, '10.00', 'code39', 'standard', '', '0.00', 1),
(23, '027', '1/2 Pizza Mussarela', 3, '26.00', 'no_image.png', '0', '6.00', 0, '11.00', 'code39', 'standard', '', '0.00', 1),
(24, '01777', '1/2 Calabresa', 3, '21.00', 'no_image.png', '0', '6.00', 0, '9.00', 'code39', 'standard', '', '0.00', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `caixa01`
--

CREATE TABLE `caixa01` (
  `id` int(11) NOT NULL,
  `data_abertura` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `data_fechamento` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `valor_inicial` decimal(10,2) NOT NULL,
  `valor_final` decimal(10,2) NOT NULL,
  `status` varchar(45) CHARACTER SET utf8 NOT NULL,
  `id_usuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `caixa01`
--

INSERT INTO `caixa01` (`id`, `data_abertura`, `data_fechamento`, `valor_inicial`, `valor_final`, `status`, `id_usuario`) VALUES
(1, '0000-00-00 00:00', '2017-01-25 15:11', '0.00', '179.00', 'Fechado', 3),
(2, '2017-01-25 00:38', '2017-01-25 15:11', '450.00', '179.00', 'Fechado', 1),
(3, '2017-01-25 15:10', '2017-01-25 15:11', '100.00', '179.00', 'Fechado', 1),
(4, '2017-01-25 17:01', '2017-01-25 17:02', '120.00', '199.00', 'Fechado', 1),
(5, '2017-01-25 17:14', '2017-01-25 17:14', '320.00', '323.00', 'Fechado', 1),
(6, '2017-01-26 19:12', '2017-01-26 19:16', '250.00', '273.00', 'Fechado', 1),
(7, '2017-01-27 01:02', '2017-01-27 01:05', '250.00', '307.00', 'Fechado', 1),
(8, '2017-01-27 01:12', '2017-01-27 01:13', '700.00', '704.00', 'Fechado', 1),
(9, '2017-01-27 18:15', '2017-01-27 18:15', '100.00', '100.00', 'Fechado', 1),
(10, '2017-01-28 16:04', '2017-01-30 20:07', '120.00', '142.66', 'Fechado', 1),
(11, '2017-01-30 20:16', '2017-01-30 20:17', '100.00', '121.40', 'Fechado', 1),
(12, '2017-01-30 20:18', '2017-02-02 15:22', '140.00', '155.77', 'Fechado', 1),
(13, '2017-02-02 17:10', '2017-02-02 18:32', '100.00', '116.87', 'Fechado', 1),
(14, '2017-02-02 18:32', '2017-02-02 18:33', '120.00', '120.00', 'Fechado', 1),
(15, '2017-02-02 18:33', '2017-02-02 18:34', '120.00', '120.00', 'Fechado', 1),
(16, '2017-02-02 18:34', '2017-02-02 18:43', '150.00', '150.00', 'Fechado', 1),
(17, '2017-02-02 18:43', '2017-02-02 18:43', '500.00', '500.00', 'Fechado', 1),
(18, '2017-02-02 18:45', '2017-02-02 18:45', '200.00', '200.00', 'Fechado', 1),
(19, '2017-02-02 18:46', '2017-02-02 19:04', '240.00', '244.40', 'Fechado', 1),
(20, '2017-02-03 15:04', '2017-02-06 16:53', '800.00', '816.17', 'Fechado', 1),
(21, '2017-02-06 16:53', '2017-02-07 23:01', '300.00', '421.46', 'Fechado', 1),
(22, '2017-02-07 23:01', '2017-02-08 01:42', '120.00', '191.40', 'Fechado', 1),
(23, '2017-02-08 01:42', '2017-02-08 01:48', '120.00', '153.00', 'Fechado', 1),
(24, '2017-02-08 01:48', '2017-02-08 10:25', '120.00', '215.00', 'Fechado', 1),
(25, '2017-02-08 10:29', '2017-02-08 10:34', '120.00', '120.00', 'Fechado', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `caixa02`
--

CREATE TABLE `caixa02` (
  `id` int(11) NOT NULL,
  `data_abertura` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `data_fechamento` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `valor_inicial` decimal(10,2) NOT NULL,
  `valor_final` decimal(10,2) NOT NULL,
  `status` varchar(45) CHARACTER SET utf8 NOT NULL,
  `id_usuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `caixa02`
--

INSERT INTO `caixa02` (`id`, `data_abertura`, `data_fechamento`, `valor_inicial`, `valor_final`, `status`, `id_usuario`) VALUES
(1, '0000-00-00 00:00', '000-00-00 00:00', '0.00', '0.00', 'Fechado', 3),
(2, '2017-01-25 00:49', '000-00-00 00:00', '0.00', '0.00', 'Fechado', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cargo`
--

CREATE TABLE `cargo` (
  `id` int(11) NOT NULL,
  `cargo` varchar(45) NOT NULL,
  `permissao` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `cargo`
--

INSERT INTO `cargo` (`id`, `cargo`, `permissao`) VALUES
(1, 'admin', 1),
(2, 'Gerente', 1),
(3, 'Operadora', 0),
(4, 'Garçom', 0),
(5, 'Motoboy', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `categoria` varchar(45) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `categorias`
--

INSERT INTO `categorias` (`id`, `categoria`) VALUES
(1, 'Esfihas'),
(2, 'Salgados'),
(3, 'Bebidas'),
(4, 'Beirutes'),
(5, 'Pizzas'),
(6, 'Pastéis'),
(7, 'Lanches'),
(8, 'Porções'),
(9, 'Doces'),
(10, 'Sorvetes'),
(11, 'Balas');

-- --------------------------------------------------------

--
-- Estrutura da tabela `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `name` varchar(55) CHARACTER SET utf8 NOT NULL,
  `cf1` varchar(255) CHARACTER SET utf8 NOT NULL,
  `cf2` varchar(255) CHARACTER SET utf8 NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8 NOT NULL,
  `celular` varchar(18) CHARACTER SET utf8 DEFAULT NULL,
  `taxa_de_entrega` int(2) DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `endereco` varchar(125) CHARACTER SET utf8 DEFAULT NULL,
  `bairro` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `cep` varchar(11) CHARACTER SET utf8 DEFAULT NULL,
  `delivery` varchar(15) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `clientes`
--

INSERT INTO `clientes` (`id`, `name`, `cf1`, `cf2`, `phone`, `celular`, `taxa_de_entrega`, `email`, `endereco`, `bairro`, `cep`, `delivery`) VALUES
(1, 'Cliente Padrão', '9999999999', '99999999', '012345678', NULL, NULL, 'cliente@pdvparatodos.com.br', 'Rua Exemplo, 12', NULL, NULL, 'entrega'),
(2, 'Mesa 01', '999999999', '999999999', '999999999', NULL, NULL, 'mesa@mesa.com', 'Avenida Teste, 230', NULL, NULL, 'entrega'),
(3, 'Amy Lee', '1234123123', '12441232', '54432039', NULL, NULL, 'amylee@teste.com', 'Rua dos Rockeiros, 2121', NULL, NULL, 'entrega'),
(4, 'Gerson', '1212121212121', '2122121212', '11998776544', NULL, NULL, 'gerson@teste.com', 'Rua do Vale, 77', NULL, NULL, 'entrega'),
(5, 'Edson', '01101010231', '1312341241', '10101010', NULL, NULL, 'reipele@santos.com', 'Rua Arantis do Nascimento, 10', NULL, NULL, 'entrega'),
(7, 'Ronaldo', '74492342309', '1234994005', '99090909', NULL, NULL, 'r9@nine.com', 'Avenida Fenomeno, 999', NULL, NULL, NULL),
(8, 'Corey Taylor', '12854822894', '456935874', '92134678', NULL, NULL, 'duality@newmetal.com', 'Rua Slipknot, 192', NULL, NULL, 'entrega'),
(11, 'MATEUS', '0', '0', '983468932', '', 4, '', 'RUA  BELISARIO CAMPANHA,', 'CASA VERDE MÉDIA', '02521-000', ''),
(12, 'PRISCILA', '0', '0', '29797598', '29797598', 0, '', 'RUA  HELENA DO SACRAMENTO,807', 'MANDAQUI', '02433-020', ''),
(13, 'PRICILA', '0', '0', '20636265', '', 5, '', 'RUA  HELENA DO SACRAMENTO,807', 'MANDAQUI', '02433-020', ''),
(14, 'CARLA', '0', '0', '22566262', '', 5, '', 'AV.  CELESTINO BOURROUL,225', 'BA.DO LIMÃO', '02710-000', ''),
(15, 'NERLIANE', '0', '0', '965462622', '', 5, '', 'MANDAQUI', 'LIMAO', '02520000', ''),
(16, 'NERLIANE', '0', '0', '965264622', '', 5, '', 'AV MANDAQUI 268', 'LIMAO', '0252000', ''),
(17, 'LARRISA', '0', '0', '979265828', '', 3, '', 'AV.  CELESTINO BOURROUL,AP 222 BLOCO 800', 'BA.DO LIMÃO', '02710-001', ''),
(18, 'ELZA', '0', '0', '38569868', '', 3, '', 'RUA  AMADEU DANIELI FILHO,240 CASA1', 'BA.DO LIMÃO', '02550-030', ''),
(19, 'MARCOS', '0', '0', '38585916', '', 3, '', 'AV.  MANDAQUI,345', 'BA.DO LIMÃO', '02550-000', ''),
(20, 'SIMONE', '0', '0', '38585772', '38585772', 3, '', 'RUA  SEBASTIÃO MORAIS,215 CASA 8', 'BA.DO LIMÃO', '02551-100', ''),
(21, 'SIMONE', '0', '0', '998681934', '', 5, '', 'RUA  ANTONIO JOÃO,281 CASA 2', 'SÍTIO DO MORRO', '02553-050', ''),
(22, 'JANE', '0', '0', '38514266', '', 5, '', 'RUA  GORDIANO GAUDENCIO ROSSI,107', 'V.PENTEADO', '02864-100', ''),
(23, 'GRAZIELE', '0', '0', '38576136', '', 5, '', 'TRAV.  DOIS RIACHOS,NUMERO CASA 5', 'JD.PEREIRA LEITE', '02712-030', ''),
(24, 'AILTON FREITAS', '0', '0', '962120586', '', 5, '', 'RUA  PROGRESSISTA,', 'V.ESTER', '02451-050', ''),
(25, 'TONINHO', '0', '0', '38571839', '', 5, '', 'RUA  CAROLINA SOARES,132', 'V.DIVA', '02554-000', ''),
(26, 'CARLOS', '0', '0', '39361267', '', 5, '', 'RUA  JANUARIO CAPUA, CASA 108', 'CJ.RES.NO.PACAEMBU VILA SIQUEI', '02722-130', ''),
(27, 'RITA DE KASSIA', '0', '0', '26383270', '', 5, '', 'AV.  NOSSA SRA.DO O,423 AP 42 BLO 2', 'BA.DO LIMÃO', '02715-000', ''),
(28, 'MARIA VANDA', '0', '0', '38574112', '', 3, '', 'AV.  CASA VERDE,3446', 'V.ESTER', '02520-300', ''),
(29, 'VANDOM', '0', '0', '965319087', '', 3, '', 'RUA  MATEUS MASCARENHAS,309', 'JD.PEREIRA LEITE', '02712-000', ''),
(30, 'BRUNO', '0', '0', '39613598', '39613598', 5, '', 'TR.  DOURADINA, CASA 4   QUADARA DO CORINTISAS', 'JD.PEREIRA LEITE', '02712-040', ''),
(31, 'CONSUELO', '0', '0', '38594622', '', 5, '', 'RUA  ESTELA BORGES MORATO,160 APT 4 TORRE 3', 'V.SIQUEIRA', '02722-000', ''),
(32, 'MARIA DOS REIS', '0', '0', '38577596', '', 3, '', 'RUA  JOAQUIM DE FREITAS,151', 'SÍTIO DO MORRO', '02551-070', ''),
(33, 'KARINA', '0', '0', '949153622', '', 3, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,163 BLO H4 A', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(34, 'MARCIA', '0', '0', '25748604', '', 5, '', 'RUA A BENTO,138LFERES ESQUINA CORNIGO ARAUJO MARCO', 'BA.DO LIMÃO', '02550-090', ''),
(35, 'GABRIELA', '0', '0', '988462964', '', 5, '', 'TR.  SEBASTIÃO AMORIM,44 PRIMEIRA CASA PORTAO VERD', 'BA.DO LIMÃO', '02550-040', ''),
(36, 'BRENDO', '0', '0', '38589367', '38589367', 3, '', 'RUA SAO DINIZ N 173 CASA', 'LIMAO', '', ''),
(37, 'MARCIA', '0', '0', '39363464', '39363464', 4, '', 'RUA  LEONEL RABELLO,24ROCK DE MORAIS', 'CJ.RES.NO.PACAEMBU PERTO ROXQ', '02722-060', ''),
(38, 'MARCELO', '0', '0', '32896925', '32896925', 3, '', 'AV.  MANDAQUI,122  APT114 BLO 3', 'BA.DO LIMÃO', '02550-000', ''),
(39, 'MARISA', '0', '0', '39315525', '', 5, '', 'AV.  SEBASTIÃO HENRIQUES,455 TRAVESSA DO O', 'V.SIQUEIRA', '02723-050', ''),
(40, 'IRACEMA', '0', '0', '22287940', '22287940', 5, '', 'RUA  MARIA ELISA SIQUEIRA,629 NO SINAL DA PRACA', 'JD.S.LUÍS', '02558-000', ''),
(41, 'SUELI', '0', '0', '39363694', '39363694', 5, '', 'RUA  ALVARO SILVA,35 PERTO DO MERCADO CARISMA', 'V.SIQUEIRA', '02723-020', ''),
(42, 'CRISTIANE', '0', '0', '25947640', '25947640', 5, '', 'RUA  ROSA MARCHETTI,130 ATRAZ DO ESTADAO', 'BA.DO LIMÃO', '02550-110', ''),
(43, 'LUIZ', '0', '0', '998976364', '', 5, '', 'RUA  MOURA,81 AP51 PERTO RUA LAREIRA', 'V.CRISTO REI', '02558-090', ''),
(44, 'JOSELI', '0', '0', '39313560', '', 5, '', 'RUA  ROQUE DE MORAIS,345 PREDIO PARAGUAI 31', 'BA.DO LIMÃO', '02721-031', ''),
(45, 'EDUARDO', '0', '0', '38588743', '', 5, '', 'RUA  CAROLINA SOARES,138 BAIRRO DO LIMAO', 'V.DIVA', '02554-000', ''),
(46, 'BRUNA', '0', '0', '988302210', '988302210', 3, '', 'AV.  CELESTINO BOURROUL,684 AP72 TORRE1', 'BA.DO LIMÃO', '02710-001', ''),
(47, 'PEDRO', '0', '0', '39363374', '', 5, '', 'RUA  VICENTE FERREIRA LEITE,761', 'V.SIQUEIRA', '02723-000', ''),
(48, 'ANA PAULA', '0', '0', '33840296', '', 5, '', 'RUA  JACOFER,161 AP 92 TORRE 2', 'JD.PEREIRA LEITE', '02712-070', ''),
(49, 'PAULO', '0', '0', '38578796', '', 5, '', 'RUA   XAVIER DE BRITO,BRIGADEIRO 57 PALELA COM ENG', 'SÍTIO DO MORRO', '02551-000', ''),
(50, 'ELISANGELA', '0', '0', '39551927', '39551927', 5, '', 'TRAV.  DIVINO DE SÃO LOURENCO,05', 'JD.PEREIRA LEITE', '02712-010', ''),
(51, 'DEUZIMAR', '0', '0', '946939755', '', 5, '', 'AV.  MANDAQUI,268 CASA 4', 'BA.DO LIMÃO', '02550-000', ''),
(52, 'ARLIMDO', '0', '0', '984736955', '', 5, '', 'RUA  FARIA PEREIRA, AP2 BROCO7', 'STA.TEREZINHA', '02430-004', ''),
(53, 'LEANDRO', '0', '0', '38587572', '', 3, '', 'RUA  XAVIER DE BRITO,229', 'SÍTIO DO MORRO', '02551-000', ''),
(54, 'DEBORA', '0', '0', '38583934', '', 5, '', 'RUA  JACIRA ROCHA,159', 'CASA VERDE MÉDIA', '02521-010', ''),
(55, 'GISLENI', '0', '0', '951503142', '951503142', 4, '', 'JD.DAS GRACAS/JD.PEREIRA LEITE,AP11 BLOCO 9 SAMP', '', '02701-000', ''),
(56, 'ELIZAGELA', '0', '0', '69875088', '', 2, '', 'RUAC  JOAQUIM DE FREITAS,ORONEL151', 'SÍTIO DO MORRO', '02551-070', ''),
(57, 'MICHELE', '0', '0', '966761575', '', 5, '', 'RUA  CARLOS PORTO CARREIRO,315 BLO 7 APT 33', 'JD.DAS GRAÇAS', '02713-030', ''),
(58, 'FLAVIA', '0', '0', '988060645', '988060645', 3, '', 'RUA  HORACIO MOURA,306', 'BA.DO LIMÃO', '02710-070', ''),
(59, 'SILVANA', '0', '0', '34597105', '', 5, '', 'RUA  JOSE DE AGUIAR CASTRO,94', 'V.CARBONE', '02752-140', ''),
(60, 'FELIPE', '0', '0', '39651077', '39651077', 3, '', 'RUA  JOAQUIM MENDES,162', 'PQ.SOUZA ARANHA', '02518-100', ''),
(61, 'HENRIQUE', '0', '0', '29257548', '', 5, '', 'TRAV.  VITORIO GNAN,1A ESQUINA RUA SAMARITA', 'PQ.SOUZA ARANHA', '02518-060', ''),
(62, 'HERIKI', '0', '0', '38659018', '', 4, '', 'RUA  JOSEFINA GONCALVES,30', 'BA.DO LIMÃO', '02550-010', ''),
(63, 'MARCELA', '0', '0', '38584804', '', 5, '', 'AV.  CASA VERDE,3094', 'V.ESTER', '02520-300', ''),
(64, 'LETICIA', '0', '0', '34562349', '34562349', 5, '', 'RUA  JACOFER,161 AT0131 TORRE 2', 'JD.PEREIRA LEITE', '02712-070', ''),
(65, 'REGIANE', '0', '0', '39311074', '', 5, '', 'JD.DAS GRACAS/JD.PEREIRA LEITE,APT 22', 'LIMAO', '02701-000', ''),
(66, 'RENATO', '0', '0', '39668328', '', 4, '', 'RUA  DARIO RIBEIRO,466', 'CASA VERDE ALTA', '02559-000', ''),
(67, 'KARE', '0', '0', '991348228', '', 5, '', 'TRAV.  DIVISA NOVA,01', 'JD.PEREIRA LEITE', '02712-020', ''),
(68, 'MARIA', '0', '0', '981583981', '', 5, '', 'AV.  ANTONIO MUNHOZ BONILHA,1349 AP82 TORRE2', 'V.CAROLINA', '02725-000', ''),
(69, 'JULIANA', '0', '0', '998981596', '998981596', 5, '', 'RUA  ESTELA BORGES MORATO,160', 'V.SIQUEIRA', '02722-000', ''),
(70, 'EDUARDO', '0', '0', '954577530', '', 5, '', 'AV.CASA VERDE 3431 PORTAO AMARELO', '', '', ''),
(71, 'LUCIA', '0', '0', '991878025', '', 5, '', 'RUA  XAVIER DE BRITO,485 CASA2', 'SÍTIO DO MORRO', '02551-000', ''),
(72, 'ANA', '0', '0', '38572267', '', 5, '', 'RUA  MIGUEL MAGALHÃES,33 DETRAZ DO ITAU', 'BA.DO LIMÃO', '02710-090', ''),
(73, 'ARLETE', '0', '0', '997586445', '', 5, '', 'RUA  JOAQUIM DE FREITAS,137 AP 3', 'SÍTIO DO MORRO', '02551-070', ''),
(74, 'MARA', '0', '0', '38587662', '', 5, '', 'RUA  GAMA CERQUEIRA,31 B NO ALTO AO LADO DO 61', 'V.DIVA', '02560-070', ''),
(75, 'RAFAEL', '0', '0', '943552007', '', 3, '', 'AV.PROFSSOR CELESTINO BURRAO753 AO LADO DA IGREJA', '', '', ''),
(76, 'CRISTIANE', '0', '0', '977340904', '', 5, '', 'AV.  SEBASTIÃO HENRIQUES,815', 'V.SIQUEIRA', '02723-050', ''),
(77, 'ROBERTA', '0', '0', '23867097', '', 5, '', 'RUA  JOSE SORIANO DE SOUSA,292 AT93 NORTE', 'CASA VERDE ALTA', '02555-050', ''),
(78, 'CRISTIANO', '0', '0', '943750422', '', 5, '', 'RUA DOUTOR MORAIS DANTAS 255 PRACA CANAAM', '', '', ''),
(79, 'DIEGO', '0', '0', '968064302', '', 5, '', 'RUA  RIBEIRÃO DAS ALMAS,193 PERTO INAJAR DE SOUSA', 'V.PALMEIRA', '02728-100', ''),
(80, 'BARBARA', '0', '0', '39515344', '', 5, '', 'RUA  PEREIRAS,61TRAVESSA DA AVENIDA CASA VERDE', 'JD.DAS LARANJEIRAS', '02517-080', ''),
(81, 'PAULA', '0', '0', '967653001', '', 5, '', 'RUA  SILVESTRE GONCALVES,31DO LADO DA IGREJA BATIS', 'V.PALMEIRA', '02752-090', ''),
(82, 'FATIMA', '0', '0', '971957086', '', 5, '', 'RUA  PAULO ADORNO,79', 'SÍTIO DO MORRO', '02551-130', ''),
(83, 'CLAUDINEIA', '0', '0', '87331456', '', 3, '', 'AV.  MANDAQUI,129PERTO DO CARTORIO', 'BA.DO LIMÃO', '02550-000', ''),
(84, 'EDINEIA', '0', '0', '39659018', '', 3, '', 'RUA  JOSEFINA GONCALVES,30PERTO DO BANCO', 'BA.DO LIMÃO', '02550-010', ''),
(85, 'EMILIA', '0', '0', '39653813', '39653813', 3, '', 'RUA  MIGUEL MAGALHÃES,39', 'BA.DO LIMÃO', '02710-090', ''),
(86, 'TEO', '0', '0', '39316869', '39316869', 5, '', 'RUA  IPUPIARA,73PERTO DA RUA ITAU', 'V.CARBONE', '02750-070', ''),
(87, 'COSTABILE', '0', '0', '39362492', '', 5, '', 'RUA  CLEMENTE FERREIRA,245', 'V.SIQUEIRA', '02723-040', ''),
(88, 'CELIO.', '0', '0', '38569799', '', 5, '', 'RUA  GALILEIA,886PERTO DO COLEGIO MAFEI VITOR', 'PQ.PERUCHE', '02530-000', ''),
(89, 'RODRIGO', '0', '0', '23095974', '', 5, '', 'RUA  ALBANO FRAGOSO,122PROXIMO A PARENTAS DELEGACI', 'V.STA.MARIA', '02561-010', ''),
(90, 'ERMERSSON', '0', '0', '34677925', '34677925', 4, '', 'AV.  NOSSA SRA.DO O,423 AP86 BLOCO 2', 'BA.DO LIMÃO', '02715-000', ''),
(91, 'VERA', '0', '0', '38582401', '', 5, '', 'RUA  FRANCISCO ARTASSIO,93 CASA 2 RUA SEM SAIDA', 'CASA VERDE BAIXA', '02517-060', ''),
(92, 'AUGUSTO', '0', '0', '930012614', '', 3, '', 'AV.CELESTINO BURROL 753 PERTO DO ESTACIONAMENTO', '', '', ''),
(93, 'GABRIEL DE PAULA', '0', '0', '947081890', '', 4, '', 'RUA  ROQUE DE MORAIS,340 APT 21 EDIFICIO RONDONIA', 'BA.DO LIMÃO', '02721-031', ''),
(94, 'CAROLINE', '0', '0', '38580995', '38580995', 4, '', 'RUA  SÃO DINIZ,173', 'V.DIVA', '02556-060', ''),
(95, 'ELIANA', '0', '0', '38580997', '', 5, '', 'RUA  RELIQUIA,365 CASA 2', 'PQ.SOUZA ARANHA', '02517-000', ''),
(96, 'ANDERSOM', '0', '0', '974917700', '', 5, '', 'AV.  CLAVASIO ALVES DA SILVA,215 APT205', 'V.SIQUEIRA', '02722-030', ''),
(97, 'CRISTINA', '0', '0', '38571280', '', 5, '', 'RUA  ANDREA DEL CASTAGNO,119', 'V.STA.MARIA', '02562-010', ''),
(98, 'KATIA', '0', '0', '39363582', '', 5, '', 'RUA  JOSE MACHADO RIBEIRO,97', 'CJ.RES.NO.PACAEMBU', '02722-170', ''),
(99, 'RICARDO', '0', '0', '38577949', '', 3, '', 'RUA  JOAQUIM DE FREITAS,187', 'SÍTIO DO MORRO', '02551-070', ''),
(100, 'EDUARDO', '0', '0', '34242406', '', 5, '', 'PR.  JOÃO LESSA,32 RUA SEM SAIDA', 'V.BRITO', '02726-170', ''),
(101, 'ELANIA', '0', '0', '24783398', '', 3, '', 'RUA  VITORIO PRIMON,161PERTO TRAVESSA CELESTINO BU', 'BA.DO LIMÃO', '02550-050', ''),
(102, 'CLAUDIA', '0', '0', '23069354', '23069354', 3, '', 'AV.  CELESTINO BOURROUL,684 AP22 TORRE3', 'BA.DO LIMÃO', '02710-001', ''),
(103, 'AUGUSTO', '0', '0', '23661956', '', 5, '', 'RUA  JACOFER,161 AP112 TORRE2', 'JD.PEREIRA LEITE', '02712-070', ''),
(104, 'SIMONE', '0', '0', '25896413', '', 5, '', 'RUA  ESTELA BORGES MORATO,618 BLOCO 2 AP21', 'V.SIQUEIRA', '02722-000', ''),
(105, 'VALERIA', '0', '0', '26597242', '', 5, '', 'RUA  ALESSANDRO ALLORI,476 A', 'JD.PRIMAVERA', '02754-090', ''),
(106, 'CRISTINA', '0', '0', '29240693', '29240693', 3, '', 'AV.  CELESTINO BOURROUL,684 TORRE 1 AP63', 'BA.DO LIMÃO', '02710-001', ''),
(107, 'FABIANA', '0', '0', '38957706', '38957706', 5, '', 'RUA  JACOFER,105 ATP91 TORRE1', 'JD.PEREIRA LEITE', '02712-070', ''),
(108, 'MARCELO', '0', '0', '39851844', '', 5, '', 'RUA  AUGUSTA BERTA,31ATRAZ DA QUARENTA', 'V.CAROLINA', '02750-010', ''),
(109, 'CELSON', '0', '0', '38560204', '', 5, '', 'RUA  SALVADOR PADILHA GIMENES,58', 'V.STA.MARIA', '02562-130', ''),
(110, 'PAULO', '0', '0', '39656678', '', 5, '', 'RUA  SÃO ROMUALDO,254', 'V.BARBOSA', '02557-060', ''),
(111, 'ANA PAULA', '0', '0', '39613152', '', 5, '', 'RUA P JOÃO LEOCADIO,168ROF', 'SÍTIO DO MORRO', '02556-180', ''),
(112, 'ANDREA', '0', '0', '974874813', '', 5, '', 'RUA  JOAQUIM DE FREITAS,137 APT 3', 'SÍTIO DO MORRO', '02551-070', ''),
(113, 'TANIA', '0', '0', '39516268', '39516268', 4, '', 'RUA  SAMARITA,898BROCO 2AP12', 'PQ.SOUZA ARANHA', '02518-080', ''),
(114, 'CLEISI', '0', '0', '39367984', '39367984', 4, '', 'RUA  ANTONIO AUGUSTO FLORINDO,12 TRAVESSA ROQUE DT', 'CJ.RES.NO.PACAEMBU', '02722-120', ''),
(115, 'SANDRO', '0', '0', '989304825', '989304825', 5, '', 'RUA  EULALIO DA COSTA CARVALHO,570', 'JD.PEREIRA LEITE', '02712-050', ''),
(116, 'NILDA', '0', '0', '38560374', '', 5, '', 'RUA  JOÃO SERRANO,68', 'SÍTIO DO MORRO', '02551-060', ''),
(117, 'ELZIO', '0', '0', '39363196', '', 5, '', 'RUA  FRANCISCO ANTONIO FURLAN,128', 'CJ.RES.NO.PACAEMBU', '02722-190', ''),
(118, 'GUILHERME', '0', '0', '954753705', '', 4, '', 'AV.  MANDAQUI,122 APT203 BL0 3', 'BA.DO LIMÃO', '02550-000', ''),
(119, 'RICARDO', '0', '0', '39511135', '', 5, '', 'RUA  RELIQUIA,365 CASA3', 'PQ.SOUZA ARANHA', '02517-000', ''),
(120, 'OTAVIANO', '0', '0', '982320686', '', 4, '', 'RUA  JOSE SORIANO DE SOUSA,292 APT84 BLO SUL', 'CASA VERDE ALTA', '02555-050', ''),
(121, 'ERIVALDO', '0', '0', '23710361', '', 5, '', 'RUA  ANTONIO ESTIGARRIBIA,199', 'V.DIVA', '02556-030', ''),
(122, 'IEDA', '0', '0', '32583139', '', 5, '', 'RUA  CARAMBEI,46.', 'V.STA.MARIA', '02561-080', ''),
(123, 'ALINE', '0', '0', '953356544', '', 5, '', 'RUA  ESTELA BORGES MORATO,160 ATO95', 'V.SIQUEIRA TORRE 3', '02722-000', ''),
(124, 'ALFREDO', '0', '0', '38576246', '', 5, '', 'RUA  DIOGO DE OLIVEIRA,65', 'SÍTIO DO MORRO', '02553-040', ''),
(125, 'RAIMUNDO', '0', '0', '36650646', '', 3, '', 'BERNANDO BIRONA 408', '', '', ''),
(126, 'VANESSA', '0', '0', '39325767', '', 4, '', 'RUA  SILVANO DE ALMEIDA,253', 'V.SIQUEIRA', '02723-110', ''),
(127, 'GLORIA', '0', '0', '967025359', '967025359', 4, '', 'BRIGADEIRO XAVIER DE BRITO 497', 'SITIO DO MORRO', '', ''),
(128, 'CHILEN', '0', '0', '39354147', '', 5, '', 'RUA  PASCOAL SOUSA,37', 'V.STA.MARIA', '02754-070', ''),
(129, 'FLAVIA', '0', '0', '28920374', '', 5, '', 'RUA  SALOMÃO MAIEROVITCH,55', 'V.STA.MARIA', '02562-120', ''),
(130, 'MONICA', '0', '0', '952536998', '', 5, '', 'AV.  SEBASTIÃO HENRIQUES,632CASA2', 'V.SIQUEIRA', '02723-050', ''),
(131, 'LUCIANA', '0', '0', '39664720', '', 5, '', 'RUA  ROQUE DE MORAIS,340EDIFICIO RIOGUANDO D SU AP', 'BA.DO LIMÃO', '02721-030', ''),
(132, 'GISELE', '0', '0', '39511082', '', 5, '', 'RUA  ROCHA LIMA,20C', 'SÍTIO DO MORRO', '02554-010', ''),
(133, 'APARECIDA', '0', '0', '957319930', '957319930', 5, '', 'RUA  SAMARITA,127', 'PQ.SOUZA ARANHA', '02518-080', ''),
(134, 'ROMULO PEREIRA', '0', '0', '39664414', '39664414', 5, '', 'RUA  CAROLINA SOARES,518', 'V.DIVA', '02554-000', ''),
(135, 'ROMULO', '0', '0', '39664417', '', 5, '', 'RUA  CAROLINA SOARES,518', 'V.DIVA', '02554-000', ''),
(136, 'MAARILUCE', '0', '0', '37919703', '37919703', 5, '', 'RUA  VITORIO PRIMON,105', 'BA.DO LIMÃO', '02550-050', ''),
(137, 'ANGELICA', '0', '0', '39368786', '', 5, '', 'RUA  EDUARDO AZEVEDO,11', 'JD.PRIMAVERA', '02756-080', ''),
(138, 'ALAIR', '0', '0', '22315598', '', 5, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,199 BLOF4', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(139, 'AMANDA', '0', '0', '23737964', '', 5, '', 'AV.  CLAVASIO ALVES DA SILVA,215AP203', 'V.SIQUEIRA', '02722-030', ''),
(140, 'MARCGADIDA', '0', '0', '39363485', '', 3, '', 'RUA  LEONEL RABELLO,67', 'CJ.RES.NO.PACAEMBU', '02722-060', ''),
(141, 'BERENICI', '0', '0', '25038373', '25038373', 5, '', 'RUA  JOÃO BATISTA LIPORONI,46', 'CJ.RES.NO.PACAEMBU', '02722-050', ''),
(142, 'ANDREA', '0', '0', '994376173', '', 5, '', 'RUA  ROQUE DE MORAIS,231', 'BA.DO LIMÃO', '02721-031', ''),
(143, 'VERA LUCIA', '0', '0', '32582401', '', 5, '', 'RUA  FRANCISCO ARTASSIO,93', 'CASA VERDE BAIXA', '02517-060', ''),
(144, 'NICOLE', '0', '0', '963064198', '', 5, '', 'RUA  GLAUCO VELASQUEZ,148', 'SÍTIO DO MORRO', '02553-000', ''),
(145, 'GEISOM', '0', '0', '39515364', '', 5, '', 'TR.  ANAJANE,61', 'BA.DO LIMÃO', '02550-100', ''),
(146, 'CICERA', '0', '0', '39552076', '39552076', 5, '', 'RUA  ARAUJO MARCONDES,217', 'BA.DO LIMÃO', '02550-080', ''),
(147, 'VILMA', '0', '0', '39650473', '', 5, '', 'RUA  MARIO DE AZEVEDO,101A AO LADO CASAS BAHIA', 'JD.PEREIRA LEITE', '02710-020', ''),
(148, 'MONIQUE', '0', '0', '962687741', '', 5, '', 'RUA  MATEUS MASCARENHAS,305', 'JD.PEREIRA LEITE', '02712-000', ''),
(149, 'FATIMA', '0', '0', '39659413', '', 4, '', 'RUA  DIOGO DE OLIVEIRA,563', 'SÍTIO DO MORRO', '02553-040', ''),
(150, 'MARIA DO CARMO', '0', '0', '38580728', '38580728', 3, '', 'RUA  JACIRA ROCHA,302', 'CASA VERDE MÉDIA', '02521-010', ''),
(151, 'FERNANDO', '0', '0', '39654023', '', 5, '', 'AV.  CAETANO ALVARES,1663', 'CASA VERDE', '02546-000', ''),
(152, 'ZILDA', '0', '0', '39655990', '39655990', 4, '', 'AUGUSTINHO MIRANDA 7C', 'SANTA MARIA', '', ''),
(153, 'LIDIA', '0', '0', '39656362', '39656362', 5, '', 'RUA  VITORIO PRIMON,141', 'BA.DO LIMÃO', '02550-050', ''),
(154, 'RAFAEL', '0', '0', '39367608', '39367608', 3, '', 'AV.  CELESTINO BOURROUL,684 APT93 TOORE 1', 'BA.DO LIMÃO', '02710-000', ''),
(155, 'CECILIA', '0', '0', '39511586', '', 5, '', 'AV.  NOSSA SRA.DO O,423 APT68 TOORE 1', 'BA.DO LIMÃO', '02715-000', ''),
(156, 'SORAIA', '0', '0', '987957112', '', 5, '', 'RUA  SALVADOR PADILHA GIMENES,28', 'V.STA.MARIA', '02562-130', ''),
(157, 'MARCIO', '0', '0', '26795458', '', 5, '', 'RUA  ARAUJO MARCONDES,129 CASA4', 'BA.DO LIMÃO', '02550-080', ''),
(158, 'ANDRESA', '0', '0', '968214792', '', 5, '', 'RUA  ALBANO FRAGOSO,191 CASA2', 'V.STA.MARIA', '02561-010', ''),
(159, 'FABIO', '0', '0', '34650646', '', 3, '', 'BERNADO WRONA408', '', '', ''),
(160, 'ANSELMO', '0', '0', '22392745', '', 5, '', 'RUA  SEBASTIÃO MORAIS,77', 'BA.DO LIMÃO', '02551-100', ''),
(161, 'DIEGO', '0', '0', '962670894', '962670894', 2, '', 'CELESTINO BOUUROL 753', '', '', ''),
(162, 'IVONE', '0', '0', '968080004', '', 5, '', 'RUA  ALONSO BERRUGUETE,389', 'JD.PRIMAVERA', '02755-110', ''),
(163, 'ELIAS', '0', '0', '26287859', '', 5, '', 'RUA  MARIA JULIA,50', 'JD.DAS LARANJEIRAS', '02521-050', ''),
(164, 'ADRIANO', '0', '0', '39650658', '', 5, '', 'RUA  CAROLINA SOARES,526', 'V.DIVA', '02554-000', ''),
(165, 'PAULA', '0', '0', '39513831', '', 5, '', 'RUA  SAMPAIO CORREA,30', 'BA.DO LIMÃO', '02710-080', ''),
(166, 'ANDREA', '0', '0', '23670349', '', 5, '', 'AV.  NOSSA SRA.DO O,423APT43B', 'BA.DO LIMÃO', '02715-000', ''),
(167, 'TAMIRES', '0', '0', '39518834', '39518834', 4, '', 'RUA  JERONIMO DOS SANTOS,31', 'V.DIVA', '02556-090', ''),
(168, 'MARIA', '0', '0', '985393613', '', 5, '', 'TR.  FRANCISCO ADRIANI,85', 'V.SANTISTA', '02560-180', ''),
(169, 'ANTONIETA', '0', '0', '33311329', '', 5, '', 'RUA  SÃO ROMUALDO,114', 'V.BARBOSA', '02557-060', ''),
(170, 'ANDRE', '0', '0', '959457218', '959457218', 4, '', 'ROQUI DI MORAS72', '', '', ''),
(171, 'NATALIA', '0', '0', '23609354', '', 2, '', 'AV.  CELESTINO BOURROUL,684AP 22 T3', 'BA.DO LIMÃO', '02710-001', ''),
(172, 'ALRELIO', '0', '0', '28380634', '', 5, '', 'RUA  SALVADOR PADILHA GIMENES,40', 'V.STA.MARIA', '02562-130', ''),
(173, 'GISLENE', '0', '0', '39511179', '', 4, '', 'AV.  MANDAQUI,122APL13 BLO2', 'BA.DO LIMÃO', '02550-000', ''),
(174, 'SIMONE', '0', '0', '35622130', '', 5, '', 'RUA  LAVINIO SALLES ARCURI,281CASA6', 'CASA VERDE ALTA', '02564-000', ''),
(175, 'RODRIGO', '0', '0', '39362811', '', 5, '', 'RUA  PEDRO MARTINS,60 ATRAS POSTO DE SAUDE', 'V.SIQUEIRA', '02723-060', ''),
(176, 'JAQUELINE', '0', '0', '23852853', '', 5, '', 'RUA  JACOFER 105 APT173 TORRE2', 'JD.PEREIRA LEITE', '02712070', ''),
(177, 'FABIO', '0', '0', '38262539', '38262539', 5, '', 'RUA  JACOFER,140 APT102 TORRE2', 'JD.PEREIRA LEITE', '02712-070', ''),
(178, 'SERGIO', '0', '0', '39693455', '39693455', 4, '', 'RUA  JACOFER,161 APT232 TERE 2', 'JD.PEREIRA LEITE', '02712-070', ''),
(179, 'VANDA', '0', '0', '39363747', '39363747', 5, '', 'RUA  ROQUE DE MORAIS,340 APT13 ED PERNAMBUCO', 'BA.DO LIMÃO', '02721-030', ''),
(180, 'JANE', '0', '0', '27728206', '', 5, '', 'RUA  JACIRA ROCHA,620', 'CASA VERDE MÉDIA', '02521-010', ''),
(181, 'BEATRIZ', '0', '0', '25745754', '25745754', 5, '', 'RUA  VITORIO PRIMON,105', 'BA.DO LIMÃO', '02550-050', ''),
(182, 'GISLENE', '0', '0', '980442823', '', 5, '', 'DIOGO DE OLIVEIRA 412', 'LIMAO', '02553040', ''),
(183, 'DAMIEL', '0', '0', '39669991', '', 5, '', 'RUA  ROCHA LIMA,499', 'SÍTIO DO MORRO', '02554-010', ''),
(184, 'LUCINE', '0', '0', '947377912', '', 5, '', 'RUA  ROCHA LIMA,392', 'SÍTIO DO MORRO', '02554-010', ''),
(185, 'LUIZ MIRAMDA', '0', '0', '39667683', '', 5, '', 'RUA  ROQUE DE MORAIS,118', 'BA.DO LIMÃO', '02721-030', ''),
(186, 'FELIPE', '0', '0', '393172', '', 5, '', 'RUA  FRANCISCO TEIXEIRA,1330 CASA2', 'FREGUESIA DO Ó', '02726-161', ''),
(187, 'IEDA', '0', '0', '38583739', '', 5, '', 'RUA  CARAMBEI,46', 'V.STA.MARIA', '02561-080', ''),
(188, 'CAREM', '0', '0', '23093793', '23093793', 3, '', 'AV.  CELESTINO BOURROUL,684 AP61 BL01', 'BA.DO LIMÃO', '02710-001', ''),
(189, 'RAFAEL', '0', '0', '43552007', '', 3, '', 'CELESTINO BOURROL 753', '', '', ''),
(190, 'FABIANA', '0', '0', '962613309', '962613309', 5, '', 'RUA  FRANCO MOREIRA,46', 'V.DIVA', '02556-020', ''),
(191, 'EDSON', '0', '0', '975044022', '975044022', 4, '', 'AV.  CELESTINO BOURROUL,684 BL3 APT4 TORRE3', 'BA.DO LIMÃO', '02710-001', ''),
(192, 'TANIA', '0', '0', '39314799', '', 5, '', 'RUA E JOSE PASTORE,60NGENHEIRO', 'JD.DAS GRAÇAS', '02714-050', ''),
(193, 'VANESSA', '0', '0', '960445702', '', 5, '', 'RUA  EUCLIDES MACHADO,176BL3 APTC', 'JD.DAS GRAÇAS', '02713-000', ''),
(194, 'MONICA', '0', '0', '39515268', '', 4, '', 'AV.  MANDAQUI,275 APT32', 'BA.DO LIMÃO', '02550-000', ''),
(195, 'CRISTINA', '0', '0', '23068831', '23068831', 5, '', 'AV.  NOSSA SRA.DO O,423AT57 BL2', 'BA.DO LIMÃO', '02715-000', ''),
(196, 'LARA', '0', '0', '421036581', '', 5, '', 'RUA  RELIQUIA,25', 'PQ.SOUZA ARANHA', '02517-000', ''),
(197, 'DAVI', '0', '0', '34478312', '', 5, '', 'RUA  DIMAS DOS SANTOS BRUNO,62', 'V.CARBONE', '02752-110', ''),
(198, 'ANDRESA', '0', '0', '976433318', '', 5, '', 'RUA  ESTELA BORGES MORATO,160 BRO2 AP 145', 'V.SIQUEIRA', '02722-000', ''),
(199, 'MIRIANE', '0', '0', '946008321', '', 5, '', 'DR BERNANDINDO GOMES 156.CASA2', 'VILA CAMPISTA', '02560000', ''),
(200, 'JUSARA', '0', '0', '26099050', '', 5, '', 'RUA  ROQUE DE MORAIS,345 ED CHILE AP11', 'BA.DO LIMÃO', '02721-031', ''),
(201, 'ANTONIO', '0', '0', '38571682', '38571682', 3, '', 'RUA  AMADEU DANIELI FILHO,276', 'BA.DO LIMÃO', '02550-030', ''),
(202, 'INES', '0', '0', '39512825', '', 4, '', 'TRAVESSA BARRA DO ROCHA 14', 'LIMAO', '02527010', ''),
(203, 'IGOR', '0', '0', '38570800', '38570800', 3, '', 'RUA  MATEUS MASCARENHAS,309', 'JD.PEREIRA LEITE', '02712-000', ''),
(204, 'IRENE', '0', '0', '39363194', '', 5, '', 'AV.  SEBASTIÃO HENRIQUES,773', 'V.SIQUEIRA', '02723-050', ''),
(205, 'FELIPE', '0', '0', '981315466', '981315466', 4, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,222AP35C', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(206, 'LUIS', '0', '0', '39554702', '39554702', 5, '', 'RUA  ANTONIO ESTIGARRIBIA,76', 'V.DIVA', '02556-030', ''),
(207, 'TAMI', '0', '0', '945415353', '945415353', 4, '', 'RUA  ESTELA BORGES MORATO,160 TR4 AP104', 'V.SIQUEIRA', '02722-000', ''),
(208, 'NERLIANE', '0', '0', '32578504', '', 0, '', '0271519', '', '', ''),
(209, 'LARA', '0', '0', '38578504', '', 3, '', 'AV.  CELESTINO BOURROUL,859', 'BA.DO LIMÃO', '02710-001', ''),
(210, 'ANDREA', '0', '0', '965920116', '', 5, '', 'TRAV.  DIVINO DE SÃO LOURENCO,09', 'JD.PEREIRA LEITE', '02712-010', ''),
(211, 'PATRICIA', '0', '0', '39652804', '39652804', 5, '', 'RUA  CASSIO DE BARROS,84', 'PQ.PERUCHE', '02545-020', ''),
(212, 'HELENA', '0', '0', '39513069', '', 5, '', 'RUA  JACOFER,105 APTO71 TORRE2', 'JD.PEREIRA LEITE', '02712-070', ''),
(213, 'TANIA', '0', '0', '38516268', '', 5, '', 'RUA  SAMARITA,898BL2 APT12', 'PQ.SOUZA ARANHA', '02518-080', ''),
(214, 'MARTA', '0', '0', '39362050', '39362050', 5, '', 'RUA  CLEMENTE FERREIRA,259', 'V.SIQUEIRA', '02723-040', ''),
(215, 'G', '0', '0', '25790480', '', 3, '', 'RUA 135 AV MANDAQUI', '', '', ''),
(216, 'DALVA', '0', '0', '39610955', '39610955', 3, '', 'AV.  MANDAQUI,286', 'BA.DO LIMÃO', '02550-000', ''),
(217, 'FABIANA', '0', '0', '958111941', '', 5, '', 'DEPUTADO EMILIO CARLOS 550', 'LIMAO', '', ''),
(218, 'MARCOS', '0', '0', '987228824', '', 5, '', 'PAULO VIDIGAL VICENTE DE AZEVEDO  222 APT25 BLO D', 'LIMAO', '', ''),
(219, 'LARA', '0', '0', '61245798', '', 0, '', 'AV.  MANDAQUI,', '', '', ''),
(220, 'EDSON', '0', '0', '439688000', '', 5, '', 'MARIA ELISA SIQUEIRA 405', 'VILA PRADO', '', ''),
(221, 'RAMON', '0', '0', '968289772', '', 5, '', 'RUA  XAVIER DE BRITO,533', 'SÍTIO DO MORRO', '02551-000', ''),
(222, 'NILZA', '0', '0', '38587170', '', 4, '', 'RUA  MARIA SETUBAL,', 'CASA VERDE MÉDIA', '02521-020', ''),
(223, 'RAMON', '0', '0', '987222879', '987222879', 4, '', 'RUA  MARIA ROBERTA,CASA 20', 'V.DIVA', '02556-160', ''),
(224, 'RAMON', '0', '0', '87222879', '87222879', 5, '', 'RUA  MARIA ROBERTA,20', 'V.DIVA', '02556-160', ''),
(225, 'VERA', '0', '0', '38581574', '', 5, '', 'RUA  ARAUJO MARCONDES,221', 'BA.DO LIMÃO', '02550-080', ''),
(226, 'EDMUNDO', '0', '0', '986472213', '986472213', 3, '', 'AV.  CELESTINO BOURROUL,684AP125 TORRE3', 'BA.DO LIMÃO', '02710-001', ''),
(227, 'SRIRLEI', '0', '0', '25379369', '', 5, '', 'RUA  RAFAEL DIAS,05', 'V.PALMEIRA', '02726-110', ''),
(228, 'RODRIGO', '0', '0', '38568518', '38568518', 5, '', 'ENGENHEIRO CAETANO ALVES 201 POSTO DE GASOLINA', '', '', ''),
(229, 'GEOVANA', '0', '0', '36244802', '36244802', 5, '', 'FEREIRA DE ALMEIDA 18', 'JARDIM DAS LARANJEIRAS', '', ''),
(230, 'DOROTI', '0', '0', '39550576', '', 5, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,91BLJ3 APT2S', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(231, 'FERNANDO', '0', '0', '97470455', '97470455', 5, '', 'PAULO VIDIGAL DE AZEVEDO 235 BLO D4 APT 32', '', '02721-000', ''),
(232, 'LAOU', '0', '0', '961179596', '', 3, '', 'AV MANDAQUI 129', '', '', ''),
(233, 'GILIANE', '0', '0', '43242513', '43242513', 5, '', 'CARLOS ORCAR RERO 271 APT N24 BLO 4', 'LIMAO', '', ''),
(234, 'KATIA DANTE', '0', '0', '38624618', '38624618', 5, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,271 APTP2 BL', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(235, 'CLEITOM', '0', '0', '984535562', '984535562', 3, '', 'AV.  NOSSA SRA.DO O,423APT118BL2', 'BA.DO LIMÃO', '02715-000', ''),
(236, 'BRUNO NEVES', '0', '0', '957240412', '', 5, '', 'RUA  EULALIO DA COSTA CARVALHO,530', 'JD.PEREIRA LEITE', '02712-050', ''),
(237, 'MARCIA', '0', '0', '39363466', '', 5, '', 'RUA  LEONEL RABELLO,24', 'CJ.RES.NO.PACAEMBU', '02722-060', ''),
(238, 'VANESSA', '0', '0', '39510057', '', 5, '', 'RUA  SÃO ROMUALDO,182', 'V.BARBOSA', '02557-060', ''),
(239, 'ERNESTO', '0', '0', '23699733', '', 5, '', 'RUA  LEONOR BARBOSA RODRIGUES,116', 'V.DIVA', '02556-040', ''),
(240, 'ROBERTO', '0', '0', '22564406', '22564406', 5, '', 'RUA B XAVIER DE BRITO,49RIGADEIRO', 'SÍTIO DO MORRO', '02551-000', ''),
(241, 'PEDREO', '0', '0', '23395429', '23395429', 5, '', 'RUA  JOÃO ROBERTO THUT,49', 'V.CARBONE', '02751-010', ''),
(242, 'ELISET', '0', '0', '977208304', '', 5, '', 'RUA  MACIEIRAS,104 CASA 4', 'JD.DAS LARANJEIRAS', '02521-090', ''),
(243, 'TELMA', '0', '0', '38582382', '38582382', 5, '', 'RUA  DOMINGOS TORRES,203', 'JD.DAS LARANJEIRAS', '02521-100', ''),
(244, 'KASSIA', '0', '0', '39321452', '', 5, '', 'RUA  ROQUE DE MORAIS,340BLO3APT14 EDIFICIO PARA', 'BA.DO LIMÃO', '02721-030', ''),
(245, 'ROSANGELA', '0', '0', '39327072', '', 4, '', 'JD.DAS GRACAS/JD.PEREIRA LEITE, BROCO 7 AP 41', '', '02701-000', ''),
(246, 'JESSICA', '0', '0', '39552174', '39552174', 5, '', 'RUA  MACIEIRAS,236 CASA 2', 'JD.DAS LARANJEIRAS', '02521-090', ''),
(247, 'JONATA', '0', '0', '26595215', '', 3, '', 'AV.  MANDAQUI,122APT11 BLO4', 'BA.DO LIMÃO', '02550-000', ''),
(248, 'PAULA', '0', '0', '975107371', '', 3, '', 'RUA  JOAQUIM DE FREITAS,253', 'SÍTIO DO MORRO', '02551-070', ''),
(249, 'MARIA', '0', '0', '39613415', '', 5, '', 'AV.  JOSE DE BRITO DE FREITAS,405', 'CASA VERDE ALTA', '02552-000', ''),
(250, 'EDILMA', '0', '0', '39328790', '', 5, '', 'OTAVIANO ALVES LIMA 2400BLO6 AP 54', 'LIMAO', '', ''),
(251, 'DINEIA', '0', '0', '35677377', '', 5, '', 'PR.  ANTONIO FRANCO VELASCO,90', 'SÍTIO DO MORRO', '02554-020', ''),
(252, 'MANUELI', '0', '0', '985393313', '', 5, '', 'GLEITI FRASISCO 85', '', '', ''),
(253, 'AMAURI', '0', '0', '39319275', '', 5, '', 'CARLOS PORTO CARREIRO 315 BLO7  APT32', '', '', ''),
(254, 'NEUZA', '0', '0', '39615253', '', 3, '', 'CELESTINO 753', '', '', ''),
(255, 'ALINE', '0', '0', '23371611', '', 4, '', 'AV.  EMILIO CARLOS,130 AP 06', 'V.BARBOSA', '02720-000', ''),
(256, 'ANDERSOM', '0', '0', '985656846', '', 4, '', 'RUA  JACOFER,105 APT124 TORRE 2', 'JD.PEREIRA LEITE', '02712-070', ''),
(257, 'CAROLINA', '0', '0', '35715887', '', 5, '', 'TRAV.  DIVISA NOVA,06', 'JD.PEREIRA LEITE', '02712-020', ''),
(258, 'DIEGO', '0', '0', '991084447', '', 5, '', 'DOM LOURENÇO LUMINE 147 ATRAS DO DARIO RIBEIRO', 'LIMAO', '', ''),
(259, 'GERALDO', '0', '0', '966140463', '', 5, '', 'RUA  DARIO RIBEIRO,', 'CASA VERDE ALTA', '02559-000', ''),
(260, 'RODRIGO', '0', '0', '29360329', '', 5, '', 'AV.  CAETANO ALVARES,651', 'CASA VERDE', '02546-000', ''),
(261, 'FELIPE', '0', '0', '947092275', '', 4, '', 'DR MORAIS DANTAS 174 APT33', 'LIMAO', '', ''),
(262, 'LARA', '0', '0', '385785047', '', 0, '', 'CELESNO  BOURROL', 'LIMAO', '', ''),
(263, 'JAQUELINE', '0', '0', '38583699', '', 5, '', 'RUA  SAMPAIO CORREA,520 BLO3 APT6B 1 ANDAR', 'BA.DO LIMÃO', '02710-080', ''),
(264, 'RAFAL', '0', '0', '21212122', '', 4, '', 'NOSA D O 382', '', '', ''),
(265, 'DANIEL', '0', '0', '39832155', '', 5, '', 'RUA  PIRACANJUBA,242', 'V.CAROLINA', '02750-040', ''),
(266, 'RAL', '0', '0', '21212158', '', 5, '', 'NOSA CEORA D O 382', '', '', ''),
(267, 'BEATRIZ', '0', '0', '38567873', '', 5, '', 'LEONOR BARBOSA RODRIGUES 139 TRAVESSA CAROLINA', '', '', ''),
(268, 'GABRIELA', '0', '0', '959492601', '', 5, '', 'TR.  SEBASTIÃO AMORIM,44', 'BA.DO LIMÃO', '02550-040', ''),
(269, 'LUCIANA', '0', '0', '38577763', '', 3, '', 'CELESTINO 685', '', '', ''),
(270, 'CELIA', '0', '0', '39657869', '', 5, '', 'CESAR VERNARAMOS 1425', 'LIMAO', '02563001', ''),
(271, 'LEONARDO.', '0', '0', '38569759', '38569759', 3, '', 'RUA  HORACIO MOURA,312', 'BA.DO LIMÃO', '02710-070', ''),
(272, 'RICARDO', '0', '0', '38589859', '', 3, '', 'AV.  MANDAQUI,275 APT64', 'BA.DO LIMÃO', '02550-000', ''),
(273, 'ROSANA', '0', '0', '39691561', '39691561', 3, '', 'AV.  MANDAQUI,122 AP191 BL2', 'BA.DO LIMÃO', '02550-000', ''),
(274, 'AILTON', '0', '0', '21212152', '21212152', 5, '', 'NOSSA SENHORA DO O..382', '', '', ''),
(275, 'BRUNA', '0', '0', '988026453', '', 5, '', 'RUA  SÃO DINIZ,213', 'V.DIVA', '02556-060', ''),
(276, 'MICHELE', '0', '0', '43715310', '43715310', 5, '', 'AV.  NOSSA SRA.DO O,423APT51 BLO1', 'BA.DO LIMÃO', '02715-000', ''),
(277, 'GILMAR', '0', '0', '39654092', '', 5, '', 'RUA  SANTO ANTONIO DA PLATINA,165', 'V.CRISTO REI', '02558-080', ''),
(278, 'RICARDO', '0', '0', '986508966', '', 4, '', 'AV.  MANDAQUI, 122 AP 51 BL 3', 'BA.DO LIMÃO', '02550-000', ''),
(279, 'ZE ROBERTO', '0', '0', '38587940', '', 5, '', 'RUA  CAROLINA SOARES,979 AP 194', 'V.DIVA', '02554-000', ''),
(280, 'CRISTIANE', '0', '0', '39362987', '', 5, '', 'RUA  CATANDUVAS DO SUL,220', 'JD.PRIMAVERA', '02755-090', ''),
(281, 'LARISSA', '0', '0', '22561988', '', 5, '', 'ZE BENTO PICA 305', 'A 14', '', ''),
(282, 'CAROL', '0', '0', '958365662', '', 5, '', 'RUA  LEITE DE BARROS,151', 'V.PRADO', '02558-040', ''),
(283, 'ANDRESSA', '0', '0', '97763318', '', 5, '', 'RUA  ESTELA BORGES MORATO,160 BLO2 APT145', 'V.SIQUEIRA', '02722-000', ''),
(284, 'JEOVANA', '0', '0', '35428502', '', 5, '', 'RUA  REIMS,577 AP 123', 'PQ.SOUZA ARANHA', '02517-010', ''),
(285, 'ISRAEL', '0', '0', '3931153', '', 5, '', 'RUA  BACAXA,73', 'V.CARBONE', '02750-060', ''),
(286, 'MAIARA', '0', '0', '985161109', '', 5, '', 'ANTONIO MUNHOS BONULHA 1449', 'LIMAO', '', ''),
(287, 'MARCOS', '0', '0', '938057160', '', 5, '', 'BERNADOS BRUNA 353', '', '', ''),
(288, 'TAUANE', '0', '0', '951213035', '', 5, '', 'PR.  JOÃO LESSA,32', 'V.BRITO', '02726-170', ''),
(289, 'DIEGO', '0', '0', '89522916', '', 5, '', 'DON LORESO LUMINI 147', '', '', ''),
(290, 'AMAURI ADRIANA', '0', '0', '39515059', '', 3, '', 'AV.  CELESTINO BOURROUL,684 BLO1 APT24', 'BA.DO LIMÃO', '02710-000', ''),
(291, 'LUCAS', '0', '0', '987749233', '987749233', 3, '', 'RUA CONEGO ARAUJO MARCONDES,129 CASA 2', 'BA.DO LIMÃO', '02550-080', ''),
(292, 'TIAGO', '0', '0', '988537579', '', 5, '', 'VENANCIO REZENDE 173', 'CASA VERDE', '', ''),
(293, 'FRANCISCA', '0', '0', '769772201', '', 5, '', 'TR.  LUIS SA,143 CASA2', 'V.SANTISTA', '02560-170', ''),
(294, 'LARISSA', '0', '0', '947844725', '', 5, '', 'RUA  JACOFER,105 APT214 TORRE1', 'JD.PEREIRA LEITE', '02712-070', ''),
(295, 'GUSTAVO', '0', '0', '39510712', '', 5, '', 'SEBASTIAO D MORAIS 113', '', '', ''),
(296, 'ROGERIO', '0', '0', '38568367', '', 5, '', 'VALDEMA MARTIS 651', '', '', ''),
(297, 'GERLIANE.', '0', '0', '38588646', '', 5, '', 'RUA  ANTONIO JOÃO,280', 'SÍTIO DO MORRO', '02553-050', ''),
(298, 'VADSOM', '0', '0', '22314103', '', 5, '', 'JACIRA ROCHA 225', 'CASA VERDE', '', ''),
(299, 'GLEDON', '0', '0', '39363379', '', 5, '', 'AV.  ANTONIO MUNHOZ BONILHA,1009', 'V.CAROLINA', '02725-000', ''),
(300, 'JOSEILSON', '0', '0', '987339251', '987339251', 3, '', 'RUA  SAMPAIO CORREA, 4', 'BA.DO LIMÃO', '02710-080', ''),
(301, 'ELIZABETH', '0', '0', '38580753', '38580753', 4, '', 'RUA  ALCIDES ALVES,155', 'V.BANDEIRANTES', '02555-030', ''),
(302, 'MICHELE', '0', '0', '973492922', '', 3, '', 'AV.  NOSSA SRA.DO O, 64B', 'BA.DO LIMÃO', '02715-000', ''),
(303, 'JUCELI', '0', '0', '29356090', '', 3, '', 'AV.  MANDAQUI,275 AP 44', 'BA.DO LIMÃO', '02550-000', ''),
(304, 'SHEILAQ', '0', '0', '29858051', '', 5, '', 'RUA  JACOFER,140TORRE1 104APT', 'JD.PEREIRA LEITE', '02712-070', ''),
(305, 'GABRIELA', '0', '0', '36612034', '', 3, '', 'AV.  CELESTINO BOURROUL,684 AP 42 TORRE 2', 'BA.DO LIMÃO', '02710-001', ''),
(306, 'ROSA', '0', '0', '946345859', '', 3, '', 'RUA SAMARITA 981', 'LIMAO', '', ''),
(307, 'MURILO', '0', '0', '953587179', '', 5, '', 'ROCK DE MORAIS 430', '', '', ''),
(308, 'MIRIANE', '0', '0', '976458844', '', 5, '', 'RUA  BERNARDINO GOMES,156', 'V.SANTISTA', '02560-000', ''),
(309, 'VINICIUS', '0', '0', '38589611', '', 5, '', 'RUA  AMADEU DANIELI FILHO,82A', 'BA.DO LIMÃO', '02550-030', ''),
(310, 'MARIA AMELIA', '0', '0', '39657256', '', 5, '', 'TRAV.  DIVISA NOVA,', 'JD.PEREIRA LEITE', '02712-020', ''),
(311, 'MARCOS', '0', '0', '962333979', '962333979', 3, '', 'AV.  SEBASTIÃO HENRIQUES,448 AP1TORRE3', 'V.SIQUEIRA', '02723-050', ''),
(312, 'ANITA', '0', '0', '38588778', '38588778', 5, '', 'DAS MARCIERRAS 236 PERTO PERTO BOI E MIL', 'CASA VERDE', '', ''),
(313, 'NEUXIR', '0', '0', '996208168', '', 3, '', 'RUA  ELVIRA BARBOSA,173 CASA3', 'V.BARBOSA', '02557-050', ''),
(314, 'DOROTI', '0', '0', '38572702', '38572702', 5, '', 'RUA  ELIAS GANNAM,483', 'V.BANDEIRANTES', '02552-040', ''),
(315, 'FRANSICO', '0', '0', '39654709', '', 5, '', 'RUA  BELISARIO CAMPANHA,554', 'CASA VERDE MÉDIA', '02521-000', ''),
(316, 'MARLI', '0', '0', '39318431', '', 5, '', 'RUA  ANTONIO AUGUSTO FLORINDO,61', 'CJ.RES.NO.PACAEMBU', '02722-120', ''),
(317, 'TACIANA', '0', '0', '969690663', '969690663', 3, '', 'RUA  ODY MANOEL TEIXEIRA MUNHOZ,18 FONDOS', 'JD.DAS GRAÇAS', '02714-040', ''),
(318, 'LUIS', '0', '0', '39615609', '', 5, '', 'RUA  ANTONIO JOÃO,873', 'SÍTIO DO MORRO', '02553-050', ''),
(319, 'DM', '0', '0', '967553612', '', 5, '', 'RUA  HELENA DO SACRAMENTO,294 FUNDOS', 'MANDAQUI', '02433-020', ''),
(320, 'LUCIANA', '0', '0', '39322936', '', 5, '', 'RUA  EMA BORTOLATO BULGARI,27', 'JD.DAS GRAÇAS', '02713-050', ''),
(321, 'EMERSOM', '0', '0', '964496085', '', 3, '', 'CELESTINO BOURROL684 APT104 TORRE2', '', '', ''),
(322, 'AILTON', '0', '0', '21216882', '', 3, '', 'AV NOSSA SENHORA DO O , 382', '', '', ''),
(323, 'LUZIA', '0', '0', '38569885', '', 5, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,91BCI3 T1', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(324, 'DAIANE', '0', '0', '948884964', '', 5, '', 'RUA  MARIA ROBERTA,141', 'V.DIVA', '02556-160', ''),
(325, 'LICIANA', '0', '0', '23875831', '23875831', 3, '', 'AV.  CELESTINO BOURROUL,684 AP 163 TR 1', 'BA.DO LIMÃO', '02710-001', ''),
(326, 'ISRAEL', '0', '0', '39311537', '39311537', 4, '', 'RUA  BACAXA,73', 'V.CARBONE', '02750-060', ''),
(327, 'DAFINE', '0', '0', '22380303', '', 5, '', 'RUA  MADALENA DE MADUREIRA,166 AP 2', 'SÍTIO DO MORRO', '02551-040', ''),
(328, 'ADRIANA', '0', '0', '38581876', '', 3, '', 'AV.  CELESTINO BOURROUL,890 11B', 'BA.DO LIMÃO', '02710-001', ''),
(329, 'GLEIDI', '0', '0', '39314003', '39314003', 5, '', 'RUA  FRANCISCO RODRIGUES NUNES,PORTAO 34 CASA13', 'JD.DAS GRAÇAS', '02712-090', ''),
(330, 'ANDRESA', '0', '0', '953712556', '', 5, '', 'RUA  ATENAGORAS,213', 'V.BARBOSA', '02557-000', ''),
(331, 'CALOS', '0', '0', '987032049', '', 4, '', 'RUA  NICOLINO STOFFA,157', 'BA.DO LIMÃO', '02550-020', ''),
(332, 'SEGIO', '0', '0', '968179748', '', 3, '', 'CELESTINO B ROU 651', '', '', ''),
(333, 'MAGALI', '0', '0', '39669913', '', 5, '', 'AV.  NOSSA SRA.DO O,423 AP2 BLO1', 'BA.DO LIMÃO', '02715-000', ''),
(334, 'EDIR', '0', '0', '39319455', '', 5, '', 'RUA  SILVANO DE ALMEIDA,228', 'V.SIQUEIRA', '02723-110', ''),
(335, 'JOAO', '0', '0', '38955526', '38955526', 4, '', 'RUA  ROCHA LIMA,511', 'SÍTIO DO MORRO', '02554-010', ''),
(336, 'CHUCA', '0', '0', '23674333', '', 1, '', '890', '', '', ''),
(337, 'RODRIGO', '0', '0', '39317013', '', 5, '', 'AV.  SEBASTIÃO HENRIQUES,300 AP 51', 'V.SIQUEIRA', '02723-050', ''),
(338, 'SILVIA', '0', '0', '39321958', '', 5, '', 'RUA  VICENTE FERREIRA LEITE,361 AP 11 BLOCO 411', 'V.SIQUEIRA', '02723-000', ''),
(339, 'MIGUEL', '0', '0', '39317484', '', 5, '', 'RUA  SILVANO DE ALMEIDA,587', 'V.SIQUEIRA', '02723-110', ''),
(340, 'SUEMIA', '0', '0', '984495928', '', 3, '', 'RUA  HORACIO MOURA,154 AP 1', 'BA.DO LIMÃO', '02710-070', ''),
(341, 'ADRIANO', '0', '0', '959959858', '959959858', 5, '', 'TRAVESSA BALTAZAR EXAVES 33', 'V.BARBOSA', '02557060', ''),
(342, 'DANILO', '0', '0', '23640080', '', 3, '', 'AV.  MANDAQUI,275APT92', 'BA.DO LIMÃO', '02550-000', ''),
(343, 'RAFAEL', '0', '0', '39550022', '39550022', 3, '', 'AV.  MANDAQUI,189 APT52 TORRE B.', 'BA.DO LIMÃO', '02550-000', ''),
(344, 'ANTONIO', '0', '0', '997082553', '', 5, '', 'RUA  ANTONIO JOÃO,621 CASA7', 'SÍTIO DO MORRO', '02553-050', ''),
(345, 'LUDMILA', '0', '0', '32947154', '', 6, '', 'RUA  QUIRINOPOLIS,74', 'IMIRIM', '02471-200', ''),
(346, 'CRISTIANE', '0', '0', '39362957', '', 5, '', 'RUA  CATANDUVAS DO SUL,220', 'JD.PRIMAVERA', '02755-090', ''),
(347, 'ANA BRITO', '0', '0', '23398279', '', 5, '', 'RUA  BENTO PICKEL,858', 'CASA VERDE ALTA', '02544-000', ''),
(348, 'JANE', '0', '0', '26090792', '', 3, '', 'RUA  ARAUJO MARCONDES,63 FUNDOS', 'BA.DO LIMÃO', '02550-080', ''),
(349, 'RICARDO', '0', '0', '952358317', '', 3, '', 'RUA  SEBASTIÃO MORAIS,203', 'BA.DO LIMÃO', '02551-100', ''),
(350, 'BRUNO', '0', '0', '38584566', '', 5, '', 'TR.  DOURADINA,4', 'JD.PEREIRA LEITE', '02712-040', ''),
(351, 'ALEXANDRES', '0', '0', '38567426', '', 3, '', 'RUA  JOSEFINA GONCALVES,56', 'BA.DO LIMÃO', '02550-010', ''),
(352, 'CLAUDIA', '0', '0', '39366729', '', 5, '', 'AV.  CLAVASIO ALVES DA SILVA,511 APT42 BLOB', 'V.SIQUEIRA', '02722-030', ''),
(353, 'JERUZA', '0', '0', '45086113', '45086113', 3, '', 'AV.  MANDAQUI,106', 'BA.DO LIMÃO', '02550-000', ''),
(354, 'LEONADO', '0', '0', '22083774', '22083774', 3, '', 'RUA  VITORIO PRIMON,73', 'BA.DO LIMÃO', '02550-050', ''),
(355, 'TEREZA', '0', '0', '962582216', '', 2, '', 'CE 685', 'CELESTINO', '', ''),
(356, 'BEATRIZ', '0', '0', '39451892', '39451892', 3, '', 'AV.  CELESTINO BOURROUL,684 TORRE2 APT 94', 'BA.DO LIMÃO', '02710-001', ''),
(357, 'RODRIGO', '0', '0', '29630329', '', 5, '', 'AV.  CAETANO ALVARES,651', 'CASA VERDE', '02546-000', ''),
(358, 'RENAN', '0', '0', '26595422', '', 5, '', 'PAULO VIDIGAL 91 BROCO I 1 AP 2', '', '', ''),
(359, 'ANA PAUILA', '0', '0', '980548559', '', 5, '', 'RUA  EMA BORTOLATO BULGARI,28', 'JD.DAS GRAÇAS', '02713-050', ''),
(360, 'YNEIS', '0', '0', '39328850', '', 5, '', 'RUA  BENIGNO,80', 'JD.PRIMAVERA', '02754-040', ''),
(361, 'GEVANI', '0', '0', '998415079', '', 3, '', 'RUA  MATEUS MASCARENHAS,363', 'JD.PEREIRA LEITE', '02712-000', ''),
(362, 'CHILEM', '0', '0', '38572151', '', 4, '', 'RUA  MADALENA DE MADUREIRA,369 CASA 2', 'SÍTIO DO MORRO', '02551-040', ''),
(363, 'EDUARDO', '0', '0', '982304166', '', 3, '', 'RUA  HORACIO MOURA,288', 'BA.DO LIMÃO', '02710-070', ''),
(364, 'CESAR', '0', '0', '988698880', '', 5, '', 'RUA  CAROLINA SOARES,493', 'V.DIVA', '02554-000', ''),
(365, 'WEDSOM', '0', '0', '952387330', '952387330', 5, '', 'RUA  FRANCISCO RODRIGUES NUNES,34 CASA 13', 'JD.DAS GRAÇAS', '02712-090', ''),
(366, 'AUGUSTO', '0', '0', '986859300', '', 5, '', 'RUA  ROQUE DE MORAIS,345 APT32 BLO CUBA..', 'BA.DO LIMÃO', '02721-031', ''),
(367, 'MIRIAN', '0', '0', '999442014', '', 4, '', 'RUA  VENANCIO DE RESENDE,160 AP 9', 'CASA VERDE MÉDIA', '02552-020', ''),
(368, 'REINALDO', '0', '0', '985631439', '', 5, '', 'RUA  FREDERICO PENTEADO JR.,305', 'JD.DAS LARANJEIRAS', '02517-100', ''),
(369, 'FABIO', '0', '0', '25793829', '', 4, '', 'RUA  SAMARITA,191', 'PQ.SOUZA ARANHA', '02518-080', ''),
(370, 'GE', '0', '0', '940668383', '940668383', 3, '', 'AV.  MANDAQUI,135 CASA 3', 'BA.DO LIMÃO', '02550-000', ''),
(371, 'JACSOM', '0', '0', '45086181', '', 5, '', 'RUA  JACOFER,140AP153 TORRE1', 'JD.PEREIRA LEITE', '02712-070', ''),
(372, 'CRISTIANE', '0', '0', '39362987', '', 5, '', 'RUA  CATANDUVAS DO SUL,220', 'JD.PRIMAVERA', '02755-090', ''),
(373, 'DANILO', '0', '0', '39513382', '39513382', 3, '', 'CELESTINO BOURROL684 TORRE1 APT82', '', '', ''),
(374, 'FRANCISCO', '0', '0', '39915072', '', 5, '', 'RUA  JACOFER, 161 APT212 TORE2', 'JD.PEREIRA LEITE', '02712-070', ''),
(375, 'MARIA', '0', '0', '54644024', '', 3, '', 'SANPAIO COREIA', '', '', ''),
(376, 'ELISA', '0', '0', '954859315', '', 5, '', 'DAS MARCIEIRAS 125  CASAS6', 'CASA VERDE', '02521090', ''),
(377, 'JUSSARA', '0', '0', '39518970', '', 5, '', 'RUA  LEONOR BARBOSA RODRIGUES,258', 'V.DIVA', '02556-040', ''),
(378, 'VANUSA', '0', '0', '25386805', '', 5, '', 'AV.  SEBASTIÃO HENRIQUES,448AP1 TORRE3', 'V.SIQUEIRA', '02723-050', ''),
(379, 'GABRIEL', '0', '0', '985855918', '', 5, '', 'SAO ROMOALDO 238', 'LIMAO', '', ''),
(380, 'CARLA', '0', '0', '967716209', '', 0, '', 'VIELA PARTICULA VASCONCELOS DE ALMEIDA 202', 'LIMAO', '', ''),
(381, 'DRICA', '0', '0', '992780554', '992780554', 4, '', 'RUA  ROQUE DE MORAIS,340 APT 31 SERGIPE', 'BA.DO LIMÃO', '02721-030', ''),
(382, 'EVESON', '0', '0', '995909845', '', 3, '', 'AV.  NOSSA SRA.DO O,423 AP151 BC 1', 'BA.DO LIMÃO', '02715-000', ''),
(383, 'ADRIANA', '0', '0', '989406600', '', 5, '', 'RUA  ANTONIO MIGUEL,39', 'V.SIQUEIRA', '02723-070', ''),
(384, 'MARIA', '0', '0', '39317571', '', 5, '', 'SAMPAIO CORREIO ALTURA520 BLO 5 APT53 PREDIO COLOR', 'LIMAO', '02701-000', ''),
(385, 'AITON', '0', '0', '21216883', '', 3, '', 'NOSA SENHO D O 382', '', '', ''),
(386, 'TATI', '0', '0', '965430195', '965430195', 3, '', 'AV ENGENHEIRO CAETANO ALEVES 370', '', '', ''),
(387, 'PRICILA', '0', '0', '38567904', '', 5, '', 'RUA  ANTONIO DE FREITAS BORGES,109', 'CASA VERDE ALTA', '02555-060', ''),
(388, 'ARIDNE', '0', '0', '985277229', '', 5, '', 'AV.  EMILIO CARLOS,521 AP76 D', 'V.CRISTO REI', '02721-000', ''),
(389, 'CHIQUINHO', '0', '0', '78214415', '', 4, '', 'JACOFER  105 APT103 TORRE2', 'LIMAO', '', ''),
(390, 'DEVID', '0', '0', '988080411', '', 3, '', 'RUA  MARIO DE AZEVEDO,153', 'JD.PEREIRA LEITE', '02710-020', ''),
(391, 'YARA', '0', '0', '981368396', '981368396', 3, '', 'AV.  MANDAQUI,122 AP 93 BRCO2', 'BA.DO LIMÃO', '02550-000', ''),
(392, 'VINICIO', '0', '0', '39612593', '', 5, '', 'RUA  JOAQUIM DE FREITAS,199', 'SÍTIO DO MORRO', '02551-070', ''),
(393, 'EDISOM', '0', '0', '988592653', '', 4, '', 'RUA  MIGUEL GUSTAVO,24 CS 6A', 'JD.PRIMAVERA', '02754-080', ''),
(394, 'ANA', '0', '0', '969594690', '', 4, '', 'RUA  ANTONIO JOÃO,160', 'SÍTIO DO MORRO', '02553-050', ''),
(395, 'SEGIO', '0', '0', '93174994', '93174994', 0, '', 'AV.  MANDAQUI,122AP172 BROCO 3', 'BA.DO LIMÃO', '02550-000', ''),
(396, 'SHIRLEI', '0', '0', '39512525', '', 5, '', 'AV.  CAETANO ALVARES,594 2 ANDAR', 'CASA VERDE', '02546-000', ''),
(397, 'RAQUEL', '0', '0', '953984805', '', 4, '', 'RUA  MACIEIRAS,104', 'JD.DAS LARANJEIRAS', '02521-090', ''),
(398, 'SILVIA', '0', '0', '39321958', '', 5, '', 'RUA  VICENTE FERREIRA LEITE,361 TEM 411', 'V.SIQUEIRA', '02723-000', ''),
(399, 'EDER', '0', '0', '22780479', '', 3, '', 'AV.  MANDAQUI,122 BLOCO3 AP 12', 'BA.DO LIMÃO', '02550-000', ''),
(400, 'MARGARET', '0', '0', '980144667', '', 4, '', 'RUA  SAMARITA,113', 'PQ.SOUZA ARANHA', '02518-080', ''),
(401, 'ALBERTO', '0', '0', '39515346', '', 3, '', 'VITORIO PRIMON  197', '', '', ''),
(402, 'VADSOM', '0', '0', '34878771', '', 5, '', 'RUA  ROQUE DE MORAIS,340 APT24 RIO DE JANEIRO', 'BA.DO LIMÃO', '02721-030', ''),
(403, 'AILA', '0', '0', '39354417', '', 5, '', 'AV.  ANTONIO MUNHOZ BONILHA,185', 'V.CAROLINA', '02725-000', ''),
(404, 'EDGAR', '0', '0', '43282583', '43282583', 3, '', 'AV.  MANDAQUI,122 APT 211 BROCO 4', 'BA.DO LIMÃO', '02550-000', ''),
(405, 'ANDREA', '0', '0', '35693892', '', 5, '', 'RUA  ROQUE DE MORAIS,340 BLO RIO GRANDE DO NORTE T', 'BA.DO LIMÃO', '02721-030', ''),
(406, 'ALINE', '0', '0', '991230153', '', 4, '', 'RUA  BERNARDO WRONA,389', 'BA.DO LIMÃO', '02710-060', ''),
(407, 'PRICILA', '0', '0', '32211516', '32211516', 3, '', 'RUA  ROQUE DE MORAIS,340 APT21 BLO RIO GRANDE SUL', 'BA.DO LIMÃO', '02721-030', ''),
(408, 'EDUARDO', '0', '0', '39363387', '', 5, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,BLJ2 AT2F2', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(409, 'CAMILA', '0', '0', '38576301', '', 5, '', 'RUA  MATEUS MASCARENHAS,421', 'JD.PEREIRA LEITE', '02712-000', ''),
(410, 'DANILO', '0', '0', '39551378', '', 5, '', 'RUA  ROQUE DE MORAIS,213 ESTACIONAMENTO', 'BA.DO LIMÃO', '02721-031', ''),
(411, 'LUCIANE', '0', '0', '38569547', '38569547', 4, '', 'RUA  GLAUCO VELASQUEZ,283', 'SÍTIO DO MORRO', '02553-000', ''),
(412, 'ROSE', '0', '0', '963606079', '', 5, '', 'RUA  SAMPAIO CORREA,451', 'BA.DO LIMÃO', '02710-080', ''),
(413, 'ALBERTO', '0', '0', '39514187', '', 5, '', 'RUA  JOSE SORIANO DE SOUSA,292 AP 11SUL', 'CASA VERDE ALTA', '02555-050', ''),
(414, 'TADEU', '0', '0', '981683076', '981683076', 4, '', 'AV.  EMILIO CARLOS,631AP 63A BCA', 'V.CRISTO REI1', '02721-000', ''),
(415, 'ESTELA', '0', '0', '39329149', '', 5, '', 'RUA  SANTA ANGELA,933', 'V.PALMEIRA', '02727-000', ''),
(416, 'MARA', '0', '0', '60228946', '', 2, '', 'CELATINO 482', '', '', ''),
(417, 'AMANDA', '0', '0', '963573934', '', 3, '', 'AV.  MANDAQUI,153', 'BA.DO LIMÃO', '02550-000', ''),
(418, 'ALESANDRO', '0', '0', '995411431', '', 5, '', 'SAMARITA 230', '', '', ''),
(419, 'RAFAEL', '0', '0', '949549970', '', 4, '', 'AV ENGREIRO CAITANO ALVIS 765', '', '', ''),
(420, 'DANIELI', '0', '0', '39613643', '', 4, '', 'AV.  NOSSA SRA.DO O,423AP11BC CASTELO', 'BA.DO LIMÃO', '02715-000', ''),
(421, 'RAFAEL', '0', '0', '957176506', '957176506', 3, '', 'RUA  ANTONIO JOÃO,364', 'SÍTIO DO MORRO', '02553-050', ''),
(422, 'SANDRA', '0', '0', '37292649', '37292649', 5, '', 'RUA  HENRIQUE JOSE DE SOUZA,46', 'JD.PRIMAVERA', '02755-020', ''),
(423, 'ROSANGRLA', '0', '0', '38367608', '', 2, '', 'CELETINO AP93 TORI 1', '', '', ''),
(424, 'GEAN', '0', '0', '969403403', '', 4, '', 'RUA  SALVADOR PADILHA GIMENES,104', 'V.STA.MARIA', '02562-130', ''),
(425, 'ROSARIA', '0', '0', '45642226', '45642226', 3, '', 'AV.  MANDAQUI,BC 2 AP 44', 'BA.DO LIMÃO', '02550-000', ''),
(426, 'ROSANA', '0', '0', '23644758', '23644758', 5, '', 'RUA  JACOFER,10 5 TORI 1AP 203', 'JD.PEREIRA LEITE', '02712-070', ''),
(427, 'LUIS', '0', '0', '54577530', '', 3, '', 'AV CASAVERDI 3431', '', '', ''),
(428, 'ELEN', '0', '0', '951435700', '', 4, '', 'AV.  IDA KOLB,387 AP24 BROCO 1', 'PQ.SOUZA ARANHA', '02518-000', ''),
(429, 'DENIS', '0', '0', '39666145', '39666145', 4, '', 'TANGERINA 147', '', '', ''),
(430, 'ALÇNBERTO', '0', '0', '979794436', '', 5, '', 'VL.  SADAJI YABUYA,9  FUNDOS', 'V.SIQUEIRA', '02723-021', ''),
(431, 'JOELMA', '0', '0', '38578395', '', 3, '', 'RUA  ROSAS DE OURO,34', 'BA.DO LIMÃO', '02714-025', ''),
(432, 'PRISCILA', '0', '0', '39550848', '', 5, '', 'RUA  MARIA ROBERTA,105', 'V.DIVA', '02556-160', ''),
(433, 'AMANDA', '0', '0', '38582480', '', 2, '', 'CELESTINO 890 APT12 A BLO A', '', '', ''),
(434, 'DENISE', '0', '0', '23862972', '23862972', 5, '', 'ROQUE DE MORAIS 400 APT14 MARANHAO', '', '', ''),
(435, 'ADRIANO', '0', '0', '993187974', '', 5, '', 'RUA  JACOFER,161ATR243BL2', 'JD.PEREIRA LEITE', '02712-070', ''),
(436, 'FLAVIA', '0', '0', '25327644', '', 5, '', 'RUA  ESTELA BORGES MORATO,618 APT72 BLO3', 'V.SIQUEIRA', '02722-000', ''),
(437, 'IRANI', '0', '0', '39320665', '', 5, '', 'AV.  SEBASTIÃO HENRIQUES,688', 'V.SIQUEIRA', '02723-050', ''),
(438, 'MARCOS', '0', '0', '967941941', '', 3, '', 'AMADEU DANIELE  FILHO 202', '', '', ''),
(439, 'ANTONIO', '0', '0', '39511243', '39511243', 4, '', 'EMGEIRO 463 .', '', '', ''),
(440, 'OLGA', '0', '0', '23872238', '', 3, '', 'CORONEL JOQUI D FERTAS 287', 'LIMAO', '', '');
INSERT INTO `clientes` (`id`, `name`, `cf1`, `cf2`, `phone`, `celular`, `taxa_de_entrega`, `email`, `endereco`, `bairro`, `cep`, `delivery`) VALUES
(441, 'SIMONE', '0', '0', '988284263', '', 5, '', 'RUA  CAROLINA SOARES,389', 'V.DIVA', '02554-000', ''),
(442, 'SUELI', '0', '0', '39668129', '', 5, '', 'TRAV.  VITORIO GNAN,35', 'PQ.SOUZA ARANHA', '02518-060', ''),
(443, 'CARLOS', '0', '0', '947040731', '', 5, '', 'RUA  SAMARITA,560BL9 APT23', 'PQ.SOUZA ARANHA', '02518-080', ''),
(444, 'VINICIUS', '0', '0', '39512206', '', 3, '', 'AV.  MANDAQUI,116', 'BA.DO LIMÃO', '02550-000', ''),
(445, 'DAVI', '0', '0', '23874523', '', 5, '', 'RUA  CESAR PENA RAMOS,1076', 'CASA VERDE ALTA', '02563-001', ''),
(446, 'CRISTINA', '0', '0', '39668296', '39668296', 3, '', 'AV.  MANDAQUI,122 AP 214 BLOCO 4', 'BA.DO LIMÃO', '02550-000', ''),
(447, 'ALEXANDRE', '0', '0', '977095820', '', 5, '', 'RUA  SAMARITA,898 BLOCO 11 AP 23', 'PQ.SOUZA ARANHA', '02518-080', ''),
(448, 'RAFAELA', '0', '0', '980174304', '', 5, '', 'RUA  MARIO DE AZEVEDO,60', 'JD.PEREIRA LEITE', '02710-020', ''),
(449, 'ANDERSON', '0', '0', '942584061', '942584061', 3, '', 'AV.  CELESTINO BOURROUL,651 CASA 2', 'BA.DO LIMÃO', '02710-000', ''),
(450, 'GUSTAVO', '0', '0', '996273266', '996273266', 5, '', 'RUA  CAROLINA SOARES,939 AP 11 BLOCO2', 'V.DIVA', '02554-000', ''),
(451, 'JOAO VITOR', '0', '0', '988532650', '', 5, '', 'RUA  GLAUCO VELASQUEZ,148', 'SÍTIO DO MORRO', '02553-000', ''),
(452, 'CAROLINE', '0', '0', '38575219', '', 3, '', 'AV.  CELESTINO BOURROUL,684 AP 61 BLOCO 3', 'BA.DO LIMÃO', '02710-001', ''),
(453, 'BRUNA', '0', '0', '39518528', '39518528', 4, '', 'RUA  JOÃO DO AMARAL,88', 'CASA VERDE MÉDIA', '02530-050', ''),
(454, 'EMESOM', '0', '0', '34677952', '', 3, '', 'NOSSA SEORA D O 423 AP 86 BROCO 2', '', '', ''),
(455, 'NATHALIA', '0', '0', '967813181', '', 5, '', 'RUA  RIBEIRÃO DAS ALMAS,193', 'V.PALMEIRA', '02728-100', ''),
(456, 'CAIO', '0', '0', '979793965', '979793965', 5, '', 'RUA  ESTELA BORGES MORATO,160 TORRE 4 AP 45', 'V.SIQUEIRA', '02722-000', ''),
(457, 'BIANCA', '0', '0', '38588530', '38588530', 4, '', 'RUA  MARIA JULIA,88', 'JD.DAS LARANJEIRAS', '02521-050', ''),
(458, 'ANTONIO', '0', '0', '983212170', '', 5, '', 'RUA  JULIA TREVISANI GANNAN,38', 'V.BANDEIRANTES', '02555-020', ''),
(459, 'FABIANA', '0', '0', '996972018', '', 0, '', 'AV.  CELESTINO BOURROUL,890AP 34A', 'BA.DO LIMÃO', '02710-001', ''),
(460, 'ROSANA', '0', '0', '39387935', '', 5, '', 'RUA  MADALENA DE MADUREIRA,82', 'SÍTIO DO MORRO', '02551-040', ''),
(461, 'MOACIR', '0', '0', '973188787', '', 3, '', 'AV.  CELESTINO BOURROUL,684 APT 46 BL3', 'BA.DO LIMÃO', '02710-001', ''),
(462, 'LEANDRO', '0', '0', '38587869', '38587869', 5, '', 'RUA  MARCOS POLAIO,81', 'V.STA.MARIA', '02561-020', ''),
(463, 'TAIS', '0', '0', '39826290', '', 5, '', 'RUA  ESTELA BORGES MORATO,618 APT113 TORRE1', 'V.SIQUEIRA', '02722-000', ''),
(464, 'CIAUDIA', '0', '0', '38571420', '', 4, '', 'RUA NOVA GRANADA 151 FONDOS', '', '', ''),
(465, 'ADRIENE', '0', '0', '26454936', '', 3, '', 'AV MANDAQYUI 122 APT134 BLO3', '', '', ''),
(466, 'IRANIR', '0', '0', '39320665', '', 5, '', 'AV SEBASTIAO HENRIQUE 688', '', '', ''),
(467, 'TIAGO', '0', '0', '974692912', '', 5, '', 'RUA  BERNARDINO GOMES,250', 'V.SANTISTA', '02560-000', ''),
(468, 'FELIPI', '0', '0', '41127412', '', 4, '', 'RUA  PAULO ALVES DA SILVA TELLES, 18', 'CJ.RES.NO.PACAEMBU', '02722-040', ''),
(469, 'LUCIANA', '0', '0', '32875831', '', 3, '', 'AV.  CELESTINO BOURROUL,684 APT163 TORRE1', 'BA.DO LIMÃO', '02710-001', ''),
(470, 'AMILCA', '0', '0', '23398066', '', 5, '', 'RUA  CRISOLIA,308 APT8', 'JD.PRIMAVERA', '02756-000', ''),
(471, 'ITAMARA', '0', '0', '24787736', '24787736', 3, '', 'AV.  MANDAQUI,275AP 182', 'BA.DO LIMÃO', '02550-000', ''),
(472, 'SINTIA', '0', '0', '38810282', '', 5, '', 'RUA  JACOFER,161 AP 124 TORI 2', 'JD.PEREIRA LEITE', '02712-070', ''),
(473, 'CARLIOS', '0', '0', '25971820', '25971820', 5, '', 'AV MARQUES DE SAO VICENTE ..2898 AP25 B.', 'AGUA BRANCA', '', ''),
(474, 'CLAUDINEI', '0', '0', '25013957', '25013957', 4, '', 'RUA  ROQUE DE MORAIS,340 BORCO BRAZILIA AP 1', 'BA.DO LIMÃO', '02721-030', ''),
(475, 'VITOR', '0', '0', '22616121', '', 5, '', 'RUA  JOÃO PIRES COELHO,91', 'V.PALMEIRA', '02752-080', ''),
(476, 'MICHEL', '0', '0', '982128698', '982128698', 5, '', 'GLAUCO VESLASQUI 505', 'CASA VERDE BAIXA', '', ''),
(477, 'DANIEL', '0', '0', '991352349', '', 4, '', 'RUA  JACOFER,161 T 2 AP 131', 'JD.PEREIRA LEITE', '02712-070', ''),
(478, 'WISOM', '0', '0', '984686531', '', 2, '', 'AV MANDAQUI 189', '', '', ''),
(479, 'LEANDRO', '0', '0', '23848379', '23848379', 4, '', 'RUA  JACOFER,161 AP52BROCO 2', 'JD.PEREIRA LEITE', '02712-070', ''),
(480, 'SIVIA', '0', '0', '23616707', '', 4, '', 'RUA  PRIULI,41 CASA3', 'V.STA.MARIA', '02559-020', ''),
(481, 'RITA', '0', '0', '25794518', '25794518', 4, '', 'RUA  ROSA MARCHETTI,116', 'BA.DO LIMÃO', '02550-110', ''),
(482, 'CEMA', '0', '0', '940067775', '', 2, '', 'CELETINO BURRUO 684 TORI AP 103', '', '', ''),
(483, 'FERNADA', '0', '0', '59328311', '', 4, '', 'CASA VERDE BAIXA/LIMÃO,BRCO 1 AP 51', '', '02501-000', ''),
(484, 'KAREN', '0', '0', '38588834', '', 3, '', 'AV.  EMILIO CARLOS,119 AP 5', 'V.CRISTO REI', '02721-000', ''),
(485, 'LEONEL', '0', '0', '974588304', '974588304', 2, '', 'AV.  MANDAQUI,122AP121 BROCO 4', 'BA.DO LIMÃO', '02550-000', ''),
(486, 'ALECHANDRE', '0', '0', '997064585', '', 4, '', 'RUA  GABRIEL MIGLIORI,363', 'JD.DAS GRAÇASSP ALINI', '02712-140', ''),
(487, 'CILENI', '0', '0', '39384290', '39384290', 4, '', 'RUA  CAROLINA SOARES,39', 'V.DIVA', '02554-000', ''),
(488, 'BRUNA', '0', '0', '98852210', '', 3, '', 'CELESTINO 684 TORRE1 APT72', '', '', ''),
(489, 'MARCELA', '0', '0', '987566589', '', 5, '', 'RUA  XIRO,168 AP82', 'CASA VERDE BAIXA', '02517-030', ''),
(490, 'WIARA', '0', '0', '39321730', '', 5, '', 'RUA  OTAVIO DE MOURA,277', 'V.CAROLINA', '02724-000', ''),
(491, 'JAIR', '0', '0', '39658779', '', 5, '', 'RUA  SEBASTIÃO MORAIS,197', 'BA.DO LIMÃO', '02551-100', ''),
(492, 'CINTIA', '0', '0', '38270406', '', 5, '', 'RUA  ROSA MARCHETTI,87', 'BA.DO LIMÃO', '02550-110', ''),
(493, 'ELIOMAR', '0', '0', '69792839', '', 5, '', 'RUA  EUCLIDES MACHADO,324 BL2 APT31', 'JD.DAS GRAÇAS', '02713-000', ''),
(494, 'CRISTIANE', '0', '0', '994089661', '994089661', 4, '', 'RUA  ROQUE DE MORAIS,345 AP21 ED GATEMALA', 'BA.DO LIMÃO', '02721-031', ''),
(495, 'SONIA.', '0', '0', '22398279', '', 5, '', 'RUA  BENTO PICKEL,858', 'SÍTIO DO MANDAQUI', '02544-000', ''),
(496, 'BRONO', '0', '0', '991606823', '', 5, '', 'RUA  SANTO ANTONIO DA PLATINA,73', 'V.CRISTO REI', '02558-080', ''),
(497, 'SIMONI', '0', '0', '965777167', '', 4, '', 'RUA  SILVERIO GONCALVES,52', 'JD.PRIMAVERA', '02754-000', ''),
(498, 'HENRIQUE', '0', '0', '36738266', '36738266', 5, '', 'RUA  FREDERICO PENTEADO JR.,107 AP 11A', 'JD.DAS LARANJEIRAS', '02517-100', ''),
(499, 'ANDRE', '0', '0', '29245222', '', 5, '', 'AV.  CASA VERDE,2901AP95', 'V.ESTER', '02519-200', ''),
(500, 'GILMARIA', '0', '0', '77324513', '', 3, '', '684 TORRE3 APT4 CELESTINO', '', '', ''),
(501, 'RAFAEL', '0', '0', '997899832', '', 4, '', 'RUA  JACOFER,161AP243 TR1', 'JD.PEREIRA LEITE', '02712-070', ''),
(502, 'DIEGO', '0', '0', '38567599', '', 3, '', 'RUA  VITORIO PRIMON,197', 'BA.DO LIMÃO', '02550-050', ''),
(503, 'BRUNO', '0', '0', '35890969', '', 5, '', 'CESAR PENA RAMOS 1744', '', '', ''),
(504, 'ANDRE', '0', '0', '39361226', '', 4, '', 'ASOSSO FAICALZANO 115 CONJUNTO REDECIAL NOVO PAQUE', '', '', ''),
(505, 'VANESA', '0', '0', '38580661', '', 4, '', 'RUA  SAMPAIO CORREA,LIDIANI 14', 'BA.DO LIMÃO', '02710-080', ''),
(506, 'ROBERTA', '0', '0', '32131355', '32131355', 4, '', 'RUA  JACOFER,105 AP 191 TORI 1', 'JD.PEREIRA LEITE', '02712-070', ''),
(507, 'FELIPE', '0', '0', '945911249', '', 3, '', 'CELESTINO 631 GAPAO FALA RAMAL720 RODRIGO', '', '', ''),
(508, 'CLARA', '0', '0', '39312932', '', 5, '', 'RUA  ESTELA BORGES MORATO,500', 'V.SIQUEIRA', '02722-000', ''),
(509, 'LARISA', '0', '0', '951282505', '951282505', 2, '', 'RUA  AMADEU DANIELI FILHO,96', 'BA.DO LIMÃO', '02550-030', ''),
(510, 'FATIMA', '0', '0', '83687435', '83687435', 4, '', 'RUA  FRANCISCO RODRIGUES NUNES,34', 'JD.DAS GRAÇAS', '02712-090', ''),
(511, 'FABIANO', '0', '0', '38581115', '38581115', 5, '', 'RUA  SÃO ROMUALDO,73', 'V.BARBOSA', '02557-060', ''),
(512, 'VIVIANI', '0', '0', '996437809', '996437809', 3, '', 'RUA  GONZAGA DE CARVALHO,469', 'JD.PRIMAVERA', '02755-130', ''),
(513, 'VAGUINE', '0', '0', '23594245', '', 3, '', 'EMGEIERO CAITANO ALVIS 800', '', '', ''),
(514, 'DIRCER', '0', '0', '38569899', '', 4, '', 'SAO ROMOALDO 104', 'CASA VERDE ALTA', '02555-060', ''),
(515, 'MARIA ISABEL', '0', '0', '986698880', '986698880', 4, '', 'RUA  CAROLINA SOARES,493', 'V.DIVA', '02554-000', ''),
(516, 'CLEITON', '0', '0', '970281638', '', 4, '', 'MASTRO GABRIEL MIGUIORI 353', 'JD.PEREIRA LEITE', '02712-040', ''),
(517, 'SINTIA', '0', '0', '29851763', '29851763', 3, '', 'PASDO LEAO 73', '', '', ''),
(518, 'ELENILSOM', '0', '0', '39322444', '39322444', 6, '', 'RUA  JOSE DOS SANTOS, 54', 'V.BANC.MUNHOZ', '02757-050', ''),
(519, 'LIZIA', '0', '0', '39231344', '', 2, '', 'AV.  CELESTINO BOURROUL,684 TORI 1AP 41', 'BA.DO LIMÃO', '02710-001', ''),
(520, 'REGINALDO', '0', '0', '38570377', '', 4, '', 'RUA  ROCHA LIMA,455', 'SÍTIO DO MORRO', '02554-010', ''),
(521, 'MARCIA', '0', '0', '37745378', '', 5, '', 'RUA  JACOFER,140 APT 132 TORRE2', 'JD.PEREIRA LEITE', '02712-070', ''),
(522, 'REGIANE', '0', '0', '957365651', '', 5, '', 'ANTONIO MONHOS BONILHA 1047', '', '', ''),
(523, 'ELOYSE', '0', '0', '28935700', '28935700', 2, '', 'AV.  MANDAQUI,122 AP161 BROC2', 'BA.DO LIMÃO', '02550-000', ''),
(524, 'FATIMA', '0', '0', '977560775', '', 3, '', 'VICTORIO PRIMON 100', '', '', ''),
(525, 'PRICILA', '0', '0', '29550848', '', 4, '', 'RUA  MARIA ROBERTA,105', 'V.DIVA', '02556-160', ''),
(526, 'DOMENICA', '0', '0', '23847916', '', 5, '', 'RUA  JACOFER,161AP64TORI1', 'JD.PEREIRA LEITE', '02712-070', ''),
(527, 'NILTON', '0', '0', '38580684', '', 5, '', 'RUA  GUARIZINHO,612', 'PQ.PERUCHE', '02530-010', ''),
(528, 'GABRIEL', '0', '0', '26679171', '26679171', 5, '', 'RUA  MADALENA DE MADUREIRA,322 CASA3', 'SÍTIO DO MORRO', '02551-040', ''),
(529, 'CELSOM', '0', '0', '39352912', '', 5, '', 'VL.  SADAJI YABUYA,7', 'V.SIQUEIRA', '02723-021', ''),
(530, 'ANA PAULA', '0', '0', '39328577', '', 5, '', 'RUA  MANUEL JUSTINIANO QUINTÃO,344', 'V.PALMEIRA', '02728-020', ''),
(531, 'MIRANI', '0', '0', '34796120', '34796120', 3, '', 'AV.  MANDAQUI,89 EN FRENTI CARTORIO 89', 'BA.DO LIMÃO', '02550-000', ''),
(532, 'ATRIANA', '0', '0', '986148104', '', 4, '', 'RUA  BERNARDO WRONA,389', 'BA.DO LIMÃO', '02710-060', ''),
(533, 'CALOS EDUARDO', '0', '0', '951446212', '', 4, '', 'RUA  JERONIMO DOS SANTOS,18A', 'V.DIVA', '02556-090', ''),
(534, 'CAROLINA', '0', '0', '35640893', '', 5, '', 'AV.  SEBASTIÃO HENRIQUES,448 AP61 TORI 3', 'V.SIQUEIRA', '02723-050', ''),
(535, 'GABRIELA', '0', '0', '39511258', '39511258', 4, '', 'RUA  SAMARITA,898 BROCO 1AP 21', 'PQ.SOUZA ARANHAAP', '02518-080', ''),
(536, 'ROSANA', '0', '0', '28587413', '', 5, '', 'RUA  XIRO,168AP82 BRO A', 'CASA VERDE BAIXA', '02517-030', ''),
(537, 'MARIANA', '0', '0', '38575032', '', 5, '', 'AV.  MONTE CELESTE,51PETY CHOPI', 'V.STA.MARIA', '02561-000', ''),
(538, 'SILVIA', '0', '0', '38560345', '', 5, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,271 BROCO A3', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(539, 'RICADO', '0', '0', '38570201', '', 5, '', 'RUA  DIAS DE OLIVEIRA,206', 'V.BANDEIRANTES', '02552-110', ''),
(540, 'DOUGLAS', '0', '0', '24955574', '', 5, '', 'RUA  JACOFER,105 AP24 TORRE2', 'JD.PEREIRA LEITE', '02712-070', ''),
(541, 'PEDRO', '0', '0', '970961256', '', 3, '', 'RUA  HORACIO MOURA,230', 'BA.DO LIMÃO', '02710-070', ''),
(542, 'DELÇOM', '0', '0', '992040404', '', 5, '', 'RUA  JACOFER,105 AP181 TORRE2', 'JD.PEREIRA LEITE', '02712-070', ''),
(543, 'GUSTAVO', '0', '0', '987096869', '987096869', 3, '', 'AV.  MANDAQUI,122 APT 93 BLO2', 'BA.DO LIMÃO', '02550-000', ''),
(544, 'LUAN A', '0', '0', '992450231', '992450231', 1, '', 'CELESLINO890APT41 A', '', '', ''),
(545, 'RODRIGO', '0', '0', '972691447', '', 5, '', 'DOM AMARAL MOUZINHO 123', ' CASA VERDE', '', ''),
(546, 'VINICIUS', '0', '0', '964539167', '', 5, '', 'RUA  LEONOR BARBOSA RODRIGUES,391 FUNDOS', 'V.DIVA', '02556-040', ''),
(547, 'GABRIELA', '0', '0', '953383062', '953383062', 5, '', 'RUA  SAMARITA,898 BLOC 8 APT 32', 'PQ.SOUZA ARANHA', '02518-080', ''),
(548, 'EDSON', '0', '0', '33927758', '', 5, '', 'RUA  CASSILANDIA,54', 'V.STA.MARIA', '02559-030', ''),
(549, 'STEFANE', '0', '0', '954164663', '', 5, '', 'RUA  ANTONIO JOÃO,160 B', 'SÍTIO DO MORRO', '02553-050', ''),
(550, 'SILVANA', '0', '0', '38578701', '', 3, '', 'RUA  SAMPAIO CORREA,520 BLOCO 1 AP23A', 'BA.DO LIMÃO', '02710-080', ''),
(551, 'MARIANI', '0', '0', '987307295', '987307295', 5, '', 'RUA  MARIA ELISA SIQUEIRA,320', 'JD.S.LUÍS', '02558-000', ''),
(552, 'LUCAS', '0', '0', '977710013', '', 5, '', 'AV SEBASTIAO HENRIQQUES688', 'LIMAO', '', ''),
(553, 'ALINE', '0', '0', '23730920', '', 3, '', 'AV.  CELESTINO BOURROUL,684 AP42 TORRE1', 'BA.DO LIMÃO', '02710-001', ''),
(554, 'JOSIAS', '0', '0', '23691720', '', 5, '', 'RUA  BALTAZAR DE QUADROS,151', 'V.PALMEIRA', '02727-080', ''),
(555, 'JESSICA', '0', '0', '963289148', '963289148', 3, '', 'RUA  SEBASTIÃO MORAIS,4 CASA 2', 'BA.DO LIMÃO', '02551-100', ''),
(556, 'KIKIS', '0', '0', '981145521', '', 5, '', 'RUA  JACOFER,140 AP 154 TORI 3', 'JD.PEREIRA LEITE', '02712-070', ''),
(557, 'RONALDO', '0', '0', '976039745', '', 5, '', 'AV.  SEBASTIÃO HENRIQUES,300', 'V.SIQUEIRA', '02723-050', ''),
(558, 'VERONICA', '0', '0', '982074472', '', 5, '', 'RUA MARCIERAS 125 ACASA 6', '', '', ''),
(559, 'RICARDO', '0', '0', '25015458', '', 5, '', 'RUA  SILVERIO GONCALVES,52', 'JD.PRIMAVERA', '02754-000', ''),
(560, 'EDUARDA', '0', '0', '39324149', '', 5, '', 'RUA  SANTA ANGELA,933', 'V.PALMEIRA', '02727-000', ''),
(561, 'TIAGO', '0', '0', '988251275', '', 5, '', 'RUA  ANTONIO MIGUEL,39', 'V.SIQUEIRA', '02723-070', ''),
(562, 'PETER', '0', '0', '954715645', '', 3, '', 'RUA  HORACIO MOURA,286', 'BA.DO LIMÃO', '02710-070', ''),
(563, 'EDEVALDO', '0', '0', '941830970', '', 5, '', 'RUA  JOAQUIM MENDES,162', 'PQ.SOUZA ARANHA', '02518-100', ''),
(564, 'GABRIELA', '0', '0', '39512472', '39512472', 5, '', 'RUA  JOAQUIM DE FREITAS,455', 'SÍTIO DO MORRO', '02551-070', ''),
(565, 'ALAN', '0', '0', '39522299', '', 3, '', 'RUA  MARIO DE AZEVEDO,138', 'JD.PEREIRA LEITE', '02710-020', ''),
(566, 'MALINE', '0', '0', '34591207', '', 3, '', 'RUA  VITORIO PRIMON,101', 'BA.DO LIMÃO', '02550-050', ''),
(567, 'CRISTIANA', '0', '0', '32139604', '32139604', 2, '', 'CELESTINO 890 BLO A AP53', '', '', ''),
(568, 'MAIARA', '0', '0', '26141277', '26141277', 3, '', 'AV.  MANDAQUI,275APT104', 'BA.DO LIMÃO', '02550-000', ''),
(569, 'WELITOM', '0', '0', '43243037', '', 3, '', 'CELESTINO 684 TORRE2 AP21', '', '', ''),
(570, 'TATIANE', '0', '0', '38075805', '', 5, '', 'RUA  JULIA TREVISANI GANNAN,188', 'V.BANDEIRANTES', '02555-020', ''),
(571, 'OSANA', '0', '0', '39515273', '', 5, '', 'TR.  FRANCISCO ADRIANI,68', 'V.SANTISTA', '02560-180', ''),
(572, 'JANAINA', '0', '0', '39319033', '', 5, '', 'RUA  SANTO UBALDO,28 APT13 BLO B', 'V.CAROLINA', '02725-050', ''),
(573, 'RENATA', '0', '0', '37961510', '', 5, '', 'RUA  ROQUE DE MORAIS,86', 'BA.DO LIMÃO', '02721-030', ''),
(574, 'PRICILA', '0', '0', '39659337', '', 5, '', 'TRAV.  VITORIO GNAN,02 ATRAI D FORU SATANA', 'PQ.SOUZA ARANHA', '02518-060', ''),
(575, 'AILIANA', '0', '0', '39321838', '39321838', 5, '', 'RUA  OSCAR DRUMOND COSTA,07', 'V.CAROLINA', '02723-100', ''),
(576, 'ROBERIO', '0', '0', '39326417', '', 5, '', 'RUA  ANTONIO MIGUEL,33', 'V.SIQUEIRA', '02723-070', ''),
(577, 'CLAUUDIA', '0', '0', '978054579', '978054579', 5, '', 'RUA  DIRCE RODRIGUES,90', 'V.DIVA', '02556-080', ''),
(578, 'LUANA', '0', '0', '959121391', '', 2, '', 'AV MANDAQUIN 122 BL2 APT 91', '', '', ''),
(579, 'GABRIANA', '0', '0', '992033933', '', 3, '', 'RUA  ROQUE DE MORAIS,340 AP 31 SERGIPE', 'BA.DO LIMÃO', '02721-030', ''),
(580, 'PATRICIA', '0', '0', '55283152', '', 3, '', 'AV.  EMILIO CARLOS,631 AP 6 A', 'V.CRISTO REI', '02721-000', ''),
(581, 'RENATA', '0', '0', '977671378', '', 5, '', 'RUA  FREDERICO PENTEADO JR.,344 A', 'JD.DAS LARANJEIRAS', '02517-100', ''),
(582, 'RENATA', '0', '0', '967170383', '967170383', 5, '', 'RUA  SAMARITA,898 BLOCO 8 AP 13', 'PQ.SOUZA ARANHA', '02518-080', ''),
(583, 'CECILIA', '0', '0', '28937088', '', 5, '', 'AV.  EMILIO CARLOS,521 AP 28B', 'V.CRISTO REI', '02721-000', ''),
(584, 'SIMONI', '0', '0', '987845061', '', 4, '', 'AV.  EMILIO CARLOS,521 AP 103 C', 'V.CRISTO REI', '02721-000', ''),
(585, 'GABRIELA', '0', '0', '983193745', '', 2, '', 'AV.  MANDAQUI,122AP131 BROCO 4', 'BA.DO LIMÃO', '02550-000', ''),
(586, 'GUILHERME', '0', '0', '23389695', '', 3, '', 'RUA CELESTINO 684 AP 55 TORRE 3', '', '', ''),
(587, 'DANIELI', '0', '0', '43011560', '', 3, '', 'RUA  AMADEU DANIELI FILHO,221', 'BA.DO LIMÃO', '02550-030', ''),
(588, 'RAQUEL', '0', '0', '25894582', '', 3, '', 'RUA  JOAQUIM DE FREITAS,187 FUNDOS', 'SÍTIO DO MORRO', '02551-070', ''),
(589, 'FERNANDO', '0', '0', '39612166', '39612166', 5, '', 'AV.  EMILIO CARLOS,521 AP 12 BLOCO A', 'V.CRISTO REI', '02721-000', ''),
(590, 'ARIANA', '0', '0', '962336129', '', 3, '', 'AV.  CELESTINO BOURROUL,890 AP 144 BLOCO A', 'BA.DO LIMÃO', '02710-000', ''),
(591, 'VALDIRENI', '0', '0', '999319288', '', 4, '', 'RUA  ALCIDES ALVES,106', 'V.BANDEIRANTES', '02555-030', ''),
(592, 'JUNIOR', '0', '0', '22831417', '', 5, '', 'AV.  SANTA INES,610', 'MANDAQUI', '02415-000', ''),
(593, 'GIZELI', '0', '0', '977147585', '977147585', 1, '', 'AV.  CELESTINO BOURROUL,890AP162A', 'BA.DO LIMÃO', '02710-001', ''),
(594, 'ALEX', '0', '0', '969973398', '', 4, '', 'AMEDEIA SENTINE,,,TRAVESSA CELESTINO;84', '', '', ''),
(595, 'RICARDO', '0', '0', '957374959', '', 5, '', 'DEPUTADO EMILHO CALOS 1010', '', '', ''),
(596, 'GRAZIELA', '0', '0', '940610890', '', 5, '', 'RUA  VICENTE FERREIRA LEITE,512 AP 56 BLO1', 'V.SIQUEIRA', '02723-000', ''),
(597, 'ATRIANA', '0', '0', '39669966', '', 3, '', 'RUA  MARIO DE AZEVEDO,211', 'JD.PEREIRA LEITE', '02710-020', ''),
(598, 'BRONO', '0', '0', '26143677', '26143677', 3, '', 'AV.  MANDAQUI,122AP113 BROCO 4', 'BA.DO LIMÃO', '02550-000', ''),
(599, 'ANGELA', '0', '0', '38562152', '', 3, '', 'TRAV.  ANTONIO FARGAS,61', 'BA.DO LIMÃO', '02551-090', ''),
(600, 'VANDERLEIA', '0', '0', '968108566', '968108566', 4, '', 'RUA FRANCISCO RODRIGUES NUNES PORTAO 34', '', '', ''),
(601, 'IASMIM', '0', '0', '984187470', '', 4, '', 'RUA  SAMPAIO CORREA,520 BRC1 AI 23A', 'BA.DO LIMÃO', '02710-080', ''),
(602, 'HORLANDO', '0', '0', '949063445', '949063445', 3, '', 'AMADEU DANIELE FILHO 268', '', '', ''),
(603, 'VERONICA', '0', '0', '949459933', '', 2, '', 'RUA  PAULO EMILIO SALLES GOMES,', 'BA.DO LIMÃO', '02710-110', ''),
(604, 'WASHINGTTON', '0', '0', '949725019', '949725019', 3, '', 'RUA  AMEDEA CENTINI,213', 'BA.DO LIMÃO', '02710-100', ''),
(605, 'ALCILIADORA', '0', '0', '39511097', '', 2, '', 'RUA  ARAUJO MARCONDES,56', 'BA.DO LIMÃO', '02550-080', ''),
(606, 'ATRIANO', '0', '0', '22397537', '', 4, '', 'CLAUCO VELAKI 269', 'LIMAO', '', ''),
(607, 'LUCAS', '0', '0', '39328050', '39328050', 5, '', 'RUA  BARTOLOMEU DO CANTO,165 AP 33', 'V.PALMEIRA', '02726-090', ''),
(608, 'MARIA VANDA', '0', '0', '39550849', '', 5, '', 'RUA  MARIA ROBERTA,105 CASA', 'V.DIVA', '02556-160', ''),
(609, 'GRASIELA', '0', '0', '981842919', '', 5, '', 'RUA  ESTELA BORGES MORATO, 618 AP 1 BLOCO 1', 'V.SIQUEIRA', '02722-000', ''),
(610, 'TAIS', '0', '0', '39343333', '', 3, '', 'RUA  BERNARDO WRONA,267 EMPRESA SELFIE DECOR', 'BA.DO LIMÃO', '02710-060', ''),
(611, 'JUNIOR', '0', '0', '991057678', '', 5, '', 'RUA  ANDRE BOLSENA,57', 'V.SANTISTA', '02559-120', ''),
(612, 'GUSTAVO', '0', '0', '997050734', '', 5, '', 'RUA  CAROLINA SOARES,939 AP 11 BLOCO E', 'V.DIVA', '02554-000', ''),
(613, 'TAMIRES', '0', '0', '984042394', '', 5, '', 'CORONEL EUCLIDES MACHADO 203', 'LIMAO', '', ''),
(614, 'MARIANE', '0', '0', '38587413', '38587413', 5, '', 'RUA  XIRO,168  APT 82', 'CASA VERDE BAIXA', '02517-030', ''),
(615, 'TATILA', '0', '0', '39353969', '', 5, '', 'RUA  EUCLIDES MACHADO,203', 'JD.DAS GRAÇAS', '02713-000', ''),
(616, 'SERGIO', '0', '0', '966326292', '', 5, '', 'RUA FRANCISCO DIOGO 445', 'PARQUE PERUCHE', '', ''),
(617, 'RULY', '0', '0', '36633408', '36633408', 5, '', 'RUA  PAULO VIDIGAL VICENTE 199 BLO F3 ATP 11', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(618, 'LUCAS', '0', '0', '994190087', '', 3, '', 'AV.  CELESTINO BOURROUL,890 31A', 'BA.DO LIMÃO', '02710-001', ''),
(619, 'VAGNER', '0', '0', '28926046', '', 3, '', 'PAULO ADONO 32 SEGUNDA A RDQUERDA DA ESPERANÇA', 'LIMAO', '', ''),
(620, 'CAROLINA', '0', '0', '997026028', '997026028', 5, '', 'RUA  ESTELA BORGES MORATO,618 APT 53 TORRE1', 'V.SIQUEIRA', '02722-000', ''),
(621, 'AMANDA', '0', '0', '38575923', '', 5, '', 'RUA  FRANCISCO DOS SANTOS BRUNO,53', 'PQ.SOUZA ARANHA', '02518-070', ''),
(622, 'ELZA', '0', '0', '23652569', '23652569', 5, '', 'AMADEU DANIELIE FILHO 264', 'LIMAO', '', ''),
(623, 'BRONA', '0', '0', '39350920', '39350920', 4, '', 'RUA  ANTONIO RIBEIRO DE MORAIS,264 BRO 3 AP 71', 'JD.TABOR', '02751-000', ''),
(624, 'KATIA', '0', '0', '39663937', '', 5, '', 'SAO LA DE LAO 102 CASA PROXIMO A A QUARENTA', 'V.DIVA', '02556-050', ''),
(625, 'CAROL', '0', '0', '28949819', '28949819', 4, '', 'RUA  SERGIO FERREIRA,63', 'BA.DO LIMÃO', '02550-060', ''),
(626, 'ORLANDO', '0', '0', '953504529', '', 5, '', 'AMADEU DANIELI FILHO 268 .', '', '', ''),
(627, 'CARINA', '0', '0', '999264259', '', 5, '', 'RUA  SEBASTIÃO MORAIS, 101', 'BA.DO LIMÃO', '02551-100', ''),
(628, 'DIEGO', '0', '0', '998680094', '998680094', 4, '', 'RUA  OSCAR DRUMOND COSTA,07', 'V.CAROLINA', '02723-100', ''),
(629, 'EULINDA', '0', '0', '38572118', '38572118', 5, '', 'RUA  DOMINGOS TORRES,105', 'JD.DAS LARANJEIRAS', '02521-100', ''),
(630, 'GAUCHO', '0', '0', '984043394', '', 4, '', 'RUA  EUCLIDES MACHADO,203', 'JD.DAS GRAÇAS', '02713-000', ''),
(631, 'JAQUELII  ANA PAULA', '0', '0', '38587734', '38587734', 3, '', 'RUA  SEBASTIÃO MORAIS,01B LARANJA BAIXO', 'BA.DO LIMÃO', '02551-100', ''),
(632, 'ELIANI', '0', '0', '981157901', '', 5, '', 'RUA  BELISARIO CAMPANHA,388', 'CASA VERDE MÉDIA', '02521-000', ''),
(633, 'BRUNO', '0', '0', '38557000', '', 5, '', 'AV.  CASA VERDE,3031', 'V.ESTER', '02519-200', ''),
(634, 'ROBERTO', '0', '0', '22563743', '22563743', 4, '', 'RUA  TOMAS ANTONIO VILANI,401 AP 84A BROCO A', 'V.STA.MARIA', '02562-000', ''),
(635, 'LETICIA', '0', '0', '958008651', '', 4, '', 'JD.DAS GRACAS/JD.PEREIRA LEITE,BROCO3 AP 44', '', '02701-000', ''),
(636, 'TAIS', '0', '0', '23870212', '', 5, '', 'RUA  ANGELO BONAVITA,170', 'V.STA.MARIA', '02562-040', ''),
(637, 'SOLANGI', '0', '0', '991753834', '991753834', 5, '', 'RUA  JOSE KILEBER,555 AP103', 'CJ.RES.NO.PACAEMBU', '02722-140', ''),
(638, 'PRICILA', '0', '0', '986417592', '986417592', 4, '', 'AV.  NOSSA SRA.DO O,423 APT22 BLO2', 'BA.DO LIMÃO', '02715-000', ''),
(639, 'LILIA', '0', '0', '971009961', '', 5, '', 'RUA  JOÃO DO AMARAL,18', 'CASA VERDE MÉDIA', '02530-050', ''),
(640, 'CAROL', '0', '0', '959549517', '', 5, '', 'RUA  FRANCISCO RODRIGUES NUNES,34', 'JD.DAS GRAÇAS', '02712-090', ''),
(641, 'LUCIENE', '0', '0', '43289821', '43289821', 5, '', 'RUA  JACOFER,105 AP´T141 TORRE 2', 'JD.PEREIRA LEITE', '02712-070', ''),
(642, 'SONIA', '0', '0', '958924973', '', 5, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,271 BLOCO A4', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(643, 'JUNIOR', '0', '0', '948110775', '948110775', 5, '', 'AV CASA VERDE , 3437', '', '', ''),
(644, 'MARCIA', '0', '0', '23650085', '', 5, '', 'AV.  CLAVASIO ALVES DA SILVA,215 APT 802', 'V.SIQUEIRA', '02722-030', ''),
(645, 'GESSIMA', '0', '0', '970823140', '', 3, '', 'AV MANDAQUI 268 CASA 3', '', '', ''),
(646, 'ROBERTA', '0', '0', '37961322', '', 5, '', 'RUA  DIOGO DE OLIVEIRA,332', 'SÍTIO DO MORRO', '02553-040', ''),
(647, 'BARBARA', '0', '0', '963154298', '', 5, '', 'RUA  DIOGO DE OLIVEIRA,279', 'SÍTIO DO MORRO', '02553-040', ''),
(648, 'ANTONIO', '0', '0', '964551034', '', 3, '', 'CELESTINO 684 APT21 TORRE 3', '', '', ''),
(649, 'LEANDRO', '0', '0', '940530027', '', 5, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,163 BLO G 2', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(650, 'MARIA DE LUZ', '0', '0', '39350175', '', 5, '', 'GODOFREDO GONÇALVES 87    COLEGIO REDINE', '', '', ''),
(651, 'ANTONIO', '0', '0', '39658968', '39658968', 4, '', 'RUA  LUCILA,183', 'JD.DAS LARANJEIRAS', '02522-090', ''),
(652, 'ELAINE', '0', '0', '39665413', '39665413', 3, '', 'RUA  SERGIO FERREIRA,30', 'BA.DO LIMÃO', '02550-060', ''),
(653, 'MARIA ELIZA', '0', '0', '23711642', '23711642', 5, '', 'RUA  LUIS VASCONCELOS,35', 'V.CÉLIA', '02551-010', ''),
(654, 'ERICA', '0', '0', '967487468', '', 5, '', 'RUA  MANUEL DORTA,40', 'V.MO.ALTO', '02753-020', ''),
(655, 'RAFAELA', '0', '0', '23733454', '', 5, '', 'RUA  MARIO DE AZEVEDO,60', 'JD.PEREIRA LEITE', '02710-020', ''),
(656, 'EMRIQUI', '0', '0', '954473198', '', 5, '', 'PAULO VIDIGAL VICENTI D AZEVEDO 271 AP 11 BROCO 1', '', '', ''),
(657, 'GIL', '0', '0', '973329441', '', 5, '', 'VIA DE PD.  FRANCELINA FAUSTINA V. DE LIMA,148', 'V.SANTISTA', '02560-120', ''),
(658, 'VALERIA', '0', '0', '23694910', '', 5, '', 'RUA  MELO REZENDE,12', 'V.SANTISTA', '02560-010', ''),
(659, 'ALENSANDRA', '0', '0', '39666990', '39666990', 4, '', 'RUA  LAGOA AZUL,258 ALADO D 271', 'V.DIVA', '02556-000', ''),
(660, 'ANDEZA', '0', '0', '984241921', '', 5, '', 'RUA  INACIO PROENCA DE GOUVEIA,971', 'PQ.PERUCHE', '02534-010', ''),
(661, 'MONICA', '0', '0', '37982542', '', 5, '', 'RUA  DOMINGOS TORRES,255', 'JD.DAS LARANJEIRAS', '02521-100', ''),
(662, 'LUIS FERNADOS', '0', '0', '949829554', '', 5, '', 'RUA  MARCELINO DE CAMARGO,314', 'V.CAROLINA', '02724-040', ''),
(663, 'LUCIANA', '0', '0', '38575552', '38575552', 1, '', 'AV.  CELESTINO BOURROUL,AP 193 B', 'BA.DO LIMÃO', '02710-001', ''),
(664, 'JULIANA', '0', '0', '39662889', '', 5, '', 'RUA  CAROLINA SOARES,1021 ASPT 61', 'V.DIVA', '02554-000', ''),
(665, 'GABRIELY', '0', '0', '949241243', '949241243', 4, '', 'AV.  ANTONIO MUNHOZ BONILHA,537', 'V.CAROLINA', '02725-000', ''),
(666, 'NATALIA', '0', '0', '991958849', '', 5, '', 'RUA  SALVADOR PADILHA GIMENES,107', 'V.STA.MARIA', '02562-130', ''),
(667, 'ALEXANDRE', '0', '0', '39853279', '39853279', 6, '', 'RUA  OSCAR ROSAS RIBEIRO,50 A', 'JD.ALMANARA', '02863-000', ''),
(668, 'RENATA', '0', '0', '987670284', '', 5, '', 'RUA  ROQUE DE MORAIS,171', 'BA.DO LIMÃO', '02721-031', ''),
(669, 'RAFAEL', '0', '0', '27690602', '27690602', 4, '', 'RUA  SEBASTIÃO MORAIS,203', 'BA.DO LIMÃO', '02551-100', ''),
(670, 'TALIA', '0', '0', '982553194', '', 5, '', 'HORACIO MOURA 83 CASA 2', '', '', ''),
(671, 'ANA', '0', '0', '28930781', '28930781', 5, '', 'EMGEEIRO JOSE D AVEREDO SANTOAS 69 CASA 2', 'VILA CARBONE', '', ''),
(672, 'VANDO', '0', '0', '88068310', '', 4, '', 'RUA  BERNARDO WRONA,408', 'BA.DO LIMÃO', '02710-060', ''),
(673, 'MARIA CICILIA', '0', '0', '943757162', '', 3, '', 'AV MADAQUI 122 BRO 3 AP 72', '', '', ''),
(674, 'LUIZA', '0', '0', '22547119', '', 5, '', 'AV.  NOSSA SRA.DO O,423 AT 107 BLO 2', 'BA.DO LIMÃO', '02715-000', ''),
(675, 'ALEX', '0', '0', '27297516', '', 5, '', 'RUA  ANTONIO JOÃO,655 AP 41', 'SÍTIO DO MORRO', '02553-050', ''),
(676, 'LILIANA', '0', '0', '964394314', '', 5, '', 'RUA  BELISARIO CAMPANHA,388', 'CASA VERDE MÉDIA', '02521-000', ''),
(677, 'CAROLIMNA', '0', '0', '985414339', '', 4, '', 'RUA  JULIETA NOTARI REIS,146', 'CH.MORRO ALTO', '02562-020', ''),
(678, 'SARA', '0', '0', '953207928', '', 5, '', 'RUA  AMADEU DANIELI FILHO,268', 'BA.DO LIMÃO', '02550-030', ''),
(679, 'LUIZA', '0', '0', '999091460', '999091460', 5, '', 'RUA  JOSE FIUZA GUIMARÃES,233', 'JD.PEREIRA LEITE', '02712-060', ''),
(680, 'FABIANA', '0', '0', '969293768', '969293768', 3, '', 'CELASTINO BURROU 684 AP104 TORRE 1', '', '', ''),
(681, 'ELEM', '0', '0', '39313055', '', 5, '', 'RUA  EPICURO,33 CASA 4', 'CASA VERDE MÉDIA', '02552-030', ''),
(682, 'WELITON', '0', '0', '991220747', '', 4, '', 'SANPAIO COREIA 265', '', '', ''),
(683, 'KAROL', '0', '0', '984021793', '', 4, '', 'RUA  ANTONIO JOÃO,508', 'SÍTIO DO MORRO', '02553-050', ''),
(684, 'MIKAELI BARRETO', '0', '0', '941107067', '', 5, '', 'CAPITAO FRACISCI TEICHEIRA NOGUEIRA 154', '', '', ''),
(685, 'PAULA', '0', '0', '992275911', '', 5, '', 'RUA  JACOFER,140AP 124 TORI 2', 'JD.PEREIRA LEITE', '02712-070', ''),
(686, 'GABRIELA', '0', '0', '968989467', '968989467', 4, '', 'RUA  SAMARITA,898BLO3 APT51', 'PQ.SOUZA ARANHA', '02518-080', ''),
(687, 'RAU', '0', '0', '25946563', '', 5, '', 'JACOFEF 105', '', '', ''),
(688, 'HERNANE', '0', '0', '961817400', '', 5, '', 'RUA  VARAN KEUTENEDJIAN,27', 'PQ.PERUCHE', '02539-010', ''),
(689, 'ARTUR', '0', '0', '39665996', '', 5, '', 'RUA  JERONIMO DE BRITO,66', 'CASA VERDE ALTA', '02563-150', ''),
(690, 'IGOR', '0', '0', '965930583', '', 5, '', 'RUA  LEONOR BARBOSA RODRIGUES,391 CASA EM CIMA DE', 'V.DIVA', '02556-040', ''),
(691, 'GEORGE', '0', '0', '992838403', '', 3, '', 'RUA  BERNARDO WRONA,339', 'BA.DO LIMÃO', '02710-060', ''),
(692, 'ROSANGELA', '0', '0', '23691629', '', 5, '', 'RUA  ELVIRA BARBOSA,43', 'V.BARBOSA', '02557-050', ''),
(693, 'RONALDO', '0', '0', '38574193', '', 5, '', 'RUA  CAROLINA SOARES,939AP21 BLO ESPARTA', 'V.DIVA', '02554-000', ''),
(694, 'LEANDRO', '0', '0', '949185957', '', 5, '', 'SANTO ANCELMO 107', ' JARDIM SAO BENTO', '', ''),
(695, 'LANAI', '0', '0', '36759489', '', 5, '', 'JACOFER 105 APT 61 TORRE 1', '', '', ''),
(696, 'GESSICA', '0', '0', '970577843', '', 5, '', 'RUA  GLAUCO VELASQUEZ,515', 'SÍTIO DO MORRO', '02553-000', ''),
(697, 'VERONICA', '0', '0', '987914024', '', 3, '', 'RUA  BERNARDO WRONA,339', 'BA.DO LIMÃO', '02710-060', ''),
(698, 'EDILSOM', '0', '0', '38576911', '38576911', 4, '', 'RUA  ROQUE DE MORAIS,340 APT 12 ED SANTA CATARINA', 'BA.DO LIMÃO', '02721-030', ''),
(699, 'MARIANI', '0', '0', '981680453', '', 5, '', 'RUA  XIRO,168 AP82', 'CASA VERDE BAIXA', '02517-030', ''),
(700, 'ANTONIA', '0', '0', '34618230', '', 5, '', 'RUA  FREDERICO PENTEADO JR.,342', 'JD.DAS LARANJEIRAS', '02517-100', ''),
(701, 'VANIA', '0', '0', '966731721', '', 4, '', 'SAMARITA BL2 APT 13', '', '', ''),
(702, 'NIVALDO', '0', '0', '38575732', '', 4, '', 'TRAV.  BALTAZAR ECHAVE,33', 'V.BARBOSA', '02557-070', ''),
(703, 'KARLA', '0', '0', '953479537', '953479537', 3, '', 'RUA  AMADEU DANIELI FILHO,268', 'BA.DO LIMÃO', '02550-030', ''),
(704, 'RITA', '0', '0', '993431315', '', 1, '', 'CELESTINO 890 APT 101 A', '', '', ''),
(705, 'DÉCIO', '0', '0', '995131751', '995131751', 4, '', 'RUA  SÃO ROMUALDO,238', 'V.BARBOSA', '02557-060', ''),
(706, 'FRACICO', '0', '0', '26672790', '', 4, '', 'RUA  ESTELA BORGES MORATO,618TORI AP84', 'V.SIQUEIRA', '02722-000', ''),
(707, 'DAIANE', '0', '0', '38573612', '', 5, '', 'RUA  SAMARITA,898 BLO 6 APT 12', 'PQ.SOUZA ARANHA', '02518-080', ''),
(708, 'DAVI', '0', '0', '983837637', '', 5, '', 'RUA  AMARAL MOUSINHO,56', 'PQ.SOUZA ARANHA', '02517-140', ''),
(709, 'JUNIOR', '0', '0', '38571577', '38571577', 4, '', 'RUA  PAULO VIDIGAL VICENTE163 BLO H1 APT 21', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(710, 'LUIZ', '0', '0', '958280971', '', 5, '', 'RUA  EUCLIDES MACHADO,217', 'JD.DAS GRAÇAS', '02713-000', ''),
(711, 'ERICA', '0', '0', '23726597', '', 3, '', 'AV.  CELESTINO BOURROUL,684 AP 51 TORI 1', 'BA.DO LIMÃO', '02710-000', ''),
(712, 'EVILEM', '0', '0', '51532842', '', 5, '', 'CAROLINA SUARES 860', '', '', ''),
(713, 'GESSICA', '0', '0', '966888005', '', 5, '', 'ANTONIO  MIGUEL 34 TRAVESSA CLAVASSIO', '', '', ''),
(714, 'BIANCA', '0', '0', '970506034', '', 3, '', 'RUA  AMADEU DANIELI FILHO,266', 'BA.DO LIMÃO', '02550-030', ''),
(715, 'EVILA', '0', '0', '39742386', '', 5, '', 'RUA  SEBASTIÃO ALVES DE SOUZA,', 'V.REGINA', '02968-140', ''),
(716, 'AMANDA', '0', '0', '38583833', '38583833', 5, '', 'AV.  JOSE DE BRITO DE FREITAS,860', 'CASA VERDE ALTA', '02552-000', ''),
(717, 'MAIRA', '0', '0', '940244791', '', 4, '', 'RUA  VITORIO PRIMON,291', 'BA.DO LIMÃO', '02550-050', ''),
(718, 'ANA', '0', '0', '952314902', '', 4, '', 'RUA  ARAUJO MARCONDES,255', 'BA.DO LIMÃO', '02550-080', ''),
(719, 'TAIS', '0', '0', '29853240', '', 3, '', 'RUA  AMADEU DANIELI FILHO,218', 'BA.DO LIMÃO', '02550-030', ''),
(720, 'GERALDO', '0', '0', '39514463', '', 4, '', 'RUA  SAMPAIO CORREA,517 CASA 2', 'BA.DO LIMÃO', '02710-080', ''),
(721, 'MACIA', '0', '0', '996124177', '', 5, '', 'ALFREDO POJO 1788', '', '', ''),
(722, 'KARLA', '0', '0', '34516240', '34516240', 4, '', 'AV.  NOSSA SRA.DO O,423AP32BROCO1', 'BA.DO LIMÃO', '02715-000', ''),
(723, 'FERNANDO', '0', '0', '39669025', '', 4, '', 'RUA  REIMS,577AP53', 'PQ.SOUZA ARANHA', '02517-010', ''),
(724, 'VAGUINE', '0', '0', '22397094', '', 5, '', 'AV.  SEBASTIÃO HENRIQUES,300 AP 53', 'V.SIQUEIRA', '02723-050', ''),
(725, 'LUCAS', '0', '0', '981963268', '', 5, '', 'RUA  FRANKLIN DO AMARAL,403', 'V.NA.CACHOEIRINHA', '02479-000', ''),
(726, 'ANTONIO', '0', '0', '39595050', '39595050', 4, '', 'JOSE AMATO 39', '', '', ''),
(727, 'ELANI', '0', '0', '25386221', '', 4, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,235AP2S1 BRO', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(728, 'AXES SANDRO', '0', '0', '985597664', '', 4, '', 'JACOFE 321', '', '', ''),
(729, 'ISABEL', '0', '0', '23871833', '', 5, '', 'RUA  JACOFER,105  TORRE 1 APT 174', 'JD.PEREIRA LEITE', '02712-070', ''),
(730, 'LUDMILA', '0', '0', '971210444', '', 3, '', 'CELESTINO 964 LOJA 10', '', '', ''),
(731, 'ALESSANDRA', '0', '0', '38666990', '', 5, '', 'RUA  LAGOA AZUL,258  AO LADO DO 271', 'V.DIVA', '02556-000', ''),
(732, 'KARINA', '0', '0', '988037466', '', 3, '', 'AV.  NOSSA SRA.DO O,382 PROCISA EMPRESA', 'BA.DO LIMÃO', '02715-000', ''),
(733, 'EVERTON', '0', '0', '992220134', '', 5, '', 'RUA  ANTONIO JOÃO,449 CASA 4', 'SÍTIO DO MORRO', '02553-050', ''),
(734, 'ENRIQUI', '0', '0', '39665581', '', 5, '', 'RUA  ANGELICO BESSONE,02', 'V.BARUEL', '02516-070', ''),
(735, 'VAGUINE', '0', '0', '29735027', '', 5, '', 'RUA  MARAMBAIA,41', 'CASA VERDE BAIXA', '02513-000', ''),
(736, 'KELI', '0', '0', '98266686', '', 5, '', 'RUA  SILVANO DE ALMEIDA,481 CASA2', 'V.SIQUEIRA', '02723-110', ''),
(737, 'PETRO TENORI', '0', '0', '39662318', '39662318', 5, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,199', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(738, 'ORLANDO', '0', '0', '28646222', '', 5, '', 'RUA  ABURA,659  APT301', 'SÍTIO DO MANDAQUI', '02542-110', ''),
(739, 'TATIANA', '0', '0', '981023035', '', 4, '', 'RUA  EUCLIDES MACHADO,203', 'JD.DAS GRAÇAS', '02713-000', ''),
(740, 'JESON', '0', '0', '38578696', '', 5, '', 'RUA  BERNARDINO GOMES,282', 'V.SANTISTA', '02560-000', ''),
(741, 'ANA', '0', '0', '959680809', '959680809', 5, '', 'RUA  FRANCISCO RODRIGUES NUNES,34', 'JD.DAS GRAÇAS', '02712-090', ''),
(742, 'CRISTIANI', '0', '0', '99408661', '', 1, '', 'RUA  ROQUE DE MORAIS,345', 'BA.DO LIMÃO', '02721-031', ''),
(743, 'ISAEL', '0', '0', '999565842', '', 4, '', 'AV.  NOSSA SRA.DO O,423 AP 154 BROCO 1', 'BA.DO LIMÃO', '02715-000', ''),
(744, 'RODRIGO', '0', '0', '954210804', '', 4, '', 'RUA  MARIA ROBERTA,56', 'V.DIVA', '02556-160', ''),
(745, 'FERNANDA', '0', '0', '981622180', '', 3, '', 'CELESTINO 684  APT 101 VILA VERDE', '', '', ''),
(746, 'GINA', '0', '0', '39516051', '', 3, '', 'RUA  SAMPAIO CORREA520 APT 35 B BLO 3', 'BA.DO LIMÃO', '02710-080', ''),
(747, 'LUIZ', '0', '0', '38571451', '', 5, '', 'RUA  DIOGO DE OLIVEIRA,80', 'SÍTIO DO MORRO', '02553-040', ''),
(748, 'VIVIANI', '0', '0', '38546897', '', 4, '', 'AV.  NOSSA SRA.DO O,423AP101 BROCO2', 'BA.DO LIMÃO', '02715-000', ''),
(749, 'PRICILA', '0', '0', '39516579', '39516579', 5, '', 'RUA  BERNARDINO GOMES,78', 'V.SANTISTA', '02560-000', ''),
(750, 'EDISON', '0', '0', '39318285', '', 4, '', 'RUA  SANTO UBALDO,28AP92C', 'V.CAROLINA', '02725-050', ''),
(751, 'LEONADO', '0', '0', '976517697', '', 6, '', 'RUA  ARMANDO RAMOS FILHO,301', 'MOINHO VELHO', '02929-100', ''),
(752, 'MARIA RAQUEL', '0', '0', '963420010', '', 6, '', 'RUA  PEDRO OSORIO FILHO,835 AP 23B', 'V.NA.CACHOEIRINHA', '02614-000', ''),
(753, 'ANTONIO', '0', '0', '38584998', '', 5, '', 'RUA  REINALDO RAMOS,69', 'CASA VERDE ALTA', '02559-110', ''),
(754, 'KEILA', '0', '0', '39315271', '', 5, '', 'RUA  CLEMENTE FERREIRA,191 CASA SALAO', 'V.SIQUEIRA', '02723-040', ''),
(755, 'JOAO', '0', '0', '39368003', '', 3, '', 'RUA  SAMPAIO CORREA,520 BL2 AP45 A', 'BA.DO LIMÃO', '02710-080', ''),
(756, 'SANT', '0', '0', '39663895', '', 3, '', 'RUA  HORACIO MOURA,252', 'BA.DO LIMÃO', '02710-070', ''),
(757, 'ANDRE', '0', '0', '965526041', '', 4, '', 'SAMARIA 1000  TRASITA', '', '', ''),
(758, 'MAIARA', '0', '0', '970542429', '', 4, '', 'RUA  PAULO ADORNO,60', 'SÍTIO DO MORRO', '02551-130', ''),
(759, 'PATRICIA', '0', '0', '26382627', '', 5, '', 'RUA  ROQUE DE MORAIS,340 AP 13 RORAIMA', 'BA.DO LIMÃO', '02721-030', ''),
(760, 'BRONA', '0', '0', '23738046', '', 4, '', 'AV.  EMILIO CARLOS,521 BRO A AP 98', 'V.CRISTO REI', '02721-000', ''),
(761, 'WILSOM', '0', '0', '953287517', '953287517', 5, '', 'RUA  SILVERIO DE CARVALHO,331', 'V.CARBONE', '02752-000', ''),
(762, 'NATALIA', '0', '0', '39717804', '', 5, '', 'RUA  ACARAPEREIRA,47', 'V.MO.GRANDE', '02809-030', ''),
(763, 'GUILHERME', '0', '0', '942119484', '', 5, '', 'RUA  INACIO PROENCA DE GOUVEIA,971', 'PQ.PERUCHE', '02534-010', ''),
(764, 'DILENI', '0', '0', '964620963', '964620963', 4, '', 'RUA  JERONIMO DOS SANTOS,102', 'V.DIVA', '02556-090', ''),
(765, 'KARLA', '0', '0', '39317859', '', 5, '', 'RUA  ROQUE DE MORAIS,345 EDIFICIO MAVILVINA AP T1', 'BA.DO LIMÃO', '02721-031', ''),
(766, 'PATRICIA    AP 2F2', '0', '0', '39512605', '39512605', 5, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,BROCO 2', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(767, 'RAFAELA', '0', '0', '930018365', '930018365', 5, '', 'RUA  EVERARDO DIAS, 51', 'CJ.RES.NO.PACAEMBU', '02722-020', ''),
(768, 'BRUNO', '0', '0', '80849641', '', 3, '', 'RUA  JOÃO MAYER, 59', 'SÍTIO DO MORRO', '02551-120', ''),
(769, 'TRIAGO', '0', '0', '977801877', '', 5, '', 'AV NOSSA DO O 423 61 BROS 1', '', '', ''),
(770, 'DEBORA', '0', '0', '963411132', '', 2, '', 'AV.  MANDAQUI,119A', 'BA.DO LIMÃO', '02550-000', ''),
(771, 'DANIEL', '0', '0', '98629330', '98629330', 4, '', 'AV.  CLAVASIO ALVES DA SILVA,525 AP 41 BROCO MATEU', 'V.SIQUEIRA', '02722-030', ''),
(772, 'RENATA', '0', '0', '39510933', '', 5, '', 'RUA  JERONIMO DE BRITO,45', 'CASA VERDE ALTA', '02563-150', ''),
(773, 'ELISIA', '0', '0', '967598218', '', 4, '', 'RUA  NELSON FRANCISCO,29', 'JD.DAS GRAÇAS', '02712-100', ''),
(774, 'KALAR', '0', '0', '39578435', '', 4, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,271 BROB2', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(775, 'ROGERIO', '0', '0', '38570898', '38570898', 3, '', 'RUA  JERONIMO DOS SANTOS,138', 'V.DIVA', '02556-090', ''),
(776, 'ELAINI', '0', '0', '984814142', '984814142', 4, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,235 AP2S1', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(777, 'PAULO', '0', '0', '22395908', '22395908', 4, '', 'RUA  EDSON PEREGRINI,139', 'V.MARISBELA', '02762-040', ''),
(778, 'VANIA', '0', '0', '965247442', '', 4, '', 'RUA  GAMA CERQUEIRA,746', 'V.DIVA', '02560-070', ''),
(779, 'NIUSN', '0', '0', '38589327', '38589327', 3, '', 'RUA  SEBASTIÃO MORAIS,271', 'BA.DO LIMÃO', '02551-100', ''),
(780, 'ANA PAULA', '0', '0', '985063457', '985063457', 3, '', 'AV.  EMILIO CARLOS,550 A 10 B', 'V.BARBOSA', '02720-000', ''),
(781, 'KLEBER', '0', '0', '977680768', '', 5, '', 'RUA  MATEUS MASCARENHAS,321', 'JD.PEREIRA LEITE', '02712-000', ''),
(782, 'ADRIANA', '0', '0', '987695861', '', 4, '', 'RUA  JACOFER,140 AP 102 TORI 2', 'JD.PEREIRA LEITE', '02712-070', ''),
(783, 'DAIS', '0', '0', '985678247', '', 3, '', 'AV.  CELESTINO BOURROUL,684 AP137 TORI3', 'BA.DO LIMÃO', '02710-001', ''),
(784, 'ROSANI', '0', '0', '970252948', '970252948', 3, '', 'AV.  ANTONIO MUNHOZ BONILHA,1079 CASA 4', 'V.CAROLINA', '02725-000', ''),
(785, 'FRED', '0', '0', '946064809', '', 5, '', 'ROCK DE MORAIS 504  BLO AMAZONAS APT 22', '', '', ''),
(786, 'LUCIA', '0', '0', '976621003', '', 4, '', 'RUA  FREDERICO PENTEADO JR.,107 APT 11A', 'JD.DAS LARANJEIRAS', '02517-100', ''),
(787, 'ADRIANA', '0', '0', '946411816', '', 3, '', 'RUA  VASCONCELOS DE ALMEIDA,144', 'V.DIVA', '02556-010', ''),
(788, 'RAFAELA', '0', '0', '25093295', '25093295', 4, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,55APT22BLOI2', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(789, 'WILLIAN', '0', '0', '965573591', '', 4, '', 'RUA CARANDERI , 219', 'PARALELA AO DEPUTADO', '', ''),
(790, 'MONIQUE', '0', '0', '964603572', '', 3, '', 'RUA  VITORIO PRIMON,91', 'BA.DO LIMÃO', '02550-050', ''),
(791, 'CRISTIANE', '0', '0', '39352971', '', 4, '', 'RUA  SANTO UBALDO,28 AP 71 BLOCO C', 'V.CAROLINA', '02725-050', ''),
(792, 'ELISANGELA', '0', '0', '962651840', '962651840', 5, '', 'RUA  JOAQUIM DE FREITAS,151 CASA 2', 'SÍTIO DO MORRO', '02551-070', ''),
(793, 'ERICA', '0', '0', '957872532', '957872532', 3, '', 'CAROLINA SOARES , 860', '', '', ''),
(794, 'TALITA', '0', '0', '956507272', '', 5, '', 'RUA  FRANCISCO DOS SANTOS BRUNO,117 CASA 1', 'PQ.SOUZA ARANHA', '02518-070', ''),
(795, 'LEO', '0', '0', '954661764', '', 3, '', 'RUA  GAMA CERQUEIRA,101', 'V.DIVA', '02560-070', ''),
(796, 'MARCELO', '0', '0', '984786892', '', 3, '', 'SAMARITA 230 PAVILHAO DE EVENTO PRONAGO', '', '', ''),
(797, 'VINICIUS', '0', '0', '985858700', '985858700', 5, '', 'RUA  JOAQUIM DE FREITAS,199', 'SÍTIO DO MORRO', '02551-070', ''),
(798, 'VALDIR  LUIZA', '0', '0', '39660872', '', 4, '', 'RUA  VASCONCELOS DE ALMEIDA,136', 'V.DIVA', '02556-010', ''),
(799, 'ELAINE', '0', '0', '998131341', '', 3, '', 'RUA  SERGIO FERREIRA,30', 'BA.DO LIMÃO', '02550-060', ''),
(800, 'DULCE', '0', '0', '26456189', '', 4, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,222 AOT128 B', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(801, 'DANILO', '0', '0', '951382186', '', 6, '', 'RUA  EDUARDO LUIS TRINDADE,187', 'V.ESPANHOLA', '02566-010', ''),
(802, 'CHICO', '0', '0', '947627473', '', 5, '', 'RUA  ANDREA DEL CASTAGNO,357', 'V.STA.MARIA', '02562-010', ''),
(803, 'DINA', '0', '0', '967840027', '', 4, '', 'EULALIO DA COSTA 08', '', '', ''),
(804, 'RITA', '0', '0', '28945332', '', 5, '', 'RUA  CASSILANDIA,173', 'V.STA.MARIA', '02559-030', ''),
(805, 'CARINA', '0', '0', '976002960', '', 5, '', 'RUA  VASCONCELOS DE ALMEIDA,382', 'V.DIVA', '02556-010', ''),
(806, 'TALITA', '0', '0', '989603160', '', 5, '', 'RUA  FREDERICO PENTEADO JR.,144', 'JD.DAS LARANJEIRAS', '02517-100', ''),
(807, 'BRUNO', '0', '0', '38560304', '38560304', 5, '', 'RUA  JOÃO ROBERTO THUT,171', 'V.CARBONE', '02751-010', ''),
(808, 'FERNADO', '0', '0', '39957706', '', 5, '', 'RUA  JACOFER,105 TORI1 AP 91', 'JD.PEREIRA LEITE', '02712-070', ''),
(809, 'RUTI', '0', '0', '39514351', '39514351', 4, '', 'RUA  BERNARDINO GOMES,78', 'V.SANTISTA', '02560-000', ''),
(810, 'LETICIA', '0', '0', '39660087', '', 5, '', 'TR.  MARIA LUCIA,138', 'V.BARBOSA', '02557-030', ''),
(811, 'ROGERIO', '0', '0', '39669552', '', 4, '', 'RUA  JOSE SORIANO DE SOUSA,243', 'CASA VERDE ALTA', '02555-050', ''),
(812, 'JULIANA', '0', '0', '35865537', '35865537', 3, '', 'AV MANDAQUI 122 AP 74 BROC 4', ' LIMAO', '', ''),
(813, 'RODRIGO', '0', '0', '986741765', '', 4, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO, 222 AP04 BL', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(814, 'GRABI', '0', '0', '39352379', '', 4, '', 'RUA  CRISOLIA, 292', 'JD.PRIMAVERA', '02756-000', ''),
(815, 'PAULO', '0', '0', '954576317', '', 3, '', 'RUA  GAMA CERQUEIRA, 160', 'V.DIVA', '02560-070', ''),
(816, 'DANIELE', '0', '0', '989599851', '989599851', 4, '', 'RUA  VENANCIO DE RESENDE, 160', 'CASA VERDE MÉDIA', '02552-020', ''),
(817, 'PATRIK', '0', '0', '982163910', '982163910', 4, '', 'NOSA SENHORA DO 423 AP 61 BORCO A OU 1', '', '', ''),
(818, 'VERA', '0', '0', '39510077', '39510077', 3, '', 'RUA  HORACIO MOURA,280', 'BA.DO LIMÃO', '02710-070', ''),
(819, 'ANDESON', '0', '0', '39510771', '39510771', 4, '', 'RUA  ZANZIBAR,1122', 'CASA VERDE BAIXA', '02512-010', ''),
(820, 'ANTONIO', '0', '0', '898493019', '', 2, '', 'BERNARDO BWONA 389', '', '', ''),
(821, 'WESLEY', '0', '0', '35281299', '35281299', 3, '', 'RUA  BERNARDO WRONA,339', 'BA.DO LIMÃO', '02710-060', ''),
(822, 'MICHEL', '0', '0', '948458060', '', 5, '', 'RUA  JERONIMO DOS SANTOS,102  CASA 5', 'V.DIVA', '02556-090', ''),
(823, 'ALESANDRO', '0', '0', '970426585', '970426585', 4, '', 'GACOFE 231', '', '', ''),
(824, 'TOM', '0', '0', '32136153', '', 5, '', 'AV.  JOSE DE BRITO DE FREITAS,405 CASA 9', 'CASA VERDE ALTA', '02552-000', ''),
(825, 'DIEGO', '0', '0', '966837405', '966837405', 4, '', 'R ISABEL CIQUEIRA BARROS 349', 'LIMAIO', '', ''),
(826, 'GABRIEL', '0', '0', '968467830', '', 5, '', 'RUA  ANTONIO RIBEIRO DE MORAIS,264 BRO3 AP124', 'JD.TABOR', '02751-000', ''),
(827, 'ALEXSANDE', '0', '0', '968150624', '', 5, '', 'JACOFE 321', '', '', ''),
(828, 'PAMELA', '0', '0', '38574694', '38574694', 5, '', 'PR.  CENTENARIO,85 CASA2', 'CASA VERDE BAIXA', '02515-040', ''),
(829, 'GIL', '0', '0', '996766316', '', 4, '', 'AV.  CLAVASIO ALVES DA SILVA,227 AP 36', 'V.SIQUEIRA', '02722-030', ''),
(830, 'DANIELA', '0', '0', '975464506', '975464506', 4, '', 'AV.  CLAVASIO ALVES DA SILVA,215 AP207', 'V.SIQUEIRA', '02722-030', ''),
(831, 'MARGARETI', '0', '0', '34767720', '', 4, '', 'RUA  ROQUE DE MORAIS,340 AP 11 ED PERNABUCO', 'BA.DO LIMÃO', '02721-030', ''),
(832, 'SUELI', '0', '0', '27292921', '', 4, '', 'RUA  IMARUI,119', 'V.MARISBELA', '02762-050', ''),
(833, 'CALOS EM RIQUI', '0', '0', '988059553', '', 4, '', 'AV NOSA SEORADO 382', '', '', ''),
(834, 'ELENA', '0', '0', '38560898', '38560898', 5, '', 'RUA  ANTONIO VERA CRUZ,477 CASA 4', 'CASA VERDE ALTA', '02555-010', ''),
(835, 'JULIANA', '0', '0', '985345420', '', 5, '', 'TRAV.  VITORIO GNAN,22', 'PQ.SOUZA ARANHA', '02518-060', ''),
(836, 'ROSANGELA', '0', '0', '959658130', '', 5, '', 'RUA  DARCI BITENCOURT, 34', 'V.PALMEIRA', '02726-030', ''),
(837, 'ISABEL', '0', '0', '23872833', '23872833', 4, '', 'RUA  JACOFER,105 AT174 TORRE1', 'JD.PEREIRA LEITE', '02712-070', ''),
(838, 'BRUNO', '0', '0', '943799501', '', 5, '', 'RUA  ALMEIDA NOBRE,114 CASA 6', 'SÍTIO DO MANDAQUI', '02543-150', ''),
(839, 'SOLANGE', '0', '0', '39669728', '', 4, '', 'RUA  FREDERICO PENTEADO JR.,163', 'JD.DAS LARANJEIRAS', '02517-100', ''),
(840, 'SHIRLEI', '0', '0', '38537897', '', 3, '', 'RUA  VITORIO PRIMON,188', 'BA.DO LIMÃO', '02550-050', ''),
(841, 'CECILIA', '0', '0', '26595553', '', 5, '', 'RUA  ROCHA LIMA,247', 'SÍTIO DO MORRO', '02554-010', ''),
(842, 'SILVIA', '0', '0', '38573935', '38573935', 3, '', 'DEPUTADO EMILIO CARLOS 521 BLOB APT37', 'LIMAO', '02721-000', ''),
(843, 'VALDIR', '0', '0', '39364483', '39364483', 5, '', 'RUA  SAGUAIRU,660 AP 21', 'CASA VERDE BAIXA', '02514-000', ''),
(844, 'EMERSON', '0', '0', '38581413', '', 5, '', 'RUA  TOMAS ANTONIO VILANI,216', 'V.STA.MARIA', '02562-000', ''),
(845, 'RODNEI', '0', '0', '947224283', '947224283', 4, '', 'RUA  JACOFER,105 AP 23 BLOCO 2', 'JD.PEREIRA LEITE', '02712-070', ''),
(846, 'MARIANA', '0', '0', '38551845', '', 5, '', 'RUA  GLAUCO VELASQUEZ,521', 'SÍTIO DO MORRO', '02553-000', ''),
(847, 'ANGELA', '0', '0', '39652152', '39652152', 3, '', 'TRAV.  ANTONIO FARGAS,61', 'BA.DO LIMÃO', '02551-090', ''),
(848, 'CARLOS', '0', '0', '998018670', '', 5, '', 'RUA  GONZAGA DE CARVALHO,469 FUNDOS', 'JD.PRIMAVERA', '02755-130', ''),
(849, 'RODRIGO', '0', '0', '29853240', '', 3, '', 'AMADEU DANIELE FILHO 218', '', '', ''),
(850, 'CAMILA', '0', '0', '985847792', '', 4, '', 'R BENRDITO AUGUSTO FEIRA 25', '', '', ''),
(851, 'RODRIGO', '0', '0', '981528181', '981528181', 4, '', 'AV.  SEBASTIÃO HENRIQUES,448 BLOCO 2 AP 66', 'V.SIQUEIRA', '02723-050', ''),
(852, 'RICARDO', '0', '0', '957630000', '', 3, '', 'RUA  JERONIMO DOS SANTOS,46', 'V.DIVA', '02556-090', ''),
(853, 'GEAN', '0', '0', '941507104', '', 5, '', 'RUA  ANTONIO JOÃO,576', 'SÍTIO DO MORRO', '02553-050', ''),
(854, 'CHILIS', '0', '0', '987794488', '', 4, '', 'RUA  JACOFER,140AP 133 TORI3', 'JD.PEREIRA LEITE', '02712-070', ''),
(855, 'DINHA', '0', '0', '967186076', '967186076', 3, '', 'A VENIDA 3453  CASA VERDI 171', '', '', ''),
(856, 'DELI', '0', '0', '28613731', '', 4, '', 'AV.  IDA KOLB,225 AP72 BLO 9', 'PQ.SOUZA ARANHA', '02518-000', ''),
(857, 'VITOR', '0', '0', '22316121', '', 4, '', 'RUA  JOÃO PIRES COELHO,91', 'V.PALMEIRA', '02752-080', ''),
(858, 'LEADRA', '0', '0', '39364929', '39364929', 4, '', 'AV.  EMILIO CARLOS,1251', 'JD.PRIMAVERA', '02721-100', ''),
(859, 'JOAO', '0', '0', '39612863', '39612863', 4, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,22 AP 64C', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(860, 'MACIO', '0', '0', '32976195', '32976195', 5, '', 'AV.  CLAVASIO ALVES DA SILVA,215 AP 508', 'V.SIQUEIRA', '02722-030', ''),
(861, 'VAGNER', '0', '0', '27680319', '', 4, '', 'RUA  MARIA ROBERTA,141 CASA 2', 'V.DIVA', '02556-160', ''),
(862, 'RENATA', '0', '0', '986566897', '986566897', 4, '', 'RUA  SILVANO DE ALMEIDA, 379', 'V.SIQUEIRA', '02723-110', ''),
(863, 'SONIA', '0', '0', '984485925', '984485925', 4, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO, 271 BLO A', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(864, 'ERIKA', '0', '0', '26288842', '', 4, '', 'RUA  MARIA SETUBAL, 70', 'CASA VERDE MÉDIA', '02521-020', ''),
(865, 'MAIARA', '0', '0', '965784554', '', 4, '', 'R ALESANDRO ALORI 51 FUDOS', '', '', ''),
(866, 'BETI', '0', '0', '39665136', '39665136', 4, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO, 235 BLO', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(867, 'GEISON', '0', '0', '38585844', '38585844', 4, '', 'RUA  HORACIO VERGUEIRO RUDGE, 489', 'CASA VERDE BAIXA', '02512-060', ''),
(868, 'KARINA', '0', '0', '39659918', '', 4, '', 'RUA  GUIOMAR DA ROCHA,258', 'JD.DAS LARANJEIRAS', '02521-060', ''),
(869, 'EDUARDO', '0', '0', '976775536', '976775536', 4, '', 'RUA ESTELA BORGES 618', 'V.SIQUEIRA', '02722-000', ''),
(870, 'REGINALDO', '0', '0', '38572828', '', 4, '', 'RUA  XAVIER DE BRITO, 430', 'SÍTIO DO MORRO', '02551-000', ''),
(871, 'MADISOM', '0', '0', '39319444', '', 4, '', 'RUA  EULALIO DA COSTA CARVALHO, 516', 'JD.PEREIRA LEITE', '02712-050', ''),
(872, 'DEBORA', '0', '0', '951504393', '', 5, '', 'RUA  DOMINGOS FASOLARI,267 AP 42', 'CASA VERDE BAIXA', '02513-010', ''),
(873, 'DONIZETI', '0', '0', '988810765', '988810765', 4, '', 'FRACO MOREIRA 157', '', '', ''),
(874, 'SANDRO', '0', '0', '36280192', '36280192', 4, '', 'RUA  ESTELA BORGES MORATO,618 AP114 BROC 3', 'V.SIQUEIRA', '02722-000', ''),
(875, 'ALINE', '0', '0', '985222862', '985222862', 4, '', 'AV.D  EMILIO CARLOS,527 APT84 DEPUTADO', 'V.CRISTO REI', '02721-000', ''),
(876, 'ELLEM', '0', '0', '972102089', '', 4, '', 'AV CASA VERDE 3437 CASA', '', '', '');
INSERT INTO `clientes` (`id`, `name`, `cf1`, `cf2`, `phone`, `celular`, `taxa_de_entrega`, `email`, `endereco`, `bairro`, `cep`, `delivery`) VALUES
(877, 'FABIANA', '0', '0', '38679922', '38679922', 4, '', 'RUA  MORAIS DANTAS,174APT 11', 'V.DIVA', '02556-170', ''),
(878, 'ELIZABETI', '0', '0', '981058135', '', 3, '', 'AV.  EMILIO CARLOS,521 AP 47 BROCO C', 'V.CRISTO REI', '02721-000', ''),
(879, 'ARNALDO', '0', '0', '39653784', '39653784', 4, '', 'RUA  FRANCISCO AUGUSTO LOPES,52', 'V.BARBOSA', '02557-120', ''),
(880, 'ANA', '0', '0', '39318533', '39318533', 3, '', 'RUA  VITORIO PRIMON,138', 'BA.DO LIMÃO', '02550-050', ''),
(881, 'JULHO', '0', '0', '964917883', '', 3, '', 'SAO ROMOALDO 382   BRIBIOTECA', '', '', ''),
(882, 'FERNANDO', '0', '0', '32974684', '', 5, '', 'RUA  ANTONIO VERA CRUZ,413', 'CASA VERDE ALTA', '02555-010', ''),
(883, 'LIDIANA', '0', '0', '970198716', '970198716', 4, '', 'OTAVIO DI MOURA 477 CASA 1', 'V CAROLINA', '', ''),
(884, 'EBER', '0', '0', '38513811', '', 3, '', 'AV.  EMILIO CARLOS, 521 AP 46 BLOCO C', 'V.CRISTO REI', '02721-000', ''),
(885, 'SIMONE', '0', '0', '39324427', '', 4, '', 'RUA  LAVRAS DO SUL, 140 CASA 1', 'V.CAROLINA', '02724-030', ''),
(886, 'DIANA', '0', '0', '961577158', '', 5, '', 'RUA  DULCELINA,218', 'CASA VERDE ALTA', '02555-120', ''),
(887, 'MICHEEL', '0', '0', '48458060', '', 4, '', 'RUA  JERONIMO DOS SANTOS, 102', 'V.DIVA', '02556-090', ''),
(888, 'TATIANA', '0', '0', '39511638', '', 4, '', 'RUA  MANUEL CARLOS DE SOUZA,93', 'V.DIVA', '02557-090', ''),
(889, 'KAROL', '0', '0', '28073488', '', 4, '', 'RUA  NOEL ROSA,68', 'V.CÉLIA', '02553-020', ''),
(890, 'AMANDA', '0', '0', '39551609', '39551609', 5, '', 'RUA  GAMA CERQUEIRA,496', 'V.DIVA', '02560-070', ''),
(891, 'CARLOS', '0', '0', '39658612', '', 3, '', 'RUA  GLAUCO VELASQUEZ,240', 'SÍTIO DO MORRO', '02553-000', ''),
(892, 'MARI', '0', '0', '393566363', '', 5, '', 'RUA  ROQUE DE MORAIS,263', 'BA.DO LIMÃO', '02721-031', ''),
(893, 'RICADO', '0', '0', '38589736', '38589736', 4, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,163 B GU', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(894, 'ADRIANA', '0', '0', '966942839', '966942839', 3, '', 'AV.  MANDAQUI,153   CAP LADO ESQUERDO', 'BA.DO LIMÃO', '02550-000', ''),
(895, 'MARCOS', '0', '0', '953515518', '', 4, '', 'RUA  ANTONIO JOÃO,443 CASA 1', 'SÍTIO DO MORRO', '02553-050', ''),
(896, 'LEONADA', '0', '0', '39660618', '', 4, '', 'RUA  ANTONIO ESTIGARRIBIA,61', 'V.DIVA', '02556-030', ''),
(897, 'ELAINE', '0', '0', '39322181', '39322181', 4, '', 'RUA  CORREIA DE SA,291', 'V.CAROLINA', '02725-060', ''),
(898, 'ANA PAULA', '0', '0', '39611219', '', 3, '', 'RUA  ROQUE DE MORAIS,340 ED RONDONIA AP 1', 'BA.DO LIMÃO', '02721-030', ''),
(899, 'EUVIRA', '0', '0', '39323406', '', 5, '', 'RUA  JOÃO BATISTA LIPORONI, 38', 'CJ.RES.NO.PACAEMBU', '02722-050', ''),
(900, 'VALDINEI', '0', '0', '38573369', '38573369', 3, '', 'RUA  ROQUE DE MORAIS, 121 CASA 3', 'BA.DO LIMÃO', '02721-031', ''),
(901, 'JOSE', '0', '0', '963753810', '963753810', 3, '', 'EULALIO CARVALIO 580 GARAGEM ONIBUS', 'LIMAO', '', ''),
(902, 'LUCIANO', '0', '0', '947376798', '', 5, '', 'RUA  XIRO, 1680AP 14', 'CASA VERDE BAIXA', '02517-030', ''),
(903, 'ANGELA', '0', '0', '39668371', '', 5, '', 'RUA  FRANCO MOREIRA,173', 'V.DIVA', '02556-020', ''),
(904, 'DANIELA', '0', '0', '38079126', '', 3, '', 'AV.  MANDAQUI,275 AP 93', 'BA.DO LIMÃO', '02550-000', ''),
(905, 'EDUARDO', '0', '0', '26137612', '26137612', 5, '', 'RUA  MARIA ELISA SIQUEIRA,220 SOBRADO', 'JD.S.LUÍS', '02558-000', ''),
(906, 'AGATHA', '0', '0', '972230053', '', 5, '', 'RUA  JULIA TREVISANI GANNAN,162', 'V.BANDEIRANTES', '02555-020', ''),
(907, 'ANTONIO', '0', '0', '39612787', '', 3, '', 'SENHORA DO O 423 EDI LAPIASA PORTARIA', 'LIMAO', '', ''),
(908, 'VIRLEITE', '0', '0', '38585091', '', 3, '', 'RUA  SAMPAIO CORREA,467', 'BA.DO LIMÃO', '02710-080', ''),
(909, 'CAIO', '0', '0', '981772956', '', 3, '', 'RUA  AMADEU DANIELI FILHO,253A', 'BA.DO LIMÃO', '02550-030', ''),
(910, 'HOLANDP', '0', '0', '983227074', '', 2, '', 'BRO 1 AP 224  NU 122', '', '', ''),
(911, 'ROGERIO', '0', '0', '949158635', '949158635', 4, '', 'RUA  CAROLINA SOARES,623', 'V.DIVA', '02554-000', ''),
(912, 'NEIA', '0', '0', '39551242', '', 5, '', 'RUA  CAROLINA SOARES, 584 SOBRADO AMARELO', 'V.DIVA', '02554-000', ''),
(913, 'ROBERTO', '0', '0', '970673389', '', 5, '', 'RUA  TOMAS ANTONIO VILANI,401APT84  A', 'V.STA.MARIA', '02562-000', ''),
(914, 'LUANA', '0', '0', '38578892', '', 5, '', 'RUA  DIOGO DE OLIVEIRA,100', 'SÍTIO DO MORRO', '02553-040', ''),
(915, 'IZA', '0', '0', '39657425', '', 5, '', 'RUA  DULCELINA, 153', 'CASA VERDE ALTA', '02555-120', ''),
(916, 'DOUGLAS', '0', '0', '946762433', '', 3, '', 'RUA   JOAO MAY', 'LIMAO', '', ''),
(917, 'PRICISLA', '0', '0', '967642304', '967642304', 5, '', 'AV.  NOSSA SRA.DO O, 423 AP62 BLO1 CATRELHO', 'BA.DO LIMÃO', '02715-000', ''),
(918, 'MANOEL', '0', '0', '983855901', '983855901', 1, '', 'AV.  CELESTINO BOURROUL,890 AP 142B', 'BA.DO LIMÃO', '02710-001', ''),
(919, 'DORA', '0', '0', '38579252', '38579252', 4, '', 'JENERAL MELO RESENDI 80', '', '', ''),
(920, 'MACIO', '0', '0', '26795912', '', 3, '', 'RUA  VITORIO PRIMON,227', 'BA.DO LIMÃO', '02550-050', ''),
(921, 'CARLA', '0', '0', '39310575', '39310575', 5, '', 'RUA  JOÃO DE LEMOS,59', 'V.BRITO', '02726-140', ''),
(922, 'CAMILA', '0', '0', '957182612', '', 3, '', 'RUA  SAMPAIO CORREA,3', 'BA.DO LIMÃO', '02710-080', ''),
(923, 'CARLOS', '0', '0', '21580982', '21580982', 3, '', 'RUA  ROQUE DE MORAIS, 159 FUNDOS', 'BA.DO LIMÃO', '02721-031', ''),
(924, 'LUCIMARA', '0', '0', '36243561', '', 4, '', 'AV.  EMILIO CARLOS,631 AP 98 BROCO A', 'V.BARBOSA', '02720-000', ''),
(925, 'BIANCA', '0', '0', '963424879', '', 5, '', 'AV.  EMILIO CARLOS, 1709 CASA 1', 'JD.PRIMAVERA', '02721-100', ''),
(926, 'NAYSA', '0', '0', '969650298', '', 0, '', 'RUA  ILDEFONSO FONTOURA, CASA 2', 'LAUZANE PAULISTA', '02443-110', ''),
(927, 'LARISSA', '0', '0', '39651943', '', 4, '', 'RUA  ANTONIO JOÃO,393', 'SÍTIO DO MORRO', '02553-050', ''),
(928, 'VANEIDE', '0', '0', '38567116', '38567116', 4, '', 'RUA  MARCOS POLAIO,94', 'V.STA.MARIA', '02561-020', ''),
(929, 'CRISTIANE', '0', '0', '985907670', '985907670', 4, '', 'RUA  VASCONCELOS DE ALMEIDA,27', 'V.DIVA', '02556-010', ''),
(930, 'LEANDRO', '0', '0', '965647474', '', 3, '', 'AV.  MANDAQUI,122 BLOCO 3 APT 173', 'BA.DO LIMÃO', '02550-000', ''),
(931, 'GESSICA', '0', '0', '38579920', '38579920', 5, '', 'RUA  DIOGO DE OLIVEIRA,265 CASA 3', 'SÍTIO DO MORRO', '02553-040', ''),
(932, 'BRONA', '0', '0', '25398182', '25398182', 3, '', 'RUA  ROQUE DE MORAIS,CONJOTO DAS AMERICA 345 CNDA', 'BA.DO LIMÃO', '02721-031', ''),
(933, 'EDISON', '0', '0', '59360645', '', 3, '', 'R CORONEL JUAQUIN 450', '', '', ''),
(934, 'AIRTON', '0', '0', '986417575', '', 4, '', 'NOSSA SEORA DO O  423 AP 22 BRCO 2', '', '', ''),
(935, 'BIANCA', '0', '0', '964331280', '', 3, '', 'MATEUS MASCARENHA 341', '', '', ''),
(936, 'MARCIO', '0', '0', '973883614', '973883614', 3, '', 'ANTONIO MIGUEL 33', '', '', ''),
(937, 'VITORIA', '0', '0', '38580948', '38580948', 4, '', 'RUA  DOMINGOS TORRES,191', 'JD.DAS LARANJEIRAS', '02521-100', ''),
(938, 'ANA', '0', '0', '966486483', '966486483', 3, '', 'RUA  ARAUJO MARCONDES,255', 'BA.DO LIMÃO', '02550-080', ''),
(939, 'DANIEL', '0', '0', '962129964', '962129964', 3, '', 'PAULO VIDIGAL 271', 'LIMAO', '', ''),
(940, 'ANDRE', '0', '0', '945116761', '945116761', 5, '', 'RUA  ANTONIO VERA CRUZ,245', 'CASA VERDE ALTA', '02555-010', ''),
(941, 'DEOLITA', '0', '0', '39572118', '39572118', 4, '', 'RUA  DOMINGOS TORRES,105', 'JD.DAS LARANJEIRAS', '02521-100', ''),
(942, 'ELEM', '0', '0', '39659134', '39659134', 4, '', 'AV.  IDA KOLB,387 AP 24 BROCO 1', 'PQ.SOUZA ARANHA', '02518-000', ''),
(943, 'ROSANA', '0', '0', '34815671', '', 5, '', 'RUA  RIBEIRÃO DAS ALMAS,72', 'V.PALMEIRA', '02728-100', ''),
(944, 'DEBORA', '0', '0', '41143949', '41143949', 4, '', 'RUA  JACOFER,140APT144 TORRE3', 'JD.PEREIRA LEITE', '02712-070', ''),
(945, 'FERNANDA', '0', '0', '23861278', '', 4, '', 'RUA  JACOFER,105APT211BL2', 'JD.PEREIRA LEITE', '02712-070', ''),
(946, 'SONIA MARIA', '0', '0', '25797627', '25797627', 3, '', 'AV MANDAQUI 122 BLO4 APT32', '', '', ''),
(947, 'ANGELA', '0', '0', '35312312', '', 3, '', 'AV.  MANDAQUI,122 AP 14 BLOCO 3', 'BA.DO LIMÃO', '02550-000', ''),
(948, 'MAURICIO', '0', '0', '23890849', '', 3, '', 'AV.  CELESTINO BOURROUL,890 AP 124B', 'BA.DO LIMÃO', '02710-001', ''),
(949, 'ANA', '0', '0', '39312057', '', 5, '', 'AV.  ANTONIO MUNHOZ BONILHA,1077', 'V.CAROLINA', '02725-000', ''),
(950, 'ERISELDO', '0', '0', '38515499', '38515499', 5, '', 'RUA  BARBOSA LIMA,55 CASA A', 'JD.CACHOEIRA', '02764-000', ''),
(951, 'MANUELA', '0', '0', '945764356', '', 3, '', 'SILVA MACHADO 62', 'V CARBONE', '', ''),
(952, 'ELIZ', '0', '0', '38646809', '', 4, '', 'AV.  NOSSA SRA.DO O,423 AP152B', 'BA.DO LIMÃO', '02715-000', ''),
(953, 'ANA PAULA', '0', '0', '34862549', '', 2, '', 'TRAV.  DIVISA NOVA,  CASA 1', 'JD.PEREIRA LEITE', '02712-020', ''),
(954, 'FERNANDA', '0', '0', '947281497', '', 3, '', 'AV MANDAQUI 122 BLO3 APT194', '', '', ''),
(955, 'MONICA', '0', '0', '39511632', '39511632', 4, '', 'RUA  LUCILA, 119  AP24  BROCO 2', 'JD.DAS LARANJEIRAS', '02522-090', ''),
(956, 'ANGELO', '0', '0', '947846771', '', 3, '', 'RUA  VITORIO PRIMON,291', 'BA.DO LIMÃO', '02550-050', ''),
(957, 'NILZA', '0', '0', '39664226', '', 3, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,55 BLOK3', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(958, 'CRISTIANE', '0', '0', '39663505', '39663505', 3, '', 'TRAV.  DIVINO DE SÃO LOURENCO, 2', 'JD.PEREIRA LEITE', '02712-010', ''),
(959, 'ROBSON', '0', '0', '39615435', '39615435', 5, '', 'RUA  CESAR PENA RAMOS, 1610', 'CASA VERDE ALTA', '02563-001', ''),
(960, 'TATIANA', '0', '0', '994362636', '', 5, '', 'TRAV.  ALEXANDRE RUIZ BARRANCO,19', 'V.DIVA', '02557-100', ''),
(961, 'VITORIA', '0', '0', '38580111', '38580111', 4, '', 'AV.  NOSSA SRA.DO O, 423 AP34 BLOCO 2', 'BA.DO LIMÃO', '02715-000', ''),
(962, 'GABRIELA', '0', '0', '32139969', '32139969', 4, '', 'RUA  EPICURO,33 CASA 4', 'CASA VERDE MÉDIA', '02552-030', ''),
(963, 'RIAN', '0', '0', '995144987', '995144987', 5, '', 'RUA  ESTELA BORGES MORATO,618 APT 53 BLOCO 1', 'V.SIQUEIRA', '02722-000', ''),
(964, 'NICKELY', '0', '0', '962579470', '962579470', 5, '', 'RUA  IAPO,125', 'CASA VERDE BAIXA', '02512-020', ''),
(965, 'JULIANA', '0', '0', '23627473', '', 5, '', 'RUA  TASSO FRAGOSO, 45', 'SÍTIO DO MORRO', '02551-050', ''),
(966, 'MURILO', '0', '0', '39324488', '', 3, '', 'ROQUE DE MORAIS 430', 'LIMAO', '', ''),
(967, 'FELIPE', '0', '0', '948110922', '', 4, '', 'RUA  FLEURY SILVEIRA,384', 'CASA VERDE ALTA', '02563-010', ''),
(968, 'ISLÃ', '0', '0', '996679364', '', 3, '', 'JACOFÉ 161 BLOCO 1 APT 192', 'LIMAO', '', ''),
(969, 'MARCIO', '0', '0', '38584612', '38584612', 4, '', 'AV.  NOSSA SRA.DO O, 423 APT 47 BLOCO 1', 'BA.DO LIMÃO', '02715-000', ''),
(970, 'TAMIRIS', '0', '0', '992132544', '992132544', 3, '', 'AV.  NOSSA SRA.DO O,423 AP156 BC 2', 'BA.DO LIMÃO', '02715-000', ''),
(971, 'RICARDO', '0', '0', '950791160', '950791160', 4, '', 'AV.  EMILIO CARLOS,1010', 'V.STA.MARIA', '02720-100', ''),
(972, 'GRASAS', '0', '0', '969371036', '', 4, '', 'NOSSA SENHORA DO O 99', '', '', ''),
(973, 'FRAVIO', '0', '0', '993661596', '', 4, '', 'ISABEL DI CIQUEIRA BARROS 501', '', '', ''),
(974, 'KAIK', '0', '0', '961504764', '', 4, '', 'RUA  ODY MANOEL TEIXEIRA MUNHOZ,18A', 'JD.DAS GRAÇAS', '02714-040', ''),
(975, 'LUCAS', '0', '0', '98631184', '', 2, '', 'MANDAQUI 182', '', '', ''),
(976, 'JANAINA', '0', '0', '989748716', '989748716', 4, '', 'RUA  BALTAZAR DE QUADROS, 184 B', 'V.PALMEIRA', '02727-080', ''),
(977, 'DIEGO', '0', '0', '981160502', '981160502', 3, '', 'RUA  SEBASTIÃO MORAIS,10 A CASA 3', 'BA.DO LIMÃO', '02551-100', ''),
(978, 'RODRIGO', '0', '0', '23668403', '23668403', 4, '', 'EMGENHEIRO CAITANO ALVIS 348 ESCOLIHA D FRAMENGO', '', '', ''),
(979, 'ALINI', '0', '0', '26495655', '', 4, '', 'RUA  GAMA CERQUEIRA,31B', 'V.DIVA', '02560-070', ''),
(980, 'KAROLINI', '0', '0', '971346387', '', 3, '', 'AV.  MANDAQUI,122 AP 713  BROCO 3', 'BA.DO LIMÃO', '02550-000', ''),
(981, 'SILVIA', '0', '0', '39513649', '', 4, '', 'R ALECSOU PAL VIVILESO 305 TRAVE D JUCELINA', '', '', ''),
(982, 'ANDREA', '0', '0', '39668344', '39668344', 4, '', 'RUA  JOSE FIUZA GUIMARÃES,233 EMPRESA', 'JD.PEREIRA LEITE', '02712-060', ''),
(983, 'AMANDA', '0', '0', '948817651', '', 4, '', 'AV.  EMILIO CARLOS,631APT63 BLOA', 'V.CRISTO REI', '02721-000', ''),
(984, 'RENATA', '0', '0', '976316954', '', 3, '', 'RUA  VITORIO PRIMON,73', 'BA.DO LIMÃO', '02550-050', ''),
(985, 'RAFAELA', '0', '0', '970761630', '', 3, '', 'CASA VERDE 3453', '', '', ''),
(986, 'ANDREIA', '0', '0', '970196338', '', 5, '', 'TRAV.  BARBARA LAMADRID,', 'V.CACHOEIRINHA', '02567-090', ''),
(987, 'INDIRA', '0', '0', '39366805', '39366805', 4, '', 'RUA  JOSE KILEBER,150 AP 12  A', 'CJ.RES.NO.PACAEMBU', '02722-140', ''),
(988, 'ELOA', '0', '0', '967797279', '', 3, '', 'RUA  ANTONIO ANTUNES MACIEL, 310', 'V.BANDEIRANTES', '02552-080', ''),
(989, 'SIMONE', '0', '0', '34291252', '', 4, '', 'RUA  LEITE DE BARROS, 79', 'V.PRADO', '02558-040', ''),
(990, 'BRENO', '0', '0', '39322575', '', 5, '', 'RUA  SANTO UBALDO,28 APT13 BLOF', 'V.CAROLINA', '02725-050', ''),
(991, 'VIVIAN', '0', '0', '38610248', '', 5, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,222APT87', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(992, 'IVO', '0', '0', '39326595', '', 5, '', 'RUA  EUCLIDES MACHADO,324 APRT43 BLO1', 'JD.DAS GRAÇAS', '02713-000', ''),
(993, 'AGUINALDO', '0', '0', '39615059', '39615059', 3, '', 'RUA  JACOFER,140APT24 TORRE3', 'JD.PEREIRA LEITE', '02712-070', ''),
(994, 'CLARA', '0', '0', '992669887', '', 4, '', 'RUA  VARAN KEUTENEDJIAN, 27', 'PQ.PERUCHE', '02539-010', ''),
(995, 'SILVA', '0', '0', '954779230', '', 5, '', 'RUA  ANTONIO MIGUEL, 29', 'V.SIQUEIRA', '02723-070', ''),
(996, 'ROLNADO', '0', '0', '39337599', '39337599', 4, '', 'RUA  GABRIEL MIGLIORI, 400 POTARIA', 'JD.DAS GRAÇAS', '02712-140', ''),
(997, 'TRAFINE', '0', '0', '28380303', '', 4, '', 'RUA  MADALENA DE MADUREIRA, 166 AP 2', 'SÍTIO DO MORRO', '02551-040', ''),
(998, 'ELZA', '0', '0', '32652569', '', 3, '', 'RUA  AMADEU DANIELI FILHO,264', 'BA.DO LIMÃO', '02550-030', ''),
(999, 'YZY', '0', '0', '952119699', '', 4, '', 'RUA  ROQUE DE MORAIS,591', 'BA.DO LIMÃO', '02721-031', ''),
(1000, 'CIDA', '0', '0', '966743519', '', 4, '', 'RUA  CARLOS PORTO CARREIRO,271 AP 21 BROCO 2', 'JD.DAS GRAÇAS', '02713-030', ''),
(1001, 'FABIANA', '0', '0', '955783503', '955783503', 4, '', 'RUA  PAULO VIDIGAL VICENTE DE 199 BLO E 4 APT 11', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(1002, 'DANIELA', '0', '0', '971400073', '', 4, '', 'RUA  ROQUE DE MORAIS,340 EDFICIO PARA AP 23  BRO 3', 'BA.DO LIMÃO', '02721-030', ''),
(1003, 'ANDREIA', '0', '0', '39656723', '39656723', 3, '', 'RUA  SEBASTIÃO MORAIS, 3A FUNDOS', 'BA.DO LIMÃO', '02551-100', ''),
(1004, 'TELMA', '0', '0', '980968954', '', 3, '', 'RUA  ELVIRA BARBOSA, 159', 'V.BARBOSA', '02557-050', ''),
(1005, 'MARIA', '0', '0', '967430949', '', 5, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO, 199BLO E2', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(1006, 'ANTONIA', '0', '0', '963264168', '', 4, '', 'RUA  SÃO DINIZ,55 CASA1', 'V.DIVA', '02556-060', ''),
(1007, 'LILIANE', '0', '0', '977972337', '977972337', 4, '', 'AV.  BARUEL, 322 CASA 2', 'V.BARUEL', '02522-000', ''),
(1008, 'CAMILA', '0', '0', '39512306', '39512306', 3, '', 'RUA  VITORIO PRIMON, 91', 'BA.DO LIMÃO', '02550-050', ''),
(1009, 'CARINAS', '0', '0', '39367790', '', 5, '', 'TR.  DOURADINA, 06', 'JD.PEREIRA LEITE', '02712-040', ''),
(1010, 'ROBSON', '0', '0', '38582115', '', 4, '', 'RUA  MANUEL CARLOS DE SOUZA, 75', 'V.DIVA', '02557-090', ''),
(1011, 'TATIANA', '0', '0', '37988961', '', 4, '', 'RUA  ANTONIO JOÃO, 655 AP 84', 'SÍTIO DO MORRO', '02553-050', ''),
(1012, 'SHELON', '0', '0', '39365491', '', 3, '', 'RUA  ROQUE DE MORAIS, 375 EQUADO 12', 'BA.DO LIMÃO', '02721-031', ''),
(1013, 'REIAN', '0', '0', '38572963', '38572963', 3, '', 'RUA  SAMPAIO CORREA, 520 AP 2 A', 'BA.DO LIMÃO', '02710-080', ''),
(1014, 'SONIA', '0', '0', '39511613', '39511613', 3, '', 'RUA  SAMPAIO CORREA, 520 B3 AP 33', 'BA.DO LIMÃO', '02710-080', ''),
(1015, 'LUAN', '0', '0', '86245321', '', 4, '', 'RUA  CARLOS PORTO CARREIRO, 271 AP31 BL 3O', 'JD.DAS GRAÇAS', '02713-030', ''),
(1016, 'CASIO', '0', '0', '35899224', '', 3, '', 'AV.  NOSSA SRA.DO O, 423 AP62 BLO1', 'BA.DO LIMÃO', '02715-000', ''),
(1017, 'IGO', '0', '0', '23061544', '', 3, '', 'RUA  ROQUE DE MORAIS, 400 AP GANABARA 31', 'BA.DO LIMÃO', '02721-030', ''),
(1018, 'GUSTAVO', '0', '0', '25288741', '', 3, '', 'AV.  NOSSA SRA.DO O, 423 AOP 6 BROCO 2', 'BA.DO LIMÃO', '02715-000', ''),
(1019, 'LEIA', '0', '0', '26408382', '26408382', 4, '', 'RUA  MANUEL CARLOS DE SOUZA,18', 'V.DIVA', '02557-090', ''),
(1020, 'AMANDA', '0', '0', '992135008', '992135008', 4, '', 'RUA  EULALIO DA COSTA CARVALHO, 10', 'JD.PEREIRA LEITE', '02712-050', ''),
(1021, 'ERIVALDO', '0', '0', '984687067', '', 4, '', 'RUA  ANTONIO ESTIGARRIBIA, 199', 'V.DIVA', '02556-030', ''),
(1022, 'FABIO', '0', '0', '947310768', '', 3, '', 'CORONEL JOAQIM DE FREITAS 199', 'LIMAO', '02551-010', ''),
(1023, 'LEANDO', '0', '0', '25742340', '', 5, '', 'AV.  EMILIO CARLOS, 3555 AP 122 BLOCO A', 'CACHOEIRINHA', '02721-200', ''),
(1024, 'ROBSON', '0', '0', '39661358', '39661358', 4, '', 'RUA  JOÃO PAULO PEREIRA, 22', 'V.BARBOSA', '02557-040', ''),
(1025, 'THIAGO', '0', '0', '39312863', '', 4, '', 'RUA  ITAU,41 CASA 2', 'V.CARBONE', '02750-020', ''),
(1026, 'ADRIANA', '0', '0', '966282446', '966282446', 3, '', 'RUA  BERNARDO WRONA,389', 'BA.DO LIMÃO', '02710-060', ''),
(1027, 'ZORAIDE', '0', '0', '968001331', '968001331', 5, '', 'RUA  MANUEL CORREIA,3', 'V.PALMEIRA', '02728-050', ''),
(1028, 'JOÃO', '0', '0', '967107267', '', 4, '', 'RUA  CARAMBEI,108', 'V.STA.MARIA', '02561-080', ''),
(1029, 'INES', '0', '0', '39318115', '39318115', 4, '', 'JD.DAS GRACAS/JD.PEREIRA LEITE, 2060 BLO 4 APT 22', '', '02701-000', ''),
(1030, 'ALEX', '0', '0', '945330110', '', 3, '', 'AV.  CELESTINO BOURROUL, 684 AP151 T1', 'BA.DO LIMÃO', '02710-001', ''),
(1031, 'GIOVANA', '0', '0', '39610248', '39610248', 5, '', 'RUA  PAULO VIDIGAL VICENTE 22 APT 87 BLO C', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(1032, 'DEVID', '0', '0', '966441804', '', 5, '', 'PRESIDENTE CASTELO BRANCO 7683', '', '', ''),
(1033, 'LINDOIA', '0', '0', '38575664', '', 3, '', 'RUA  HORACIO MOURA,332', 'BA.DO LIMÃO', '02710-070', ''),
(1034, 'JOA', '0', '0', '25060751', '', 3, '', 'AV.  MANDAQUI,122 AP133 BROCO 2', 'BA.DO LIMÃO', '02550-000', ''),
(1035, 'CRISTIANI', '0', '0', '25944737', '', 4, '', 'RUA  SILVANO DE ALMEIDA,253 AP 85', 'V.SIQUEIRA', '02723-110', ''),
(1036, 'FELIPI', '0', '0', '45616909', '', 4, '', 'SAO  PROFESO GAMA CIQUEI 31D  EMTRA SAO DINIS', '', '', ''),
(1037, 'LUIS', '0', '0', '38541655', '38541655', 2, '', 'CELES 864 33 A BRO 1', '', '', ''),
(1038, 'ODAI', '0', '0', '38586351', '38586351', 4, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,271 BA1', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(1039, 'TAMIRIS', '0', '0', '984043694', '', 4, '', 'CORONEL EU CLIDIS MACHADO 203', '', '', ''),
(1040, 'JEFESOM', '0', '0', '966457626', '966457626', 3, '', 'RUA  ROSA MARCHETTI,  86', 'BA.DO LIMÃO', '02550-110', ''),
(1041, 'MAIARA', '0', '0', '26492046', '26492046', 4, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,222 AP25', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(1042, 'GIZELI', '0', '0', '954415355', '954415355', 4, '', 'AV.  MONTE CELESTE,331', 'V.STA.MARIA', '02561-000', ''),
(1043, 'EDUADO', '0', '0', '941411920', '', 4, '', 'RUA  VASCONCELOS DE ALMEIDA,257', 'V.DIVA', '02556-010', ''),
(1044, 'CIDA', '0', '0', '39516613', '', 4, '', 'AV.  EMILIO CARLOS,631 AP 12A BROC A', 'V.CRISTO REI', '02721-000', ''),
(1045, 'SUZANA', '0', '0', '39265238', '39265238', 4, '', 'RUA  PAULO ALVES DA SILVA TELLES,12', 'CJ.RES.NO.PACAEMBU', '02722-040', ''),
(1046, 'HERIKA', '0', '0', '977655885', '977655885', 5, '', 'TRA INACIO BOMFIM 18', '', '', ''),
(1047, 'PRICILA', '0', '0', '39321333', '', 3, '', 'RUA  SILVANO DE ALMEIDA,434', 'V.SIQUEIRA', '02723-110', ''),
(1048, 'GUILERMI', '0', '0', '986852632', '', 1, '', 'CELEST  890  AP 124B', '', '', ''),
(1049, 'TELMA', '0', '0', '28549085', '', 5, '', 'RUA  SILVERIO DE CARVALHO,331', 'V.CARBONE', '02752-000', ''),
(1050, 'GEOVANA', '0', '0', '977853186', '', 3, '', 'MATEUS MARCAREIA 321', '', '', ''),
(1051, 'ALEX', '0', '0', '36243502', '36243502', 4, '', 'JOA RAMALHO 1505 AP 72', '', '', ''),
(1052, 'INGUIDI', '0', '0', '975335553', '', 4, '', 'RUA  XIRO,168 AP 14', 'CASA VERDE BAIXA', '02517-030', ''),
(1053, 'NATALIA', '0', '0', '39575674', '', 3, '', 'RUA  FRANCO MOREIRA,45', 'V.DIVA', '02556-020', ''),
(1054, 'ANDREIA', '0', '0', '23623389', '', 4, '', 'RUA  JOSE FRUTUOSO DIAS,50', 'V.BANDEIRANTES', '02552-070', ''),
(1055, 'JOSIANI', '0', '0', '35391852', '35391852', 2, '', 'AV.  MANDAQUI,122 AP 102 BROCO 3', 'BA.DO LIMÃO', '02550-000', ''),
(1056, 'LUCIANA', '0', '0', '38577674', '', 5, '', 'RUA  JOSE SORIANO DE SOUSA,243 CASA 3', 'CASA VERDE ALTA', '02555-050', ''),
(1057, 'MOHAMED', '0', '0', '974386525', '974386525', 5, '', 'AV.  EMILIO CARLOS,130 CASA 2', 'V.CRISTO REI', '02721-000', ''),
(1058, 'ROSA', '0', '0', '39668394', '', 3, '', 'TR.  ANAJANE,39', 'BA.DO LIMÃO', '02550-100', ''),
(1059, 'ALEXANDRE', '0', '0', '946741171', '', 5, '', 'RUA  JOSE SORIANO DE SOUSA,213 CASA 8', 'CASA VERDE ALTA', '02555-050', ''),
(1060, 'CAMILA', '0', '0', '39320884', '39320884', 5, '', 'AV.  CLAVASIO ALVES DA SILVA,422', 'V.SIQUEIRA', '02722-030', ''),
(1061, 'MARILIA', '0', '0', '38569795', '', 5, '', 'RUA  MADALENA DE MADUREIRA,265', 'SÍTIO DO MORRO', '02551-040', ''),
(1062, 'PAULA', '0', '0', '942335951', '', 5, '', 'RUA  GUIOMAR DA ROCHA,307', 'JD.DAS LARANJEIRAS', '02521-060', ''),
(1063, 'IRA', '0', '0', '39667606', '', 4, '', 'RUA  HELIO DE LIMA CARVALHO,147', 'V.BANDEIRANTES', '02553-080', ''),
(1064, 'SILMARA', '0', '0', '972078977', '', 5, '', 'RUA  MARIA RENATA,64', 'V.DIVA', '02556-150', ''),
(1065, 'NADEILSON', '0', '0', '39319644', '39319644', 5, '', 'RUA  EULALIO DA COSTA CARVALHO,516', 'JD.PEREIRA LEITE', '02712-050', ''),
(1066, 'VINICIUS', '0', '0', '979852562', '', 5, '', 'RUA  BERNARDINO FANGANIELLO,661 FUNDOS', 'CASA VERDE BAIXA', '02512-000', ''),
(1067, 'KETY', '0', '0', '952179000', '', 4, '', 'RUA  CAROLINA SOARES,979 AP 84 CAROLINA', 'V.DIVA', '02554-000', ''),
(1068, 'VAGUINE', '0', '0', '39652023', '', 4, '', 'RUA  AUGUSTO FRANCO DE SOUZA,128', 'V.SANTISTA', '02560-060', ''),
(1069, 'RENATA', '0', '0', '23064546', '', 2, '', 'AV.  MANDAQUI,122 AP 143 BRO3', 'BA.DO LIMÃO', '02550-000', ''),
(1070, 'LUIS', '0', '0', '49829554', '', 4, '', 'RUA  MARCELINO DE CAMARGO,314', 'V.CAROLINA', '02724-040', ''),
(1071, 'CEGIO', '0', '0', '38574733', '', 4, '', 'RUA  BELISARIO CAMPANHA,443', 'CASA VERDE MÉDIA', '02521-000', ''),
(1072, 'MARIA', '0', '0', '38587934', '', 4, '', 'RUA  VASCONCELOS DE ALMEIDA,369', 'V.DIVA', '02556-010', ''),
(1073, 'AMANDA', '0', '0', '28922829', '', 3, '', 'RUA  VITORIO PRIMON,293', 'BA.DO LIMÃO', '02550-050', ''),
(1074, 'FERNANDO', '0', '0', '39314339', '39314339', 5, '', 'RUA  EMA BORTOLATO BULGARI,20', 'JD.DAS GRAÇAS', '02713-050', ''),
(1075, 'LUZIA', '0', '0', '38580821', '', 4, '', 'RUA  FRANCISCO AUGUSTO LOPES,202', 'V.BARBOSA', '02557-120', ''),
(1076, 'CECILIA', '0', '0', '38581084', '', 3, '', 'AV.  JOSE DE BRITO DE FREITAS,426', 'CASA VERDE ALTA', '02552-000', ''),
(1077, 'VERA', '0', '0', '39319576', '39319576', 3, '', 'RUA  ROQUE DE MORAIS,350APTPARA TERREO1', 'BA.DO LIMÃO', '02721-030', ''),
(1078, 'SONIA', '0', '0', '991625759', '', 3, '', 'AV MANDAQUI 122 BLO4 APT 32', 'BA.DO LIMÃO', '02550-050', ''),
(1079, 'DANIEL', '0', '0', '39366620', '', 4, '', 'RUA  EULALIO DA COSTA CARVALHO,16', 'JD.PEREIRA LEITE', '02712-050', ''),
(1080, 'RAFAEL', '0', '0', '39664732', '39664732', 3, '', 'TR.  ANAJANE,92', 'BA.DO LIMÃO', '02550-100', ''),
(1081, 'WEDER', '0', '0', '986070113', '', 5, '', 'RUA  MORAIS DANTAS,174APT42', 'V.DIVA', '02556-170', ''),
(1082, 'ROSE', '0', '0', '39551883', '', 3, '', 'DEPURAD EMILIO CARLOS 221', '', '', ''),
(1083, 'ALAN', '0', '0', '970212694', '970212694', 4, '', 'RUA  FREDERICO PENTEADO JR.,144', 'JD.DAS LARANJEIRAS', '02517-100', ''),
(1084, 'EDISON', '0', '0', '951464008', '', 4, '', 'RUA  ELVIRA BARBOSA,212', 'V.BARBOSA', '02557-050', ''),
(1085, 'MICHAEL', '0', '0', '952005851', '952005851', 4, '', 'TR.  FRANCISCO ADRIANI,34', 'V.SANTISTA', '02560-180', ''),
(1086, 'WISOM', '0', '0', '999280645', '', 3, '', 'NOSSASEORA D O 669', '', '', ''),
(1087, 'EDILEUSA', '0', '0', '21970666', '', 5, '', 'DR BUBENS MEIRELES 477', '', '', ''),
(1088, 'RENATA  EDNALDO', '0', '0', '83002604', '', 3, '', 'RUA  SAMPAIO CORREA,7 CASA', 'BA.DO LIMÃO', '02710-080', ''),
(1089, 'SIGIO', '0', '0', '953535989', '', 3, '', 'ENGEIRO 1997', '', '', ''),
(1090, 'ALINE', '0', '0', '981050726', '', 4, '', 'AV.  JOSE DE BRITO DE FREITAS, 729', 'CASA VERDE ALTA', '02552-000', ''),
(1091, 'AMANDA', '0', '0', '965596587', '', 5, '', 'AV.  CASA VERDE, 3522', 'V.ESTER', '02520-300', ''),
(1092, 'BARBARA', '0', '0', '39614809', '', 5, '', 'TR. LUIS SÁ 139', '', '', ''),
(1093, 'KATIA', '0', '0', '39514765', '39514765', 4, '', 'TR.  MARIA LUCIA,12 CASA 9', 'V.BARBOSA', '02557-030', ''),
(1094, 'SONIA', '0', '0', '987178202', '987178202', 4, '', 'AV.  SEBASTIÃO HENRIQUES,658', 'V.SIQUEIRA', '02723-050', ''),
(1095, 'ROBLES', '0', '0', '39350269', '', 4, '', 'RUA  SANTO UBALDO,28 APT 54 BLOCO E', 'V.CAROLINA', '02725-050', ''),
(1096, 'ROGERIO', '0', '0', '965384085', '', 4, '', 'RUA  BARTOLOMEU DO CANTO,165 APT 14', 'V.PALMEIRA', '02726-090', ''),
(1097, 'EDJALVA', '0', '0', '948196768', '', 3, '', 'RUA  SAMPAIO CORREA, 449', 'BA.DO LIMÃO', '02710-080', ''),
(1098, 'TATIANE', '0', '0', '38587680', '', 4, '', 'TRAV.  BALTAZAR ECHAVE,28', 'V.BARBOSA', '02557-070', ''),
(1099, 'LANA', '0', '0', '39610828', '', 4, '', 'RUA  MARIA ELISA SIQUEIRA, 32', 'JD.S.LUÍS', '02558-000', ''),
(1100, 'GESSICA', '0', '0', '69377483', '69377483', 4, '', 'RUA  ANTONIO MIGUEL,34', 'V.SIQUEIRA', '02723-070', ''),
(1101, 'MARLON', '0', '0', '37575000', '', 5, '', 'JOSE SVAKALL 71 EMPRESA TRANS VIP', '', '', ''),
(1102, 'ANDREIA', '0', '0', '38574262', '', 3, '', 'RUA  BERNARDO WRONA,408 FUNDOS', 'BA.DO LIMÃO', '02710-060', ''),
(1103, 'RENATA', '0', '0', '985313463', '985313463', 4, '', 'RUA  MARIA ELISA SIQUEIRA, 320', 'JD.S.LUÍS', '02558-000', ''),
(1104, 'ALESANDRO E GESSICA', '0', '0', '986555547', '986555547', 5, '', 'RUA PAULO VIDIGAL VICENTE DE AZE 271 BLO A1 APT S1', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(1105, 'DANILO', '0', '0', '979776446', '', 3, '', 'RUA ALGUSTO FRANCO DE SOUSA 100', '', '', ''),
(1106, 'SAMUEL', '0', '0', '940024010', '940024010', 4, '', 'PORTARIA DA RAIDIO ATUAL', '', '', ''),
(1107, 'CLEUSA', '0', '0', '38577056', '38577056', 4, '', 'RUA  GUIOMAR DA ROCHA,170', 'JD.DAS LARANJEIRAS', '02521-060', ''),
(1108, 'JOSE CALOS', '0', '0', '32716852', '', 4, '', 'RUA  SILVANO DE ALMEIDA,438', 'V.SIQUEIRA', '02723-110', ''),
(1109, 'CIDA', '0', '0', '38569485', '', 4, '', 'RUA  EDMUNDO DANTAS,83', 'CJ.RES.NO.PACAEMBU', '02721-020', ''),
(1110, 'SAMUEL FRADI', '0', '0', '39666076', '', 4, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,235 BROC', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(1111, 'RAIANI', '0', '0', '943050167', '', 4, '', 'BALTAZA ABADAL 63', '', '', ''),
(1112, 'ERICA', '0', '0', '977655887', '', 4, '', 'TRAVEESA INACIO BOM FIM 18', '', '', ''),
(1113, 'LARISSA', '0', '0', '977655866', '', 4, '', 'JD.DAS GRACAS/JD.PEREIRA LEITE,BLO3APT21', '', '02701-000', ''),
(1114, 'FERNADO', '0', '0', '954148419', '', 4, '', 'R ALVARO SILVA 211 CASA3', '', '', ''),
(1115, 'ELANI', '0', '0', '954664818', '954664818', 5, '', 'TRAV.  BOCAIUVA DO SUL, 2 CAPAINHA 3', 'V.BARBOSA', '02556-041', ''),
(1116, 'RODRIGO', '0', '0', '38589992', '', 3, '', 'AV.  MANDAQUI, 122 AP162 BLO 4', 'BA.DO LIMÃO', '02550-000', ''),
(1117, 'AILTON', '0', '0', '948061724', '948061724', 5, '', 'FRANCISCA BERIBA 117', 'SANTA TEREZINHA', '', ''),
(1118, 'SAMARA', '0', '0', '976302095', '', 5, '', 'RUA  SÃO ROMUALDO, 109', 'V.BARBOSA', '02557-060', ''),
(1119, 'CIMONI', '0', '0', '944320510', '', 1, '', 'AV.  CELESTINO BOURROUL,890 BROCO B AP 232', 'BA.DO LIMÃO', '02710-001', ''),
(1120, 'ELANI', '0', '0', '38568629', '38568629', 3, '', 'AV.  MANDAQUI,275 AP 91', 'BA.DO LIMÃO', '02550-000', ''),
(1121, 'SILVIA', '0', '0', '23590919', '', 4, '', 'AV.  EMILIO CARLOS,631 AP85 B', 'V.CRISTO REI', '02721-000', ''),
(1122, 'ANA CAROLINA', '0', '0', '39652521', '39652521', 4, '', 'RUA  ANDREA DEL CASTAGNO,230', 'V.STA.MARIA', '02562-010', ''),
(1123, 'LIANA', '0', '0', '947439004', '', 5, '', 'RUA  NICOLAS,41', 'JD.PRIMAVERA', '02756-150', ''),
(1124, 'CLAUDIO', '0', '0', '26135813', '26135813', 4, '', 'RUA  ROQUE DE MORAIS,340 AP3 BRCO PARAIBA', 'BA.DO LIMÃO', '02721-030', ''),
(1125, 'MARCOS', '0', '0', '980992077', '', 4, '', 'TR.  ESPIRITO SANTO DO PINHAL,15', 'V.BARBOSA', '02557-140', ''),
(1126, 'ROGERIO', '0', '0', '38586551', '38586551', 4, '', 'RUA  MANUEL CARLOS DE SOUZA,69', 'V.DIVA', '02557-090', ''),
(1127, 'LUCAS', '0', '0', '965175560', '', 3, '', 'BERNARDO WUNA 339  LANCI', '', '', ''),
(1128, 'ADRIANI', '0', '0', '979944616', '', 4, '', 'RUA  CASTRINOPOLIS,43', 'CASA VERDE ALTA', '02561-100', ''),
(1129, 'GABRIEL', '0', '0', '952728330', '952728330', 4, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,222', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(1130, 'FELIPI', '0', '0', '962126496', '', 4, '', 'RUA  FLEURY SILVEIRA,384', 'CASA VERDE ALTA', '02563-010', ''),
(1131, 'RODRIGO', '0', '0', '953208663', '', 3, '', 'RUA  AMADEU DANIELI FILHO,276', 'BA.DO LIMÃO', '02550-030', ''),
(1132, 'ADRIANA', '0', '0', '980901015', '', 4, '', 'RUA  DOMINGOS TORRES,151', 'JD.DAS LARANJEIRAS', '02521-100', ''),
(1133, 'ALESSANDRO', '0', '0', '961916860', '', 4, '', 'RUA  CESAR PENA RAMOS,1610', 'CASA VERDE ALTA', '02563-001', ''),
(1134, 'EVILIN', '0', '0', '971467327', '971467327', 5, '', 'RUA  MARIA RENATA,71', 'V.DIVA', '02556-150', ''),
(1135, 'VITORIA', '0', '0', '22395032', '', 5, '', 'RUA  FERNANDO MACHADO,100', 'V.ESPANHOLA', '02566-070', ''),
(1136, 'VALDINEI', '0', '0', '38573669', '', 3, '', 'RUA  ROQUE DE MORAIS,121 CAMPAINHA 3', 'BA.DO LIMÃO', '02721-031', ''),
(1137, 'VICTORIA', '0', '0', '982214323', '', 4, '', 'RUA ANTONIO JOAO 369', 'CASA VERDE', '', ''),
(1138, 'ESTELA', '0', '0', '949739760', '', 5, '', 'RUA  MIRANDA DA SILVA REIS,86 CASA 4', 'JD.TABOR', '02751-040', ''),
(1139, 'CRISTINA', '0', '0', '981867106', '', 5, '', 'RUA  JORGE BRAND,125 CASA 8', 'V.BARUEL', '02522-020', ''),
(1140, 'GISIA', '0', '0', '39513558', '', 4, '', 'RUA  HENRIQUE CASTANHO,34 PORTAO CINZA GRANDE', 'CASA VERDE ALTA', '02563-110', ''),
(1141, 'KETLIN', '0', '0', '952246824', '', 5, '', 'RUA  MANUEL DIAS DO CAMPO,148', 'CASA VERDE ALTA', '02564-010', ''),
(1142, 'THAIS', '0', '0', '38568411', '', 5, '', 'RUA  VILMA DE OLIVEIRA VIEIRA,31', 'CASA VERDE MÉDIA', '02530-041', ''),
(1143, 'GIOVANA', '0', '0', '39362391', '', 5, '', 'AV.  SEBASTIÃO HENRIQUES,815', 'V.SIQUEIRA', '02723-050', ''),
(1144, 'CAROLINA', '0', '0', '38589300', '38589300', 5, '', 'RUA  ELVIRA BARBOSA,170', 'V.BARBOSA', '02557-050', ''),
(1145, 'BRUNA', '0', '0', '991306736', '991306736', 5, '', 'RUA  VASCONCELOS DE ALMEIDA,13', 'V.DIVA', '02556-010', ''),
(1146, 'LUIZ FERNANDO', '0', '0', '956071520', '956071520', 4, '', 'RUA FRANCISCO RODRIGUES NUNES 34 CASA 7', 'LIMAO', '', ''),
(1147, 'ODAI', '0', '0', '39586351', '39586351', 4, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,271', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(1148, 'ANDESON', '0', '0', '940059800', '', 4, '', 'OTAVIANO ALVIS DI LIMA 3600 BROCO 6 AP 43', '', '', ''),
(1149, 'ARI', '0', '0', '39656129', '', 4, '', 'RUA  JERONIMO DOS SANTOS,46', 'V.DIVA', '02556-090', ''),
(1150, 'BRUNO', '0', '0', '38588263', '', 4, '', 'RUA  VASCONCELOS DE ALMEIDA,61', 'V.DIVA', '02556-010', ''),
(1151, 'CHILEI', '0', '0', '39512931', '39512931', 4, '', 'RUA  SAMARITA,898  BROCO 11  AP 34', 'PQ.SOUZA ARANHA', '02518-080', ''),
(1152, 'CARLA', '0', '0', '38578435', '38578435', 4, '', 'RUA  PAULO VIDIGAL VICENTE 271 BLO B2 APT1 2 S2', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(1153, 'EDNA', '0', '0', '22392292', '', 4, '', 'RUA  ESTELA BORGES MORATO,618 APT43 TORRE1', 'V.SIQUEIRA', '02722-000', ''),
(1154, 'RENATO', '0', '0', '34777841', '', 4, '', 'RUA  ATENAGORAS,152', 'V.BARBOSA', '02557-000', ''),
(1155, 'PAULO ROBERTO', '0', '0', '37296938', '37296938', 3, '', 'RUA  ROQUE DE MORAIS,345 APT32 BLO PANAMA', 'BA.DO LIMÃO', '02721-031', ''),
(1156, 'VAGNER', '0', '0', '946427891', '', 4, '', 'RUA  BENIGNO,226', 'JD.PRIMAVERA', '02754-040', ''),
(1157, 'MILANA', '0', '0', '45671007', '', 4, '', 'RUA  JACOFER,161 AP 93 TORI 1', 'JD.PEREIRA LEITE', '02712-070', ''),
(1158, 'ADIL', '0', '0', '38589048', '', 5, '', 'RUA  SAMARITA,147', 'PQ.SOUZA ARANHA', '02518-080', ''),
(1159, 'FABIO', '0', '0', '947869997', '947869997', 3, '', 'DIOGO DI OLIVEIRA 74', '', '', ''),
(1160, 'REGINALDO', '0', '0', '940310813', '', 3, '', 'RUA BENADO WUNA 408', '', '', ''),
(1161, 'CRISTIANI', '0', '0', '983016515', '', 4, '', 'RUA  LAGOA AZUL,240', 'V.DIVA', '02556-000', ''),
(1162, 'MARCIO', '0', '0', '947955586', '', 3, '', 'EURARIO COSTA CAVALIO  560 ENPERSA DE ONI', '', '', ''),
(1163, 'ANDRE', '0', '0', '990078509', '990078509', 5, '', 'RUA  JOÃO BATISTA LIPORONI,96', 'CJ.RES.NO.PACAEMBU', '02722-050', ''),
(1164, 'REGINALDO', '0', '0', '995139999', '', 5, '', 'RUA  ROCHA LIMA,461', 'SÍTIO DO MORRO', '02554-010', ''),
(1165, 'MOISES', '0', '0', '986667033', '', 3, '', 'RUA IRENOPOLIS 161 ENFRETE A NOSSA SENHORA DO Ó', '', '', ''),
(1166, 'EDSON', '0', '0', '985896521', '985896521', 5, '', 'AV.  VITORIA,67', 'CASA VERDE MÉDIA', '02552-010', ''),
(1167, 'MARCUS 3', '0', '0', '37135339', '', 5, '', 'RUA  ATENAGORAS,199', 'V.BARBOSA', '02557-000', ''),
(1168, 'GABRIELY', '0', '0', '949680524', '', 4, '', 'AV.  ANTONIO MUNHOZ BONILHA,537', 'V.CAROLINA', '02725-000', ''),
(1169, 'VIVIAN', '0', '0', '25289741', '', 4, '', 'RUAPAULO VIDIGAL VICENTE DE AZEV 222 APT 124 BL C', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(1170, 'FLAVIANA', '0', '0', '945777474', '945777474', 2, '', 'AV.  CELESTINO BOURROUL,684 APT 151 BLO 1', 'BA.DO LIMÃO', '02710-001', ''),
(1171, 'NOEDISON', '0', '0', '39313901', '', 4, '', 'RUA CLAVASIO ALVIS DA SILVA 215', '', '', ''),
(1172, 'GESICA', '0', '0', '998035713', '', 4, '', 'RUA  DIOGO DE OLIVEIRA,265', 'SÍTIO DO MORRO', '02553-040', ''),
(1173, 'VALTE', '0', '0', '999551685', '', 2, '', 'AV.  CELESTINO BOURROUL,6840TORI 1 AP 173', 'BA.DO LIMÃO', '02710-001', ''),
(1174, 'JEFESOM', '0', '0', '941117228', '', 4, '', 'RUA  BALTAZAR DE QUADROS,26 CASA 2', 'V.PALMEIRA', '02727-080', ''),
(1175, 'GABRIEL', '0', '0', '38815024', '', 4, '', 'RUA  ROQUE DE MORAIS,345 AP 22 ESTADU UNIDOS', 'BA.DO LIMÃO', '02721-031', ''),
(1176, 'RENAN', '0', '0', '949983384', '949983384', 5, '', 'RUA SANTA ANGELA  338 FUNDOS', '', '', ''),
(1177, 'ROGERIO', '0', '0', '966582587', '', 3, '', 'MANDA QUI 345', '', '', ''),
(1178, 'ANDESOM', '0', '0', '39575113', '', 3, '', 'RUA  ARAUJO MARCONDES,167', 'BA.DO LIMÃO', '02550-080', ''),
(1179, 'VINICIUS', '0', '0', '974985564', '', 5, '', 'AV.  CLAVASIO ALVES DA SILVA,525 AP 23 B', 'V.SIQUEIRA', '02722-030', ''),
(1180, 'RAQUEL', '0', '0', '998496124', '', 5, '', 'RUA JOSE FRUTUOSO DIAS , 200 CASA 2', 'TERMINAL CASA VERDE', '', ''),
(1181, 'ITAMAR', '0', '0', '39318503', '', 5, '', 'RUA  NELSON FRANCISCO,224 QUADRA DE FUTEBOL', 'JD.DAS GRAÇAS', '02712-100', ''),
(1182, 'THAIS', '0', '0', '986183280', '986183280', 3, '', 'AV.  NOSSA SRA.DO O,423 AP 61 BLOCO 1', 'BA.DO LIMÃO', '02715-000', ''),
(1183, 'WILLIAN', '0', '0', '945281451', '', 5, '', 'RUA  ZANZIBAR,687 ATRAS DO PAO DE ACUCAR', 'CASA VERDE BAIXA', '02512-010', ''),
(1184, 'EDUARDO', '0', '0', '38549362', '', 5, '', 'AV.  ORDEM E PROGRESSO,1236 AP 14 B', 'CASA VERDE BAIXA', '02518-130', ''),
(1185, 'JEFFERSON', '0', '0', '39324083', '', 5, '', 'RUA  OSVALDO CASELLA,16', 'CJ.RES.NO.PACAEMBU', '02722-150', ''),
(1186, 'LEANDRO', '0', '0', '43015449', '', 5, '', 'RUA  SILVA MACHADO,151', 'V.CAROLINA', '02750-080', ''),
(1187, 'MARGARIDA', '0', '0', '26673713', '26673713', 5, '', 'RUA  DOMICIANO RIBEIRO,758A', 'V.ESPANHOLA', '02565-090', ''),
(1188, 'SAULO', '0', '0', '23089567', '', 3, '', 'RUA  SEBASTIÃO MORAIS,1B', 'BA.DO LIMÃO', '02551-100', ''),
(1189, 'CRISTINA', '0', '0', '23380278', '', 4, '', 'RUA  ESTANISLAU DE CAMPOS,140', 'V.CAROLINA', '02724-050', ''),
(1190, 'GISLANI', '0', '0', '39610070', '', 3, '', 'RUA  CAROLINA SOARES,299', 'V.DIVA', '02554-000', ''),
(1191, 'RENATA', '0', '0', '32970865', '32970865', 5, '', 'RUA  NILCEO MARQUES CASTRO,64 CAMPAINHA2', 'V.STA.MARIA', '02562-100', ''),
(1192, 'CAROLINI', '0', '0', '979551797', '', 4, '', 'RUA  IMARUI,100', 'V.MARISBELA', '02762-050', ''),
(1193, 'EMOA', '0', '0', '39611035', '', 4, '', 'RUA  ANTONIO ANTUNES MACIEL,310', 'V.BANDEIRANTES', '02552-080', ''),
(1194, 'IRENI', '0', '0', '39314156', '', 4, '', 'RUA  VICENTE FERREIRA LEITE,569', 'V.SIQUEIRA', '02723-000', ''),
(1195, 'OTAVIO', '0', '0', '970765068', '', 5, '', 'AV.  SEBASTIÃO HENRIQUES,636', 'V.SIQUEIRA', '02723-050', ''),
(1196, 'ADALBERTO', '0', '0', '35966198', '35966198', 5, '', 'RUA  ROBERTO CAVALHEIRO BRISOLLA,82', 'CASA VERDE ALTA', '02563-070', ''),
(1197, 'RICARDO', '0', '0', '39325897', '39325897', 5, '', 'RUA  RIBEIRÃO DAS ALMAS,255', 'V.PALMEIRA', '02728-100', ''),
(1198, 'CAIO', '0', '0', '38586658', '38586658', 5, '', 'RUA  ROCHA LIMA,246', 'SÍTIO DO MORRO', '02554-010', ''),
(1199, 'TALIA', '0', '0', '977587602', '', 4, '', 'HORACIO MOURA 83', '', '', ''),
(1200, 'LAIZ', '0', '0', '39315725', '39315725', 4, '', 'CARLOS PORTO CARREIRO 315 BLO 5 APT 14', 'LIMAO', '', ''),
(1201, 'CARLOS', '0', '0', '23722004', '', 4, '', 'RUA  ZANZIBAR,687', 'CASA VERDE BAIXA', '02512-010', ''),
(1202, 'KATIA', '0', '0', '38585528', '', 5, '', 'RUA  GLAUCO VELASQUEZ,709', 'SÍTIO DO MORRO', '02553-000', ''),
(1203, 'PRICILA', '0', '0', '39657924', '', 3, '', 'RUA  VITORIO PRIMON,215', 'BA.DO LIMÃO', '02550-050', ''),
(1204, 'JULIO', '0', '0', '39508084', '', 4, '', 'RUA  EULALIO DA COSTA CARVALHO,860', 'JD.PEREIRA LEITE', '02712-050', ''),
(1205, 'ALINE', '0', '0', '33756747', '33756747', 3, '', 'RUA  ROQUE DE MORAIS,47', 'BA.DO LIMÃO', '02721-031', ''),
(1206, 'JULIO', '0', '0', '39661175', '39661175', 5, '', 'RUA  JOSE RANGEL DE CAMARGO,155', 'PQ.PERUCHE', '02538-010', ''),
(1207, 'KAIO', '0', '0', '946470082', '', 3, '', 'RUA  MARCELINO DE CAMARGO,189', 'V.CAROLINA', '02724-040', ''),
(1208, 'SDRIANA', '0', '0', '39229391', '', 3, '', 'AV.  EMILIO CARLOS,521 APT 118A', 'LIMAO', '02721-000', ''),
(1209, 'JEAN', '0', '0', '41061473', '', 5, '', 'RUA  SALVADOR PADILHA GIMENES,104', 'V.STA.MARIA', '02562-130', ''),
(1210, 'MARIA', '0', '0', '994740574', '', 3, '', 'EULALIO DA COSTA CARVALHIO 560', '', '', ''),
(1211, 'VANESSA', '0', '0', '39354467', '', 5, '', 'RUA   SANGIRARDI,CORREGO 171', 'V.SIQUEIRA', '02723-090', ''),
(1212, 'LUZINETE', '0', '0', '39514603', '', 2, '', 'RUA  AMADEU DANIELI FILHO,210', 'BA.DO LIMÃO', '02550-030', ''),
(1213, 'FABIO', '0', '0', '975613765', '975613765', 4, '', 'AV.  SEBASTIÃO HENRIQUES,300 AP 1', 'V.SIQUEIRA', '02723-050', ''),
(1214, 'HUGO', '0', '0', '38536563', '', 3, '', 'NOSSA SENHORA DO O 423 APT87 BLO2', 'LIMAO', '', ''),
(1215, 'CLAUDEMI', '0', '0', '39361434', '39361434', 4, '', 'AV.  SEBASTIÃO HENRIQUES,300APT82', 'V.SIQUEIRA', '02723-050', ''),
(1216, 'LUDIS', '0', '0', '39368324', '', 4, '', 'RUA  ROQUE DE MORAIS,438 AP 31 CEARA', 'BA.DO LIMÃO', '02721-030', ''),
(1217, 'MAURICIO', '0', '0', '39652072', '', 4, '', 'RUA  DIRCE RODRIGUES,98', 'V.DIVA', '02556-080', ''),
(1218, 'KELI', '0', '0', '38560559', '', 4, '', 'RUA  XAVIER DE BRITO,545', 'SÍTIO DO MORRO', '02551-000', ''),
(1219, 'VAGUINE', '0', '0', '38716054', '38716054', 5, '', 'RUA ADRIANO JOSE MARTINI  32', '', '', ''),
(1220, 'JADESON', '0', '0', '964413602', '', 4, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,163 BH3', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(1221, 'MARIA', '0', '0', '39613451', '', 4, '', 'RUA  CAROLINA SOARES,12', 'V.DIVA', '02554-000', ''),
(1222, 'ALINI', '0', '0', '39581851', '', 4, '', 'TRAV.  BALTAZAR ECHAVE,60', 'V.BARBOSA', '02557-070', ''),
(1223, 'JANI', '0', '0', '23050177', '23050177', 4, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,222', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(1224, 'WIARA', '0', '0', '39328874', '', 5, '', 'RUA  PIRACANJUBA,83 CASA 4', 'V.CAROLINA', '02750-040', ''),
(1225, 'CAROL', '0', '0', '23698560', '', 3, '', 'MANOEL CARLOS DE SOUSA 93', 'LIMAO', '', ''),
(1226, 'AGATA', '0', '0', '33849138', '', 4, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,271', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(1227, 'FABIO', '0', '0', '962371401', '962371401', 4, '', 'TRAVESA MIGUEL SUFIA 16', '', '', ''),
(1228, 'DHENIFFER', '0', '0', '944225043', '', 4, '', 'RUA  EUCLIDES MACHADO,', 'JD.DAS GRAÇAS', '02713-000', ''),
(1229, 'JOSUÉ', '0', '0', '988041245', '', 4, '', 'RUA  CAROLINA SOARES,979 APT 84 B', 'V.DIVA', '02554-000', ''),
(1230, 'NAYARA', '0', '0', '995142175', '', 5, '', 'RUA  ALVARO SILVA,242', 'V.SIQUEIRA', '02723-020', ''),
(1231, 'ANDREA', '0', '0', '39366372', '39366372', 3, '', 'RUA  ROQUE DE MORAIS,340 EDF SAO PALO APT T 2', 'BA.DO LIMÃO', '02721-030', ''),
(1232, 'CIRILO', '0', '0', '946463533', '', 5, '', 'RUA CORENAL CASSIOS DE BARROSS 80', '', '', ''),
(1233, 'PAULO', '0', '0', '983343295', '', 5, '', 'CASA VERDE BAIXA/LIMÃO,109', '', '02501-000', ''),
(1234, 'CAMILA', '0', '0', '952367837', '', 5, '', 'RUA NELSON FRANCISCO 29', '', '', ''),
(1235, 'FABIO', '0', '0', '974628080', '', 3, '', 'RUA  ROQUE DE MORAIS,312 EDF PIAUI APT 34', 'BA.DO LIMÃO', '02721-030', ''),
(1236, 'VITORIA', '0', '0', '985740456', '', 4, '', 'RUA  GAMA CERQUEIRA,36 CASA 2', 'V.DIVA', '02560-070', ''),
(1237, 'JACKLINE', '0', '0', '965896190', '965896190', 5, '', 'RUA MANUEL CARLOS DE SOUSA 101', '', '', ''),
(1238, 'CRISTIANI', '0', '0', '985825427', '', 3, '', 'AV.  NOSSA SRA.DO O,423 AP 95 BROCO 1', 'BA.DO LIMÃO', '02715-000', ''),
(1239, 'CICILIA', '0', '0', '39361355', '', 4, '', 'RUA  ROQUE DE MORAIS,340 EDIFICIO PARAIBA AP 22', 'BA.DO LIMÃO', '02721-030', ''),
(1240, 'MILENA', '0', '0', '45621007', '45621007', 4, '', 'RUA  JACOFER,161  AP93 TORI 1', 'JD.PEREIRA LEITE', '02712-070', ''),
(1241, 'CHILEI', '0', '0', '38537697', '38537697', 3, '', 'RUA  VITORIO PRIMON,188', 'BA.DO LIMÃO', '02550-050', ''),
(1242, 'ROBERTO', '0', '0', '995794370', '', 3, '', 'TR.  ANAJANE,71', 'BA.DO LIMÃO', '02550-100', ''),
(1243, 'MAGUIDA', '0', '0', '39610009', '39610009', 4, '', 'RUA  MACIEIRAS,128', 'JD.DAS LARANJEIRAS', '02521-090', ''),
(1244, 'SARA', '0', '0', '38571142', '', 4, '', 'AV.  JOSE DE BRITO DE FREITAS,73', 'CASA VERDE ALTA', '02552-000', ''),
(1245, 'MAURICIO', '0', '0', '86852632', '', 2, '', 'CELESTINO BURROU 890 AP 24B TORI 2', '', '', ''),
(1246, 'JAN', '0', '0', '940556356', '', 4, '', 'RUA  MATEUS MASCARENHAS,363', 'JD.PEREIRA LEITE', '02712-000', ''),
(1247, 'JANAINA', '0', '0', '965023703', '', 3, '', 'RUA   MANOEL CALOS DI SOUSA 113 FUNDOS', '', '', ''),
(1248, 'GRASIELI', '0', '0', '947619008', '', 3, '', 'EMGENEIRO 651', '', '', ''),
(1249, 'VALTER', '0', '0', '39657758', '', 4, '', 'RUA  GONCALVES FIGUEIRA,262', 'V.BANDEIRANTES', '02552-140', ''),
(1250, 'VIVIANI', '0', '0', '38570635', '', 3, '', 'AV.  EMILIO CARLOS,521 AP114 BROC A', 'V.CRISTO REI', '02721-000', ''),
(1251, 'PEDRO LUIS', '0', '0', '982034398', '', 3, '', 'RUA PIRACANJUBA  183  CASA2', '', '', ''),
(1252, 'WELEI', '0', '0', '38583869', '', 4, '', 'RUA  MARCOS POLAIO,81', 'V.STA.MARIA', '02561-020', ''),
(1253, 'FABIANA', '0', '0', '22362254', '', 4, '', 'RUA  TOMAS ANTONIO VILANI,41 AP 53A', 'V.STA.MARIA', '02562-000', ''),
(1254, 'ROSANGELA', '0', '0', '966553080', '', 5, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,271 APT22', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(1255, 'RUBEM', '0', '0', '39353288', '', 5, '', 'RUA  ANTONIO PIRES,254 CASA 9', 'V.ALBERTINA', '02730-000', ''),
(1256, 'EDNA', '0', '0', '39352983', '39352983', 4, '', 'RUA  ROQUE DE MORAIS,436 AP 24 ED. ESPIRITO SANTO', 'BA.DO LIMÃO', '02721-030', ''),
(1257, 'FELIPI', '0', '0', '982367223', '982367223', 6, '', 'RUA BONIFACIO DI ANDRADA 147', '', '', ''),
(1258, 'EVILIN', '0', '0', '971462327', '', 5, '', 'RUA  MARIA RENATA,71', 'V.DIVA', '02556-150', ''),
(1259, 'GILMARA', '0', '0', '979875713', '', 3, '', 'CELESTINO 684 684 TORRE3 ATR4', '', '', ''),
(1260, 'EBER', '0', '0', '987088276', '', 4, '', 'AV.  EMILIO CARLOS,521APT46 C', 'V.CRISTO REI', '02721-000', ''),
(1261, 'ROSE', '0', '0', '39664948', '39664948', 5, '', 'RUA  ANTONIO ESTIGARRIBIA,61', 'V.DIVA', '02556-030', ''),
(1262, 'ALVARO', '0', '0', '965035210', '', 3, '', 'AV.  NOSSA SRA.DO O,423 PORTARIA', 'BA.DO LIMÃO', '02715-000', ''),
(1263, 'ALEXANDRE', '0', '0', '39517354', '39517354', 5, '', 'RUA  JACOFER,105 AP212 BLOCO B', 'JD.PEREIRA LEITE', '02712-070', ''),
(1264, 'SILMARA', '0', '0', '39316506', '', 5, '', 'RUA  ABAIBAS,260', 'V.PALMEIRA', '02728-080', ''),
(1265, 'KEILA', '0', '0', '947261071', '', 5, '', 'BRIGADEIRO XAVIER DE BRITO 225', 'SITIO MORRO', '02551-010', ''),
(1266, 'RAFAEL', '0', '0', '38575674', '38575674', 5, '', 'RUA  FRANCO MOREIRA,45', 'V.DIVA', '02556-020', ''),
(1267, 'BEATRIZ', '0', '0', '986794900', '', 3, '', 'DIOGO DE OLIVEIRA 279', '', '', ''),
(1268, 'LUCI', '0', '0', '49817256', '49817256', 5, '', 'RUA  FRANCISCO RODRIGUES NUNES,34 CASA 7', 'JD.DAS GRAÇAS', '02712-090', ''),
(1269, 'GIUMARA', '0', '0', '970257917', '970257917', 5, '', 'AV.  VITORIA,19 CASA 2 FUNDOS', 'CASA VERDE MÉDIA', '02552-010', ''),
(1270, 'JAILTON', '0', '0', '963911597', '', 5, '', 'RUA  BENEDETTO BONFIGLI,755B', 'CASA VERDE ALTA', '02564-040', ''),
(1271, 'MONICA', '0', '0', '959877844', '', 5, '', 'AV.  SEBASTIÃO HENRIQUES,300APT142', 'V.SIQUEIRA', '02723-050', ''),
(1272, 'DANILO', '0', '0', '23386096', '23386096', 3, '', 'ENGENHEIRO CAETANO  ALVES 1822 CASA 3', '', '', ''),
(1273, 'SONETE', '0', '0', '39322262', '', 5, '', 'RUA  CAMPOS DO CAJURU,104', 'V.PALMEIRA', '02726-100', ''),
(1274, 'ELISANGELA', '0', '0', '951088400', '', 5, '', 'AV.  VITORIA,BARAO DA VITORIA 67', 'CASA VERDE MÉDIA', '02552-010', ''),
(1275, 'KAREM', '0', '0', '39511821', '', 4, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,235BLOC', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(1276, 'EDLEUZA', '0', '0', '39611156', '', 5, '', 'RUA  SÃO DINIZ,217 APERTAR OS DOIS INTERFONES', 'V.DIVA', '02556-060', ''),
(1277, 'ALEXANDRE', '0', '0', '999992519', '', 3, '', 'RUA JOSE KIBELER 150 AP 12A', 'LIMAO', '', ''),
(1278, 'GEANE', '0', '0', '973773325', '', 4, '', 'DIOGO DE OLIVEIRA 364 A', '', '', ''),
(1279, 'VITOR', '0', '0', '981838793', '', 5, '', 'RUA  FRANCISCO RODRIGUES NUNES,34', 'JD.DAS GRAÇAS', '02712-090', ''),
(1280, 'TALIA', '0', '0', '986663146', '', 5, '', 'RUA PF JOSE MELO LORISOM 170 BC 3 AP 52', '', '', ''),
(1281, 'RODRIGO  VERA', '0', '0', '39317991', '39317991', 4, '', 'RUA  BALSA,218', 'FREGUESIA DO Ó', '02910-000', ''),
(1282, 'SANDRA', '0', '0', '985545575', '', 6, '', 'SOARES DE BIVAR 13  DO LADO JAPONES', 'VILA NOVA CACHOEIRNHA', '', ''),
(1283, 'VITORIA', '0', '0', '948209882', '', 5, '', 'RUA  ANDREA DEL CASTAGNO, 70', 'V.STA.MARIA', '02562-010', ''),
(1284, 'GLADES', '0', '0', '39665396', '39665396', 4, '', 'RUA  MATEUS CORREA,117', 'JD.PRIMAVERA', '02754-030', ''),
(1285, 'MARTA GUEDES', '0', '0', '39511329', '39511329', 4, '', 'TRAV.  DOIS RIACHOS,4', 'JD.PEREIRA LEITE', '02712-030', ''),
(1286, 'MARCELO', '0', '0', '997401895', '997401895', 4, '', 'RUA  SAMARITA,113 ATRAS DO FORÚN DA ENGENHEIRO', 'PQ.SOUZA ARANHA', '02518-080', ''),
(1287, 'CAROL', '0', '0', '39314228', '', 4, '', 'RUA  ALVARO SILVA,211 CASA 3', 'V.SIQUEIRA', '02723-020', ''),
(1288, 'LUCIANO', '0', '0', '23092679', '', 4, '', 'RUA  BRITO PEIXOTO,294 APT 56 BLOC B', 'V.S.VICENTE', '02735-030', ''),
(1289, 'KARINA', '0', '0', '941232461', '', 4, '', 'RUA DOM JOSE DE MARAES TORRES 130 APT 6', '', '', ''),
(1290, 'TAINA', '0', '0', '948186833', '948186833', 4, '', 'RUA  MARIA RENATA,57', 'V.DIVA', '02556-150', ''),
(1291, 'MIRIAM', '0', '0', '39322391', '39322391', 3, '', 'RUA  ROQUE DE MORAIS,345APTMEXICO 32', 'BA.DO LIMÃO', '02721-031', ''),
(1292, 'CELSOM', '0', '0', '963471490', '', 4, '', 'RUA  FRANCISCO AUGUSTO LOPES,290', 'V.BARBOSA', '02557-120', ''),
(1293, 'EDUARDO', '0', '0', '54153284', '54153284', 4, '', 'RUA  GAMA CERQUEIRA,31A', 'V.DIVA', '02560-070', ''),
(1294, 'ELEN', '0', '0', '951012851', '', 4, '', 'RUA  EULALIO DA COSTA CARVALHO,19', 'JD.PEREIRA LEITE', '02712-050', ''),
(1295, 'FABIANA CARDOSO', '0', '0', '977258889', '', 4, '', 'RUA  FRANCISCO RODRIGUES NUNES,34', 'JD.DAS GRAÇAS', '02712-090', ''),
(1296, 'FABIO', '0', '0', '39362204', '', 5, '', 'RUA  SILVERIO DE CARVALHO,331', 'V.CARBONE', '02752-000', ''),
(1297, 'ROBERTO', '0', '0', '39658382', '39658382', 3, '', 'AV.  EMILIO CARLOS,249', 'V.CRISTO REI', '02721-000', ''),
(1298, 'LIGIA', '0', '0', '39365448', '39365448', 4, '', 'RUA  GODOFREDO GONCALVES,69', 'CJ.RES.NO.PACAEMBU', '02722-200', ''),
(1299, 'ADRIANI', '0', '0', '39514015', '', 3, '', 'AV.  EMILIO CARLOS,351 AP 62 BRO', 'V.BARBOSA1', '02720-000', ''),
(1300, 'LUCAS', '0', '0', '961575560', '', 3, '', 'BERNARDO WUNA 339 LANCE', '', '', ''),
(1301, 'MACELO', '0', '0', '39515013', '', 4, '', 'TRAV.  TULIPA REAL,28', 'SÍTIO DO MANDAQUI', '02543-040', ''),
(1302, 'ADESON', '0', '0', '39330593', '', 4, '', 'RUA  GABRIEL MIGLIORI,400 ALT PRASTICA', 'JD.DAS GRAÇAS', '02712-140', ''),
(1303, 'SERGIO', '0', '0', '39367072', '39367072', 5, '', 'RUA  LAVRAS DO SUL,203', 'V.CAROLINA', '02724-030', ''),
(1304, 'VINICIUS', '0', '0', '955782921', '955782921', 6, '', 'BONIFACIO DE ANDRADA 147', 'AGUA BRANCA', '', ''),
(1305, 'PAULO', '0', '0', '983501432', '', 4, '', 'RUA  DIOGO DE OLIVEIRA,279', 'SÍTIO DO MORRO', '02553-040', ''),
(1306, 'JOAO', '0', '0', '21330399', '', 4, '', 'RUA  SAMARITA,1117 RAMAL 456', 'PQ.SOUZA ARANHA', '02518-080', ''),
(1307, 'DIEGO', '0', '0', '43043314', '', 4, '', 'TRAV.  VITORIO GNAN,5', 'PQ.SOUZA ARANHA', '02518-060', ''),
(1308, 'MARIA TERESA', '0', '0', '26454452', '26454452', 4, '', 'TR.  MARIA LUCIA, 12 CASA 8', 'V.BARBOSA', '02557-030', ''),
(1309, 'ALEXANDRE', '0', '0', '973211431', '', 4, '', 'AV.  SEBASTIÃO HENRIQUES,300 APT 82', 'V.SIQUEIRA', '02723-050', ''),
(1310, 'LUCIANO', '0', '0', '39362434', '39362434', 4, '', 'RUA  CRISOLIA,415', 'JD.PRIMAVERA', '02756-000', '');
INSERT INTO `clientes` (`id`, `name`, `cf1`, `cf2`, `phone`, `celular`, `taxa_de_entrega`, `email`, `endereco`, `bairro`, `cep`, `delivery`) VALUES
(1311, 'CRISTIANE', '0', '0', '982736515', '', 4, '', 'AV.  MANDAQUI,189 BLO A APT 108', 'BA.DO LIMÃOBO', '02550-000', ''),
(1312, 'ROSE', '0', '0', '39613534', '', 4, '', 'RUA  BUQUIRA,384 CASA 2', 'V.BARUEL', '02522-010', ''),
(1313, 'JANAINA', '0', '0', '39327833', '', 4, '', 'RUA  SANTO UBALDO,28 APT 13 BLO B', 'V.CAROLINA', '02725-050', ''),
(1314, 'RAFAEL', '0', '0', '86762098', '', 4, '', 'TR.  ANAJANE,121', 'BA.DO LIMÃO', '02550-100', ''),
(1315, 'NADIA', '0', '0', '38727829', '', 5, '', 'RUA  PEDRO JUAN CABALLERO 192', 'CASA VERDE ALTA', '02545-090', ''),
(1316, 'GABRIEL', '0', '0', '35894486', '35894486', 4, '', 'RUA  JACOFER,161APT43 TORE 2', 'JD.PEREIRA LEITE', '02712-070', ''),
(1317, 'ELISABETH', '0', '0', '39351606', '39351606', 4, '', 'RUA  OTAVIO DE MOURA,314', 'V.CAROLINA', '02724-000', ''),
(1318, 'ESTEVANI', '0', '0', '956373013', '', 4, '', 'RUA  VICENTE FERREIRA LEITE,512 AP 56 BROCO B', 'V.SIQUEIRA', '02723-000', ''),
(1319, 'MARIA CRISTINA', '0', '0', '38585503', '38585503', 4, '', 'RUA  PEDRO JUAN CABALLERO,93', 'CASA VERDE ALTA', '02545-090', ''),
(1320, 'ADEJAI', '0', '0', '39317345', '39317345', 3, '', 'CASA VERDE BAIXA/LIMÃO,2060 BROCO 2 AP 24', '', '02501-000', ''),
(1321, 'ALESANDRA', '0', '0', '39312712', '39312712', 4, '', 'RUA  ROQUE DE MORAIS,438 EDIFICIO BAIA AP 34', 'BA.DO LIMÃO', '02721-030', ''),
(1322, 'ROSA', '0', '0', '970409456', '', 4, '', 'RUA  BARTOLOMEU BERMEJO,206', 'CASA VERDE ALTA', '02565-000', ''),
(1323, 'FERNANDA', '0', '0', '39317394', '', 5, '', 'RUA  ODY MANOEL TEIXEIRA MUNHOZ,51A', 'JD.DAS GRAÇAS', '02714-040', ''),
(1324, 'RICARDO', '0', '0', '931101973', '', 5, '', 'RUA  ESTELA BORGES MORATO,618APT105 BTORR2', 'V.SIQUEIRA', '02722-000', ''),
(1325, 'CLARICE', '0', '0', '39324007', '39324007', 4, '', 'RUA  ISABEL DE SIQUEIRA BARROS,197', 'JD.PEREIRA LEITE', '02712-080', ''),
(1326, 'CRISTIANE', '0', '0', '986652964', '', 5, '', 'RUA  VENANCIO DE RESENDE,270', 'CASA VERDE MÉDIA', '02552-020', ''),
(1327, 'PAULA', '0', '0', '26149636', '', 4, '', 'RUA  JERONIMO DOS SANTOS,94', 'V.DIVA', '02556-090', ''),
(1328, 'EVERSOM', '0', '0', '39612429', '', 5, '', 'RUA  ATENAGORAS,159', 'V.BARBOSA', '02557-000', ''),
(1329, 'LUCAS', '0', '0', '39551440', '39551440', 3, '', 'SAMPAIO CORREIA 481', '', '', ''),
(1330, 'ESTEFANI', '0', '0', '39612710', '', 4, '', 'RUA SAO DINIS 64', '', '', ''),
(1331, 'ANDRESSA', '0', '0', '23645358', '', 5, '', 'RUA  ESTELA BORGES MORATO,160BL2 APT145', 'V.SIQUEIRA', '02722-000', ''),
(1332, 'IGOR', '0', '0', '957183183', '957183183', 5, '', 'RUA  VICENTE FERREIRA LEITE,512 ...BLO B APT 106', 'V.SIQUEIRA', '02723-000', ''),
(1333, 'ALBA', '0', '0', '982752116', '', 5, '', 'RUA  GUIOMAR DA ROCHA,368 CASA 1', 'JD.DAS LARANJEIRAS', '02521-060', ''),
(1334, 'AMANDA', '0', '0', '963036176', '', 3, '', 'AV MANDAQUI 54', '', '', ''),
(1335, 'LAIZ', '0', '0', '39519387', '', 4, '', 'RUA  JOAQUIM DE FREITAS,450 PORTAO MAROM', 'SÍTIO DO MORRO4', '02551-070', ''),
(1336, 'ALAN', '0', '0', '957886355', '957886355', 4, '', 'RUA  MARIA RENATA,57', 'V.DIVA', '02556-150', ''),
(1337, 'WELTON', '0', '0', '947350768', '', 4, '', 'RUA VICTORIO PRIMO 156', 'V.ESTER', '02520-300', ''),
(1338, 'CLAUDIA', '0', '0', '953828825', '', 4, '', 'RUA  MARIA ELISA SIQUEIRA,608 FUNDOS', 'JD.S.LUÍS', '02558-000', ''),
(1339, 'EVERTON', '0', '0', '981873762', '', 3, '', 'AV.  CELESTINO BOURROUL,684 APT 132 TORRE 2', 'BA.DO LIMÃO', '02710-001', ''),
(1340, 'CAMILA', '0', '0', '983327264', '983327264', 4, '', 'RUA NELSON FRANCISCO 29', '', '', ''),
(1341, 'VICTOR', '0', '0', '23711560', '', 4, '', 'RUA  ESTELA BORGES MORATO,377', 'V.SIQUEIRA', '02722-000', ''),
(1342, 'SEM NOME', '0', '0', '39321239', '', 0, '', 'AV528', '', '', ''),
(1343, 'MARLI', '0', '0', '39319486', '', 4, '', 'RUA  BALTAZAR DE QUADROS,141', 'V.PALMEIRA', '02727-080', ''),
(1344, 'LURDIS', '0', '0', '39367924', '39367924', 5, '', 'RUA  SILVERIO GONCALVES,232 FUNDOS', 'JD.PRIMAVERA', '02754-000', ''),
(1345, 'APARECIDA', '0', '0', '38577045', '38577045', 5, '', 'PRUA P LINS E SILVA,PROFESSOR 97', 'V.S.LUÍS', '02559-070', ''),
(1346, 'MARIANA', '0', '0', '39551223', '39551223', 3, '', 'RUA  HORACIO MOURA,232', 'BA.DO LIMÃO', '02710-070', ''),
(1347, 'CIDA', '0', '0', '951281499', '', 3, '', 'CELESTINO 684 TORRE3 APT 5', '', '', ''),
(1348, 'OSVALDO', '0', '0', '34888980', '34888980', 3, '', 'RUA  JOÃO SERRANO,250', 'SÍTIO DO MORRO', '02551-060', ''),
(1349, 'CLEBER', '0', '0', '974554183', '', 5, '', 'RUA  ROCHA LIMA,392', 'SÍTIO DO MORRO', '02554-010', ''),
(1350, 'KAREM', '0', '0', '967967275', '967967275', 4, '', 'RUA  BUQUIRA,508', 'V.BARUEL', '02522-010', ''),
(1351, 'JULIANA', '0', '0', '992190035', '', 4, '', 'RUA  ESTELA BORGES MORATO,618 TORI 2 AP 72', 'V.SIQUEIRA', '02722-000', ''),
(1352, 'ELIO', '0', '0', '95552097', '', 4, '', 'RUA  BUQUIRA, 158', 'V.BARUEL', '02522-010', ''),
(1353, 'BRAMARA', '0', '0', '98574694', '', 4, '', 'PR.  CENTENARIO, 85 CASA 2', 'CASA VERDE BAIXA', '02515-040', ''),
(1354, 'PEDRO', '0', '0', '38574899', '38574899', 4, '', 'RUA  LEONOR BARBOSA RODRIGUES,161', 'V.DIVA', '02556-040', ''),
(1355, 'MALEIDI', '0', '0', '39690401', '', 4, '', 'RUA  CARLOS PORTO CARREIRO,271 AP 54 BROCO 2', 'JD.DAS GRAÇAS', '02713-030', ''),
(1356, 'VINICIO', '0', '0', '953320692', '953320692', 4, '', 'RUA  SILVERIO DE CARVALHO,129', 'V.CARBONE', '02752-000', ''),
(1357, 'AMANDA', '0', '0', '39660287', '', 3, '', 'RUA  EULALIO DA COSTA CARVALHO,10', 'JD.PEREIRA LEITE', '02712-050', ''),
(1358, 'EDSON', '0', '0', '969625726', '969625726', 4, '', 'ROK DIMORAIS 55 ESTACIONAMENTO LEMA PARK', '', '', ''),
(1359, 'FABIANA', '0', '0', '975138783', '975138783', 4, '', 'RUA  PAULO VIDIGAL VICENTE 199 BLOC E4 APT 11', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(1360, 'SUELY', '0', '0', '39324943', '', 4, '', 'RUA  NILO DE SOUZA CARVALHO, 7 A', 'JD.DAS GRAÇAS', '02714-060', ''),
(1361, 'MARCELO', '0', '0', '962993153', '', 4, '', 'RUA  SILVERIO DE CARVALHO,180', 'V.CARBONE', '02752-000', ''),
(1362, 'CRISTIANE', '0', '0', '957647405', '957647405', 4, '', 'BILHETERIA DA CTN', '', '', ''),
(1363, 'JULIANA', '0', '0', '958003781', '958003781', 4, '', 'RUA  MAUES,248', 'V.CRISTO REI', '02558-010', ''),
(1364, 'WESLEY', '0', '0', '41115681', '41115681', 5, '', 'RUA  CLEMENTE FERREIRA,219', 'V.SIQUEIRA', '02723-040', ''),
(1365, 'ALINE', '0', '0', '38581851', '38581851', 4, '', 'TRAV.  BALTAZAR ECHAVE,60 CASA 1', 'V.BARBOSA', '02557-070', ''),
(1366, 'DANILO', '0', '0', '987030009', '', 4, '', 'AV.  MANDAQUI,275 APT 132', 'BA.DO LIMÃO', '02550-000', ''),
(1367, 'MARIA', '0', '0', '39313513', '', 4, '', 'RUA  MANUEL PINTO DE CARVALHO,150', 'JD.DAS GRAÇAS', '02712-120', ''),
(1368, 'GABRIELA', '0', '0', '38567421', '', 4, '', 'RUA  SAMARITA,898 BLO 3 APT 51', 'PQ.SOUZA ARANHA', '02518-080', ''),
(1369, 'MEIRE', '0', '0', '979919685', '', 4, '', 'RUA  SÃO ROMUALDO,368', 'V.BARBOSA', '02557-060', ''),
(1370, 'JUNIOR', '0', '0', '39322467', '', 4, '', 'RUA  GAMA CERQUEIRA,31 A', 'V.DIVA', '02560-070', ''),
(1371, 'VICTOR', '0', '0', '38584588', '', 3, '', 'AV.  NOSSA SRA.DO O,423 APT 37 BLO 2', 'BA.DO LIMÃO', '02715-000', ''),
(1372, 'RITA', '0', '0', '38589487', '', 4, '', 'RUA  DARIO RIBEIRO,553', 'CASA VERDE ALTA', '02559-000', ''),
(1373, 'ZILMA', '0', '0', '39658680', '39658680', 4, '', 'RUA  MARIA ELISA SIQUEIRA,141', 'JD.S.LUÍS', '02558-000', ''),
(1374, 'HENRIQUE', '0', '0', '997515048', '', 4, '', 'AV.  MANDAQUI,122 APT 121 BLO 4', 'BA.DO LIMÃO', '02550-000', ''),
(1375, 'CAROLINE', '0', '0', '34458341', '', 4, '', 'AV.  EMILIO CARLOS,351 APT 84 BLO A', 'V.CRISTO REI', '02721-000', ''),
(1376, 'CLAUDINEI', '0', '0', '38581193', '', 5, '', 'TRAV.  INES BAROY,173 CASA 6', 'CASA VERDE MÉDIA', '02545-010', ''),
(1377, 'FERNANDA', '0', '0', '34593653', '', 5, '', 'RUA  ANTONIO JOÃO,470', 'SÍTIO DO MORRO', '02553-050', ''),
(1378, 'LUANA', '0', '0', '997375763', '', 4, '', 'RUA  LUCILA, 193', 'JD.DAS LARANJEIRAS', '02522-090', ''),
(1379, 'MARIA', '0', '0', '966259869', '', 4, '', 'RUA  EVERARDO DIAS,51', 'CJ.RES.NO.PACAEMBU', '02722-020', ''),
(1380, 'MALAQUIAS', '0', '0', '39614706', '39614706', 4, '', 'RUA  SAMARITA,898 BLO 10 APT 22', 'PQ.SOUZA ARANHA', '02518-080', ''),
(1381, 'PAULO', '0', '0', '39319291', '', 4, '', 'RUA  VICENTE FERREIRA LEITE,512  APT 107 B', 'V.SIQUEIRA', '02723-000', ''),
(1382, 'EDUARDA', '0', '0', '972608493', '972608493', 4, '', 'RUA  ANDREA DEL CASTAGNO,128', 'V.STA.MARIA', '02562-010', ''),
(1383, 'STHER', '0', '0', '39510184', '', 4, '', 'RUA  DARIO RIBEIRO,885 CASA 2', 'CASA VERDE ALTA', '02559-000', ''),
(1384, 'NILO', '0', '0', '942100798', '942100798', 4, '', 'AV.  MANDAQUI,135 CASA 3', 'BA.DO LIMÃO', '02550-000', ''),
(1385, 'NICE', '0', '0', '22087609', '', 5, '', 'RUA  ANTONIO VIEIRA,243', 'CASA VERDE MÉDIA', '02544-040', ''),
(1386, 'LUCAS', '0', '0', '39316699', '', 4, '', 'RUA  GASTÃO THOMAZ DE ALMEIDA,39', 'V.SIQUEIRA', '02722-010', ''),
(1387, 'ELIO', '0', '0', '39552067', '', 4, '', 'RUA  BUQUIRA,158', 'V.BARUEL', '02522-010', ''),
(1388, 'CARLOS', '0', '0', '39319006', '39319006', 5, '', 'RUA  ESTANISLAU DE CAMPOS,46', 'V.CAROLINA', '02724-050', ''),
(1389, 'REGINALDO', '0', '0', '982177597', '', 4, '', 'RUA  PEDRO BRASIL BANDECCHI,75 APT 42', 'V.AMÉLIA', '02615-040', ''),
(1390, 'JULIANA', '0', '0', '985553877', '', 5, '', 'RUA  SANTA ANGELA, 953 AP43', 'V.PALMEIRA', '02727-000', ''),
(1391, 'THIAGO', '0', '0', '38571850', '', 4, '', 'TR.  ANAJANE,122', 'BA.DO LIMÃO', '02550-100', ''),
(1392, 'HOLIN', '0', '0', '34777812', '', 5, '', 'RUA  CAROLINA SOARES,979 APT 132 BLO MARINA', 'V.DIVA', '02554-000', ''),
(1393, 'RODRIGO', '0', '0', '38750845', '', 5, '', 'RUA  ESTELA BORGES MORATO,160 APT 154 TORRE 3', 'V.SIQUEIRA', '02722-000', ''),
(1394, 'THIAGO', '0', '0', '39362329', '', 5, '', 'AV.  CLAVASIO ALVES DA SILVA,820 BLO 3 APT 114', 'V.SIQUEIRA', '02722-030', ''),
(1395, 'FELIPE', '0', '0', '987725635', '987725635', 4, '', 'RUA  SAMARITA,898 BLO 12 APT 11', 'PQ.SOUZA ARANHA', '02518-080', ''),
(1396, 'LEANDRO E LUANA', '0', '0', '39655783', '', 4, '', 'TRAV.  BALTAZAR ECHAVE,42', 'V.BARBOSA', '02557-070', ''),
(1397, 'CIBELI', '0', '0', '953873608', '', 3, '', 'RUA  ROSA MARCHETTI,92', 'BA.DO LIMÃO', '02550-110', ''),
(1398, 'RICADO', '0', '0', '39332099', '', 4, '', 'RUA MAIESTRO MIGLIRO 510  CASA', '', '', ''),
(1399, 'MAURO', '0', '0', '38574992', '', 4, '', 'RELIQUIA 758', '', '', ''),
(1400, 'ALINI', '0', '0', '27765983', '', 4, '', 'AV.  EMILIO CARLOS,631 AP 106A  C BELBENERI 2', 'V.CRISTO REI', '02721-000', ''),
(1401, 'LUCIANA', '0', '0', '25324775', '', 5, '', 'RUA  AUGUSTO DA SILVA,170', 'V.CAROLINA', '02724-090', ''),
(1402, 'ANTONIO', '0', '0', '983605248', '983605248', 4, '', 'RUA  SAMARITA,1000', 'PQ.SOUZA ARANHA', '02518-080', ''),
(1403, 'MAISI', '0', '0', '38585778', '', 4, '', 'RUA  BELISARIO CAMPANHA,135A CASA 3', 'CASA VERDE MÉDIA', '02521-000', ''),
(1404, 'ALINI', '0', '0', '961127451', '', 4, '', 'RUA  VASCONCELOS DE ALMEIDA,142', 'V.DIVA', '02556-010', ''),
(1405, 'ANDRE', '0', '0', '969876282', '969876282', 4, '', 'RUA  OTAVIO DE MOURA,392', 'V.CAROLINA', '02724-000', ''),
(1406, 'VANESA', '0', '0', '39345300', '', 4, '', 'RUA  MANUEL PINTO DE CARVALHO,150', 'JD.DAS GRAÇAS', '02712-120', ''),
(1407, 'TAINA', '0', '0', '983014801', '', 4, '', 'RUA  SÃO DINIZ, 171', 'V.DIVA', '02556-060', ''),
(1408, 'DIENIFE', '0', '0', '956546618', '', 4, '', 'TRAV.  BOCAIUVA DO SUL,2 CANPAINHA 3', 'V.BARBOSA', '02556-041', ''),
(1409, 'LANA', '0', '0', '38542197', '', 0, '', 'CELTINO 895', '', '', ''),
(1410, 'CINTIA', '0', '0', '38570406', '38570406', 3, '', 'RUA  ROSA MARCHETTI,87', 'BA.DO LIMÃO', '02550-110', ''),
(1411, 'ALESSANDRO', '0', '0', '972522853', '', 3, '', 'RUA BERNARDO BRONA 389 EMPRESA', '', '', ''),
(1412, 'VINICIUS', '0', '0', '22386994', '', 5, '', 'RUA  JOAQUIM LEME DA SILVA,74 CASA 1', 'CASA VERDE ALTA', '02565-010', ''),
(1413, 'VANESSA', '0', '0', '961985736', '', 5, '', 'RUA  GODOFREDO GONCALVES,47', 'CJ.RES.NO.PACAEMBU', '02722-200', ''),
(1414, 'REGIANA', '0', '0', '23059425', '', 5, '', 'RUA  JOÃO MAYER,49 FUNDOS', 'SÍTIO DO MORRO', '02551-120', ''),
(1415, 'MARIA LUIZA', '0', '0', '39658596', '', 5, '', 'RUA  MATEUS MASCARENHAS,381', 'JD.PEREIRA LEITE', '02712-000', ''),
(1416, 'KETLEI', '0', '0', '940045386', '', 5, '', 'ENGENHEIRO JOSE PASTORI 173', 'JARDIM  DAS GRAÇAS', '', ''),
(1417, 'MAIARA', '0', '0', '38576064', '', 5, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,91 BI3 AP1S1', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(1418, 'SANDRA', '0', '0', '962928921', '', 5, '', 'RUA  JOSE KILEBER, 185 BLO Y B2 APT32', 'CJ.RES.NO.PACAEMBU', '02722-140', ''),
(1419, 'FELIPE', '0', '0', '39517549', '', 5, '', 'RUA  NORDESTE,30', 'CASA VERDE MÉDIA', '02521-030', ''),
(1420, 'ROBERTO', '0', '0', '29585534', '29585534', 3, '', 'AV.  NOSSA SRA.DO O,423 BLO1 APT24', 'BA.DO LIMÃO', '02715-000', ''),
(1421, 'MARCEL', '0', '0', '983905593', '', 5, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,222 95D', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(1422, 'CLUDIO', '0', '0', '965147240', '', 4, '', 'RUA  SÃO ROMUALDO,174 FUNDOS', 'V.BARBOSA', '02557-060', ''),
(1423, 'BRUNO', '0', '0', '964297112', '', 4, '', 'ALESSO VALDO DOVINETO 305 TRAVESSA DARIO RIBEIRO', '', '', ''),
(1424, 'ERICA', '0', '0', '985225412', '', 5, '', 'AV.D  EMILIO CARLOS,DEP 505 APT 47 BLO B', 'V.CRISTO REI', '02721-000', ''),
(1425, 'DEBORA', '0', '0', '967405297', '', 3, '', 'RUA  ARAUJO MARCONDES,62 FUNDOS CASA 2', 'BA.DO LIMÃO', '02550-080', ''),
(1426, 'ANA CAROLINA', '0', '0', '982781678', '', 3, '', 'RUA  CAROLINA SOARES,836', 'V.DIVA', '02554-000', ''),
(1427, 'RICARDO', '0', '0', '39318451', '', 4, '', 'RUA  JOÃO LIBRINA,55', 'V.BRITO', '02726-120', ''),
(1428, 'RISONETI', '0', '0', '38571133', '', 4, '', 'RUA  CESAR PENA RAMOS,1590  CASA FUNDO', 'CASA VERDE ALTA', '02563-001', ''),
(1429, 'GIL', '0', '0', '969972823', '', 4, '', 'RUA   SAO DINIS 206', '', '', ''),
(1430, 'GISELE', '0', '0', '25797091', '', 5, '', 'ENGENHEIRO 637', '', '', ''),
(1431, 'ELISETE', '0', '0', '994609933', '', 3, '', 'RUA  SEBASTIÃO MORAIS,3 FINAL 2 VIELA', 'BA.DO LIMÃO', '02551-100', ''),
(1432, 'DOUGLAS', '0', '0', '385632441', '', 3, '', 'CELESTINO 100  ESTADAO 2441', '', '', ''),
(1433, 'NILDA', '0', '0', '39367869', '', 4, '', 'RUA  SANTO UBALDO,28  AP 83  BROCO C', 'V.CAROLINA', '02725-050', ''),
(1434, 'CRISTIANE', '0', '0', '38569328', '', 5, '', 'RUA  DIRCE RODRIGUES,61', 'V.DIVA', '02556-080', ''),
(1435, 'ROSELI', '0', '0', '39362233', '39362233', 5, '', 'DEPUTADO EMILIO CARLOS 1251 40DP', '', '', ''),
(1436, 'CARLA', '0', '0', '998574846', '', 6, '', 'RUA  RIBEIRÃO DAS ALMAS,130', 'V.PALMEIRA', '02728-100', ''),
(1437, 'CLAUDIA', '0', '0', '96942520', '', 5, '', 'RUA  JERONIMO DOS SANTOS,3', 'V.DIVA', '02556-090', ''),
(1438, 'VANIA', '0', '0', '989015701', '', 3, '', 'CONEGO MARCONDES ARAUJO 63', '', '', ''),
(1439, 'CAROL', '0', '0', '39310628', '39310628', 5, '', 'RUA  ALVARO SILVA,188', 'V.SIQUEIRA', '02723-020', ''),
(1440, 'AVARO', '0', '0', '958554503', '958554503', 3, '', 'RUA  SEBASTIÃO MORAIS,04', 'BA.DO LIMÃO', '02551-100', ''),
(1441, 'MARCELIO', '0', '0', '965942102', '', 4, '', 'AV.  INAJAR DE SOUZA,ENTRADA 75 CASA 101', 'LIMÃO', '02717-000', ''),
(1442, 'SERGIO', '0', '0', '39662234', '39662234', 4, '', 'RUA  CAROLINA SOARES,979 APT 43', 'V.DIVA', '02554-000', ''),
(1443, 'WELVIS', '0', '0', '987154656', '', 5, '', 'RUA  MACIEIRAS,125 CASA 4', 'JD.DAS LARANJEIRAS,', '02521-090', ''),
(1444, 'AMANDA', '0', '0', '23895486', '', 3, '', 'AV MANDAQUI 54', '', '', ''),
(1445, 'STHERFANO', '0', '0', '26383226', '26383226', 3, '', 'AV.  MONTE CELESTE,464', 'V.STA.MARIA', '02561-000', ''),
(1446, 'MARTA', '0', '0', '38560543', '', 4, '', 'RUA  VICHY,129', 'V.BARUEL', '02522-100', ''),
(1447, 'FABIO', '0', '0', '39313144', '', 4, '', 'RUA  JOSE CASTILHO MAGAN,86', 'V.BELA', '02728-010', ''),
(1448, 'FATIMA', '0', '0', '39311955', '', 4, '', 'RUA  FRANCISCO RODRIGUES NUNES,34', 'JD.DAS GRAÇAS', '02712-090', ''),
(1449, 'RAFAEL', '0', '0', '946859325', '', 3, '', 'RUA  ROQUE DE MORAIS, 55', 'BA.DO LIMÃO', '02721-031', ''),
(1450, 'ANDERSOM', '0', '0', '38576210', '', 5, '', 'TANGERINA 142', 'PQ SOUSA ARANHA', '', ''),
(1451, 'JEANI', '0', '0', '992863658', '', 4, '', 'RUA  ITALO BALBO,3', 'PQ.SOUZA ARANHA', '02550-070', ''),
(1452, 'LUCIANA', '0', '0', '949082388', '', 5, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,55BLOK1APTF2', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(1453, 'GERSOM', '0', '0', '38569045', '38569045', 4, '', 'RUA  CAROLINA SOARES,591', 'V.DIVA', '02554-000', ''),
(1454, 'ANDRE', '0', '0', '39322975', '', 4, '', 'RUA  ISABEL DE SIQUEIRA BARROS,241 EMPRESA SEGURAN', 'JD.PEREIRA LEITE', '02712-080', ''),
(1455, 'BEATRIZ', '0', '0', '983797954', '983797954', 3, '', 'AV.  CELESTINO BOURROUL,684 TORE2 APT 132', 'BA.DO LIMÃO', '02710-001', ''),
(1456, 'GUILHERME', '0', '0', '86566897', '', 5, '', 'RUA  SILVANO DE ALMEIDA,379', 'V.SIQUEIRA', '02723-110', ''),
(1457, 'VRAVIANA', '0', '0', '94577747', '', 2, '', 'AV.  CELESTINO BOURROUL,684 AP51 TORI 1', 'BA.DO LIMÃO', '02710-001', ''),
(1458, 'AUDRI', '0', '0', '849961394', '', 3, '', 'RUA  HORACIO MOURA,83', 'BA.DO LIMÃO', '02710-070', ''),
(1459, 'ROCELI', '0', '0', '26139401', '', 4, '', 'RUA  SÃO DINIZ,119  CASA 6', 'V.DIVA', '02556-060', ''),
(1460, 'DANIELI', '0', '0', '41137685', '41137685', 4, '', 'RUA  ANDREA DEL CASTAGNO,115', 'V.STA.MARIA', '02562-010', ''),
(1461, 'IVANETE', '0', '0', '946930869', '', 5, '', 'RUA  JOSE SORIANO DE SOUSA,213 CASA4', 'CASA VERDE ALTA', '02555-050', ''),
(1462, 'ROBERTA', '0', '0', '969830969', '', 5, '', 'RUA  MARCELINO DE CAMARGO,343', 'V.CAROLINA', '02724-040', ''),
(1463, 'GEIDER', '0', '0', '38575941', '', 4, '', 'AV.  EMILIO CARLOS,665AP104', 'V.CRISTO REI', '02721-000', ''),
(1464, 'PAULA', '0', '0', '985375792', '', 5, '', 'GUILMAR DA ROCHA 307', '', '', ''),
(1465, 'IGOR', '0', '0', '38560758', '', 5, '', 'RUA  MATEUS MASCARENHAS,305', 'JD.PEREIRA LEITE', '02712-000', ''),
(1466, 'CAROL', '0', '0', '984402030', '', 5, '', 'RUA  MARIA ROBERTA,56', 'V.DIVA', '02556-160', ''),
(1467, 'TALITA', '0', '0', '25061326', '', 4, '', 'RUA  JOSE KILEBER,55 APT93', 'CJ.RES.NO.PACAEMBU', '02722-140', ''),
(1468, 'MARTA', '0', '0', '386560543', '', 4, '', 'RUA  VICHY,129', 'V.BARUEL', '02522-100', ''),
(1469, 'LAIS', '0', '0', '45618981', '', 4, '', 'RUA  JOSE KILEBER, 185 AP 2 BLO B2', 'CJ.RES.NO.PACAEMBU', '02722-140', ''),
(1470, 'TATIANE', '0', '0', '941259009', '', 5, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO, 271 B4', 'CJ.RES.NO.PACAEMBU  1F1', '02722-090', ''),
(1471, 'BRUNO', '0', '0', '39582124', '', 5, '', 'RUA  GAMA CERQUEIRA, 31A', 'V.DIVA', '02560-070', ''),
(1472, 'BETI', '0', '0', '984563749', '', 4, '', 'RUA  JOSE AMATO,39', 'CASA VERDE BAIXA', '02518-120', ''),
(1473, 'MIKAEL', '0', '0', '987156885', '', 8, '', 'ALFREDO PUJOL 681', 'SANTANA', '', ''),
(1474, 'ROSANGELA', '0', '0', '960913605', '', 5, '', 'SANTO ANTONIO DA PLATINA 168 C', 'VILA PRADO', '', ''),
(1475, 'DANIEL', '0', '0', '949272223', '', 3, '', 'CELESTINO 650 CASA 1', '', '', ''),
(1476, 'ARNALDO', '0', '0', '39653774', '', 4, '', 'RUA  FRANCISCO AUGUSTO LOPES,52', 'V.BARBOSA', '02557-120', ''),
(1477, 'GUSTAVO', '0', '0', '36163555', '36163555', 6, '', 'MARQUES DE SAO VICENTE 1697 TERREO AUTO PEÇAS', '', '', ''),
(1478, 'ALEXHANDRA', '0', '0', '39570921', '', 4, '', 'RUA  MANUEL CARLOS DE SOUZA,88', 'V.DIVA', '02557-090', ''),
(1479, 'CRISTIANO', '0', '0', '43280949', '43280949', 4, '', 'RUA  CAROLINA SOARES,593  CASA 3', 'V.DIVA', '02554-000', ''),
(1480, 'MARCOS', '0', '0', '992605854', '', 4, '', 'ROQUE DE MORAIS 345ED  COSTA RICA TERREO 2', '', '', ''),
(1481, 'JUNIOR', '0', '0', '38929642', '', 3, '', 'AV.  MANDAQUI,275 APT161', 'BA.DO LIMÃO', '02550-000', ''),
(1482, 'TIAGO', '0', '0', '989125431', '', 4, '', 'RUA  MARIA ELISA SIQUEIRA,320', 'JD.S.LUÍS', '02558-000', ''),
(1483, 'RICADO', '0', '0', '984239742', '984239742', 4, '', 'RUA  NINETE,212', 'V.ESPANHOLA', '02545-110', ''),
(1484, 'LURDES', '0', '0', '39614338', '', 4, '', 'TRAV.  BARRA DO RIBEIRO,22', 'V.DIVA', '02556-110', ''),
(1485, 'BRUNO', '0', '0', '984087132', '', 4, '', 'RUA  VASCONCELOS DE ALMEIDA,61', 'V.DIVA', '02556-010', ''),
(1486, 'ROBSON', '0', '0', '996009121', '', 4, '', 'RUA  MADALENA DE MADUREIRA,264 FUNDOS', 'SÍTIO DO MORRO', '02551-040', ''),
(1487, 'ANDREA', '0', '0', '38574261', '', 4, '', 'RUA  BERNARDO WRONA,408 FUNDOS', 'BA.DO LIMÃO', '02710-060', ''),
(1488, 'ANDERSON', '0', '0', '38533484', '38533484', 4, '', 'RUA  PAULO VIDIGAL VICENTE 55 BLO K2 APT 2S1', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(1489, 'MARLI', '0', '0', '962288655', '', 4, '', 'RUA  ANTONIO JOÃO,334', 'SÍTIO DO MORRO', '02553-050', ''),
(1490, 'NALVOCIÉS', '0', '0', '983351484', '983351484', 5, '', 'RUA  DULCELINA,154 CASA 3', 'CASA VERDE ALTA', '02555-120', ''),
(1491, 'ANDRESA', '0', '0', '41023803', '', 6, '', 'RUA  FERNANDO MACHADO,80', 'V.ESPANHOLA', '02566-070', ''),
(1492, 'CAMILA', '0', '0', '947223236', '', 4, '', 'DIONISIA BARBOSA 41', '', '', ''),
(1493, 'FERNADO', '0', '0', '943690442', '', 3, '', 'AV.  NOSSA SRA.DO O,423 AP 43B', 'BA.DO LIMÃO', '02715-000', ''),
(1494, 'CLEIDE', '0', '0', '38568692', '', 4, '', 'RUA  SAMARITA,898 BLO 12  APT 12', 'PQ.SOUZA ARANHA', '02518-080', ''),
(1495, 'ALESANDRA', '0', '0', '32975228', '', 5, '', 'RUA  SILVANO DE ALMEIDA,253 APT 38', 'V.SIQUEIRA', '02723-110', ''),
(1496, 'JANAINA', '0', '0', '969748464', '', 4, '', 'RUA  ROCHA LIMA,44 CASA 6', 'SÍTIO DO MORRO', '02554-010', ''),
(1497, 'WILLIAN', '0', '0', '985833343', '', 4, '', 'AV MANDAQUI 189 APT 22 BLO A', '', '', ''),
(1498, 'NICKELY', '0', '0', '38585832', '', 5, '', 'RUA  IAPO,125', 'CASA VERDE BAIXA', '02512-020', ''),
(1499, 'ALEX', '0', '0', '949429631', '', 4, '', 'RUA  CAMPOS DO CAJURU,86', 'V.PALMEIRA', '02726-100', ''),
(1500, 'GILBERTO', '0', '0', '959149503', '959149503', 3, '', 'RUA  HORACIO MOURA,75 CAMPANHIA 3', 'BA.DO LIMÃO', '02710-070', ''),
(1501, 'CAMILA', '0', '0', '39511341', '', 4, '', 'RUA  PAULO VIDIGAL VICENTE 199 APT 1S2 BLO E2', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(1502, 'MARCOS', '0', '0', '39318620', '', 4, '', 'TR.  MANUEL DO CARMO,15', 'JD.DAS GRAÇAS', '02713-035', ''),
(1503, 'MAXSUEL', '0', '0', '26792141', '', 4, '', 'DEPUTADO EMILIO  CARLOS 367 LIGA ANTES', '', '', ''),
(1504, 'LETICIA', '0', '0', '946543369', '946543369', 4, '', 'RUA  CAROLINA SOARES,284 CASA 4', 'V.DIVA', '02554-000', ''),
(1505, 'JUDITE', '0', '0', '961581130', '', 5, '', 'RUA  OTAVIO DE MOURA,317', 'V.CAROLINA', '02724-000', ''),
(1506, 'ZEZE', '0', '0', '28082657', '', 5, '', 'ROQUE DE MORAES 55 LADO DA QUACENTER', '', '', ''),
(1507, 'ALEXANDRE', '0', '0', '983157537', '', 5, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,163 AP32 BH1', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(1508, 'JO', '0', '0', '958773244', '', 5, '', 'RUA  CAROLINA SOARES,450', 'V.DIVA', '02554-000', ''),
(1509, 'ROBERTA', '0', '0', '967659330', '', 3, '', 'NICOLINO STOPELY 151', '', '', ''),
(1510, 'IGOR', '0', '0', '914941601', '', 5, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,271 APA421', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(1511, 'MARCOS', '0', '0', '38585961', '', 3, '', 'AV.  MANDAQUI,345', 'BA.DO LIMÃO', '02550-000', ''),
(1512, 'GABRIEL', '0', '0', '21330432', '', 4, '', 'RUA  SAMARITA,1117 EDIFICIO LIMAO 4 RAMAL 432', 'PQ.SOUZA ARANHA', '02518-080', ''),
(1513, 'MARA', '0', '0', '977358596', '', 5, '', 'RUA  ANTONIO CUSTODIO GODOY,217', 'V.CARBONE', '02752-070', ''),
(1514, 'VALDI', '0', '0', '39319273', '', 4, '', 'RUA  EUCLIDES MACHADO,324', 'JD.DAS GRAÇAS', '02713-000', ''),
(1515, 'GIUMA', '0', '0', '960933329', '', 3, '', 'RUA SANPIO CORREIA 520 PREDIO COLORIDO B 4 AP 11', '', '', ''),
(1516, 'RAQUEL MOXERIFADO', '0', '0', '980123722', '', 3, '', 'NOSSA SENHORA DO O382 EMPRESA PROCISA', '', '', ''),
(1517, 'NEIDE', '0', '0', '38567048', '', 3, '', 'JOAO MAIER 41 TRAVESSA JOAO SERRANO', '', '', ''),
(1518, 'CAMILA', '0', '0', '941555688', '941555688', 5, '', 'AV.  ANTONIO MUNHOZ BONILHA,791 CASA3', 'V.CAROLINA', '02725-000', ''),
(1519, 'DORALICE', '0', '0', '980573512', '980573512', 4, '', 'RUA  VASCONCELOS DE ALMEIDA,4', 'V.DIVA', '02556-010', ''),
(1520, 'ANTONIO', '0', '0', '974478719', '974478719', 3, '', 'CORONEL JOAQUI DE FREITAS 455', 'LIMAO', '', ''),
(1521, 'DOUGLAS', '0', '0', '941073459', '', 5, '', 'RODRIGUES DA COSTA 33', 'SITIO DO MORRO', '', ''),
(1522, 'RAFAEL', '0', '0', '957961013', '', 4, '', 'RUA  SILVERIO GONCALVES,240  CASA 4', 'JD.PRIMAVERA', '02754-000', ''),
(1523, 'MARINEIS', '0', '0', '977716043', '977716043', 4, '', 'RUA  HORACIO MOURA,83 CASA 2', 'BA.DO LIMÃO', '02710-070', ''),
(1524, 'MARIA EDUARDA', '0', '0', '39660717', '', 4, '', 'RUA ANTONIESTIGARIDA  198', '', '', ''),
(1525, 'AMANDA', '0', '0', '976005903', '', 5, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO, 199 AP', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(1526, 'NATALIA', '0', '0', '985971226', '', 3, '', 'AV.  CELESTINO BOURROUL,684 APT22 TORRE3', 'BA.DO LIMÃO', '02710-001', ''),
(1527, 'FLAVIO', '0', '0', '953192245', '', 5, '', 'RUA  GUIOMAR DA ROCHA,380 CASA1', 'JD.DAS LARANJEIRAS', '02521-060', ''),
(1528, 'CAIO', '0', '0', '987350254', '', 5, '', 'CARLOS BELMIRO CORREIA 638 CASA 1', 'PERUCHE', '', ''),
(1529, 'RICHARD', '0', '0', '993396058', '993396058', 4, '', 'RUA  CAROLINA SOARES,718', 'V.DIVA', '02554-000', ''),
(1530, 'AMANDA', '0', '0', '968310071', '', 4, '', 'AV.  EMILIO CARLOS,505APT 116 BLO B', 'V.CRISTO REI', '02721-000', ''),
(1531, 'LARISA', '0', '0', '39237926', '', 0, '', 'AV.  CELESTINO BOURROUL,890', 'BA.DO LIMÃO', '02710-001', ''),
(1532, 'ROZI', '0', '0', '957314835', '957314835', 4, '', 'RUA  LAGOA AZUL,208', 'V.DIVA', '02556-000', ''),
(1533, 'DENIZI', '0', '0', '976746673', '', 4, '', 'RUA  JACOFER,140  AP 93 TORI 3', 'JD.PEREIRA LEITE', '02712-070', ''),
(1534, 'MARIA', '0', '0', '28940220', '', 4, '', 'RUA  CAROLINA SOARES,979 APT 51 EDIFICIO CAROLINA', 'V.DIVA', '02554-000', ''),
(1535, 'GUILHERME', '0', '0', '958630431', '', 4, '', 'OTAVIO BARRETO 90 TRAVESA DARIO RIBEIRO', 'LIMAO', '', ''),
(1536, 'MARIA', '0', '0', '94740574', '', 3, '', 'RUA EULALIO DA COSTA CARVALHO,560 DANUBIO AZUL', 'JD.PEREIRA LEITE', '02712-050', ''),
(1537, 'DIDA', '0', '0', '982747852', '', 4, '', 'RUA  SANTO UBALDO,28  AP 94 H', 'V.CAROLINA', '02725-050', ''),
(1538, 'LUCAS', '0', '0', '38575401', '', 4, '', 'AV.  EMILIO CARLOS,631AOT36 BLO B', 'V.CRISTO REI', '02721-000', ''),
(1539, 'ELOISA', '0', '0', '39314191', '39314191', 4, '', 'RUA ROQUI DI MORAIS 345 AP HURUGUAI 21', '', '', ''),
(1540, 'ANDERSOM', '0', '0', '963401343', '', 5, '', 'RUA  INACIO PROENCA DE GOUVEIA,395', 'PQ.PERUCHE', '02534-010', ''),
(1541, 'SAULO', '0', '0', '988328517', '', 3, '', 'RUA  SEBASTIÃO MORAIS,1B SEM SAIDA', 'BA.DO LIMÃO', '02551-100', ''),
(1542, 'MARLENE', '0', '0', '954231831', '', 4, '', 'RUA  SAMPAIO CORREA,7 COMUNIDADE', 'BA.DO LIMÃO', '02710-080', ''),
(1543, 'SUSI', '0', '0', '38579723', '38579723', 5, '', 'AV.  ORDEM E PROGRESSO,1140APT12V BLO 1 MARIA CRIS', 'CASA VERDE BAIXA', '02518-130', ''),
(1544, 'LEA DE JESUS', '0', '0', '39512140', '39512140', 5, '', 'RUA  BERNARDINO FANGANIELLO,715APT2', 'CASA VERDE BAIXA', '02512-000', ''),
(1545, 'LEANDRO', '0', '0', '949512079', '', 5, '', 'RUA  CRISOLIA,48', 'JD.PRIMAVERA', '02756-000', ''),
(1546, 'TAIZ', '0', '0', '952048851', '', 5, '', 'TR.  FRANCISCO ADRIANI,48 SEM SAIDA', 'V.SANTISTA', '02560-180', ''),
(1547, 'CAMILA', '0', '0', '29854317', '', 1, '', 'CELESTINO 890 APT 204 B', '', '', ''),
(1548, 'GESSICA', '0', '0', '996488598', '996488598', 4, '', 'RUA  ATENAGORAS,152', 'V.BARBOSA', '02557-000', ''),
(1549, 'DANIELE', '0', '0', '39318435', '', 4, '', 'AV.  NOSSA SRA.DO O,1407 CASA 32', 'BA.DO LIMÃO', '02715-000', ''),
(1550, 'ROSELI', '0', '0', '28629690', '', 5, '', 'RUA  ARMANDO COELHO SILVA,892', 'PQ.PERUCHE', '02539-000', ''),
(1551, 'EVANDRO', '0', '0', '984112917', '', 5, '', 'RUA  DIONISIO BARBOSA,71', 'CASA VERDE ALTA', '02559-100', ''),
(1552, 'IGOR', '0', '0', '961730825', '', 4, '', 'RUA  JACOFER,105 APT 201 TORRE 2', 'JD.PEREIRA LEITE', '02712-070', ''),
(1553, 'KARINA', '0', '0', '35899999', '35899999', 4, '', 'AV.  ANTONIO MUNHOZ BONILHA,1347 APT 21 TORRE 3', 'V.CAROLINA', '02725-000', ''),
(1554, 'DARA', '0', '0', '38577700', '', 4, '', 'RUA  SAMARITA,898 BLO 14 APT 31', 'PQ.SOUZA ARANHA', '02518-080', ''),
(1555, 'ALINI', '0', '0', '23710838', '', 3, '', 'AV.  NOSSA SRA.DO O,423 AP 155 BROCO 1', 'BA.DO LIMÃO', '02715-000', ''),
(1556, 'TIAGO', '0', '0', '985488163', '', 4, '', 'RUA  EDUARDO ADENIS,40', 'V.SANTISTA', '02560-190', ''),
(1557, 'ALESANDRO', '0', '0', '969069458', '', 5, '', 'MIGUEL CASA GLANDI 173', '', '', ''),
(1558, 'GUILERMI', '0', '0', '39653801', '', 3, '', 'RUA  ARAUJO MARCONDES,34', 'BA.DO LIMÃO', '02550-080', ''),
(1559, 'VALTER', '0', '0', '23897330', '', 4, '', 'RUA  LAVRAS DO SUL,117', 'V.CAROLINA', '02724-030', ''),
(1560, 'DENISE', '0', '0', '985659919', '', 6, '', 'RUA  ELIAS GANNAM,229', 'V.BANDEIRANTES', '02552-040', ''),
(1561, 'VANIA', '0', '0', '65247442', '65247442', 4, '', 'RUA  GAMA CERQUEIRA,43 E', 'V.DIVA', '02560-070', ''),
(1562, 'EDGAR', '0', '0', '951391608', '', 5, '', 'RUA  ANDREA DEL CASTAGNO,371', 'V.STA.MARIA', '02562-010', ''),
(1563, 'FAGNER', '0', '0', '946433892', '', 5, '', 'TR.  JOÃO LUSO,31  CASA 2', 'V.SANTISTA', '02560-160', ''),
(1564, 'SUELI', '0', '0', '38587288', '', 3, '', 'RUA  SAMARITA, 898 BLO 3 AP 14', 'PQ.SOUZA ARANHA', '02518-080', ''),
(1565, 'FATIMA', '0', '0', '983652130', '', 3, '', 'TR.  ANAJANE,61', 'BA.DO LIMÃO', '02550-100', ''),
(1566, 'MARIA', '0', '0', '39352038', '', 4, '', 'RUA  ESTANISLAU DE CAMPOS,288', 'V.CAROLINA', '02724-050', ''),
(1567, 'JANAINA', '0', '0', '989478716', '', 4, '', 'RUA  BALTAZAR DE QUADROS, 184B', 'V.PALMEIRA', '02727-080', ''),
(1568, 'SORAIA', '0', '0', '963254679', '', 5, '', 'RUA  EULALIO DA COSTA CARVALHO, 530', 'JD.PEREIRA LEITE', '02712-050', ''),
(1569, 'JULIANA', '0', '0', '39510408', '', 5, '', 'RUA  CARAMBEI, 108', 'V.STA.MARIA', '02561-080', ''),
(1570, 'JOAO', '0', '0', '990115032', '', 4, '', 'RUA  JOSE SORIANO DE SOUSA, 401', 'CASA VERDE ALTA', '02555-050', ''),
(1571, 'CAROL', '0', '0', '952547708', '', 3, '', 'RUA  SAMPAIO CORREA, 520', 'BA.DO LIMÃO', '02710-080', ''),
(1572, 'MACELO', '0', '0', '38586994', '', 4, '', 'RUA  JOSE FRUTUOSO DIAS,210', 'V.BANDEIRANTES', '02552-070', ''),
(1573, 'DIEGO', '0', '0', '23694128', '', 4, '', 'RUA  BALTAZAR DE QUADROS, 185', 'V.PALMEIRA', '02727-080', ''),
(1574, 'MARISAA', '0', '0', '954861401', '', 5, '', 'RUA  RIBEIRÃO DAS ALMAS, 183', 'V.PALMEIRA', '02728-100', ''),
(1575, ',MARIA APARECIDA', '0', '0', '26091826', '', 5, '', 'RUA  SANTA ANGELA,60', 'V.PALMEIRA', '02727-000', ''),
(1576, 'CILENI', '0', '0', '985213606', '', 5, '', 'RUA  TEODORO HENRIQUE MAURER JR,36', 'JD.CACHOEIRA', '02765-060', ''),
(1577, 'RENATO SOARES', '0', '0', '25331840', '', 5, '', 'RUA  VASCONCELOS DE ALMEIDA,339', 'V.DIVA', '02556-010', ''),
(1578, 'ALBERTO', '0', '0', '27484189', '', 5, '', 'AV.  SEBASTIÃO HENRIQUES,300 AP101', 'V.SIQUEIRA', '02723-050', ''),
(1579, 'JACKON', '0', '0', '997488971', '', 5, '', 'RUA  OTAVIO DE MOURA,142 CASA 1', 'V.CAROLINA', '02724-000', ''),
(1580, 'TATIANE', '0', '0', '982837295', '', 3, '', 'RUA  ROQUE DE MORAIS,340ED PARA APT 23', 'BA.DO LIMÃO', '02721-030', ''),
(1581, 'THIAGO', '0', '0', '960122446', '', 5, '', 'RUA  BENTO PICKEL, TRAVESSA LUIZ ALBANEZE CASA 5', 'SÍTIO DO MANDAQUI', '02544-000', ''),
(1582, 'JULIANA', '0', '0', '38569420', '', 3, '', 'RUA  ROQUE DE MORAIS,159', 'BA.DO LIMÃO', '02721-031', ''),
(1583, 'CAMILA', '0', '0', '39613146', '', 6, '', 'RUA  DARIO RIBEIRO,775A CASA 10 MUURO AMARELO PORT', 'CASA VERDE ALTA', '02559-000', ''),
(1584, 'HENRIQIUE', '0', '0', '993015959', '', 4, '', 'AV.  EMILIO CARLOS,631APT15BLO A', 'V.CRISTO REI', '02721-000', ''),
(1585, 'BRUNO', '0', '0', '25073460', '', 3, '', 'PROFESSOR EVERALDO DIAS 51', 'LIMAAO', '', ''),
(1586, 'SABRINA', '0', '0', '952913663', '', 5, '', 'RUA  VASCONCELOS DE ALMEIDA,13', 'V.DIVA', '02556-010', ''),
(1587, 'PRISCILA', '0', '0', '39665091', '', 5, '', 'RUA  FRANCISCO AUGUSTO LOPES,28', 'V.BARBOSA', '02557-120', ''),
(1588, 'FATMA', '0', '0', '39026972', '', 3, '', 'AV.  MANDAQUI,275 AP61', 'BA.DO LIMÃO', '02550-000', ''),
(1589, 'MINA', '0', '0', '977930064', '', 5, '', 'RUA  GAMA CERQUEIRA,28 CASA 2', 'V.DIVA', '02560-070', ''),
(1590, 'CRISTIANO', '0', '0', '991564376', '', 3, '', 'AV.  MANDAQUI,122 AP BRO 4 AP 182', 'BA.DO LIMÃO', '02550-000', ''),
(1591, 'LEONADO', '0', '0', '39363952', '', 4, '', 'RUA  CLEMENTE FERREIRA,94', 'V.SIQUEIRA', '02723-040', ''),
(1592, 'DANIELA', '0', '0', '947223064', '', 4, '', 'RUA  JACOFER, 140 AP 71 TORE 3', 'JD.PEREIRA LEITE', '02712-070', ''),
(1593, 'MARIA', '0', '0', '39756372', '', 4, '', 'RUA  JACOFER, 140 AP 42 TORE 1', 'JD.PEREIRA LEITE', '02712-070', ''),
(1594, 'KELLI', '0', '0', '973327387', '', 5, '', 'NATECIA 55 FUNDOS', 'VILA CAROLINA', '', ''),
(1595, 'ELAINE', '0', '0', '37960093', '', 5, '', 'AV.  CASA VERDE,2362', 'V.ESTER', '02520-200', ''),
(1596, 'ESTEFANY', '0', '0', '967884421', '', 5, '', 'RUA  CESAR PENA RAMOS,251', 'CASA VERDE ALTA', '02563-000', ''),
(1597, 'TADEU', '0', '0', '983080996', '', 4, '', 'TR.  MARIA LUCIA,4A', 'V.BARBOSA', '02557-030', ''),
(1598, 'MARIA', '0', '0', '973023171', '', 5, '', 'RUA  MADALENA DE MADUREIRA, 264', 'SÍTIO DO MORRO', '02551-040', ''),
(1599, 'NATALI', '0', '0', '947659502', '947659502', 3, '', 'RUA  AMADEU DANIELI FILHO,42', 'BA.DO LIMÃO', '02550-030', ''),
(1600, 'PATRICIA', '0', '0', '996537111', '', 4, '', 'RUA  LAGOA AZUL,389', 'V.DIVA', '02556-000', ''),
(1601, 'ANDRE', '0', '0', '38322975', '38322975', 4, '', 'RUA  ISABEL DE SIQUEIRA BARROS,241', 'JD.PEREIRA LEITE', '02712-080', ''),
(1602, 'TALIA', '0', '0', '38575161', '', 4, '', 'TR.  JOÃO LUSO,65 CASA 2', 'V.SANTISTA', '02560-160', ''),
(1603, 'ANDRE', '0', '0', '938008008', '', 4, '', 'RUA  ISABEL DE SIQUEIRA BARROS,241 EMPRESA', 'JD.PEREIRA LEITE', '02712-080', ''),
(1604, 'MARIANA', '0', '0', '981285257', '', 3, '', 'AV.  CELESTINO BOURROUL,684APT174 TORRE2', 'BA.DO LIMÃO', '02710-001', ''),
(1605, 'CAMILA', '0', '0', '967700626', '', 4, '', 'NELSOM FRANCISCO 29', 'LIMAO', '', ''),
(1606, 'RUBENS', '0', '0', '995488009', '995488009', 4, '', 'RUA  LEONOR BARBOSA RODRIGUES,326', 'V.DIVA', '02556-040', ''),
(1607, 'SILVANA', '0', '0', '39514743', '', 4, '', 'RUA  LAGOA AZUL,141', 'V.DIVA', '02556-000', ''),
(1608, 'AMANSDA', '0', '0', '38813043', '', 4, '', 'RUA  ELIAS GANNAM,357', 'V.BANDEIRANTES', '02552-040', ''),
(1609, 'TAINAR', '0', '0', '957290991', '', 5, '', 'RUA  JOÃO MAYER,59', 'SÍTIO DO MORRO', '02551-120', ''),
(1610, 'SIMONE', '0', '0', '26400510', '', 3, '', 'AV.  CELESTINO BOURROUL,890 APT 232 B', 'BA.DO LIMÃO', '02710-001', ''),
(1611, 'LUIZ', '0', '0', '39659305', '', 4, '', 'RUA  PAULO VIDIGAL VICENTE 91 BLO J1 APT 12', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(1612, 'ERNANE', '0', '0', '38716095', '', 6, '', 'RUA ADRIANO JOSE MACHINI 32 PROTEJE', '', '', ''),
(1613, 'LUAN', '0', '0', '954058920', '', 5, '', 'RUA  JERONIMO DOS SANTOS,20', 'V.DIVA', '02556-090', ''),
(1614, 'GUILHERME', '0', '0', '947348004', '947348004', 5, '', 'RUA  REIMS,120', 'PQ.SOUZA ARANHA', '02517-010', ''),
(1615, 'ANDERSON', '0', '0', '35678887', '', 4, '', 'RUA  VITORIO PRIMON,115 FUNDOS', 'BA.DO LIMÃO', '02550-050', ''),
(1616, 'HENRIQUE', '0', '0', '964937117', '', 4, '', 'RUA  JACOFER,140 APT 144 TORRE 1', 'JD.PEREIRA LEITE', '02712-070', ''),
(1617, 'SOLDADO GALVÃO', '0', '0', '981496586', '', 6, '', 'RUA ALFREDO PULJO 681', '', '', ''),
(1618, 'RILARY', '0', '0', '948700860', '', 5, '', 'RUA  BALBINO JOSE DOS SANTOS,46', 'V.MO.ALTO', '02753-040', ''),
(1619, 'THIAGO DD 19', '0', '0', '997035349', '', 5, '', 'RUA BONIFACIO ANDRADA 147', '', '', ''),
(1620, 'GIOVANA', '0', '0', '962227510', '', 1, '', 'CELESTINO 890 BLO BAPT 232', '', '', ''),
(1621, 'ROSERI', '0', '0', '28639690', '', 4, '', 'RUA  ARMANDO COELHO SILVA,892', 'PQ.PERUCHE', '02539-000', ''),
(1622, 'FELIPE', '0', '0', '41135784', '', 4, '', 'RUA  PAULO VIDIGAL VICENTE DE AZEVEDO,91APTI BLO', 'CJ.RES.NO.PACAEMBU', '02722-090', ''),
(1623, 'BRUNO', '0', '0', '956185130', '', 3, '', 'NOSSA SENHORA DO O273 ZARA PAST', '', '', ''),
(1624, 'DAIANE', '0', '0', '993806386', '', 4, '', 'RUA  CARLOS PORTO CARREIRO,271BLO2 APT54', 'JD.DAS GRAÇAS', '02713-030', ''),
(1625, 'CAROL', '0', '0', '950219514', '', 5, '', 'RUA  ESTELA BORGES MORATO,160 APT126 TORRE 2', 'V.SIQUEIRA', '02722-000', ''),
(1626, 'MARINEIDE', '0', '0', '977716041', '', 3, '', 'RUA  HORACIO MOURA,210', 'BA.DO LIMÃO', '02710-070', ''),
(1627, 'JULIANE', '0', '0', '38560601', '', 5, '', 'RUA  ROCHA LIMA,511', 'SÍTIO DO MORRO', '02554-010', ''),
(1628, 'BENI', '0', '0', '39662052', '', 3, '', 'TRAV.  ANTONIO FARGAS,68', 'BA.DO LIMÃO', '02551-090', ''),
(1629, 'MACELO', '0', '0', '970309343', '', 4, '', 'RUA  LAVRAS DO SUL,259A', 'V.CAROLINA', '02724-030', ''),
(1630, 'RAIMUNDO', '0', '0', '985562599', '', 3, '', 'BERNADO WRONA 408', '', '', ''),
(1631, 'VICTOR', '0', '0', '36152052', '', 5, '', 'R. BONIFACIL DE ANDRADA 147', 'AGUA BRANCA', '', ''),
(1632, 'LUCIANA', '0', '0', '967960619', '967960619', 3, '', 'AV.  MANDAQUI,189 AP 13 BROCO B', 'BA.DO LIMÃO', '02550-000', ''),
(1633, 'MATEUS', '0', '0', '983468932', '', 4, '', 'RUA  BELISARIO CAMPANHA,', 'CASA VERDE MÉDIA', '02521-000', ''),
(3268, 'Cliente Teste', '', '', '41064921', '', 3, '', 'Rua de teste, 00', 'Jd Teste', '02315-010', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `ID` int(11) NOT NULL,
  `NOME` varchar(100) DEFAULT NULL,
  `FONE` varchar(15) NOT NULL,
  `CELULAR` varchar(15) NOT NULL,
  `EMAIL` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos`
--

CREATE TABLE `contatos` (
  `id` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `email` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `telefone` varchar(45) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contatos`
--

INSERT INTO `contatos` (`id`, `nome`, `email`, `telefone`) VALUES
(1, 'Diogo Cezar', 'xgordo@gmail.com', '(43) 3523-2956'),
(2, 'Mario Sergio', 'padariajoia@gmail.com', '(43) 9915-7944'),
(3, 'JoÃ£o da Silva', 'joao@gmail.com', '(41) 3453-9876'),
(4, 'Junior', 'teste', '1231231'),
(5, 'Felipe', 'teste12', '1241414'),
(6, 'outro gato', 'outro@teste', '7783894');

-- --------------------------------------------------------

--
-- Estrutura da tabela `fechamentos`
--

CREATE TABLE `fechamentos` (
  `id` int(11) NOT NULL DEFAULT '0',
  `id_caixa` int(11) DEFAULT NULL,
  `data_abertura` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `data_fechamento` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `valor_inicial` decimal(10,2) NOT NULL,
  `valor_final` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `forma_pagamento`
--

CREATE TABLE `forma_pagamento` (
  `id` int(11) NOT NULL,
  `forma_pagamento` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `icone` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `forma_pagamento`
--

INSERT INTO `forma_pagamento` (`id`, `forma_pagamento`, `icone`, `link`) VALUES
(1, 'Dinheiro', 'money icon', 'din_vendaDAO.php'),
(2, 'Cartão de Débito', 'payment icon', 'vendaDAO.php'),
(3, 'Cartão de Crédito', 'credit card alternative icon', 'vendaDAO.php'),
(4, 'Dinheiro + Débito', '', ''),
(5, 'Dinheiro + Crédito', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `movimentacao_caixa01`
--

CREATE TABLE `movimentacao_caixa01` (
  `id` int(11) NOT NULL,
  `valor` decimal(10,2) NOT NULL,
  `tipo_movimentacao` varchar(2) NOT NULL,
  `saldo` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pde_fato_vendas`
--

CREATE TABLE `pde_fato_vendas` (
  `id` int(11) NOT NULL,
  `data_venda` varchar(60) NOT NULL,
  `origem_venda` varchar(60) NOT NULL,
  `num_nota_fiscal` varchar(25) NOT NULL,
  `id_forma_pagamento` int(11) NOT NULL,
  `id_abertura` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `pde_fato_vendas`
--

INSERT INTO `pde_fato_vendas` (`id`, `data_venda`, `origem_venda`, `num_nota_fiscal`, `id_forma_pagamento`, `id_abertura`) VALUES
(1, '2017-01-24 19:36', 'Caixa 01', 'NF187', 2, 2),
(2, '2017-01-24 21:28', 'Caixa 01', 'NF991', 1, 2),
(3, '2017-01-25 11:53', 'Caixa 01', 'NF584', 1, 2),
(4, '2017-01-25 12:12', 'Caixa 01', 'NF427', 1, 2),
(5, '2017-01-25 12:21', 'Caixa 01', 'NF282', 1, 2),
(6, '2017-01-25 12:28', 'Caixa 01', 'NF815', 1, 2),
(7, '2017-01-25 12:29', 'Caixa 01', 'NF648', 2, 2),
(8, '2017-01-25 17:14', 'Caixa 01', 'NF370', 1, 5),
(9, '2017-01-26 19:14', 'Caixa 01', 'NF166', 1, 6),
(10, '2017-01-26 19:15', 'Caixa 01', 'NF624', 2, 6),
(11, '2017-01-26 19:16', 'Caixa 01', 'NF810', 3, 6),
(12, '2017-01-27 01:03', 'Caixa 01', 'NF761', 3, 7),
(13, '2017-01-27 01:03', 'Caixa 01', 'NF482', 1, 7),
(14, '2017-01-27 01:04', 'Caixa 01', 'NF135', 1, 7),
(15, '2017-01-27 01:05', 'Caixa 01', 'NF596', 2, 7),
(16, '2017-01-27 01:12', 'Caixa 01', 'NF452', 3, 8),
(17, '2017-01-30 19:28', 'Caixa 01', 'NF556', 1, 10),
(18, '2017-01-30 19:32', 'Caixa 01', 'NF174', 1, 10),
(19, '2017-01-30 20:02', 'Delivery', 'NF411', 1, 10),
(20, '2017-01-30 20:07', 'Caixa 01', 'NF280', 1, 10),
(21, '2017-01-30 20:16', 'Caixa 01', 'NF472', 3, 11),
(22, '2017-01-30 20:17', 'Delivery', 'NF820', 1, 11),
(23, '2017-02-01 20:14', 'Delivery', 'NF319', 3, 12),
(24, '2017-02-02 15:16', 'Delivery', 'NF838', 1, 12),
(25, '2017-02-02 18:11', 'Caixa 01', 'NF401', 2, 13),
(26, '2017-02-02 18:12', 'Delivery', 'NF556', 3, 13),
(27, '2017-02-02 19:04', 'Caixa 01', 'NF751', 2, 19),
(28, '2017-02-06 16:52', 'Caixa 01', 'NF355', 2, 20),
(29, '2017-02-06 16:53', 'Caixa 01', 'NF388', 1, 20),
(30, '2017-02-06 16:54', 'Caixa 01', 'NF807', 1, 21),
(31, '2017-02-06 19:36', 'Caixa 01', 'NF850', 1, 21),
(32, '2017-02-06 19:42', 'Caixa 01', 'NF548', 2, 21),
(33, '2017-02-06 21:36', 'Caixa 01', 'NF426', 3, 21),
(34, '2017-02-07 22:59', 'Caixa 01', 'NF449', 1, 21),
(35, '2017-02-07 23:24', 'Caixa 01', 'NF940', 2, 22),
(36, '2017-02-07 23:26', 'Caixa 01', 'NF985', 3, 22),
(37, '2017-02-08 01:41', 'Caixa 01', 'NF563', 3, 22),
(38, '2017-02-08 01:48', 'Caixa 01', 'NF958', 2, 23),
(39, '2017-02-08 07:36', 'Caixa 01', 'NF536', 1, 24),
(40, '2017-02-08 07:37', 'Caixa 01', 'NF128', 1, 24),
(41, '2017-02-08 09:54', 'Caixa 01', 'NF707', 1, 24),
(42, '2017-02-08 09:54', 'Delivery', 'NF693', 1, 24);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pde_fato_vendas_produtos`
--

CREATE TABLE `pde_fato_vendas_produtos` (
  `id` int(11) NOT NULL,
  `num_nota_fiscal` varchar(45) DEFAULT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `pde_fato_vendas_produtos`
--

INSERT INTO `pde_fato_vendas_produtos` (`id`, `num_nota_fiscal`, `id_produto`, `quantidade`, `obs`) VALUES
(1, 'NF187', 5, 1, ''),
(2, 'NF187', 15, 1, ''),
(3, 'NF991', 1, 1, ''),
(4, 'NF991', 5, 2, ''),
(5, 'NF991', 15, 1, ''),
(6, 'NF815', 1, 2, ''),
(7, 'NF815', 15, 1, ''),
(8, 'NF648', 2, 2, ''),
(9, 'NF648', 14, 1, ''),
(10, 'NF370', 2, 1, ''),
(11, 'NF166', 3, 1, ''),
(12, 'NF166', 15, 1, ''),
(13, 'NF624', 5, 2, ''),
(14, 'NF624', 14, 1, ''),
(15, 'NF810', 2, 1, ''),
(16, 'NF810', 16, 1, ''),
(17, 'NF761', 5, 2, ''),
(18, 'NF761', 15, 2, ''),
(19, 'NF482', 1, 1, ''),
(20, 'NF135', 3, 1, ''),
(21, 'NF135', 16, 2, ''),
(22, 'NF596', 11, 10, ''),
(23, 'NF452', 15, 1, ''),
(24, 'NF411', 0, 0, ''),
(25, 'NF411', 5, 1, ''),
(26, 'NF411', 7, 1, ''),
(27, 'NF411', 1, 1, ''),
(28, 'NF280', 1, 1, ''),
(29, 'NF280', 2, 1, ''),
(30, 'NF472', 2, 1, ''),
(31, 'NF472', 3, 1, ''),
(32, 'NF472', 10, 1, ''),
(33, 'NF820', 0, 0, ''),
(34, 'NF820', 5, 3, ''),
(35, 'NF820', 6, 2, ''),
(36, 'NF319', 0, 0, ''),
(37, 'NF319', 5, 2, ''),
(38, 'NF319', 6, 1, ''),
(39, 'NF319', 1, 2, ''),
(40, 'NF838', 2, 1, ''),
(41, 'NF838', 1, 1, ''),
(42, 'NF838', 3, 1, ''),
(43, 'NF401', 1, 1, ''),
(44, 'NF401', 2, 1, ''),
(45, 'NF401', 6, 1, ''),
(46, 'NF556', 0, 0, ''),
(47, 'NF556', 1, 1, ''),
(48, 'NF556', 6, 1, ''),
(49, 'NF751', 2, 2, ''),
(50, 'NF355', 1, 3, ''),
(51, 'NF355', 2, 2, ''),
(52, 'NF355', 4, 1, ''),
(53, 'NF388', 5, 1, ''),
(54, 'NF388', 20, 1, ''),
(55, 'NF807', 1, 3, ''),
(56, 'NF807', 10, 1, ''),
(57, 'NF807', 11, 1, ''),
(58, 'NF850', 3, 1, ''),
(59, 'NF850', 20, 1, ''),
(60, 'NF850', 49, 1, ''),
(61, 'NF548', 100, 1, ''),
(62, 'NF548', 101, 1, ''),
(63, 'NF426', 99, 1, ''),
(64, 'NF426', 9, 1, ''),
(65, 'NF426', 13, 1, ''),
(66, 'NF449', 390, 1, '33.00'),
(67, 'NF449', 398, 1, ''),
(68, 'NF449', 414, 1, ''),
(69, 'NF449', 1, 1, ''),
(70, 'NF449', 320, 1, ''),
(71, 'NF449', 314, 1, '29.00'),
(72, 'NF940', 6, 1, ''),
(73, 'NF985', 3, 1, ''),
(74, 'NF563', 432, 1, ''),
(75, 'NF563', 431, 1, ''),
(76, 'NF958', 433, 1, ''),
(77, 'NF536', 434, 1, ''),
(78, 'NF536', 187, 1, ''),
(79, 'NF128', 434, 1, ''),
(80, 'NF128', 188, 1, ''),
(81, 'NF707', 435, 1, ''),
(82, 'NF693', 0, 0, ''),
(83, 'NF693', 18, 1, ''),
(84, 'NF693', 19, 1, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pde_movimentacao`
--

CREATE TABLE `pde_movimentacao` (
  `id` int(11) NOT NULL,
  `tipo_movimentacao` varchar(2) NOT NULL,
  `valor` decimal(10,2) NOT NULL,
  `origem` varchar(45) NOT NULL,
  `id_forma_pagamento` int(11) NOT NULL,
  `num_nota_fiscal` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `pde_movimentacao`
--

INSERT INTO `pde_movimentacao` (`id`, `tipo_movimentacao`, `valor`, `origem`, `id_forma_pagamento`, `num_nota_fiscal`) VALUES
(1, 'E', '7.00', 'Caixa 01', 2, 'NF187'),
(2, 'E', '20.00', 'Caixa 01', 1, 'NF991'),
(3, 'S', '6.00', 'Caixa 01', 1, 'NF991'),
(4, 'E', '12.00', 'Caixa 01', 1, 'NF584'),
(5, 'E', '12.00', 'Caixa 01', 1, 'NF427'),
(6, 'E', '12.00', 'Caixa 01', 1, 'NF282'),
(7, 'E', '12.00', 'Caixa 01', 1, 'NF815'),
(8, 'E', '10.00', 'Caixa 01', 2, 'NF648'),
(9, 'E', '3.00', 'Caixa 01', 1, 'NF370'),
(10, 'E', '7.00', 'Caixa 01', 1, 'NF166'),
(11, 'E', '10.00', 'Caixa 01', 2, 'NF624'),
(12, 'E', '6.00', 'Caixa 01', 3, 'NF810'),
(13, 'E', '14.00', 'Caixa 01', 3, 'NF761'),
(14, 'E', '4.00', 'Caixa 01', 1, 'NF482'),
(15, 'E', '20.00', 'Caixa 01', 1, 'NF135'),
(16, 'S', '11.00', 'Caixa 01', 1, 'NF135'),
(17, 'E', '30.00', 'Caixa 01', 2, 'NF596'),
(18, 'E', '4.00', 'Caixa 01', 3, 'NF452'),
(19, 'E', '10.00', 'Caixa 01', 1, 'NF556'),
(20, 'S', '3.51', 'Caixa 01', 1, 'NF556'),
(21, 'E', '10.00', 'Caixa 01', 1, 'NF174'),
(22, 'S', '3.51', 'Caixa 01', 1, 'NF174'),
(23, 'E', '10.00', 'Delivery', 1, 'NF411'),
(24, 'S', '3.51', 'Delivery', 1, 'NF411'),
(25, 'E', '5.00', 'Caixa 01', 1, 'NF280'),
(26, 'S', '1.81', 'Caixa 01', 1, 'NF280'),
(27, 'E', '7.40', 'Caixa 01', 3, 'NF472'),
(28, 'E', '15.00', 'Delivery', 1, 'NF820'),
(29, 'S', '1.00', 'Delivery', 1, 'NF820'),
(30, 'E', '10.28', 'Delivery', 3, 'NF319'),
(31, 'E', '5.49', 'Delivery', 1, 'NF838'),
(32, 'E', '6.29', 'Caixa 01', 2, 'NF401'),
(33, 'E', '4.09', 'Delivery', 3, 'NF556'),
(34, 'E', '4.40', 'Caixa 01', 2, 'NF751'),
(35, 'E', '9.57', 'Caixa 01', 2, 'NF355'),
(36, 'E', '10.00', 'Caixa 01', 1, 'NF388'),
(37, 'S', '3.40', 'Caixa 01', 1, 'NF388'),
(38, 'E', '10.00', 'Caixa 01', 1, 'NF807'),
(39, 'S', '1.03', 'Caixa 01', 1, 'NF807'),
(40, 'E', '50.00', 'Caixa 01', 1, 'NF850'),
(41, 'S', '16.70', 'Caixa 01', 1, 'NF850'),
(42, 'E', '9.60', 'Caixa 01', 2, 'NF548'),
(43, 'E', '6.60', 'Caixa 01', 3, 'NF426'),
(44, 'E', '70.00', 'Caixa 01', 1, 'NF449'),
(45, 'S', '7.01', 'Caixa 01', 1, 'NF449'),
(46, 'E', '3.10', 'Caixa 01', 2, 'NF940'),
(47, 'E', '2.30', 'Caixa 01', 3, 'NF985'),
(48, 'E', '66.00', 'Caixa 01', 3, 'NF563'),
(49, 'E', '33.00', 'Caixa 01', 2, 'NF958'),
(50, 'E', '35.00', 'Caixa 01', 1, 'NF536'),
(51, 'S', '2.00', 'Caixa 01', 1, 'NF536'),
(52, 'E', '50.00', 'Caixa 01', 1, 'NF128'),
(53, 'S', '20.00', 'Caixa 01', 1, 'NF128'),
(54, 'E', '50.00', 'Caixa 01', 1, 'NF707'),
(55, 'S', '23.00', 'Caixa 01', 1, 'NF707'),
(56, 'E', '20.00', 'Delivery', 1, 'NF693'),
(57, 'S', '15.00', 'Delivery', 1, 'NF693');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_balcao`
--

CREATE TABLE `pedido_balcao` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_balcao2`
--

CREATE TABLE `pedido_balcao2` (
  `id` int(11) NOT NULL DEFAULT '0',
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_delivery`
--

CREATE TABLE `pedido_delivery` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `id_motoboy` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `pedido_delivery`
--

INSERT INTO `pedido_delivery` (`id`, `id_produto`, `quantidade`, `obs`, `id_cliente`, `id_motoboy`) VALUES
(1, 0, 0, '', 14, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa1`
--

CREATE TABLE `pedido_mesa1` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos_suspensos`
--

CREATE TABLE `produtos_suspensos` (
  `id` int(11) NOT NULL,
  `id_suspensao` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `id_motoboy` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `produtos_suspensos`
--

INSERT INTO `produtos_suspensos` (`id`, `id_suspensao`, `id_produto`, `quantidade`, `id_motoboy`) VALUES
(38, 'ID582', 0, 0, 10);

-- --------------------------------------------------------

--
-- Estrutura da tabela `sabores_pizza`
--

CREATE TABLE `sabores_pizza` (
  `id` int(11) NOT NULL,
  `sabor1` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `sabor2` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `sabor3` varchar(45) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `sabores_pizza`
--

INSERT INTO `sabores_pizza` (`id`, `sabor1`, `sabor2`, `sabor3`) VALUES
(1, 'PIZZA MUSSARELA', 'PIZZA CALABRESA', ''),
(2, 'PIZZA DOIS QUEIJOS', 'PIZZA MILHO VERDE', ''),
(3, 'ATUM', 'CALABRESA', ''),
(4, 'MUSSARELA', 'ATUM', '4 QUEIJOS');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tabela_auxiliar_venda`
--

CREATE TABLE `tabela_auxiliar_venda` (
  `id` int(50) NOT NULL,
  `total` int(100) NOT NULL,
  `pago` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_mesas`
--

CREATE TABLE `tec_mesas` (
  `id` int(11) NOT NULL DEFAULT '0',
  `mesa` varchar(45) CHARACTER SET utf8 NOT NULL,
  `estado` varchar(45) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tec_mesas`
--

INSERT INTO `tec_mesas` (`id`, `mesa`, `estado`) VALUES
(1, 'Mesa 01', 'free'),
(2, 'Mesa 02', 'free'),
(3, 'Mesa 03', 'free'),
(4, 'Mesa 04', 'free'),
(5, 'Mesa 05', 'free'),
(6, 'Mesa 06', 'free'),
(7, 'Mesa 07', 'free'),
(8, 'Mesa 08', 'free'),
(9, 'Mesa 09', 'free'),
(10, 'Mesa 10', 'free'),
(11, 'Mesa 11', 'free'),
(12, 'Mesa 12', 'free'),
(13, 'Mesa 13', 'free'),
(14, 'Mesa 14', 'free'),
(15, 'Mesa 15', 'free'),
(16, 'Mesa 16', 'free'),
(17, 'Mesa 17', 'free'),
(18, 'Mesa 18', 'free'),
(19, 'Mesa 19', 'free'),
(20, 'Mesa 20', 'free'),
(21, 'Mesa 21', 'free'),
(22, 'Mesa 22', 'free'),
(23, 'Mesa 23', 'free'),
(24, 'Mesa 24', 'free'),
(25, 'Mesa 25', 'free'),
(26, 'Mesa 26', 'free'),
(27, 'Mesa 27', 'free');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_pedido_mesa`
--

CREATE TABLE `tec_pedido_mesa` (
  `id` int(11) NOT NULL DEFAULT '0',
  `id_produto` int(11) NOT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `total` decimal(10,2) NOT NULL,
  `id_mesa` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `impresso` int(11) NOT NULL,
  `cozinha` int(11) DEFAULT NULL,
  `foi_pedido` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_products`
--

CREATE TABLE `tec_products` (
  `id` int(11) NOT NULL,
  `code` int(10) NOT NULL,
  `name` varchar(65) CHARACTER SET utf8 NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '1',
  `price` decimal(25,2) NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT 'no_image.png',
  `tax` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `cost` decimal(25,2) DEFAULT NULL,
  `tax_method` tinyint(1) DEFAULT '1',
  `quantity` decimal(15,2) DEFAULT '0.00',
  `barcode_symbology` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT 'code39',
  `type` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT 'standard',
  `details` text CHARACTER SET utf8,
  `alert_quantity` decimal(10,2) DEFAULT '0.00',
  `cozinha` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tec_products`
--

INSERT INTO `tec_products` (`id`, `code`, `name`, `category_id`, `price`, `image`, `tax`, `cost`, `tax_method`, `quantity`, `barcode_symbology`, `type`, `details`, `alert_quantity`, `cozinha`) VALUES
(1, 1, 'ESFIHA CARNE', 1, '0.00', 'carne.jpg', '0', '0.99', 0, '10.00', '', '', '', '5.00', 1),
(2, 2, 'ESFIHA QUEIJO', 1, '0.00', 'queijo.jpg', '0', '2.20', 0, '10.00', '', '', '', '5.00', 1),
(3, 3, 'ESFIHA CATUPIRY', 1, '0.00', 'catupiry.jpg', '0', '2.30', 0, '10.00', '', '', '', '5.00', 1),
(4, 4, 'ESFIHA CALABRESA', 1, '0.00', 'calabresa.jpg', '0', '2.20', 0, '10.00', '', '', '', '5.00', 1),
(5, 5, 'ESFIHA FRANGO', 1, '0.00', 'frango.jpg', '0', '2.60', 0, '10.00', '', '', '', '5.00', 1),
(6, 6, 'ESFIHA ATUM', 1, '0.00', 'atum.jpg', '0', '3.10', 0, '10.00', '', '', '', '5.00', 1),
(7, 7, 'ESFIHA BAURU', 1, '0.00', 'bauru.jpg', '0', '2.90', 0, '10.00', '', '', '', '5.00', 1),
(8, 8, 'ESFIHA ESCAROLA', 1, '0.00', 'escarola.jpg', '0', '2.90', 0, '10.00', '', '', '', '5.00', 1),
(9, 9, 'ESFIHA FRANGO C/ REQUEIJA', 1, '0.00', 'frangorequeijao.jpg', '0', '2.90', 0, '10.00', '', '', '', '5.00', 1),
(10, 10, 'ESFIHA PALMITO', 1, '0.00', 'palmito.jpg', '0', '2.90', 0, '10.00', '', '', '', '5.00', 1),
(11, 114, 'ESFIHA MILHO C/ CATUPIRY', 1, '0.00', 'milhocatupiry.jpg', '0', '3.10', 0, '10.00', '', '', '', '5.00', 1),
(12, 12, 'ESFIHA BACON C/ QUEIJO', 1, '0.00', 'baconqueijo.jpg', '0', '2.90', 0, '10.00', '', '', '', '5.00', 1),
(13, 13, 'ESFIHA 4 QUEIJOS', 1, '0.00', '4queijos.jpg', '0', '3.20', 0, '10.00', '', '', '', '5.00', 1),
(14, 14, 'ESFIHA PORTUGUESA', 1, '0.00', 'portuguesa.jpg', '0', '3.20', 0, '10.00', '', '', '', '5.00', 1),
(15, 15, 'ESFIHA 3 QUEIJOS', 1, '0.00', '3queijos.jpg', '0', '3.20', 0, '10.00', '', '', '', '5.00', 1),
(16, 16, 'ESFIHA CALABRESA C/ CATUP', 1, '0.00', 'calabresacatupiry.jpg', '0', '2.90', 0, '10.00', '', '', '', '5.00', 1),
(17, 17, 'ESFIHA CARNE C/ QUEIJO', 1, '0.00', 'carnequeijo.jpg', '0', '2.90', 0, '10.00', '', '', '', '5.00', 1),
(18, 19, 'SALGADO', 2, '0.00', 'salgados.jpg', '0', '2.50', 0, '10.00', '', '', '', '5.00', 1),
(19, 20, 'SALGADOS PARA VIAGEM', 2, '0.00', 'salgadosviagem.jpg', '0', '2.50', 0, '10.00', '', '', '', '5.00', 1),
(20, 21, 'REFRIGERANTE LATA', 3, '0.00', 'refri.jpg', '0', '4.00', 0, '10.00', '', '', '', '5.00', 1),
(21, 22, 'GUARAVITON', 3, '0.00', 'guaraviton.jpg', '0', '4.00', 1, '10.00', '', '', '', '5.00', 1),
(22, 23, 'SUCO EM LATA', 3, '0.00', 'sucolata.jpg', '0', '4.00', 1, '10.00', '', '', '', '5.00', 1),
(23, 24, 'CERVEJA PRETA LATA', 3, '0.00', 'cervejapretalata.jpg', '0', '5.00', 0, '10.00', '', '', '', '5.00', 1),
(24, 25, 'CERVEJA LATA 350', 3, '0.00', 'lata350.jpg', '0', '4.50', 0, '10.00', '', '', '', '5.00', 1),
(25, 26, 'CERVEJA LATAO 475', 3, '0.00', 'lata475.jpg', '0', '5.50', 0, '10.00', '', '', '', '5.00', 1),
(26, 27, 'CEREVEJA LATAO 550', 3, '0.00', 'lata550.jpg', '0', '6.50', 0, '10.00', '', '', '', '5.00', 1),
(27, 28, 'H2O', 3, '0.00', 'h2o.jpg', '0', '5.00', 0, '10.00', '', '', '', '5.00', 1),
(28, 29, 'GATORADE', 3, '0.00', 'gatorade.jpg', '0', '5.50', 0, '10.00', '', '', '', '5.00', 1),
(29, 30, 'AGUA', 3, '0.00', 'agua.jpg', '0', '2.50', 0, '10.00', '', '', '', '5.00', 1),
(30, 31, 'AGUA COM GAZ', 3, '0.00', 'aguagas.jpg', '0', '3.00', 0, '10.00', '', '', '', '5.00', 1),
(31, 557, 'SUCO DEL VALE GARRAFA', 3, '0.00', 'delvalegarrafa.jpg', '0', '3.80', 0, '10.00', '', '', '', '5.00', 1),
(32, 33, 'SUCO GARRAFA DIVERSOS', 3, '0.00', 'sucogarrafa.jpg', '0', '3.00', 0, '10.00', '', '', '', '5.00', 1),
(33, 34, 'COCA 2,5', 3, '0.00', 'coca25.jpg', '0', '9.50', 0, '10.00', '', '', '', '5.00', 1),
(34, 35, 'REFRIGERANTE 2L', 3, '0.00', 'refri2.jpg', '0', '8.00', 0, '10.00', '', '', '', '5.00', 1),
(35, 36, 'SUCO MAQUINA G', 3, '0.00', 'sucogrande.jpg', '0', '2.00', 0, '10.00', '', '', '', '5.00', 1),
(36, 37, 'SUCO MAQUINA PEQUENO', 3, '0.00', 'sucopequeno.jpg', '0', '1.00', 0, '10.00', '', '', '', '5.00', 1),
(37, 38, 'SUCO POLPA AGUA', 3, '0.00', 'polpa.jpg', '0', '5.50', 0, '10.00', '', '', '', '5.00', 1),
(38, 39, 'SUCO POLPA LEITE', 3, '0.00', 'polpaleite.jpg', '0', '6.50', 0, '10.00', '', '', '', '5.00', 1),
(39, 40, 'PEDACO DE TORTA', 2, '0.00', 'pedtorta.jpg', '0', '5.00', 0, '10.00', '', '', '', '5.00', 1),
(40, 41, 'CAFE', 3, '0.00', 'cafe.jpg', '0', '2.00', 0, '10.00', '', '', '', '5.00', 1),
(41, 42, 'SUCO NATURAL', 3, '0.00', 'suconatural.jpg', '0', '6.50', 0, '10.00', '', '', '', '5.00', 1),
(42, 43, 'BEIRUTE A MODA', 4, '0.00', 'bmoda.jpg', '0', '19.99', 0, '10.00', '', '', '', '5.00', 1),
(43, 44, 'BEIRUTE ATUM', 4, '0.00', 'batum.jpg', '0', '19.99', 0, '10.00', '', '', '', '5.00', 1),
(44, 45, 'BEIRUTE FRANGO C/ CATUPIRY', 4, '0.00', 'bfrangocatupiry.jpg', '0', '19.99', 0, '10.00', '', '', '', '5.00', 1),
(45, 46, 'BEIRUTE CALABRESA', 4, '0.00', 'bcalabresa.jpg', '0', '19.99', 0, '10.00', '', '', '', '5.00', 1),
(46, 47, 'BEIRUTE CALABRESA C/ CATUPIR', 4, '0.00', 'bcalabresacatupiry.jpg', '0', '19.99', 0, '10.00', '', '', '', '5.00', 1),
(47, 48, 'BEIRUTE FRANGO', 4, '0.00', 'bfrango.jpg', '0', '19.99', 0, '10.00', '', '', '', '5.00', 1),
(48, 49, 'PIZZA MUSSARELA', 5, '0.00', 'pmussarela.jpg', '0', '27.00', 0, '10.00', '', '', '', '5.00', 1),
(49, 50, 'PIZZA CALABRESA', 5, '0.00', 'pcalabresa.jpg', '0', '27.00', 0, '10.00', '', '', '', '5.00', 1),
(50, 51, 'PIZZA CATUPIRY', 5, '0.00', 'pcatupiry.jpg', '0', '28.00', 0, '10.00', '', '', '', '5.00', 1),
(51, 52, 'PIZZA MARGUERITA', 5, '0.00', 'pmarguerita.jpg', '0', '28.00', 0, '10.00', '', '', '', '5.00', 1),
(52, 53, 'PIZZA ESCAROLA', 5, '0.00', 'pescarola.jpg', '0', '28.00', 0, '10.00', '', '', '', '5.00', 1),
(53, 54, 'PIZZA DOIS QUEIJOS', 5, '0.00', 'p2queijos.jpg', '0', '29.00', 0, '10.00', '', '', '', '5.00', 1),
(54, 55, 'PIZZA PORTUGUESA', 5, '0.00', 'pportuguesa.jpg', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(55, 56, 'PIZZA BAIANA', 5, '0.00', 'pbaiana.jpg', '0', '29.00', 0, '10.00', '', '', '', '5.00', 1),
(56, 57, 'PIZZA TOSCANA', 5, '0.00', 'ptoscana.jpg', '0', '31.00', 0, '10.00', '', '', '', '5.00', 1),
(57, 58, 'PIZZA MILHO VERDE', 5, '0.00', 'pmilho.jpg', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(58, 59, 'PIZZA PALMITO', 5, '0.00', 'ppalmito.jpg', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(59, 60, 'PIZZA 3 QUEIJOS', 5, '0.00', 'p3queijos.jpg', '0', '29.00', 0, '10.00', '', '', '', '5.00', 1),
(60, 61, 'PIZZA ESPANHOLA', 5, '0.00', 'pespanhola.jpg', '0', '29.00', 0, '10.00', '', '', '', '5.00', 1),
(61, 62, 'PIZZA CATOLES', 5, '0.00', 'pcatoles.jpg', '0', '29.00', 0, '10.00', '', '', '', '5.00', 1),
(62, 63, 'PIZZA ATUM', 5, '0.00', 'patum.jpg', '0', '33.00', 0, '10.00', '', '', '', '5.00', 1),
(63, 64, 'PIZZA FRANGO E CATUPIRY', 5, '0.00', 'pfrangocatupiry.jpg', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(64, 65, 'PIZZA JARDINEIRA', 5, '0.00', 'pjardineira.jpg', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(65, 66, 'PIZZA A MODA', 5, '0.00', 'pmoda.jpg', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(66, 67, 'PIZZA BRASILEIRA', 5, '0.00', 'pbrasileira.jpg', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(67, 68, 'PIZZA 4 QUEIJO', 5, '0.00', 'p4queijos.jpg', '0', '33.00', 0, '10.00', '', '', '', '5.00', 1),
(68, 69, 'PASTEL DE CARNE', 6, '0.00', 'pastel.jpg', '0', '5.00', 0, '10.00', '', '', '', '5.00', 1),
(69, 70, 'PASTEL DE QUEIJO', 6, '0.00', 'pastel.jpg', '0', '5.00', 0, '10.00', '', '', '', '5.00', 1),
(70, 71, 'PASTEL DE CALABRESA', 6, '0.00', 'pastel.jpg', '0', '5.00', 0, '10.00', '', '', '', '5.00', 1),
(71, 72, 'PASTEL DE ATUM', 6, '0.00', 'pastel.jpg', '0', '5.00', 0, '10.00', '', '', '', '5.00', 1),
(72, 73, 'PASTEL DE CATUPIRY', 6, '0.00', 'pastel.jpg', '0', '5.00', 0, '10.00', '', '', '', '5.00', 1),
(73, 74, 'PASTEL DE FRANGO E CATUPIRY', 6, '0.00', 'pastel.jpg', '0', '6.00', 0, '10.00', '', '', '', '5.00', 1),
(74, 75, 'PASTEL DE BAURU', 6, '0.00', 'pastel.jpg', '0', '6.00', 0, '10.00', '', '', '', '5.00', 1),
(75, 76, 'PASTEL DE ESCAROLA', 6, '0.00', 'pastel.jpg', '0', '6.00', 0, '10.00', '', '', '', '5.00', 1),
(76, 77, 'PASTEL DE PIZZA', 6, '0.00', 'pastel.jpg', '0', '6.00', 0, '10.00', '', '', '', '5.00', 1),
(77, 78, 'PASTEL DE CALABRESA C QUEIJO', 6, '0.00', 'pastel.jpg', '0', '6.00', 0, '10.00', '', '', '', '5.00', 1),
(78, 79, 'PASTEL DE CARNE C/ QUEIJO', 6, '0.00', 'pastel.jpg', '0', '6.00', 0, '10.00', '', '', '', '5.00', 1),
(79, 80, 'MISTO QUENTE', 7, '0.00', 'mistoquente.jpg', '0', '5.50', 0, '10.00', '', '', '', '5.00', 1),
(80, 81, 'X-BURGER', 7, '0.00', 'xburguer.jpg', '0', '5.50', 0, '10.00', '', '', '', '5.00', 1),
(81, 82, 'X-SALADA', 7, '0.00', 'xsalada.jpg', '0', '8.00', 0, '10.00', '', '', '', '5.00', 1),
(82, 83, 'AMERICANO', 7, '0.00', 'americano.jpg', '0', '7.00', 0, '10.00', '', '', '', '5.00', 1),
(83, 84, 'X-BACON', 7, '0.00', 'xbacon.jpg', '0', '8.00', 0, '10.00', '', '', '', '5.00', 1),
(84, 85, 'X-TUDO', 7, '0.00', 'xtudo.jpg', '0', '12.00', 0, '10.00', '', '', '', '5.00', 1),
(85, 86, 'PORCAO FRITAS', 8, '0.00', 'porcaofritas.jpg', '0', '15.00', 0, '10.00', '', '', '', '5.00', 1),
(86, 87, 'PORCAO DE CALABRESA', 8, '0.00', 'porcaocalabresa.jpg', '0', '20.00', 0, '10.00', '', '', '', '5.00', 1),
(87, 88, 'PORCAO MISTA', 8, '0.00', 'porcaomista.jpg', '0', '22.00', 0, '10.00', '', '', '', '5.00', 1),
(88, 89, 'PIRULITO', 9, '0.00', 'pirulitos.jpg', '0', '0.50', 0, '10.00', '', '', '', '5.00', 1),
(89, 90, 'BALAS', 9, '0.00', 'balas.jpg', '0', '0.15', 0, '10.00', '', '', '', '5.00', 1),
(90, 91, 'CHOCOLATE SUFLAIR', 9, '0.00', 'suflair.jpg', '0', '4.50', 0, '10.00', '', '', '', '5.00', 1),
(91, 92, 'BOM BOM', 9, '0.00', 'bombom.jpg', '0', '1.50', 0, '10.00', '', '', '', '5.00', 1),
(92, 93, 'CHICLETE', 9, '0.00', 'chicletes.jpg', '0', '0.25', 0, '10.00', '', '', '', '5.00', 1),
(93, 94, 'TIC TAC', 9, '0.00', 'tictac.jpg', '0', '2.00', 0, '10.00', '', '', '', '5.00', 1),
(94, 95, 'HALLS', 9, '0.00', 'halls.jpg', '0', '2.50', 0, '10.00', '', '', '', '5.00', 1),
(95, 96, 'TRIDENT', 9, '0.00', 'tridente.jpg', '0', '2.50', 0, '10.00', '', '', '', '5.00', 1),
(96, 97, 'CHOCOLATE PRESTIGIO', 9, '0.00', 'prestigio.jpg', '0', '2.50', 0, '10.00', '', '', '', '5.00', 1),
(97, 98, 'CHOKITO', 9, '0.00', 'chokito.png', '0', '2.50', 0, '10.00', '', '', '', '5.00', 1),
(98, 99, 'DOCE DIVERSOS', 9, '0.00', 'docesdiversos.jpg', '0', '2.00', 0, '10.00', '', '', '', '5.00', 1),
(99, 100, 'PACOCA', 9, '0.00', 'pacoca.jpg', '0', '0.50', 0, '10.00', '', '', '', '5.00', 1),
(100, 101, 'ESFIHA DOCE', 1, '0.00', 'esfihadoce.jpg', '0', '3.60', 0, '10.00', '', '', '', '5.00', 1),
(101, 102, 'PASTEL DOCE', 6, '0.00', 'pastel.jpg', '0', '6.00', 0, '10.00', '', '', '', '5.00', 1),
(102, 103, 'ESFIHA DE BACON', 1, '0.00', 'esfihabacon.jpg', '0', '2.70', 0, '10.00', '', '', '', '5.00', 1),
(103, 104, 'FATIA DE PIZZA', 7, '0.00', 'fatiapizza.jpg', '0', '3.50', 0, '10.00', '', '', '', '5.00', 1),
(104, 106, 'FOGAZZA 1 SABOR', 2, '0.00', 'fogaca1.jpg', '0', '3.50', 0, '10.00', '', '', '', '5.00', 1),
(105, 123, 'X-CALABRESA', 7, '0.00', 'xcalabresa.jpg', '0', '8.00', 0, '10.00', '', '', '', '5.00', 1),
(106, 124, 'X-EGG', 7, '0.00', 'xegg.jpg', '0', '8.00', 0, '10.00', '', '', '', '5.00', 1),
(107, 110, 'BEIRUTE SIMPLES', 4, '0.00', 'beirutesimples.jpg', '0', '19.99', 0, '10.00', '', '', '', '5.00', 1),
(108, 111, 'ESFIHA CALABRESA C/ QUEIJO', 1, '0.00', 'calabresaqueijo.jpg', '0', '3.00', 0, '10.00', '', '', '', '5.00', 1),
(109, 112, 'SUCO 2 SABORES', 3, '0.00', 'suco2sabores.jpg', '0', '7.00', 0, '10.00', '', '', '', '5.00', 1),
(110, 113, 'LANCHE BAURU', 7, '0.00', 'lanchebauru.jpg', '0', '5.00', 0, '10.00', '', '', '', '5.00', 1),
(111, 118, 'CERVEJA GARRAFA', 3, '0.00', 'cervejagarrafa.jpg', '0', '10.00', 0, '10.00', '', '', '', '5.00', 1),
(112, 555, 'CHOCOLATE QUENTE GRANDE', 3, '0.00', 'chocolatequente.jpg', '0', '5.50', 0, '10.00', '', '', '', '5.00', 1),
(113, 554, 'CHOCOLATE QUENTE MEDIO', 3, '0.00', 'chocolatequente.jpg', '0', '3.50', 0, '10.00', '', '', '', '5.00', 1),
(114, 121, 'ESFIHA DE FRANGO C/ CHEDDAR', 1, '0.00', 'frangocheddar.jpg', '0', '3.20', 0, '10.00', '', '', '', '5.00', 1),
(115, 122, 'PASTEL ESPECIAL', 6, '0.00', 'pastel.jpg', '0', '7.00', 0, '10.00', '', '', '', '5.00', 1),
(116, 125, 'CHOCOLATE QUENTE', 3, '0.00', 'chocolatequente.jpg', '0', '2.50', 0, '10.00', '', '', '', '5.00', 1),
(117, 126, 'PAO NA CHAPA', 7, '0.00', 'paonachapa.jpg', '0', '1.50', 0, '10.00', '', '', '', '5.00', 1),
(118, 127, 'PASTEL PALMITO', 6, '0.00', 'pastel.jpg', '0', '5.00', 0, '10.00', '', '', '', '5.00', 1),
(119, 128, 'HENIKEN LONG NECK', 3, '0.00', 'henikenlong.jpg', '0', '6.00', 0, '10.00', '', '', '', '5.00', 1),
(120, 11, 'ESFIHAA DE MILHO', 1, '0.00', 'milhocatupiry.jpg', '0', '2.80', 0, '10.00', '', '', '', '5.00', 1),
(121, 230, 'SCHWEPPES', 3, '0.00', 'schweppes.jpg', '0', '4.50', 0, '10.00', '', '', '', '5.00', 1),
(122, 130, 'REFRI CONVENCAO', 3, '0.00', 'refriconvencao.jpg', '0', '2.50', 0, '10.00', '', '', '', '5.00', 1),
(123, 131, 'FOGAZZA 2 SABORES', 2, '0.00', 'fogaca1.jpg', '0', '4.50', 0, '10.00', '', '', '', '5.00', 1),
(124, 132, 'BROTINHO 4 QUEIJOS', 5, '0.00', '4queijos.jpg', '0', '18.00', 0, '10.00', '', '', '', '5.00', 1),
(125, 133, 'BROTINHO CALABRESA', 5, '0.00', 'calabresa.jpg', '0', '17.00', 0, '10.00', '', '', '', '5.00', 1),
(126, 134, 'BROTINHO MUSSARELA', 5, '0.00', 'queijo.jpg', '0', '17.00', 0, '10.00', '', '', '', '5.00', 1),
(127, 135, 'BROTINHO FRANGO CATUPIRY', 5, '0.00', 'frangorequeijao.jpg', '0', '18.00', 0, '10.00', '', '', '', '5.00', 1),
(128, 136, 'BROTINHO CATOLES', 5, '0.00', 'catupiry.jpg', '0', '18.00', 0, '10.00', '', '', '', '5.00', 1),
(129, 137, 'BROTINHO CATUPIRY', 5, '0.00', 'catupiry.jpg', '0', '18.00', 0, '10.00', '', '', '', '5.00', 1),
(130, 138, 'BROTINHO MARGUERITA', 5, '0.00', 'calabresa.jpg', '0', '18.00', 0, '10.00', '', '', '', '5.00', 1),
(131, 139, 'BROTINHO ESCAROLA', 5, '0.00', 'catupiry.jpg', '0', '18.00', 0, '10.00', '', '', '', '5.00', 1),
(132, 140, 'BROTINHO DOIS QUEIJOS', 5, '0.00', '4queijos.jpg', '0', '18.00', 0, '10.00', '', '', '', '5.00', 1),
(133, 141, 'BROTINHO PORTUGUESA', 5, '0.00', 'portuguesa.jpg', '0', '18.00', 0, '10.00', '', '', '', '5.00', 1),
(134, 142, 'BROTINHO BAIANA', 5, '0.00', 'bauru.jpg', '0', '18.00', 0, '10.00', '', '', '', '5.00', 1),
(135, 143, 'BROTINHO TOSCANA', 5, '0.00', 'baconqueijo.jpg', '0', '18.00', 0, '10.00', '', '', '', '5.00', 1),
(136, 144, 'BROTINHO MILHO VERDE', 5, '0.00', 'milhocatupiry.jpg', '0', '18.00', 0, '10.00', '', '', '', '5.00', 1),
(137, 145, 'BROTINHO PALMITO', 5, '0.00', 'palmito.jpg', '0', '18.00', 0, '10.00', '', '', '', '5.00', 1),
(138, 146, 'BROTINHO 3 QUEIJOS', 5, '0.00', '4queijos.jpg', '0', '18.00', 0, '10.00', '', '', '', '5.00', 1),
(139, 147, 'BROTINHO ESPANHOLA', 5, '0.00', 'bauru.jpg', '0', '18.00', 0, '10.00', '', '', '', '5.00', 1),
(140, 148, 'BROTINHO ATUM', 5, '0.00', 'atum.jpg', '0', '18.00', 0, '10.00', '', '', '', '5.00', 1),
(141, 149, 'BROTINHO JARDINEIRA', 5, '0.00', 'calabresa.jpg', '0', '18.00', 0, '10.00', '', '', '', '5.00', 1),
(142, 150, 'BROTINHO A MODA', 5, '0.00', 'bauru.jpg', '0', '18.00', 0, '10.00', '', '', '', '5.00', 1),
(143, 151, 'BROTINHO BRASILEIRA', 5, '0.00', 'calabresa.jpg', '0', '18.00', 0, '10.00', '', '', '', '5.00', 1),
(144, 152, 'PAO DE QUEIJO', 2, '0.00', 'paodequeijo.jpg', '0', '2.50', 0, '10.00', '', '', '', '5.00', 1),
(145, 153, 'BHAMA ZERO GARRF', 3, '0.00', 'bramazero.jpg', '0', '5.00', 0, '10.00', '', '', '', '5.00', 1),
(146, 156, 'MEIA PORCAO FRITAS', 8, '0.00', 'meiaporcaofritas.jpg', '0', '7.50', 0, '10.00', '', '', '', '5.00', 1),
(147, 157, 'PASTEL ATUM E CATUPIRY', 6, '0.00', 'pastel.jpg', '0', '6.00', 0, '10.00', '', '', '', '5.00', 1),
(148, 158, 'REDBULL', 3, '0.00', 'redbull.jpg', '0', '12.00', 0, '10.00', '', '', '', '5.00', 1),
(149, 159, 'AGUA TONICA', 3, '0.00', 'tonica.jpg', '0', '4.50', 0, '10.00', '', '', '', '5.00', 1),
(150, 160, 'PASTEL ATUM COM QUEIJO', 6, '0.00', 'pastel.jpg', '0', '6.00', 0, '10.00', '', '', '', '5.00', 1),
(151, 161, 'PASTEL DE FRANGO', 6, '0.00', 'pastel.jpg', '0', '5.00', 0, '10.00', '', '', '', '5.00', 1),
(152, 162, 'SUCO MAQUINA VIAGEM', 3, '0.00', 'sucogrande.jpg', '0', '3.00', 0, '10.00', '', '', '', '5.00', 1),
(153, 163, 'ESFIHA DE CALABRESA COM QUEI', 1, '0.00', 'calabresacatupiry.jpg', '0', '2.90', 0, '10.00', '', '', '', '5.00', 1),
(154, 165, 'SMIrNOFF', 3, '0.00', 'smirnoff.jpg', '0', '7.00', 0, '10.00', '', '', '', '5.00', 1),
(155, 166, 'PASTEL DE VENTO', 6, '0.00', 'pastel.jpg', '0', '3.00', 0, '10.00', '', '', '', '5.00', 1),
(156, 167, 'MEIA PORCAO', 8, '0.00', 'meiaporcao.jpg', '0', '7.50', 0, '10.00', '', '', '', '5.00', 1),
(157, 168, 'MEIA PORCAO', 8, '0.00', 'meiaporcao.jpg', '0', '7.50', 0, '10.00', '', '', '', '5.00', 1),
(158, 169, 'SORVETE JUND FRUTY', 10, '0.00', 'sorvetejund.jpg', '0', '2.00', 0, '10.00', '', '', '', '5.00', 1),
(159, 170, 'MAX', 10, '0.00', 'sorvetemax.jpg', '0', '5.00', 0, '10.00', '', '', '', '5.00', 1),
(160, 171, 'NAPOLITANO', 10, '0.00', 'sorvetenapolitano.jpg', '0', '3.25', 0, '10.00', '', '', '', '5.00', 1),
(161, 172, 'GRANCONE', 10, '0.00', 'sorvetegrancone.jpg', '0', '4.50', 0, '10.00', '', '', '', '5.00', 1),
(162, 173, 'BIANCO,SKIMO,BANANA', 10, '0.00', 'skimo.jpg', '0', '3.25', 0, '10.00', '', '', '', '5.00', 1),
(163, 174, 'BRIGOLE,CHOCOBAR', 10, '0.00', 'brigole.jpg', '0', '3.75', 0, '10.00', '', '', '', '5.00', 1),
(164, 175, 'MINI BRIGOLE, SKIMO', 10, '0.00', 'minibrigole.jpg', '0', '6.50', 0, '10.00', '', '', '', '5.00', 1),
(165, 176, 'FASCINO 215', 10, '0.00', 'fascino.jpg', '0', '5.00', 0, '10.00', '', '', '', '5.00', 1),
(166, 177, 'SUNDAE', 10, '0.00', 'sundae.jpg', '0', '3.25', 0, '10.00', '', '', '', '5.00', 1),
(167, 178, 'COPAO', 10, '0.00', 'copao.jpg', '0', '4.50', 0, '10.00', '', '', '', '5.00', 1),
(168, 179, 'JUNDMILK', 10, '0.00', 'jundmilk.jpg', '0', '2.00', 0, '10.00', '', '', '', '5.00', 1),
(169, 180, 'AZULOKO', 10, '0.00', 'azuloko.jpg', '0', '1.50', 0, '10.00', '', '', '', '5.00', 1),
(170, 181, 'ALGODAO DOCE', 10, '0.00', 'algodaodoce.jpg', '0', '2.25', 0, '10.00', '', '', '', '5.00', 1),
(171, 182, 'ACAI 220G', 10, '0.00', 'acai200.jpg', '0', '6.90', 0, '10.00', '', '', '', '5.00', 1),
(172, 183, 'ACAI PALITO', 10, '0.00', 'acaipalito.jpg', '0', '3.00', 0, '10.00', '', '', '', '5.00', 1),
(173, 184, 'ACAI 400 G', 10, '0.00', 'acai400.jpg', '0', '16.50', 0, '10.00', '', '', '', '5.00', 1),
(174, 185, 'ESFIHA CATUPIRY ORIGINAL', 1, '0.00', 'catupiry.jpg', '0', '4.20', 0, '10.00', '', '', '', '5.00', 1),
(175, 187, 'LACTA', 9, '0.00', 'lacta.jpg', '0', '2.50', 0, '10.00', '', '', '', '5.00', 1),
(176, 188, 'LAKA', 9, '0.00', 'laka.jpg', '0', '2.00', 0, '10.00', '', '', '', '5.00', 1),
(177, 189, 'SENSACAO', 9, '0.00', 'sensacao.jpg', '0', '2.50', 0, '10.00', '', '', '', '5.00', 1),
(178, 190, 'CATAVENTO', 9, '0.00', 'catavento.jpg', '0', '3.00', 0, '10.00', '', '', '', '5.00', 1),
(179, 191, 'SORVETE POTE 2L', 10, '0.00', 'pote2l.jpg', '0', '19.00', 0, '10.00', '', '', '', '5.00', 1),
(180, 192, 'SORVETE POTE 1,8L', 10, '0.00', 'pote18l.jpg', '0', '20.00', 0, '10.00', '', '', '', '5.00', 1),
(181, 193, 'COCA 2L', 3, '0.00', 'coca2.jpg', '0', '10.00', 0, '10.00', '', '', '', '5.00', 1),
(182, 559, 'MINE REFRI GARRAFA', 3, '0.00', 'minirefri.jpg', '0', '2.50', 0, '10.00', '', '', '', '5.00', 1),
(183, 195, 'SUCO SKINKA', 3, '0.00', 'skinka.jpg', '0', '3.50', 0, '10.00', '', '', '', '5.00', 1),
(184, 560, 'MINE REFRI LATA', 3, '0.00', 'minilata.jpg', '0', '3.00', 0, '10.00', '', '', '', '5.00', 1),
(185, 198, 'ICETEA', 4, '0.00', 'bmoda.jpg', '0', '4.00', 0, '10.00', '', '', '', '5.00', 1),
(186, 199, 'ESFIHA FRANGO COM QUEIJO', 1, '0.00', 'frangorequeijao.jpg', '0', '3.20', 0, '10.00', '', '', '', '5.00', 1),
(187, 200, 'COCA 600', 3, '0.00', 'coca600.jpg', '0', '6.00', 0, '10.00', '', '', '', '5.00', 1),
(188, 201, 'COCA 300ML', 3, '0.00', 'coca300.jpg', '0', '3.00', 0, '10.00', '', '', '', '5.00', 1),
(189, 202, 'GRECO', 10, '0.00', 'sorvetegrego.jpg', '0', '4.50', 0, '10.00', '', '', '', '5.00', 1),
(190, 203, 'MUSSE DE MARACUJA OU LIMAO', 10, '0.00', 'maracujalimao.jpg', '0', '3.75', 0, '10.00', '', '', '', '5.00', 1),
(191, 204, 'ENERGETICO DIVERSOS', 3, '0.00', 'energeticodiversos.jpg', '0', '8.00', 0, '10.00', '', '', '', '5.00', 1),
(192, 205, 'TRUFFA', 9, '0.00', 'truffa.jpg', '0', '2.00', 0, '10.00', '', '', '', '5.00', 1),
(193, 206, 'CATUPIRY ORIGINAL', 1, '0.00', 'catupiry.jpg', '0', '2.00', 0, '10.00', '', '', '', '5.00', 1),
(194, 207, 'DOLLYNHO', 3, '0.00', 'minidolly.jpg', '0', '2.50', 0, '10.00', '', '', '', '5.00', 1),
(195, 208, 'ESFIGA SEM RECHEIO', 1, '0.00', 'esfihavazia.jpg', '0', '1.70', 0, '10.00', '', '', '', '5.00', 1),
(196, 209, 'PAO COM OVO', 7, '0.00', 'paocomovo.jpg', '0', '3.00', 0, '10.00', '', '', '', '5.00', 1),
(197, 210, 'ESFIHA 2 QUEIJOS', 1, '0.00', 'queijo.jpg', '0', '2.90', 0, '10.00', '', '', '', '5.00', 1),
(198, 211, 'EISENBAHN LONGNECK', 3, '0.00', 'eisenbahnlongneck.jpg', '0', '6.00', 0, '10.00', '', '', '', '5.00', 1),
(199, 212, 'BUDWEISER LONG NECK', 3, '0.00', 'budweiserlongneck.jpg', '0', '6.00', 0, '10.00', '', '', '', '5.00', 1),
(200, 213, 'PAO DE MEL', 9, '0.00', 'paodemel.jpg', '0', '4.00', 0, '10.00', '', '', '', '5.00', 1),
(201, 214, 'SUCO DEL VALE LATA', 3, '0.00', 'dellvalelata.jpg', '0', '4.50', 0, '10.00', '', '', '', '5.00', 1),
(202, 215, 'REFRI FANTA 250', 3, '0.00', 'fanta250.jpg', '0', '2.50', 0, '10.00', '', '', '', '5.00', 1),
(203, 216, 'ESFIHA CARNE COM QUEIJO', 1, '0.00', 'calabresacatupiry.jpg', '0', '2.90', 0, '10.00', '', '', '', '5.00', 1),
(204, 217, 'X- MAIONESE', 7, '0.00', 'xmaionese.jpg', '0', '6.00', 0, '10.00', '', '', '', '5.00', 1),
(205, 218, 'SUCO COPO.. SUQO', 3, '0.00', 'sucocopaq.jpg', '0', '4.50', 0, '10.00', '', '', '', '5.00', 1),
(206, 219, 'SALGADOS', 2, '0.00', 'salgados.jpg', '0', '2.00', 0, '10.00', '', '', '', '5.00', 1),
(207, 220, 'MINEE SALGADOS', 2, '0.00', 'salgados.jpg', '0', '0.56', 0, '10.00', '', '', '', '5.00', 1),
(208, 221, 'SUFRESH LATA', 3, '0.00', 'sufreshlata.jpg', '0', '4.00', 0, '10.00', '', '', '', '5.00', 1),
(209, 222, 'BRIGADEIRO', 9, '0.00', 'brigadeiro.jpg', '0', '1.99', 0, '10.00', '', '', '', '5.00', 1),
(210, 223, 'LANCHE NATURAL', 7, '0.00', 'lanchenatural.jpg', '0', '4.99', 0, '10.00', '', '', '', '5.00', 1),
(211, 224, 'FATIA BEIRUTE', 4, '0.00', 'fatiabeirute.jpg', '0', '4.00', 0, '10.00', '', '', '', '5.00', 1),
(212, 225, 'ESFIHA DE ALHO COM QUEIJO', 1, '0.00', '4queijos.jpg', '0', '3.20', 0, '10.00', '', '', '', '5.00', 1),
(213, 226, 'SUCO COPO SUQO ACAI', 3, '0.00', 'sucocopoacai.jpg', '0', '6.00', 0, '10.00', '', '', '', '5.00', 1),
(214, 229, 'ESFIHA DE ATUM COM QUEIJO', 1, '0.00', 'carnequeijo.jpg', '0', '3.20', 0, '10.00', '', '', '', '5.00', 1),
(215, 228, 'PASTEL FRNGO COM MILHO', 6, '0.00', 'pastel.jpg', '0', '6.00', 0, '10.00', '', '', '', '5.00', 1),
(216, 231, 'VIAGEM', 3, '0.00', 'no_image.png', '0', '0.50', 0, '10.00', '', '', '', '5.00', 1),
(217, 232, 'PENADINHO', 10, '0.00', 'penadinho.jpg', '0', '2.50', 0, '10.00', '', '', '', '5.00', 1),
(218, 233, 'DUPLO', 7, '0.00', 'lancheduplo.jpg', '0', '3.00', 0, '10.00', '', '', '', '5.00', 1),
(219, 234, 'NUCITA', 9, '0.00', 'nucita.jpg', '0', '1.00', 0, '10.00', '', '', '', '5.00', 1),
(220, 235, 'BATON GAROTO', 9, '0.00', 'batom.jpg', '0', '2.00', 0, '10.00', '', '', '', '5.00', 1),
(221, 236, 'COCADA PEQUENA', 9, '0.00', 'cocada.jpg', '0', '1.00', 0, '10.00', '', '', '', '5.00', 1),
(222, 237, 'PASTEL CATUPIRY ORIGINAL', 6, '0.00', 'pastel.jpg', '0', '6.70', 0, '10.00', '', '', '', '5.00', 1),
(223, 238, 'SALGADO PRA REVENDAAA', 2, '0.00', 'salgados.jpg', '0', '1.80', 0, '10.00', '', '', '', '5.00', 1),
(224, 239, 'BROTINHO BRIGADEIRO', 5, '0.00', 'brotobrigadeiro.jpg', '0', '20.00', 0, '10.00', '', '', '', '5.00', 1),
(225, 240, 'COCA 300', 3, '0.00', 'coca300.jpg', '0', '3.00', 0, '10.00', '', '', '', '5.00', 1),
(226, 241, 'PASTEL BACON COM QUEIJO', 6, '0.00', 'pastel.jpg', '0', '6.00', 0, '10.00', '', '', '', '5.00', 1),
(227, 242, 'PAO QUEIJO QUENTE', 7, '0.00', 'paoqueijoquente.jpg', '0', '3.00', 0, '10.00', '', '', '', '5.00', 1),
(228, 243, 'DOLLY', 3, '0.00', 'dolly.jpg', '0', '6.00', 0, '10.00', '', '', '', '5.00', 1),
(229, 244, 'PIZZA CATUPIRYY ORIGINAL', 5, '0.00', 'pcatupiry.jpg', '0', '41.00', 0, '10.00', '', '', '', '5.00', 1),
(230, 245, 'ACRECIMO DE CATUPIRY ORIG, P', 5, '0.00', 'no_image.png', '0', '7.00', 0, '10.00', '', '', '', '5.00', 1),
(231, 246, 'PIZZA PAULISTA', 5, '0.00', 'ppaulista.jpg', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(232, 247, 'PIZZA BROCOLIS COM REQUEIJAO', 5, '0.00', 'pbrocolisrequeijao.jpg', '0', '25.00', 0, '10.00', '', '', '', '5.00', 1),
(233, 248, 'PIZZA CAIPIRA', 5, '0.00', 'pcaipira.jpg', '0', '33.00', 0, '10.00', '', '', '', '5.00', 1),
(234, 249, 'PIZZA AMERICANA', 5, '0.00', 'pamericana.jpg', '0', '31.00', 0, '10.00', '', '', '', '5.00', 1),
(235, 250, 'PIZZA JARDINEIRA II', 5, '0.00', 'pjardineira.jpg', '0', '32.00', 0, '10.00', '', '', '', '5.00', 1),
(236, 251, 'PIZZA VEGETARIANA', 5, '0.00', 'pvegetariana.jpg', '0', '34.00', 0, '10.00', '', '', '', '5.00', 1),
(237, 252, 'PIZZA SICILIANA', 5, '0.00', 'psiciliana.jpg', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(238, 253, 'PIZZA PIZZAIOLO', 5, '0.00', 'pcaipira.jpg', '0', '32.00', 0, '10.00', '', '', '', '5.00', 1),
(239, 254, 'PIZZA ATUM ESPECIAL', 5, '0.00', 'patum.jpg', '0', '34.00', 0, '10.00', '', '', '', '5.00', 1),
(240, 255, 'PIZZA PRESUNTO', 5, '0.00', 'ppresunto.jpg', '0', '28.00', 0, '10.00', '', '', '', '5.00', 1),
(241, 256, 'PIZZA TROPICAL', 5, '0.00', 'ptropical.jpg', '0', '31.00', 0, '10.00', '', '', '', '5.00', 1),
(242, 257, 'PIZZA MINEIRA', 5, '0.00', 'pjardineira.jpg', '0', '33.00', 0, '10.00', '', '', '', '5.00', 1),
(243, 258, 'PIZZA GREGA', 5, '0.00', 'pgrega.jpg', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(244, 259, 'PASTEL BROCOLIS COM QUEIJO', 6, '0.00', 'pastel.jpg', '0', '5.00', 0, '10.00', '', '', '', '5.00', 1),
(245, 500, 'DOLLY 2L', 3, '0.00', 'dolly2.jpg', '0', '6.00', 0, '10.00', '', '', '', '5.00', 1),
(246, 501, 'PALETAS MEXICANA', 10, '0.00', 'paletas,exicanas.jpg', '0', '6.50', 0, '10.00', '', '', '', '5.00', 1),
(247, 502, 'SHUEPPS', 3, '0.00', 'schweppes2.jpg', '0', '4.50', 0, '10.00', '', '', '', '5.00', 1),
(248, 503, 'CERVEJA PETRA', 3, '0.00', 'cervejapreta.jpg', '0', '5.00', 0, '10.00', '', '', '', '5.00', 1),
(249, 504, 'SUCO BIOLEVE', 3, '0.00', 'sucobioleve.jpg', '0', '3.50', 0, '10.00', '', '', '', '5.00', 1),
(250, 558, 'AGUA DE COCO', 3, '0.00', 'aguacoco.jpg', '0', '3.00', 0, '10.00', '', '', '', '5.00', 1),
(251, 506, 'KAPO', 3, '0.00', 'kapo.jpg', '0', '3.50', 0, '10.00', '', '', '', '5.00', 1),
(252, 507, 'BROTINHO AMERICANO', 5, '0.00', 'pamericana.jpg', '0', '18.00', 0, '10.00', '', '', '', '5.00', 1),
(253, 508, 'BOLO PEDACO', 11, '0.00', 'pedacodebolo.jpg', '0', '3.00', 0, '10.00', '', '', '', '5.00', 1),
(254, 509, 'MASSA DE ESFIHA', 1, '0.00', 'massaesfiha.jpg', '0', '0.80', 0, '10.00', '', '', '', '5.00', 1),
(255, 510, 'PIZZA 2 QUEIJO', 5, '0.00', 'queijo.jpg', '0', '29.00', 0, '10.00', '', '', '', '5.00', 1),
(256, 511, 'PIZZA DE BROCOLIS', 5, '0.00', 'pbrocolisrequeijao.jpg', '0', '24.00', 0, '10.00', '', '', '', '5.00', 1),
(257, 512, 'TORTA INTEIRA', 2, '0.00', 'torta.jpg', '0', '36.00', 0, '10.00', '', '', '', '5.00', 1),
(258, 514, 'CHARGE', 9, '0.00', 'charge.jpg', '0', '2.50', 0, '10.00', '', '', '', '5.00', 1),
(259, 515, 'BALAS ELCALIPTO', 9, '0.00', 'balaselcalipto.jpg', '0', '1.00', 0, '10.00', '', '', '', '5.00', 1),
(260, 516, 'DOCE DE LEITE', 9, '0.00', 'docedeleite.jpg', '0', '0.50', 0, '10.00', '', '', '', '5.00', 1),
(261, 517, 'PROMOCAO', 2, '0.00', 'salgados.jpg', '0', '3.00', 0, '10.00', '', '', '', '5.00', 1),
(262, 518, 'X-EGG-BACON', 7, '0.00', 'xegg.jpg', '0', '9.00', 0, '10.00', '', '', '', '5.00', 1),
(263, 519, 'BROTINHO BROCOLIS', 5, '0.00', 'pbrocolisrequeijao.jpg', '0', '18.00', 0, '10.00', '', '', '', '5.00', 1),
(264, 520, 'BROTINHO TROPICAL', 5, '0.00', 'ptropical.jpg', '0', '18.00', 0, '10.00', '', '', '', '5.00', 1),
(265, 521, 'TAXA', 9, '0.00', 'no_image.png', '0', '2.00', 0, '10.00', '', '', '', '5.00', 1),
(266, 522, 'ESFIHA DE MILHO E FRANGO', 1, '0.00', 'milhocatupiry.jpg', '0', '3.20', 0, '10.00', '', '', '', '5.00', 1),
(267, 523, 'ACRECIMO', 1, '0.00', 'no_image.png', '0', '1.50', 0, '10.00', '', '', '', '5.00', 1),
(268, 524, 'PASTEL 2 QUEIJOS', 6, '0.00', 'pastel.jpg', '0', '6.00', 0, '10.00', '', '', '', '5.00', 1),
(269, 525, 'ALFAJOR', 9, '0.00', 'alfajor.jpg', '0', '4.00', 0, '10.00', '', '', '', '5.00', 1),
(270, 526, 'ENERGETICOS HND', 3, '0.00', 'hinode.jpg', '0', '7.00', 0, '10.00', '', '', '', '5.00', 1),
(271, 556, 'CHICLETE CAIXINHA', 9, '0.00', 'caixachiclete.jpg', '0', '0.50', 0, '10.00', '', '', '', '5.00', 1),
(272, 528, 'PROMOCAO ESFINHA CARNE', 1, '0.00', 'carne.jpg', '0', '0.99', 0, '10.00', '', '', '', '5.00', 1),
(273, 529, 'CONVENCAO', 3, '0.00', 'convencao.jpg', '0', '3.00', 0, '10.00', '', '', '', '5.00', 1),
(274, 530, 'COMBO 1', 1, '0.00', 'combo1.jpg', '0', '29.50', 0, '10.00', '', '', '', '5.00', 1),
(275, 531, 'COMBO 2', 1, '0.00', 'combo2.jpg', '0', '38.50', 0, '10.00', '', '', '', '5.00', 1),
(276, 532, 'COMBO 3', 1, '0.00', 'combo3.jpg', '0', '59.00', 0, '10.00', '', '', '', '5.00', 1),
(277, 533, 'COMBO 4', 1, '0.00', 'combo4.jpg', '0', '61.50', 0, '10.00', '', '', '', '5.00', 1),
(278, 534, 'QUEIJO BRANCO', 1, '0.00', 'queijo.jpg', '0', '2.20', 0, '10.00', '', '', '', '5.00', 1),
(279, 535, 'ESFIHA CHOCOLATE', 1, '0.00', 'esfihachocolate.jpg', '0', '3.60', 0, '10.00', '', '', '', '5.00', 1),
(280, 536, 'ESFIHA BRIGADEIRO', 1, '0.00', 'esfihabrigadeiro.jpg', '0', '3.60', 0, '10.00', '', '', '', '5.00', 1),
(281, 537, 'ESFINHA ROMEU E JULIETA', 1, '0.00', 'romeuejuieta.jpg', '0', '3.60', 0, '10.00', '', '', '', '5.00', 1),
(282, 538, 'ESFIHA PRESTIGIO', 1, '0.00', 'esfihaprestigio.jpg', '0', '3.60', 0, '10.00', '', '', '', '5.00', 1),
(283, 539, 'PIZZA BAURU', 5, '0.00', 'pbauru.jpg', '0', '29.00', 0, '10.00', '', '', '', '5.00', 1),
(284, 540, 'BROTINHO BAURU', 5, '0.00', 'pbauru.jpg', '0', '18.00', 0, '10.00', '', '', '', '5.00', 1),
(285, 541, 'CHOP', 3, '0.00', 'chop.jpg', '0', '6.99', 0, '10.00', '', '', '', '5.00', 1),
(286, 542, '+ UMA CARNE DE HAMBURG', 7, '0.00', 'hamburguer.jpg', '0', '3.00', 0, '10.00', '', '', '', '5.00', 1),
(287, 543, 'FINA', 3, '0.00', 'no_image.png', '0', '3.80', 0, '10.00', '', '', '', '5.00', 1),
(288, 544, 'TODINHO CAIXA', 3, '0.00', 'todinho.jpg', '0', '3.00', 0, '10.00', '', '', '', '5.00', 1),
(289, 545, 'ESTUFA CARNE', 1, '0.00', 'no_image.png', '0', '0.99', 0, '10.00', '', '', '', '5.00', 1),
(290, 546, 'ESTUFA QUEIJO', 1, '0.00', 'no_image.png', '0', '2.20', 0, '10.00', '', '', '', '5.00', 1),
(291, 547, 'LOLO CHOCOLATE', 9, '0.00', 'lolo.jpg', '0', '2.50', 0, '10.00', '', '', '', '5.00', 1),
(292, 548, 'GARRRAFA', 3, '0.00', 'garrafa.jpg', '0', '1.00', 0, '10.00', '', '', '', '5.00', 1),
(293, 550, 'BROTINHO CAIPIRA', 5, '0.00', 'pcaipira.jpg', '0', '18.00', 0, '10.00', '', '', '', '5.00', 1),
(294, 551, 'ESFIHA BACON E REQUEIJAO', 1, '0.00', 'baconqueijo.jpg', '0', '2.90', 0, '10.00', '', '', '', '5.00', 1),
(295, 552, 'GRECO 2L', 10, '0.00', 'grego2l.jpg', '0', '21.00', 0, '10.00', '', '', '', '5.00', 1),
(296, 553, 'ITUBAINA GARRAFA', 3, '0.00', 'itubaina.jpg', '0', '5.00', 0, '10.00', '', '', '', '5.00', 1),
(297, 561, 'ACRECIMO BEIRUTE', 4, '0.00', 'no_image.png', '0', '4.00', 0, '10.00', '', '', '', '5.00', 1),
(298, 562, 'PALETA JUNDIA', 10, '0.00', 'paletajundia.jpg', '0', '5.90', 0, '10.00', '', '', '', '5.00', 1),
(299, 563, 'BROTINHO SICILIANA', 5, '0.00', 'psiciliana.jpg', '0', '18.00', 0, '10.00', '', '', '', '5.00', 1),
(300, 564, 'PIZZA DOCE', 5, '0.00', 'pdoce.jpg', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(301, 565, 'X- CHEDAR', 7, '0.00', 'xcheddar.jpg', '0', '8.00', 0, '10.00', '', '', '', '5.00', 1),
(302, 566, 'BROTINHO MINEIRA', 5, '0.00', 'pjardineira.jpg', '0', '18.00', 0, '10.00', '', '', '', '5.00', 1),
(303, 567, 'PIZZA BROCOLIS E QUEIJO', 5, '0.00', 'pbrocolisrequeijao.jpg', '0', '27.00', 0, '10.00', '', '', '', '5.00', 1),
(304, 568, 'PAO DE QUEIJOO RECHEADO', 2, '0.00', 'paodequeijo.jpg', '0', '3.00', 0, '10.00', '', '', '', '5.00', 1),
(305, 569, 'ITUBAINA 2L', 3, '0.00', 'itubaina2l.jpg', '0', '7.00', 0, '10.00', '', '', '', '5.00', 1),
(306, 570, 'GUARAVITOM', 3, '0.00', 'guaraviton.jpg', '0', '4.00', 0, '10.00', '', '', '', '5.00', 1),
(307, 571, '2 QUEIJOS', 99, '0.00', 'no_image.png', '0', '29.00', 0, '10.00', '', '', '', '5.00', 1),
(308, 572, '3 QUEIJOS', 99, '0.00', 'no_image.png', '0', '29.00', 0, '10.00', '', '', '', '5.00', 1),
(309, 573, '4 QUEIJOS', 99, '0.00', 'no_image.png', '0', '33.00', 0, '10.00', '', '', '', '5.00', 1),
(310, 574, 'A MODA', 99, '0.00', 'no_image.png', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(311, 575, 'AMERICANA', 99, '0.00', 'no_image.png', '0', '31.00', 0, '10.00', '', '', '', '5.00', 1),
(312, 576, 'ATUM', 99, '0.00', 'no_image.png', '0', '33.00', 0, '10.00', '', '', '', '5.00', 1),
(313, 577, 'ATUM ESPECIAL', 99, '0.00', 'no_image.png', '0', '34.00', 0, '10.00', '', '', '', '5.00', 1),
(314, 578, 'BAIANA', 99, '0.00', 'no_image.png', '0', '29.00', 0, '10.00', '', '', '', '5.00', 1),
(315, 579, 'BAURU', 99, '0.00', 'no_image.png', '0', '29.00', 0, '10.00', '', '', '', '5.00', 1),
(316, 580, 'BRASILEIRA', 99, '0.00', 'no_image.png', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(317, 581, 'BROCOLIS COM REQUEIJAO', 99, '0.00', 'no_image.png', '0', '25.00', 0, '10.00', '', '', '', '5.00', 1),
(318, 582, 'BROCOLIS E QUEIJO', 99, '0.00', 'no_image.png', '0', '27.00', 0, '10.00', '', '', '', '5.00', 1),
(319, 583, 'CAIPIRA', 99, '0.00', 'no_image.png', '0', '33.00', 0, '10.00', '', '', '', '5.00', 1),
(320, 584, 'CALABRESA', 99, '0.00', 'no_image.png', '0', '27.00', 0, '10.00', '', '', '', '5.00', 1),
(321, 585, 'CATOLES', 99, '0.00', 'no_image.png', '0', '29.00', 0, '10.00', '', '', '', '5.00', 1),
(322, 586, 'CATUPIRY', 99, '0.00', 'no_image.png', '0', '28.00', 0, '10.00', '', '', '', '5.00', 1),
(323, 587, 'CATUPIRYY ORIGINAL', 99, '0.00', 'no_image.png', '0', '41.00', 0, '10.00', '', '', '', '5.00', 1),
(324, 588, 'DE BROCOLIS', 99, '0.00', 'no_image.png', '0', '24.00', 0, '10.00', '', '', '', '5.00', 1),
(325, 589, 'DOCE', 99, '0.00', 'no_image.png', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(326, 590, 'DOIS QUEIJOS', 99, '0.00', 'no_image.png', '0', '29.00', 0, '10.00', '', '', '', '5.00', 1),
(327, 591, 'ESCAROLA', 99, '0.00', 'no_image.png', '0', '28.00', 0, '10.00', '', '', '', '5.00', 1),
(328, 592, 'ESPANHOLA', 99, '0.00', 'no_image.png', '0', '29.00', 0, '10.00', '', '', '', '5.00', 1),
(329, 593, 'FRANGO E CATUPIRY', 99, '0.00', 'no_image.png', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(330, 594, 'GREGA', 99, '0.00', 'no_image.png', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(331, 595, 'JARDINEIRA', 99, '0.00', 'no_image.png', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(332, 596, 'JARDINEIRA II', 99, '0.00', 'no_image.png', '0', '32.00', 0, '10.00', '', '', '', '5.00', 1),
(333, 597, 'MARGUERITA', 99, '0.00', 'no_image.png', '0', '28.00', 0, '10.00', '', '', '', '5.00', 1),
(334, 598, 'MILHO VERDE', 99, '0.00', 'no_image.png', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(335, 599, 'MINEIRA', 99, '0.00', 'no_image.png', '0', '33.00', 0, '10.00', '', '', '', '5.00', 1),
(336, 600, 'MUSSARELA', 99, '0.00', 'no_image.png', '0', '27.00', 0, '10.00', '', '', '', '5.00', 1),
(337, 601, 'PALMITO', 99, '0.00', 'no_image.png', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(338, 602, 'PAULISTA', 99, '0.00', 'no_image.png', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(339, 603, 'PIZZAIOLO', 99, '0.00', 'no_image.png', '0', '32.00', 0, '10.00', '', '', '', '5.00', 1),
(340, 604, 'PORTUGUESA', 99, '0.00', 'no_image.png', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(341, 605, 'PRESUNTO', 99, '0.00', 'no_image.png', '0', '28.00', 0, '10.00', '', '', '', '5.00', 1),
(342, 606, 'SICILIANA', 99, '0.00', 'no_image.png', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(343, 607, 'TOSCANA', 99, '0.00', 'no_image.png', '0', '31.00', 0, '10.00', '', '', '', '5.00', 1),
(344, 608, 'TROPICAL', 99, '0.00', 'no_image.png', '0', '31.00', 0, '10.00', '', '', '', '5.00', 1),
(345, 609, 'VEGETARIANA', 99, '0.00', 'no_image.png', '0', '34.00', 0, '10.00', '', '', '', '5.00', 1),
(385, 610, '2 QUEIJOS', 98, '0.00', 'no_image.png', '0', '29.00', 0, '10.00', '', '', '', '5.00', 1),
(386, 611, '3 QUEIJOS', 98, '0.00', 'no_image.png', '0', '29.00', 0, '10.00', '', '', '', '5.00', 1),
(387, 612, '4 QUEIJOS', 98, '0.00', 'no_image.png', '0', '33.00', 0, '10.00', '', '', '', '5.00', 1),
(388, 613, 'A MODA', 98, '0.00', 'no_image.png', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(389, 614, 'AMERICANA', 98, '0.00', 'no_image.png', '0', '31.00', 0, '10.00', '', '', '', '5.00', 1),
(390, 615, 'ATUM', 98, '0.00', 'no_image.png', '0', '33.00', 0, '10.00', '', '', '', '5.00', 1),
(391, 616, 'ATUM ESPECIAL', 98, '0.00', 'no_image.png', '0', '34.00', 0, '10.00', '', '', '', '5.00', 1),
(392, 617, 'BAIANA', 98, '0.00', 'no_image.png', '0', '29.00', 0, '10.00', '', '', '', '5.00', 1),
(393, 618, 'BAURU', 98, '0.00', 'no_image.png', '0', '29.00', 0, '10.00', '', '', '', '5.00', 1),
(394, 619, 'BRASILEIRA', 98, '0.00', 'no_image.png', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(395, 620, 'BROCOLIS COM REQUEIJAO', 98, '0.00', 'no_image.png', '0', '25.00', 0, '10.00', '', '', '', '5.00', 1),
(396, 621, 'BROCOLIS E QUEIJO', 98, '0.00', 'no_image.png', '0', '27.00', 0, '10.00', '', '', '', '5.00', 1),
(397, 622, 'CAIPIRA', 98, '0.00', 'no_image.png', '0', '33.00', 0, '10.00', '', '', '', '5.00', 1),
(398, 623, 'CALABRESA', 98, '0.00', 'no_image.png', '0', '27.00', 0, '10.00', '', '', '', '5.00', 1),
(399, 624, 'CATOLES', 98, '0.00', 'no_image.png', '0', '29.00', 0, '10.00', '', '', '', '5.00', 1),
(400, 625, 'CATUPIRY', 98, '0.00', 'no_image.png', '0', '28.00', 0, '10.00', '', '', '', '5.00', 1),
(401, 626, 'CATUPIRYY ORIGINAL', 98, '0.00', 'no_image.png', '0', '41.00', 0, '10.00', '', '', '', '5.00', 1),
(402, 627, 'DE BROCOLIS', 98, '0.00', 'no_image.png', '0', '24.00', 0, '10.00', '', '', '', '5.00', 1),
(403, 628, 'DOCE', 98, '0.00', 'no_image.png', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(404, 629, 'DOIS QUEIJOS', 98, '0.00', 'no_image.png', '0', '29.00', 0, '10.00', '', '', '', '5.00', 1),
(405, 630, 'ESCAROLA', 98, '0.00', 'no_image.png', '0', '28.00', 0, '10.00', '', '', '', '5.00', 1),
(406, 631, 'ESPANHOLA', 98, '0.00', 'no_image.png', '0', '29.00', 0, '10.00', '', '', '', '5.00', 1),
(407, 632, 'FRANGO E CATUPIRY', 98, '0.00', 'no_image.png', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(408, 633, 'GREGA', 98, '0.00', 'no_image.png', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(409, 634, 'JARDINEIRA', 98, '0.00', 'no_image.png', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(410, 635, 'JARDINEIRA II', 98, '0.00', 'no_image.png', '0', '32.00', 0, '10.00', '', '', '', '5.00', 1),
(411, 636, 'MARGUERITA', 98, '0.00', 'no_image.png', '0', '28.00', 0, '10.00', '', '', '', '5.00', 1),
(412, 637, 'MILHO VERDE', 98, '0.00', 'no_image.png', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(413, 638, 'MINEIRA', 98, '0.00', 'no_image.png', '0', '33.00', 0, '10.00', '', '', '', '5.00', 1),
(414, 639, 'MUSSARELA', 98, '0.00', 'no_image.png', '0', '27.00', 0, '10.00', '', '', '', '5.00', 1),
(415, 640, 'PALMITO', 98, '0.00', 'no_image.png', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(416, 641, 'PAULISTA', 98, '0.00', 'no_image.png', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(417, 642, 'PIZZAIOLO', 98, '0.00', 'no_image.png', '0', '32.00', 0, '10.00', '', '', '', '5.00', 1),
(418, 643, 'PORTUGUESA', 98, '0.00', 'no_image.png', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(419, 644, 'PRESUNTO', 98, '0.00', 'no_image.png', '0', '28.00', 0, '10.00', '', '', '', '5.00', 1),
(420, 645, 'SICILIANA', 98, '0.00', 'no_image.png', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1),
(421, 646, 'TOSCANA', 98, '0.00', 'no_image.png', '0', '31.00', 0, '10.00', '', '', '', '5.00', 1),
(422, 647, 'TROPICAL', 98, '0.00', 'no_image.png', '0', '31.00', 0, '10.00', '', '', '', '5.00', 1),
(423, 648, 'VEGETARIANA', 98, '0.00', 'no_image.png', '0', '34.00', 0, '10.00', '', '', '', '5.00', 1),
(431, 0, '1/2 ATUM 1/2 CALABRESA', 100, '0.00', 'no_image.png', '0', '33.00', 0, '10.00', '', '', '', '5.00', 1),
(432, 0, '1/2 CALABRESA 1/2 ATUM', 100, '0.00', 'no_image.png', '0', '33.00', 0, '10.00', '', '', '', '5.00', 1),
(433, 0, '1/3 ATUM 1/3 CALABRESA 1/3 MUSSARELA', 101, '0.00', 'no_image.png', '0', '33.00', 0, '10.00', '', '', '', '5.00', 1),
(434, 0, '1/2 CALABRESA 1/2 MUSSARELA', 100, '0.00', 'no_image.png', '0', '27.00', 0, '10.00', '', '', '', '5.00', 1),
(435, 0, '1/2 MUSSARELA 1/2 CALABRESA', 100, '0.00', 'no_image.png', '0', '27.00', 0, '10.00', '', '', '', '5.00', 1),
(436, 0, '1/2 MUSSARELA 1/2 FRANGO E CATUPIRY', 100, '0.00', 'no_image.png', '0', '30.00', 0, '10.00', '', '', '', '5.00', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(100) CHARACTER SET utf8 NOT NULL,
  `usuario` varchar(50) CHARACTER SET utf8 NOT NULL,
  `senha` varchar(50) CHARACTER SET utf8 NOT NULL,
  `avatar` varchar(150) CHARACTER SET utf8 NOT NULL,
  `id_cargo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `nome`, `usuario`, `senha`, `avatar`, `id_cargo`) VALUES
(1, 'Junior Nascimento', 'jjunior', 'ejwkh24', 'images/boy.png', 1),
(10, 'Jackson Faive', 'jackson', '1234567890', 'images/boy.png', 5),
(11, 'Funcionario1 ', 'francisco', '123456789', 'images/boy.png', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `vendas_history`
--

CREATE TABLE `vendas_history` (
  `id` int(11) NOT NULL,
  `code` int(10) NOT NULL,
  `Produto` varchar(65) CHARACTER SET utf8 NOT NULL,
  `quantidade` int(11) NOT NULL,
  `Preço` decimal(25,2) DEFAULT NULL,
  `Total` decimal(35,2) DEFAULT NULL,
  `obs` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '1',
  `id_produto` int(11) NOT NULL DEFAULT '0',
  `num_nota_fiscal` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `vendas_history`
--

INSERT INTO `vendas_history` (`id`, `code`, `Produto`, `quantidade`, `Preço`, `Total`, `obs`, `category_id`, `id_produto`, `num_nota_fiscal`) VALUES
(1, 1, 'ESFIHA CARNE', 1, '0.99', '0.99', '', 1, 1, NULL),
(2, 575, 'AMERICANA', 1, '31.00', '31.00', '31.00', 99, 311, NULL),
(3, 580, 'BRASILEIRA', 1, '30.00', '30.00', '', 99, 316, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `vendas_suspensas`
--

CREATE TABLE `vendas_suspensas` (
  `id` int(11) NOT NULL,
  `id_suspensao` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_suspensao` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `tipo` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `total` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `venda_balcao_hist`
--

CREATE TABLE `venda_balcao_hist` (
  `id` int(11) NOT NULL,
  `num_venda` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `data_venda` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `forma_pagamento` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `total_dinheiro` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `venda_balcao_hist`
--

INSERT INTO `venda_balcao_hist` (`id`, `num_venda`, `id_produto`, `quantidade`, `obs`, `data_venda`, `forma_pagamento`, `total_dinheiro`) VALUES
(1, 1, 1, 1, '', '2017-01-16 15:22', 'Cartão de Débito', NULL),
(2, 1, 15, 1, '', '2017-01-16 15:22', 'Cartão de Débito', NULL),
(3, 1, 5, 1, '', '2017-01-16 19:28', 'Dinheiro', NULL),
(4, 1, 17, 1, '', '2017-01-16 19:28', 'Dinheiro', NULL),
(5, 2, 3, 1, '', '2017-01-16 19:59', 'Dinheiro', NULL),
(6, 2, 4, 1, '', '2017-01-16 19:59', 'Dinheiro', NULL),
(7, 3, 5, 2, '', '2017-01-16 22:45', '', NULL),
(8, 4, 12, 1, '', '2017-01-16 22:47', '', NULL),
(9, 5, 10, 1, '', '2017-01-16 22:48', 'Cartão de Crédito', NULL),
(10, 6, 1, 2, '', '2017-01-21 17:26', 'Dinheiro   Crédito', NULL),
(11, 7, 5, 4, '', '2017-01-21 17:31', 'Dinheiro / Débito', NULL),
(12, 8, 5, 3, '', '2017-01-22 18:36', 'Dinheiro', NULL),
(13, 9, 8, 7, '', '2017-01-22 18:47', 'Dinheiro', NULL),
(14, 10, 1, 10, '', '2017-01-23 18:41', 'Dinheiro', NULL),
(15, 11, 2, 1, '', '2017-01-23 19:21', 'Dinheiro', NULL),
(16, 12, 5, 1, '', '2017-01-23 19:22', 'Dinheiro', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `caixa01`
--
ALTER TABLE `caixa01`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `caixa02`
--
ALTER TABLE `caixa02`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cargo`
--
ALTER TABLE `cargo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `forma_pagamento`
--
ALTER TABLE `forma_pagamento`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `movimentacao_caixa01`
--
ALTER TABLE `movimentacao_caixa01`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pde_fato_vendas`
--
ALTER TABLE `pde_fato_vendas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pde_fato_vendas_produtos`
--
ALTER TABLE `pde_fato_vendas_produtos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pde_movimentacao`
--
ALTER TABLE `pde_movimentacao`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_balcao`
--
ALTER TABLE `pedido_balcao`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_delivery`
--
ALTER TABLE `pedido_delivery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa1`
--
ALTER TABLE `pedido_mesa1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produtos_suspensos`
--
ALTER TABLE `produtos_suspensos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sabores_pizza`
--
ALTER TABLE `sabores_pizza`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabela_auxiliar_venda`
--
ALTER TABLE `tabela_auxiliar_venda`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tec_products`
--
ALTER TABLE `tec_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendas_history`
--
ALTER TABLE `vendas_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendas_suspensas`
--
ALTER TABLE `vendas_suspensas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `venda_balcao_hist`
--
ALTER TABLE `venda_balcao_hist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `caixa01`
--
ALTER TABLE `caixa01`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `caixa02`
--
ALTER TABLE `caixa02`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cargo`
--
ALTER TABLE `cargo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3269;
--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `forma_pagamento`
--
ALTER TABLE `forma_pagamento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `movimentacao_caixa01`
--
ALTER TABLE `movimentacao_caixa01`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pde_fato_vendas`
--
ALTER TABLE `pde_fato_vendas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `pde_fato_vendas_produtos`
--
ALTER TABLE `pde_fato_vendas_produtos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;
--
-- AUTO_INCREMENT for table `pde_movimentacao`
--
ALTER TABLE `pde_movimentacao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `pedido_balcao`
--
ALTER TABLE `pedido_balcao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_delivery`
--
ALTER TABLE `pedido_delivery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pedido_mesa1`
--
ALTER TABLE `pedido_mesa1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `produtos_suspensos`
--
ALTER TABLE `produtos_suspensos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `sabores_pizza`
--
ALTER TABLE `sabores_pizza`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tabela_auxiliar_venda`
--
ALTER TABLE `tabela_auxiliar_venda`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tec_products`
--
ALTER TABLE `tec_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=437;
--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `vendas_history`
--
ALTER TABLE `vendas_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `vendas_suspensas`
--
ALTER TABLE `vendas_suspensas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `venda_balcao_hist`
--
ALTER TABLE `venda_balcao_hist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
